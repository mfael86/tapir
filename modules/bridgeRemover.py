## @package bridgeRemover
# The here described filter class modules.bridgeRemover.BridgeRemover just contracts bridges (i.e. lines that do not correspond to loops) from diagrams.


import logging
from typing import List
from modules.diagram import Diagram
from modules.filter import Filter
import multiprocessing as mp
import time
from modules.topoAnalyzer import BridgeFinder
import signal


## A simple Filter class which contracts bridges (i.e. lines that do not correspond to loops) from diagrams destructively.
# The main method is #filter().
# It is used to minimize the entries in the topsel files such that exp can always map it.
# Without the usage of exp, the bridge information is gone for good.
# Tree level diagrams are removes seperately with a warning.
# Note: Spinor indices are not contracted!
class BridgeRemover(Filter):
    ## Constructor
    # @param conf modules.config.Conf instance where modules.config.Conf::topoIgnoreBridges is (possibly) specified
    def __init__(self, conf, verbose=False, kernels=1):
        ## Local modules.config.Conf instance
        self.conf = conf
        ## verbose flag to print progress
        self.verbose = verbose
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels


    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)


    ## Basic pipe method that changes the topological structure of an input list of modules.diagram.Diagram's to the specifications of modules.config.Conf::topoIgnoreBridges. 
    # We call #contractBridgesOfDiagram() from here which does the main job.
    # It is run in parallel for best performance result.
    # As a result, get the modules.diagram.Diagram's with bridge lines contracted but properly initialized (i.e. the line and vertex numbers are ascending without gaps)
    # Be aware, that this mehtod is destructive! I.e. The diagrams are intrinsically changed.
    # Note: Spinor indices are not contracted!
    # Also note that tree level diagrams are completely removed from \p diagrams
    # @param diagrams List of modules.diagram.Diagram instances we want to filter
    # \return \p diagrams where the bridges are contracted
    def filter(self, diagrams: List[Diagram]) -> List[Diagram]:
        # continue with main program when there's nothing to remove
        if not self.conf.topoIgnoreBridges:
            return diagrams

        # check for tree-level diagrams and remove them explicitely
        if len([d for d in diagrams if d.numLoops == 0]) > 0:
            logging.warning(F"Option 'topo_ignore_bridges' is applied to tree-level diagrams: {', '.join([d.name for d in diagrams if d.numLoops == 0])}.\nWe remove those from the topologies!")
            diagrams = [d for d in diagrams if d.numLoops != 0]

        # remove unwanted lines from the diagrams in main thread
        if self.kernels == 1:
            dias = []
            for i,d in enumerate(diagrams):
                self.vprint(" Remove bridges: {0:^7}/{1:^7}\r".format(i, len(diagrams)), end = "")
                dias.append(self.contractBridgesOfDiagram(d))
            self.vprint(" Remove bridges: Done                  ")
            return dias
        
        # remove unwanted lines from the diagrams in parallel
        else:
             # initializer function for each worker in the multithread pool
            def initializer():
                # ignore CTRL+C in the worker process
                signal.signal(signal.SIGINT, signal.SIG_IGN)

            if self.kernels == 0:
                pool = mp.Pool(mp.cpu_count(),initializer=initializer)
            else:
                pool = mp.Pool(self.kernels,initializer=initializer)

            # start asynchronous workers
            result = pool.map_async(self.contractBridgesOfDiagram, diagrams, chunksize=1)

            # print progress with actualization rate of 10 Hz
            while not result.ready():
                self.vprint(" Remove bridges: {0:^7}/{1:^7}\r".format(len(diagrams)-result._number_left, len(diagrams)), end = "")
                time.sleep(0.1)
            self.vprint(" Remove bridges: Done                  ")
            pool.close()
            pool.join()

            return result.get()

    
    ## Contract all bridges from a given diagram.
    # Here, we apply the the modules.topoAnalyzer.BridgeFinder to find all bridges and evoke modules.diagram.Diagram.contractLines().
    # Then we contract them and rename the vertices and lines.
    # Note: Spinor indices are not contracted!
    # @param diagrams List of modules.diagram.Diagram instances of which we want to remove the bridges. Note that this list is altered afterwards!
    # \return List of diagrams with changed topology
    def contractBridgesOfDiagram(self, diagram):
        # find all bridges 
        bridgeNumbers = BridgeFinder().findBridges(diagram.topologicalLineList)
        diagram.contractLines(bridgeNumbers)
        return diagram