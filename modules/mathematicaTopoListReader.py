## @package mathematicaTopoListReader
# This module reads Mathematica topology list files and generates according modules.integralFamily.IntegralFamily instances from it.


import logging
from modules.partialFractionDecomposer import IntegralFamily
import re


## Main class to read and translate topology list files in Mathematica format to modules.integralFamily.IntegralFamily instances
class MathematicaTopoListReader:
    ## Constructor
    #  @param conf An initialized config object
    #  @param verbose Increase verbosity level if set to true
    #  @param kernels Run certain parts of the module in parallel using the given number of cores
    def __init__(self, conf, verbose=False, kernels=1):
        ## config.Conf object reference.
        self.config = conf
        ## Verbose flag to print progress during file reading.
        self.verbose = verbose
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels


    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)


    ## Translate a Mathematica string to a modules.integralFamily.IntegralFamily format which includes only the topological information
    # This method is the inversion of modules.mathematicaTopoListGenerator.MathematicaTopoListGenerator::integralFamilyToMathematica().
    # @param mathematicaString String which describes the integral family
    # \return Initialized modules.integralFamily.IntegralFamily instance which corresponds to the Mathematica input. None if the input was invalid.
    def mathematicaToIntegralFamily(self, mathematicaString):
        # Remove white spaces
        mathematicaString = re.sub(r'\s', r'', mathematicaString)
        # Extract the three different parts of the array
        match = re.match(r'{\"([^\"]*)\",{([^}]*)},{([^}]*)}}', mathematicaString)
        if match:
            return IntegralFamily(
                                topoName=match.group(1), 
                                analyticTopologyExpression=[ re.sub(r'\^', r'**', s) for s in match.group(2).split(",") ],
                                loopMomenta=match.group(3).split(","),
                                indices=[1 for i in [ re.sub(r'\^', r'**', s) for s in match.group(2).split(",") ]]
                            )
        else:
            # If the entry is not interpretable, send warning to user
            logging.warning('Cannot interpret the entry integral family entry"' + mathematicaString + '"')
            return None

    
    ## Read integral families from a topology list file in Mathematica format (which can be used for e.g. FIRE).
    # The output is written in terms of modules.integralFamily.IntegralFamily instances.
    # @param fileName Path and name to the topology list file input
    # \return List of IntegralFamily instances representing the entries of \p fileName
    def readIntegralFamilies(self, fileName):
        # Try to open the topo list file
        families = []
        try:
            with open(fileName, 'r') as listFile:
                # Fill families with the entries of the file
                lines = "".join(listFile.readlines())
                lines = re.sub(r'\s', r'', lines)
                matches = re.findall(r'{\"[^\"]*\",{[^}]*},{[^}]*}}', lines)
                
                # Translate the individual string entries to a integral family, respectively
                for match in matches:
                    family = self.mathematicaToIntegralFamily(match)
                    if family != None:
                        families.append(family)

        except FileNotFoundError:
            logging.error(F"Mathematica topology list file '{fileName}' not found!")
            exit(1)

        return families