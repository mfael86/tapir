## @package qgrafReader
# This module reads the qlist file and generates a simple Diagram class representation of every diagram.


import logging
from modules.diagram import Diagram
import re
import multiprocessing as mp
import time
import signal


## DiagramCorruptedError is raised if a diagram in the qlist file does not adher to the normal format.
class DiagramCorruptedError(Exception):
    ## The constructor
    #  @param self The object pointer.
    #  @param dia String with the first part of the diagram which has not been read correctly.
    def __init__(self, dia):
        ## Contains the parts of the corrupted diagram which were read in.
        self.dia = dia

## The QgrafReader class reads in a Qgraf output file generated with the q2e.sty style file.
#
# It reads in individual diagrams and can return a list of diagram.Diagram objects containing the relevant information.
# Additionally, it is possible to do the reverse operation and write to a qlist file, see #writeDiagrams()
class QgrafReader:

    ## The constructor, requires a valid configuration as input.
    #  @param conf An initialized config object
    #  @param verbose Increase verbosity level if set to true
    #  @param kernels Run certain parts of the module in parallel using the given number of cores
    def __init__(self, conf, verbose=False, kernels=1):
        ## config.Conf object reference.
        self.config = conf
        ## Verbose flag to print progress during file reading.
        self.verbose = verbose
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels


    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)

    
    ## This function reads in a Qgraf file and returns a list of modules.diagram.Diagram objects.
    #  @param self The object pointer.
    #  @param qlistFile Path to the Qgraf output
    #  @return List of modules.diagram.Diagram objects.
    #
    #  If the file specified in \p qlistFile is not readable, an error is print and the program terminates.
    #  Catches DiagramCorruptedError in case the qlist file has corrupted entries, prints the diagram and the terminates.
    def getDiagrams(self, qlistFile):
        # File handle for the qlist file
        try:
            with open(qlistFile, "r") as qlistHandle:
                qlist = qlistHandle.readlines()
        except FileNotFoundError:
            logging.error(qlistFile + " not found!")
            exit(1)

        # get lines in which a new diagram starts and ends, indicated by "{" or "}" without comment sign "#" before
        validBracketLine = re.compile(r'^[^#]*[{}]')

        starts = []
        ends = []
        for n, line in enumerate(qlist):
            if validBracketLine.match(line):
                if "{" in line:
                    starts.append(n)
                if "}" in line:
                    ends.append(n)
        
        # read single diagrams in main thread
        if self.kernels == 1:
            dias = []
            for i in range(0,len(starts)):
                self.vprint(" Read diagrams: {0:^7}/{1:^7}\r".format(i, len(starts)), end = "")
                lines = qlist[starts[i]+1:ends[i]]
                dias.append(self.readDiagram(lines))
            self.vprint(" Read diagrams: Done                  ")
            return dias
        # read single diagrams in parallel
        else:
            # initializer function for each worker in the multithread pool
            def initializer():
                # ignore CTRL+C in the worker process
                signal.signal(signal.SIGINT, signal.SIG_IGN)

            if self.kernels == 0:
                pool = mp.Pool(mp.cpu_count(),initializer=initializer)
            else:
                pool = mp.Pool(self.kernels,initializer=initializer)

            # start asynchronous workers
            result = pool.map_async(self.readDiagram, [qlist[starts[i]+1:ends[i]] for i in range(0,len(starts))], chunksize=1)

            # print progress with actualization rate of 10 Hz
            while not result.ready():
                self.vprint(" Read diagrams: {0:^7}/{1:^7}\r".format(len(starts)-result._number_left, len(starts)), end = "")
                time.sleep(0.1)
            self.vprint(" Read diagrams: Done                  ")
            pool.close()
            pool.join()

            return result.get()


    ## Check for mass of the given particle name (or its anti-particle) in #modules.Conf.config::mass
    def getMass(self, particleName, antiParticleName = ""):
        # check for particle in mass dict
        if particleName in self.config.mass.keys():
            return self.config.mass[particleName]
        # check for anti-particle in mass dict
        elif antiParticleName != "" and antiParticleName in self.config.mass.keys():
            return self.config.mass[antiParticleName]
        else:
            return ""


    ## This function reads in one diagram a Qgraf file and returns a modules.diagram.Diagram object.
    #  @param self The object pointer.
    #  @param diagramLines String containing the Qgraf input for one diagram
    #  @return One modules.diagram.Diagram object.
    #
    #  Raises DiagramCorruptedError if something goes wrong.
    def readDiagram(self,diagramLines):
        # check configurations line-by-line 
        diagram = Diagram()

        lineNumber = 0

        for lineNumber, line in enumerate(diagramLines):
            # Skip comments
            if re.match(r'\s*#',line):
                continue
            if "diagram" in line:
                diagram.diagramNumber = eval(re.match(r'diagram\s*([0-9]+)',line).group(1))
            elif "pre_factor" in line:
                # Do not eval preFactor, since it could be 1/2 and Form does not want 0.5
                diagram.preFactor = re.match(r'pre_factor\s*(\S+)',line).group(1)
            elif "number_loops" in line:
                diagram.numLoops = eval(re.match(r'number_loops\s*(\S+)',line).group(1))
            elif "number_legs_in" in line:
                diagram.numLegsIn = eval(re.match(r'number_legs_in\s*(\S+)',line).group(1))
            elif "number_legs_out" in line:
                diagram.numLegsOut = eval(re.match(r'number_legs_out\s*(\S+)',line).group(1))
            # Note: this automatically treats outgoing external momenta as "incoming" ones, so they are compatible with the rest of the code
            elif "external_leg" in line:
                match =re.match(r'external_leg\s*([^|]*)\|([^|]*)\|(.*)$',line)
                diagram.externalMomenta.append(
                        [match.group(1), # "q1"
                         eval(match.group(2)), # "1"
                         match.group(3), # "g"
                         int(match.group(1)[1:]) # "i1"
                        ]
                    )
            elif "momentum" in line:
                match =re.match(r'momentum\s*([^|]*)\|([^|]*)\|(.*)$',line)
                numIndependentLegs = len(diagram.externalMomenta) - 1
                particleIn = match.group(3).split(",")[0]
                particleOut = match.group(3).split(",")[1]
                mass = self.getMass(particleIn, particleOut)
                diagram.internalMomenta.append(
                    [match.group(1), # "p1"
                     eval(match.group(2).split(",")[0]), # 1
                     eval(match.group(2).split(",")[1]), # 2
                     particleIn, # "ft1"
                     particleOut, # "fT1"
                     2*(int(match.group(1)[1:])+numIndependentLegs)+1, # i1
                     2*(int(match.group(1)[1:])+numIndependentLegs)+2, # i2
                     mass # "M1"
                     ]
                    )

        # Check if diagram information has been read in correctly
        assert diagram.diagramNumber != None and diagram.diagramNumber != 0, F"Line {lineNumber}: diagramNumber has invalid value: {diagram.diagramNumber}"
        assert diagram.preFactor != None and diagram.preFactor != "", F"Line {lineNumber}: preFactor has invalid value: {diagram.preFactor}"
        assert diagram.numLoops != None and diagram.numLoops >= 0, F"Line {lineNumber}: numLoops has invalid value: {diagram.numLoops}"


        # apply q2e.anti_fermion option (currently unused, since we deal differently with fermions)
        if self.config.antiFermion != {}:
            pass

        # determine number of vertices (maximal vertex index)
        diagram.numVertices = max([l[1] for l in diagram.externalMomenta] + [l[1] for l in diagram.internalMomenta] + [l[2] for l in diagram.internalMomenta])
        # Create the diagrams vertex list
        diagram.createTopologicalVertexAndLineLists()
        if not self.config.spinorIndices:
            try:
                diagram.groupFermions()
            except Exception as e:
                logging.error(e)
                exit(1)
        # give the diagram a unique name
        if self.config.topologyName.isspace():
            diagram.setName()
        else:
            diagram.setName(self.config.topologyName)

        return diagram


    ## Write the given diagrams to a qlist file
    #  @param qlistOutputFile Filename to which we dump the diagrams
    #  @param diagrams List of modules.diagram.Diagram objects which we want to write
    #
    #  This is basically the reverse operation to #readDiagram()
    def writeDiagrams(self, qlistOutputFile, diagrams):
        try:
            with open(qlistOutputFile, 'w') as outFile:
                outFile.write("#\n# file generated by tapir\n#")
                for i,diagram in enumerate(diagrams):
                    self.vprint(" Write diagrams: {0:^7}/{1:^7}\r".format(i, len(diagrams)), end = "")
                    outFile.write("\n\n\n{\n" + self.diagram2Qlist(diagram) + "\n}")
                self.vprint(" Write diagrams: Done                  ")

        except:
            logging.error("Not able to write '" + qlistOutputFile + "'")
            exit(1)


    ## Translate a diagram to a qlist string
    #  @param diagram modules.diagram.Diagram object that we want to translate
    #  \return String of a qlist representation of the given diagram
    @staticmethod
    def diagram2Qlist(diagram):
        qlistString = F"""diagram            {diagram.diagramNumber}
pre_factor         {diagram.preFactor}

number_propagators {len(diagram.internalMomenta)}
number_loops       {diagram.numLoops}
number_legs_in     {diagram.numLegsIn}
number_legs_out    {diagram.numLegsOut}

"""
        for q in diagram.externalMomenta:
            qlistString += F"\nexternal_leg       {q[0]}|{q[1]}|{q[2]}"

        qlistString += "\n\n"

        for p in diagram.internalMomenta:
            qlistString += F"\nmomentum           {p[0]}|{p[1]},{p[2]}|{p[3]},{p[4]}"

        return qlistString
