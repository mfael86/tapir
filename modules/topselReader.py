## @package @topselReader 
# This class reads a topsel file and generates a simple modules.diagram.Diagram representation of every topsel entry.
# Since we cannot reconstruct the particles of the diagram uniquely, a .dia file cannot be generated from this input.
# The main class here is modules.topselReader.TopselReader.
# 
# For more information about topsel files, see ediaGenerator.


import logging
from modules.diagram import Diagram
import re


## With this module, one can read in a topsel file and get a proper modules.diagram.Diagram representation. 
# Also the config is changed, since we internally need a particle to assign a mass. Therefore, we add dummy particles to the modules.config.Conf.
# 
# The main method is #getDiagrams() which can be called right after instantiation.
class TopselReader:
    ## Constructor
    # @param conf modules.config.Conf instance
    def __init__(self, conf):
        ## internal reference to the modules.config.Conf instance
        self.config = conf

    
    ## By calling this method one directly get modules.diagram.Diagram instances from the input topsel file.
    # Internally, we call #readDiagram() for every topsel entry
    # @param topselFile A name string of the file, we want to read (relative or full path)
    def getDiagrams(self, topselFile):
        # read all lines that does not start with a comment (#...) 
        topselHandle = open(topselFile)
        topsel = []
        for line in topselHandle:
            if not re.match(r'\s*#', line):
                topsel.append(line)
        topselHandle.close() 
        
        # get lines in which a new diagram starts and ends
        starts = [n for n,line in enumerate(topsel) if "{" in line]
        ends   = [n for n,line in enumerate(topsel) if "}" in line]

        # read single diagrams one-by-one
        diagrams = []

        for i in range(0,len(starts)):
            topselEntry = "".join(topsel[starts[i]:ends[i]+1])
            topselEntry = re.sub(r'{([^{}]*)}', r'\1', topselEntry)
            topselEntry = re.sub(r' ', r'', topselEntry)
            topselEntry = re.sub(r'\n', r'', topselEntry)
            diagrams.append(self.readDiagram(topselEntry, i+1))

        return diagrams


    ## This method reads a topsel entry string and converts it to a diagram.
    # Thereby, #config is expanded by new dummy particles describing the mass content.
    # Also the #modules.diagram.Diagram::diagramNumber is set in the order as the entries are given in the \p topselFile
    # @param topselEntry A string describing a topsel entry, e.g. <code>{inpt1,poco_scale,copy_scale;5;2;1;0; ;(q1:1,3)(p1:2,3)(p2:1,2)(p3:4,1)(p4:3,4)(p5:4,2); 00100}</code>
    # @param entryNumber The number of the given entry, this will be the #modules.diagram.Diagram::diagramNumber of the returned modules.diagram.Diagram
    # \return A modules.diagram.Diagram instance representing the topselEntry
    def readDiagram(self, topselEntry, entryNumber):
        # check configurations line-by-line 
        diagram = Diagram()
        topselEntry = topselEntry.split(";")

        # there must be eight fields to be a correct topsel file
        if len(topselEntry) != 8:
            logging.error("The {}'th topsel entry has {} entries, only except exactly 8!".format(entryNumber, len(topselEntry)))

        diagram.diagramNumber = entryNumber

        # 0: name of the topology (ignore additional options after the name)
        diagram.setName(name=topselEntry[0].split(",")[0], appendNumbering=False)

        # 1: number of lines
        pass # read later on

        # 2: number of loops
        diagram.numLoops = int(topselEntry[2])

        # 3: number of external momenta (define all momenta as incoming)
        pass # read later on

        # 4: number of masses
        pass # not used

        # 5: scales information
        pass # not used

        # 7: mass distribution
        # create dummy particles with the specified mass
        for mass in ["M" + str(i) for i in range(1,10) if str(i) in topselEntry[7]]:
            self.config.mass["dummy" + mass + "particle"] = mass

        # 6: line specifications
        momentumEntries = re.sub(r'^\(', r'', topselEntry[6])
        momentumEntries = re.sub(r'\)$', r'', momentumEntries)
        momentumEntries = momentumEntries.split(")(")

        # extract all momenta from entries of the form (q1:1,2)(q2:3,2)(p1:1,2)(p2:3,1)(p3:2,3)
        internalIndex = 0
        externalIndex = 0
        externalEndPoint = None
        for momentumEntry in momentumEntries:
            momentumEntry = momentumEntry.split(":")
            momentumName = momentumEntry[0]
            start = int(momentumEntry[1].split(",")[0])
            end = int(momentumEntry[1].split(",")[1])
            # internal momentum
            if momentumName[0] == "p":
                # complain if there is a non-numeral (e.g. x) in the mass distribution list
                if topselEntry[7][internalIndex] not in [str(i) for i in range(0,10)]:
                    logging.error(diagram.name + ": " + "Non-numeral character \"{}\" in mass distribution list!".format(topselEntry[7][internalIndex]))
                    exit(1)
                # look for mass
                mass = "M" + topselEntry[7][internalIndex]
                internalIndex += 1

                # insert dummy particles for each mass
                if mass == "M0":
                    particle = "dummyMasslessParticle"
                    mass = ""
                else:
                    particle = "dummy" + mass + "particle"

                diagram.internalMomenta.append(
                    [momentumName, start, end, particle, particle, start, end, mass]
                )

            # external momentum
            else:
                diagram.externalMomenta.append(
                    [momentumName, start, "dummyMasslessParticle", start]
                )
                externalIndex += 1
                # save endpoint to append an additional external momentum later on
                if externalEndPoint == None:
                    externalEndPoint = end 
                elif externalEndPoint != end:
                    logging.error(diagram.name + ": " + "External momenta have no common end point: {} != {} !".format(externalEndPoint, end))

        # add additional external momentum if graph is no tadpole
        if externalEndPoint != None:
            # assign first q_i which is not used in the diagram yet
            newQIndex = 1
            while "q" + str(newQIndex) in [extMom[0] for extMom in diagram.externalMomenta]:
                newQIndex += 1
            diagram.externalMomenta.append(
                    ["q" + str(newQIndex), externalEndPoint, "dummyMasslessParticle", externalEndPoint]
                )

        # cross-check with input
        if externalIndex != int(topselEntry[3]):
            logging.warning(diagram.name + ": " + "Number of indedendent external indices (" + str(externalIndex) +
             ") does not agree with fourth entry of {" + ", ".join(topselEntry) +  "} (" + topselEntry[3] + ")! Use ({}) for convenience.".format(externalIndex))

        # create the topological vertex list
        diagram.createTopologicalVertexAndLineLists()

        return diagram