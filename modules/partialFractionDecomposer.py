## @package partialFractionDecomposer
# This modules performs a partial fraction decomposition on given topologies and keeps track of them to find a minimal set of independent topologies with independent entries (propagators).
# The decomposition is given in FORM readable code.
# The way of how the decomposition is done follows the PhD thesis of Jens Hoff, section 2.2, using Gröbner bases.
# The main class is modules.partialFractionDecomposer.PartialFractionDecomposer, 
# an additional simple ancillary class provides modules.integralFamily.IntegralFamily to wrap the important informations of the topologies.

import sympy as sy
import logging
import re
from modules.topoAnalyzer import TopologyAnalyzer
from modules.topoMinimizer import TopoMinimizer
from modules.integralFamily import IntegralFamily
from modules.outputHelpers import writeCodeFile
from modules.yamlGenerator import YamlGenerator
from modules.yamlReader import YamlReader
from modules.mathematicaTopoListGenerator import MathematicaTopoListGenerator
from modules.mathematicaTopoListReader import MathematicaTopoListReader
import time
import multiprocessing as mp
import signal
import os


## The main class of the partial fraction decomposition module
# For a partial fraction procedure only one instance is needed.
# To apply partial fractioning, one has to call decompose() with all topologies.
# After doing so, calling minimizeTopologies() will reduce the number of topologies
# To get the partial fractioning FORM expression, apply getPartialFractioningString()
class PartialFractionDecomposer:
    ## The plain constructor. 
    # @param config modules.config.Conf instance that describes the meta data of current diagrams
    # @param partFracFile Name of the input list file that we want to compare to, or empty String with no input file
    # @param verbose Print additional information during the topology minimization, default is True
    # @param kernels Define on how many cores to run on
    def __init__(self, config, partFracFile="", verbose=True, kernels=1):
        ## modules.config.Conf instance that describes the meta data of current diagrams
        self.config = config
        ## Initialize reference to local AnalyticTopoMapper instance
        self.analyticTopoMapper = AnalyticTopoMapper()
        ## Dict of diagram names with the remaining propagators after partial fractioning decomposition
        self.remainingPropagatorCombinations = {}
        ## Dict of diagram names with the first bits of the partial fractioning FORM string. This is computed in #decompose()
        self.firstPartFracStringBits = {}
        ## List of IntegralFamily instances which occur after #decompose() was used.
        # If \p partfrac is a FIRE topology list file, it is initialized with the contents there
        self.unreducedTopologyList = []
        ## verbose flag to print progress during the topology minimization
        self.verbose = verbose
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels
        ## Possible list of known IntegralFamily instances from \p partFracFile argument file
        self.knownTopologies = []
        # Dict of found mappings in #minimizeTopologies()
        self.mapping = {}

        # use also known topologies for the mapping with #analyticTopoMapper
        if partFracFile != "" and partFracFile != None:
            if partFracFile.split(".")[-1].lower() in ["yml", "yaml"]:
                self.knownTopologies = YamlReader(self.config).readIntegralFamilies(partFracFile)
            else:
                self.knownTopologies = MathematicaTopoListReader(self.config).readIntegralFamilies(partFracFile)
            
            self.unreducedTopologyList.extend(self.knownTopologies)
            

    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)


    ## Apply partial fraction decomposition to the input family file and write a partial fraction decomposition string to dedicated files
    # This method is used for the standalone mode since it only requires analytic integral families as input in form of a topology list file.
    # The latter is again exported after the decomposition. Additionally, a minization of the new integral families using Pak's algorithm can be applied using #minimizeTopologies().
    # @param topologyListFile Path and name to the input topology list file. This can be either a Mathematica or YAML file
    # @param topologyFolder Output folder name to which the partial fraction decomposed topology files are written
    # @param mathematicaListFileName Name of the newly generated topology list file inside \p topologyFolder in Mathematica format
    # @param yamlListFile mathematicaListFileName Name of the newly generated topology list file inside \p topologyFolder in YAML format
    # @param minimize Boolean flag whether the subleading integral families shall be minimized with Pak's algorithm
    def standaloneDecomposition(self, topologyListFile, topologyFolder, mathematicaListFileName="topologyList.m", yamlListFile="topologyList.yaml", minimize=False):
        # Read file with integral families that shall be decomposed
        if topologyListFile.split(".")[-1].lower() in ["yml", "yaml"]:
            families = YamlReader(self.config,needFRs=False).readIntegralFamilies(topologyListFile)
        else:
            families = MathematicaTopoListReader(self.config).readIntegralFamilies(topologyListFile)

        # Run the actual partial fraction decomposition
        for i,family in enumerate(families):
            print(" Apply partial fraction decomposition: {0:^7}/{1:^7}\r".format(i, len(families)), end = "")
            self.decompose(family.topoName, family.analyticTopologyExpression, family.loopMomenta)
        print(" Apply partial fraction decomposition: Done                        ")

        # Apply minimization
        if minimize:
            self.minimizeTopologies()

        # Write output
        self.writePartFracFiles(topologyFolder, mathematicaListFileName, yamlListFile, families)
    
    
    ## This method writes out partial fractioning FORM code that was previously generated
    # Before calling this routine topologies need to be decomposed by calling #decompose()
    # 
    # We also create two list files of all minimal topologies. They are called integral family files.
    # The first file is Mathematica readible and can be used for a reduction with FIRE. Its entries have the structure `{topo Name, list of propagators, list of loop momenta}`
    # The second one follows the conventions of Kira and uses modules.yamlGenerator.YamlGenerator::integralFamilyToYaml().
    # Both are written in the same folder as the topology files (\p path)
    # @param path Folder string the topology files are written to
    # @param mathematicaListFileName Name of the topology output list file in Mathematica redable format
    # @param yamlListFileName Name of the topology output list file in YAML format
    # @param families List of IntegralFamily instances of which the partial fractioning is applied to
    def writePartFracFiles(self, path, mathematicaListFileName, yamlListFileName, families):
        # create path if it does not exist already
        if not os.path.exists(path) and path != "":
            try:
                os.makedirs(path)
            except:
                logging.error("Cannot create path of " + str(path))
                exit(1)          
        
        # Write partial fraction strings to dedicated files
        for t in families:
            writeCodeFile(path, t.topoName, self.getPartialFractioningString(t.topoName, t.analyticTopologyExpression, t.loopMomenta))

        # Extract all remaining integral families for the case that a minimization was applied...
        if self.mapping != {}:
            remainingFamilies = self.getNewUniqueTopologies()
        # ... or not
        else:
            remainingFamilies = self.getAllNewTopologies()

        # Write the families to topology list files
        # Mathematica
        MathematicaTopoListGenerator(self.config).writeIntegralFamilies(os.path.join(path, mathematicaListFileName), remainingFamilies)         

        # YAML
        YamlGenerator(self.config).writeIntegralFamilies(os.path.join(path, yamlListFileName), remainingFamilies)         


    ## Return a list of all topologies that are found by AnalyticTopoMapper without initial, known topologies from #knownTopologies.
    # It is assumed that #minimizeTopologies() was called beforehand.
    # \return List of IntegralFamily instances that are new and unique according to #knownTopologies and #analyticTopoMapper
    def getNewUniqueTopologies(self):
        listOfNewTopos = []

        # filter
        for topo in self.analyticTopoMapper.uniqueTopologies:
            if topo.topoName not in [knownTopo.topoName for knownTopo in self.knownTopologies]:
                listOfNewTopos.append(topo)

        return listOfNewTopos


    ## Return a list of all topologies that are decomposed so far without known topologies from #knownTopologies.
    # \return List of new IntegralFamily instances
    def getAllNewTopologies(self):
        listOfNewTopos = []

        # filter
        for topo in self.unreducedTopologyList:
            if topo.topoName not in [knownTopo.topoName for knownTopo in self.knownTopologies]:
                listOfNewTopos.append(topo)

        return listOfNewTopos


    ## The main method of PartialFractionDecomposer. Here, we prepare everything for the partial fractioning, including finding of reduced topologies.
    # This method is the first step for the partial fractioning.
    # We fill #remainingPropagatorCombinations, #firstPartFracStringBits and #unreducedTopologyList.
    # Afterwards, we can call #minimizeTopologies() and #getPartialFractioningString().
    # @param topoName The name of the topology we want to decompose into smaller topologies
    # @param analyticTopologyExpression A list of terms that define the denominators/numerators of the reducible topology.
    #  The same as #modules.momentumAssigner.MomentumAssigner.analyticTopologyExpression
    # @param loopMomenta A list of strings that correspond to the loop momentum names in \p analyticTopologyExpression
    # @param aggregate Boolean value to specify whether #aggregateTopology() is called, and thus AnalyticTopoMapper is used. For further calls of #minimizeTopologies() and #getPartialFractioningString(), this option is mandatory. Default is true
    # @param externalFourVectors A list of strings that correspond to the names of all external four vectors: external (true) momenta as well as eikonal external momenta (such as the velocity parameter v in HQET).
    #  This is needed to rewrite the scalar products to proper FORM code.
    # \returns List of PartialFractionTopology instances that are "basis" topologies (present in #topologies) of the input topology.
    def decompose(self, topoName, analyticTopologyExpression, loopMomenta, aggregate=True, externalFourVectors=[]):
        logging.info(" ---------------------- start partialFractionDecomposer.decompose() ----------------------")
        logging.info(F"Try to decompose {analyticTopologyExpression}")

        # number of propagators
        numTopoSpace = len(analyticTopologyExpression)

        # create coefficient matrix
        coeffMatrix, coeffs = self.expressionListToCoeffMatrix(analyticTopologyExpression, loopMomenta)

        # switch "1"-column (corresponding to non loop-momentum containing factors) to end of matrix to keep this favourably during gaussian elimination
        oneIndex = coeffs.tolist().index([1])
        coeffs.row_swap(oneIndex, coeffs.rows-1)
        coeffMatrix.col_swap(oneIndex, coeffs.rows-1)

        # extend coeffMatrix by a unity matrix that correspond to the single terms of analyticTopologyExpression
        coeffMatrix = coeffMatrix.row_join(sy.eye(numTopoSpace))

        # use gaussian elimination to find linear dependencies between rows
        coeffMatrix = coeffMatrix.rref()[0]

        logging.info("Coefficient matrix:")
        logging.info("[" + ",".join(str(c[0]) for c in coeffs.tolist()) + "," + ",".join(["D" + str(i) for i in range(0,numTopoSpace)]) + "]")
        logging.info(coeffMatrix._format_str() + "\n")
        
        # extract linear equations that relate the terms of analyticTopologyExpression (propagators) instead of only the momenta
        relationMatrix = []
        for i in range(coeffMatrix.rows):
            # the row corresponds to a relevant linear equation if it does not depend on loop momenta anymore, i.e. the first len(coeffs)-1 entries are zero
            if coeffMatrix.rowspace()[i][:len(coeffs)-1] == (len(coeffs)-1) * [0]:
                relationMatrix.append(coeffMatrix.rowspace()[i][(len(coeffs)-1):])

        logging.info("Relation matrix:")
        logging.info("[" + ",".join(str(c[0]) for c in coeffs.tolist()[len(coeffs)-1:]) + "," + ",".join(["D" + str(i) for i in range(0,numTopoSpace)]) + "]")
        logging.info(sy.Matrix(relationMatrix)._format_str() + "\n")

        # add the plain topology if there are no partial fraction relations, i.e. the basis is minimal
        if len(relationMatrix) == 0:
            self.firstPartFracStringBits[topoName] = ""
            self.remainingPropagatorCombinations[topoName] = [set(range(numTopoSpace))]
            topology = IntegralFamily(
                    topoName = topoName, 
                    analyticTopologyExpression = analyticTopologyExpression.copy(),
                    loopMomenta = loopMomenta.copy(),
                    indices = [1 for i in analyticTopologyExpression]
                    )
            # put the topology to the beginning of the list that it is mapped on 
            self.unreducedTopologyList =  [topology] + self.unreducedTopologyList
            return 

        # build the topology relations as linear equations of propagators. We rename them D0,D1,... to handle them better
        # the 0'th entry (the 1 column) must be inverted to be part of the propagators equations
        partFracRelations = sy.Matrix(relationMatrix) * sy.Matrix([-1] + [sy.Symbol("D" + str(i)) for i in range(numTopoSpace)])
        partFracRelations = [r[0] for r in partFracRelations.tolist()]

        # add Di/Di = 1 relations to expand the basis of the ideal. Then take the inverse di as independent variables.
        partFracRelations += ["iD" + str(i) + "*D"+str(i) + " - 1" for i in range(numTopoSpace)]
        logging.info("Relations for partial fractioning: " + str(partFracRelations))

        # define reverse lexicographic ordering for the computation of the Gröbner Basis
        revLex =  lambda monom: tuple(reversed(monom))

        # now calculate the Gröbner Basis of the linear equation system to find a bunch of independent relations without overlap, one can use for partial fraction decomposition
        variables = [sy.Symbol("iD" + str(i)) for i in range(numTopoSpace)] + [sy.Symbol("D" + str(i)) for i in range(numTopoSpace)]
        groebnerBasis = sy.groebner(partFracRelations, *variables, order=revLex)

        # keep track of the indices in the Gröbner relations to find a minimal set of propagators
        groebnerIndicesList = []

        # generate list of all momenta and four vectors (loop and external) to rewrite scalar products after the use of sympy
        allFourVectors = externalFourVectors + loopMomenta
        scalarProductReplacementsForFORM = {(i+"*"+j if i != j else i+"**2"):i + "." + j for i in allFourVectors for j in allFourVectors}

        # translate the Gröbner basis to FORM readable substitutions
        partFracString = "repeat;\n"
        for eq in groebnerBasis:
            eq = sy.Poly(eq, *variables)
            lm = sy.LM(eq,order=revLex)
            # relations with numerators are ignored here
            # i.e. discard D1/D1 = 1 type of trivial replacements
            if re.match(r'D([0-9]+)\*iD\1', str(lm)) or re.match(r'iD([0-9]+)\*D\1', str(lm)):
                continue
            else:
                rhs = sy.solve(eq, lm)[0]
                partFracString += "   id " + str(lm).replace("iD","1/D").replace("*1/","/") + " = " + str(rhs).replace("iD","1/D").replace("*1/","/") + ";\n"
                # write correct FORM code for scalar products: qi * qj -> qi.qj, qi**2 -> qi.qi
                for key, value in scalarProductReplacementsForFORM.items():
                    partFracString = partFracString.replace(key, value)
                partFracString = partFracString.replace("**", "^")
                # ignore D1*1/D2 cases 
                if not re.search(r'([^i]|^)D([0-9]*)', str(lm)):
                    g = GroebnerRelationIndices()
                    # left-hand side
                    g.lhsIndices = set(re.findall(r'iD([0-9]*)', str(lm)))
                    # right-hand side
                    g.rhsIndicesList = [b[0] for b in sy.expand(rhs).as_terms()[0]]
                    g.rhsIndicesList = [set(re.findall(r'iD([0-9]*)', str(term))) for term in g.rhsIndicesList]

                    g.lhsIndices = {int(i) for i in g.lhsIndices}
                    g.rhsIndicesList = [{int(ii) for ii in i} for i in g.rhsIndicesList]
                    groebnerIndicesList.append(g)

        partFracString += "endrepeat;\n\n.sort\n\n"

        # Now replace the denominators back into a new set of topologies
        # Default: no denominators. No problem if we only look at tree level diagrams, scaleless integral for loops
        if len(loopMomenta) == 0:
            partFracString += "if( occurs(" + ", ".join(["D" + str(i) for i in range(numTopoSpace)]) + ") == 0 );\n*  Do nothing\n"
        else:
            partFracString += "if( occurs(" + ", ".join(["D" + str(i) for i in range(numTopoSpace)]) + ") == 0 );\n*  Integral has no scale\n   multiply FLAGSCALELESS;\n"

        self.firstPartFracStringBits[topoName] = partFracString

        # get all combinations of propagators that can occur due to the Gröbner bases
        self.remainingPropagatorCombinations[topoName] = self.getAllRemainingPropagators(groebnerIndicesList, numTopoSpace)

        # prepare the new topologies for analyticTopoMapper
        if aggregate:
            for remainingPropagators in self.remainingPropagatorCombinations[topoName]:
                topology = IntegralFamily(
                    topoName = topoName + "w"+"w".join([str(i) for i in remainingPropagators]), 
                    analyticTopologyExpression = [analyticTopologyExpression[i] for i in remainingPropagators],
                    loopMomenta = loopMomenta.copy(),
                    indices = [1 for i in remainingPropagators]
                    )
                # save topology for later
                self.unreducedTopologyList.append(topology)
                

    ## Minimize the topologies we found so far during partial fractioning.
    # Here, we take all entries from #unreducedTopologyList and use #analyticTopoMapper to reduce them.
    # Hence, #decompose() must be called first!
    def minimizeTopologies(self):
        # compute the Symanzik polynomials of each topology in the main thread
        if self.kernels == 1:
            for i,topo in enumerate(self.unreducedTopologyList):
                self.vprint(" Prepare decomposed topologies for minimization: {0:^7}/{1:^7}\r".format(i, len(self.unreducedTopologyList)), end = "")
                self.analyticTopoMapper.initializeTopology(topo)
        # compute the Symanzik polynomials of each topology in parallel
        else:
             # initializer function for each worker in the multithread pool
            def initializer():
                # ignore CTRL+C in the worker process
                signal.signal(signal.SIGINT, signal.SIG_IGN)

            if self.kernels == 0:
                pool = mp.Pool(mp.cpu_count(),initializer=initializer)
            else:
                pool = mp.Pool(self.kernels,initializer=initializer)
            
            # start asynchronous workers
            result = pool.map_async(self.analyticTopoMapper.initializeTopology, [topo for topo in self.unreducedTopologyList], chunksize=1)

            # print progress with actualization rate of 10 Hz
            while not result.ready():
                self.vprint(" Prepare decomposed topologies for minimization: {0:^7}/{1:^7}\r".format(len(self.unreducedTopologyList)-result._number_left, len(self.unreducedTopologyList)), end = "")
                time.sleep(0.1)

            pool.close()
            pool.join()

            self.unreducedTopologyList = result.get()
        self.vprint(" Prepare decomposed topologies for minimization: Done                  ")

        # now map the topologies onto each other using the computed results
        for i,topo in enumerate(self.unreducedTopologyList):
            self.vprint(" Compare decomposed topologies: {0:^7}/{1:^7}\r".format(i, len(self.unreducedTopologyList)), end = "")
            self.mapping[topo.topoName] = self.analyticTopoMapper.findTopologyMapping(topo, aggregate=True)

        self.vprint(" Compare decomposed topologies: Done                      ")


    ## Get the full partial fractioning string after calling #decompose().
    # Also this method can be called after #minimizeTopologies() to get the correct mapping
    def getPartialFractioningString(self, topoName, analyticTopologyExpression, loopMomenta):
        partFracString = self.firstPartFracStringBits[topoName]

        # in case there is no partial fractioning but a renaming 
        justRename = False
        if (len(self.remainingPropagatorCombinations[topoName]) == 1) and not "FLAGSCALELESS" in partFracString:
            justRename = True

        # build, with that knowledge, all possible independent topologies
        for remainingPropagators in self.remainingPropagatorCombinations[topoName]:
            propagatorsToRemove = set(range(len(analyticTopologyExpression))).difference(remainingPropagators)
            # when we do not remove any propagators, the topology stays the same
            if propagatorsToRemove == set():
                newName = topoName
            else:
                newName = topoName + "w"+"w".join([str(i) for i in remainingPropagators])

            topology = IntegralFamily(
                topoName = newName, 
                analyticTopologyExpression = [analyticTopologyExpression[i] for i in remainingPropagators],
                loopMomenta = loopMomenta.copy(),
                indices = [1 for i in remainingPropagators]
                )

            # take the mapping result from minimizeTopologies if it was called before
            if newName in self.mapping:
                newTopoName, propMapping = self.mapping[newName]
            else:
                newTopoName, propMapping = newName, {i:i for i in range(len(topology.analyticTopologyExpression))}

            # return empty string if there is no partial fractioning or renaming of the final topology
            if newTopoName == topoName:
                return ""

            # build a list that says in which order the index replacement takes place
            # E.g. for propMapping = {1: 0, 0: 1, 3: 2, 2: 4, 4: 3} the righthand-side of the mapping is given by: INT(-n1,-n0,-n3,-n4,-n2)
            # That means the righthand-side list needs to be [1,0,3,4,2] which is the key as list entry at the values position
            propMap = [-1]*len(remainingPropagators)
            for key, val in propMapping.items():
                propMap[val] = key

            
            # just do the renaming
            if justRename:
                partFracString += "id " + " * ".join(["D" + str(num) + "^n" + str(i) + "?" for i,num in enumerate(remainingPropagators)]) + " = "
                partFracString += newTopoName + "(" + ", ".join(["-n" + str(propMap[i]) for i in range(len(remainingPropagators))]) + ");\n"
            # append to the elseif chain
            else:
                partFracString += "elseif( occurs(" + ", ".join(["D" + str(i) for i in propagatorsToRemove]) + ") == 0 );\n"
                partFracString += "   id " + " * ".join(["D" + str(num) + "^n" + str(i) + "?" for i,num in enumerate(remainingPropagators)]) + " = "
                partFracString += newTopoName + "(" + ", ".join(["-n" + str(propMap[i]) for i in range(len(remainingPropagators))]) + ");\n"

        if not justRename:
            partFracString += "endif;\n\n"
            
        # if operating in standalone mode, add code to replace scalar functions by D's and do a check at the end
        partFracStringCore = partFracString
        partFracString = "\n\n* Perform partial fraction decomposition\n"
        partFracString += "id " + topoName 
        partFracString += "(" + ", ".join(["n" + str(i) + "?" for i in range(len(analyticTopologyExpression))]) + ") = "
        partFracString += " * ".join(["1/D" + str(i) + "^n" + str(i) for i in range(len(analyticTopologyExpression))]) + ";\n\n"
        partFracString += partFracStringCore
        partFracString += ".sort\n\n"

        # check whether all denominators are replaced
        partFracString += "* Check if all topology factors are substituted back\n"
        partFracString += "if( occurs("
        partFracString += ", ".join([F"D{i}" for i in range(len(analyticTopologyExpression))]) 
        partFracString += ") );\n   exit \">> Error: Topology entries after partial fraction decomposition found!\";\nendif;\n\n"
        partFracString += ".sort\n"

        logging.info(" ---------------------- end of partialFractionDecomposer.decompose() ----------------------\n")

        return partFracString


    ## Construct all possible combinations of propagators that can occur due to the applied partial fraction decomposition relations from the groebner basis.
    # Here we use the indices of the Gröbner relations, as given in GroebnerRelationIndices, to find all combinations of (from partial fraction perspective) irreducible topologies.
    # We use only the propagators for this, e.g. 1/D1, for D1 = M1^2 - p1^2 etc. Relations with numerators are ignored here!
    # For a topology with D1 (1),D2 (2) and D3 (3), and a single Gröbner p.f. relation 1/D1/D2 = a*(1/D1+1/D2), we end up with possible minimal propagators of (1,3) and (2,3).
    # For the case of 1:1 relations, e.g. 1/D2 = 1/D4; D4 can (and will) always be replaced by D2. This is also true for numerators. Hence, D4 can be safely removed.
    # @param groebnerIndicesList List of GroebnerRelationIndices from the applied groebner base relations
    # @param numPropagators Amount of propagators in the problem
    # \returns A list with all possible propagator combinations, e.g. [(1,2,3), (2,3,4), (1,3,4)]
    def getAllRemainingPropagators(self, groebnerIndicesList, numPropagators):
        # Begin with the set of all available propagators (1,2,3,...,N)
        uniqueConfigs = [set(range(numPropagators))]

        # Update uniqueConfigs iteratively with each Gröbner relation
        for groebnerRelationIndices in groebnerIndicesList:
            # fill this list with the newly found topologies, that are left after applying groebnerRelationIndices
            newConfigs = []
            # Apply Gröbner relation to all configs, we have so far
            for config in uniqueConfigs:
                # Applying the relation only makes sense if all indices are actually in the topology
                if groebnerRelationIndices.lhsIndices.issubset(config):
                    # Use the relation to kill one of the terms of the righthand side of the relation
                    for rhsIndices in groebnerRelationIndices.rhsIndicesList:
                        # Remove the indices from the lefthand side and add the ones from one of the terms on the righthandside back
                        copyConfig = config.difference(groebnerRelationIndices.lhsIndices).union(rhsIndices)
                        newConfigs.append(copyConfig)
                # Topologies not affected by the relation remain
                else:
                    newConfigs.append(config)

            # Update possible configs so far
            uniqueConfigs = newConfigs
            
        # Filter out unique configurations
        configs = []
        for conf in uniqueConfigs:
            if conf not in configs:
                configs.append(conf)

        # order them properly (does not change much; non-critical)
        configs.sort()

        logging.info("getAllRemainingPropagators: Found possible propagator configurations: " + str(configs))

        return configs


    ## Turn an analytic expression expression of a topology into a coefficient matrix.
    #
    # Each row represents a single propagator and the columns define their content via the coefficients defined in \p coeffs.
    #
    # Example: From \f$ [-2pk-k^2, 2pk-k^2, -k^2, -l^2, -(l+k)^2, qk, 2lk, M^2-k^2, M^2 - l^2]\f$, we get the matrix
    # \f{align*}
    #   \begin{pmatrix}
    #    0 & -2 & -1 &  0 &  0 & 0 \\
    #    0 &  2 & -1 &  0 &  0 & 0 \\
    #    0 &  0 & -1 &  0 &  0 & 0 \\
    #    0 &  0 &  0 & -1 &  0 & 0 \\
    #    0 &  0 & -1 & -1 & -2 & 0 \\
    #    0 &  0 &  0 &  0 &  0 & 1 \\
    #    0 &  0 &  0 &  0 &  2 & 0 \\
    # M^2 &  0 & -1 &  0 &  0 & 0 \\
    # M^2 &  0 &  0 & -1 &  0 & 0
    # \end{pmatrix}
    # \f}
    # and the correspinding coefficient vector
    # \f{align*}
    # [1, k^2, kp, l^2, kl, kq]
    # \f}
    # @param analyticTopologyExpression List of sympy expressions that describe the topology 
    # @param loopMomenta List of loop momentum names
    # \returns 
    #  * \p coeffMatrix:  A matrix representing the topology entries when miltiplied by \p coeffs
    #  * \p coeffs:  List of coefficients that build the elements of the propagators
    def expressionListToCoeffMatrix(self, analyticTopologyExpression, loopMomenta):
        coeffMatrix, coeffs = [], [1]

        # convert loop momenta to symbols
        loopMomenta = [sy.Symbol(k) for k in loopMomenta]
        for prop in analyticTopologyExpression:
            # initialize a new row of ceffMatrix with zero
            row = len(coeffs)*[0]
            prop = sy.sympify(prop).expand()
            # add numerical values to first row
            if 1 in prop.as_coefficients_dict().keys():
                row[0] = prop.as_coefficients_dict()[1]
            # extract relevant monomials, i.e. the ones containing loop momenta
            for monomial in self.sympyMonomials(prop):
                relevant = False
                for k in loopMomenta:
                    if k in monomial.free_symbols:
                        relevant = True
                        # add monomial to coefficient vector if not present yet
                        if not monomial in coeffs:
                            coeffs.append(monomial)
                            # expand row, too
                            row = row + [0]
                        break
                # add coefficients of relevant monomial to row
                if relevant:
                    index = coeffs.index(monomial)
                    # extract numerical coefficients
                    # non-numerical ones would double counted since we mark loop*non-loop also as relevant 
                    coeffDict = prop.coeff(monomial).as_coefficients_dict()
                    if 1 in coeffDict.keys():
                        row[index] = coeffDict[1]
                # non-relevant terms are appended to the first column
                else:
                    row[0] += prop.coeff(monomial)*monomial

            # add row to coeffMatrix
            coeffMatrix.append(row)
            
        # expand matrix to fixed length
        for i,row in enumerate(coeffMatrix):
            coeffMatrix[i] = coeffMatrix[i] + (len(coeffs)-len(row))*[0]

        return sy.Matrix(coeffMatrix), sy.Matrix(coeffs)


    ## This method extracts all monomials of a sympy expression
    # @param expr A sympy expression
    # \returns List of monomials of \p expr as sympy expressions
    @staticmethod
    def sympyMonomials(expr):
        monomials = []
        # extract all symbols of expr
        symbols = list(expr.free_symbols)
        # fix polynomial order with symbols
        if len(symbols) == 0:
            return []

        expr = sy.Poly(expr, symbols)
        # translate "monomial" from sy.monom (exponent vector) to real sympy expression 
        for exponents in expr.monoms():
            if not exponents == tuple(len(symbols)*[0]):
                monomials.append(
                    sy.Mul(*map(lambda x,y: x**y, symbols, exponents))
                    )

        return monomials



## Ancillary plain class to keep track of the indices in Gröbner relations.
# E.g. for 1/D1/D2/D3 = a/D1/D2 + b/D2/D3 + c/D1/D3, we are interested in the l.h.s. (1,2,3) and r.h.s. (1,2), (2,3), (1,3)
# Relations with numerators are ignored here!
class GroebnerRelationIndices:
    def __init__(self, lhsIndices=set(), rhsIndicesList=[]):
        self.lhsIndices = lhsIndices
        self.rhsIndicesList = rhsIndicesList



## An ancillary class that keeps track of the analytic topologies (modules.IntegralFamily) and tries to map them on each other.
# The main methods here are initializeTopology() and findTopologyMapping().
# The former computes the Symanzik polynomials of a topology (modules.IntegralFamily::U and modules.integralFamily.IntegralFamily::F) and applies Pak's algorithm to bring them into canonical form.
# The latter compares the the Symanzik polynomials with others from known topologies and provides a mapping onto them.
# In addition we have two static methods UF() and UF2() which compute modules.integralFamily.IntegralFamily::U and modules.integralFamily.IntegralFamily::F of an analytic topology. The former is the preferred, since faster implementation.
class AnalyticTopoMapper:
    ## Plain Constructor
    def __init__(self):
        ## List of modules.integralFamily.IntegralFamily instances that keeps track of the minimal amount topologies.
        self.uniqueTopologies = []
        ## Dict of all modules.integralFamily.IntegralFamily::U * modules.integralFamily.IntegralFamily::F of #uniqueTopologies with symmetries applied as keys and (toponame, symmetry) as values
        # If modules.integralFamily.IntegralFamily.indices are non-empty, the indices are appended at the end of the key.
        self.uniqueSymmetrizedUFDict = {}


    ## Compute both Symanzik polynomials U and F from a analytic topology.
    # This is an implementation of A. Smirnov's "UF" routine, originally implemented in Mathematica [http://ttp.kit.edu/~asmirnov/Tools-UF.htm]
    # @param loopMomenta List of loop momentum names
    # @param analyticPropagators List of analytic propagators, defining the topology under consideration
    # \return [U,F] List of first and second Symanzik polynomial as sympy objects
    @staticmethod
    def UF(loopMomenta, analyticPropagators):
        degree = -sum([sy.sympify(F"({prop})*x{i+1}") for i,prop in enumerate(analyticPropagators)])
        coeff = 1
        for k in loopMomenta:
            degreeCoeffs = sy.Poly(degree, sy.sympify(k)).all_coeffs()
            degreeCoeffs.reverse()
            # Coefficients of k^0
            t0 = degreeCoeffs[0]
            # Coefficients of k^1
            if len(degreeCoeffs) > 1:
                t1 = degreeCoeffs[1]
            else:
                t1 = 0
            # Coefficients of k^2
            if len(degreeCoeffs) > 2:
                t2 = degreeCoeffs[2]
            else:
                t2 = 0
            
            coeff = coeff*t2
            degree = t0 - (t1**2)/(4*t2)
        coeff = sy.together(coeff).factor()
        degree = sy.together(-coeff*degree).factor().expand()
        
        return [coeff.expand(), degree]


    ## Compute both Symanzik polynomials U and F from a analytic topology. This is an alternative to #UF
    # Here we follow eq. (8) and (9) of [C. Bogner and S. Weinzierl, Int. J. of mod. Phys. A, Vol. 25 No. 13 (2010) 2585-2618 (DOI: 10.1142/S0217751X10049438)] to compute U and F.
    # #UF() is preferred since it's O(5) times faster. This method is kept for performance comparison reasons.
    # @param loopMomenta List of loop momentum names
    # @param analyticPropagators List of analytic propagators, defining the topology under consideration
    # \return [U,F] List of first and second Symanzik polynomial as sympy objects
    @staticmethod
    def UF2(loopMomenta, analyticPropagators):
        # Build the Symanzik polynomials U and F for the topology
        exponent = sum([sy.sympify(F"x{i+1}*({D})") for i,D in enumerate(analyticPropagators)]).expand()

        symLoopMomenta = [sy.Symbol(k) for k in loopMomenta]

        # check out the exponential of the Schwinger representation of the topology (see Jens Hoff's PhD thesis eq. (40))
        exponent = sy.poly(exponent, symLoopMomenta)
        # construct a dict that describes the terms for each power in the loop momenta 
        exponent = exponent.as_dict()
        logging.info(F"topology: {analyticPropagators}")
        logging.info(F"Schwinger exponent: {exponent}")

        # create loop momentum square matrix M, which is sandwiched between loop momentum vectors as -(k1, k2) M (k1, k2)^T in exponent
        M = [[0 for i in symLoopMomenta] for j in symLoopMomenta]
        # now extract the the vector Q that includes external momenta which is multiplied by loop momenta. I.e. 2 Q (k1, k2)^T in exponent
        Q = [0 for i in symLoopMomenta]
        # the remainder of exponent goes into J
        J = 0

        # now actually fill M, Q and J
        for key, val in exponent.items():
            # diagonal elements of M
            if 2 in key:
                index = key.index(2)
                M[index][index] = -val
            # off-diagonal elements of M
            elif key.count(1) == 2:
                indices = [i for i, n in enumerate(key) if n == 1]
                M[indices[0]][indices[1]] = -val/2
                M[indices[1]][indices[0]] = -val/2
            # elements of Q
            elif key.count(1) == 1:
                index = key.index(1)
                Q[index] = val/2
            # remainder J
            else:
                J += val
        
        M = sy.Matrix(M)
        Q = sy.Matrix(Q)
        U = M.det()

        # in case U vanishes (i.e. F is undefined), the integral is zero.
        # to be sure of that just keep the topology as it is, until we're sure about that statement #TODO
        if U == 0:
            logging.warning(F" Encountered topology with U=0 : {analyticPropagators} and loop momenta {symLoopMomenta}")
            return [0,0]

        # F = U * ( J+Q M^-1 Q )
        F =  J*U + (Q.T*(U*M.inv(method="LU"))*Q)[0]

        # simplify and expand results. This is the main bottleneck of this computation
        U = U.simplify().expand()
        F = F.factor().simplify().expand()

        logging.info(F"M = {sy.Matrix(M)._format_str()}")
        logging.info(F"Q = {sy.Matrix(Q)._format_str()}")
        logging.info(F"J = {J}")
        logging.info(F"U = {U}")
        logging.info(F"F = {F}")

        return[U,F]

    
    ## Rename loop momenta and compute the Symanzik polynomials of a modules.integralFamily.IntegralFamily.
    # The input topology is changed after calling this method.
    # @param topology modules.integralFamily.IntegralFamily instance from which we compute modules.integralFamily.IntegralFamily::U and modules.integralFamily.IntegralFamily::F
    # \return the same topology reference as the input
    def initializeTopology(self, topology):
        logging.info("Initialize topsel topology "+topology.topoName)
        # remove zero indices from the topology
        topology.stripIndices()

        # rename loop momenta to k1, k2, ...
        substitute = [(re.sub(r'\s',r'',k),F"k{i+1}") for i,k in enumerate(topology.loopMomenta)]
        topology.analyticTopologyExpression = [sy.sympify(expr).subs(substitute) for expr in topology.analyticTopologyExpression]
        topology.loopMomenta = [sub[1] for sub in substitute]

        # Build the Symanzik polynomials U and F for the topology
        topology.U, topology.F = self.UF(topology.loopMomenta, topology.analyticTopologyExpression)

        # order the Feynman parameters canonically
        topology.U, topology.F, topology.permutations, topology.lineSymmetries = TopologyAnalyzer.applyPaksAlgorithm(topology.U, topology.F, topology.permutations)
        
        logging.info(F"ordered U = {topology.U}")
        logging.info(F"ordered F = {topology.F}")

        return topology

    
    ## Try to map a topology onto one of #uniqueTopologies
    # Use the U and F polynomials from #initializeTopology() to find an equal (defined as unique) topology to the given one.
    # If #initializeTopology() wasn't called before, do it now
    # @param topology modules.integralFamily.IntegralFamily instance with or without defined modules.integralFamily.IntegralFamily::U and modules.integralFamily.IntegralFamily::F
    # @param aggregate Boolean value, if the topology should be added to #uniqueTopologies if it is not known yet
    # \return * Name of the topology (one of #uniqueTopologies) we mapped on, this is the same as the input if the topology is new
    #         * Dict of mapping configurations, e.g. {0:1, 1:0, 2:2} means, map modules.integralFamily.IntegralFamily::analyticTopologyExpression[0] of the input topology onto modules.integralFamily.IntegralFamily::analyticTopologyExpression[1] of one of the #uniqueTopologies, we mapped on
    def findTopologyMapping(self, topology, aggregate):
        # initialize topology if it wasn't done before
        if topology.U == "" and topology.F == "":
            self.initializeTopology(topology)

        UF = topology.U * topology.F
        UF.expand()

        if str(UF) + F"{topology.getPermutatedIndices()}" in self.uniqueSymmetrizedUFDict:
            uniqueTopoName, symmetry = self.uniqueSymmetrizedUFDict[str(UF) + F"{topology.getPermutatedIndices()}"]
            uniqueTopo = [topo for topo in self.uniqueTopologies if topo.topoName == uniqueTopoName][0]
            symmetryPermutations1 = uniqueTopo.getSymmetricalPermutations(symmetry)
            symmetryPermutations2 = topology.getSymmetricalPermutations(())
            propMappingConfigurations = {symmetryPermutations2.index(i+1) : symmetryPermutations1.index(i+1) for i in range(len(topology.analyticTopologyExpression))}
            return uniqueTopoName, propMappingConfigurations
                
        # no mapping found -> toplogy is new
        if aggregate:
            self.uniqueTopologies.append(topology)
            # construct all U's and F's that are connected by symmetry of the new unique topology
            for symmetry in TopoMinimizer.allCombinationsOfVariableLength(topology.lineSymmetries):
                UF = topology.getPermutatedU(symmetry) * topology.getPermutatedF(symmetry)
                UF.expand()
                # add UF as key to uniqueSymmetrizedUFDict
                if str(UF) + F"{topology.getPermutatedIndices(symmetry)}" not in self.uniqueSymmetrizedUFDict:
                    self.uniqueSymmetrizedUFDict[str(UF) + F"{topology.getPermutatedIndices(symmetry)}"] = (topology.topoName, symmetry)

        return topology.topoName, {i:i for i in range(len(topology.analyticTopologyExpression))}
