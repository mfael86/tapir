## @package vertexFilter
# This module contains the modules.vertexFilter.VertexFilter class.
# The filter is used to extract diagrams with a specific number of vertices of a certain type (particle content).


import logging
import signal
import re
import multiprocessing as mp
import time
from typing import List
from modules.diagram import Diagram
from modules.filter import Filter
from modules.cutFilter import CutFilter


## A Filter class derivative which filters diagrams according to the particle content of their vertices.
# The main function is #filter(), which calls #parseFilterOptions() and #matchesVertexConstraints() to check if each diagram
# satisfies the filter constraints.
class VertexFilter(Filter):
    ## Constructor
    def __init__(self, conf, verbose=False, kernels=1):
        ## Local modules.config.Conf instance
        self.conf = conf
        ## verbose flag to print progress
        self.verbose = verbose
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels


    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)


    ## Apply the vertex filter to the given diagrams, discarding all that do not comply.
    # @param diagrams List of modules.diagram.Diagram instances we want to filter
    # \return \p diagrams matching the vertex filter constraints
    def filter(self, diagrams: List[Diagram], filterOptions=[]) -> List[Diagram]:
        # Skip trivial diagrams list
        if len(diagrams) == 0:
            return []

        filterOptions = self.parseFilterOptions(filterOptions=filterOptions)

        # List of boolean values indicating for each diagram in diagrams if the cut filter conditions were met
        constraintsValidList = []

        # Apply the vertex filter in the main thread for all diagrams
        if self.kernels == 1:
            for i,d in enumerate(diagrams):
                self.vprint("  > vertex: {0:^7}/{1:^7}\r".format(i, len(diagrams)), end = "")
                # Check for valid cut constraints
                constraintsValidList.append(self.matchesVertexConstraints(d,filterOptions))

        # Apply the vertex filter for all diagrams in parallel
        else:
             # Initializer function for each worker in the multithread pool
            def initializer():
                # Ignore CTRL+C in the worker process
                signal.signal(signal.SIGINT, signal.SIG_IGN)

            if self.kernels == 0:
                pool = mp.Pool(mp.cpu_count(),initializer=initializer)
            else:
                pool = mp.Pool(self.kernels,initializer=initializer)

            # Start asynchronous workers
            result = pool.starmap_async(self.matchesVertexConstraints, [[d,filterOptions] for d in diagrams], chunksize=1)

            # Print progress with actualization rate of 10 Hz
            while not result.ready():
                self.vprint("  > cuts: {0:^7}/{1:^7}\r".format(len(diagrams)-result._number_left, len(diagrams)), end = "")
                time.sleep(0.1)
            pool.close()
            pool.join()
            # Read results
            constraintsValidList = result.get()

        # Return only those diagrams with valid cut conditions
        return [diagrams[i] for i,vertexConditionMet in enumerate(constraintsValidList) if vertexConditionMet]


    ## Bring a bunch of filter options to a further usable form and exit with an error for bad input.
    # The argument filterOptions is a list of multiple entries of the following type:
    #  ["1" , "q1,q2" , "ft" , "0,0, 1,14"]
    # The arguments are described in the user manual.
    # Note that exclusive interval options are translated to inclusive ones.
    # Also, empty cut intervals are expanded (i.e. replaced by their numeric representation).
    #
    # @param filterOptions List of filter options with the four defined entries
    # @param externalMomenta List of all external momenta defined as incoming, e.g. ["q1", "-q2"]
    # \return parsed and checked version of \p filterOptions
    def parseFilterOptions(self, filterOptions):
        # Check for valid filter options and straighten the filterOptions entries
        for option in filterOptions:
            # Check first argument
            if re.sub(r'\s', r'', option[0].lower()) in ["1", "true", "t"]:
                option[0] = True
            elif re.sub(r'\s', r'', option[0].lower()) in ["0", "false", "f"]:
                option[0] = False
            else:
                logging.error(F"Invalid filter option \"{option}\". First argument must be 1/true/t/0/false/f.")
                exit(1)
            # Second argument is not used (should be empty, but in any case ignored)
            # Rewrite third argument
            # option[2] = re.sub(r'\s', r'', option[2]).split(",")
            if option[2] == "":
                logging.error(F"Vertex filter requires a non-empty list of vertices.")
                exit(1)
            option[2] = [s.split(",") for s in re.sub(r'\s', r'', option[2]).split(";")]
            # Check fourth argument
            option[3] = re.sub(r'\s', r'', option[3]).split(",")
            # Invert selection if empty range (i.e. [1,Inf) ) was given
            if option[3] == [""]:
                option[0] = not option[0]
                option[3] = [[0,0]]
            # Otherwise, split the ranges accordingly in pairs
            else:
                # Check for odd amount of range numbers
                if(len(option[3])%2 == 1):
                    logging.error(F"Invalid filter option \"{option}\". Fourth argument must be a comma separated list of numeric ranges \"from,to,...\". The specified input must be even.")
                    exit(1)
                for i, rangeVal in enumerate(option[3]):
                    # Check for non-numeric range numbers
                    if not rangeVal.isnumeric():
                        logging.error(F"Invalid filter option \"{option}\". Fourth argument must be a comma separated list of numeric ranges \"from,to,...\". \"{rangeVal}\" is invalid.")
                        exit(1)
                    # Left bound is greater than right bound
                    if (i%2 == 0) and int(option[3][i]) > int(option[3][i+1]):
                        logging.error(F"Invalid filter option \"{option}\". Interval [{option[3][i]},{option[3][i+1]}] is not allowed.")
                        exit(1)
                # Combine range pairs and sort them correctly
                option[3] = sorted(  [ [int(option[3][2*i]), int(option[3][2*i+1])] for i in range(int(len(option[3])/2)) ]  )

                # Combine overlapping intervals. Start with first interval
                intervals = [option[3][0]]
                for i in range(1,len(option[3])):
                    # Right bound of the last interval piece lies in the range of the next interval, e.g. [1,3] and [2,5]
                    if intervals[-1][1] >= option[3][i][0]:
                        # Extend last interval if the next interval to add extends the last one
                        if intervals[-1][1] < option[3][i][1]:
                            intervals[-1] = [ intervals[-1][0], option[3][i][1] ]
                    # If no overlapping happens, just add the interval
                    else:
                        intervals.append(option[3][i])
                option[3] = intervals

            # Exclusive intervals are translated to inclusive ones by inversion
            if option[0] == False:
                option[3] = CutFilter.invertIntervals(CutFilter(self.conf,verbose=self.verbose,kernels=self.kernels),option[3])
                option[0] = True
        return filterOptions


    ## Check whether a diagram matches the vertex filter conditions
    # The argument filterOptions is a list of multiple entries of the following type:
    #  [ True , ANYTHINGBUTPREFERABLYEMPTY , ["ft", "fT", "h"] , [[0,0], [1,14]] ]
    # \p filterOptions are given by #parseFilterOptions(). See also the reference there to see what all entries mean.
    #
    # @param diagram modules.diagram.Diagram instance which we want to check
    # @param filterOptions List of above mentioned single filter options
    # \return Boolean value whether filter condition was met
    def matchesVertexConstraints(self, diagram, filterOptions):
        # In this dictionary we will keep track of the particle content of all vertices of a diagram
        vertexDict = {}

        for vertex in diagram.topologicalVertexList:
            vertexParticles = vertex.getOutgoingParticles()

            # Check if the vertex has the same particle content as one of the filter specifications
            for filt in filterOptions:
                for f in filt[2]:
                    if len(f) == len(vertexParticles) and sorted(f) == sorted(vertexParticles):
                        # If they are the same, reorder the particles in the same way as the filter specifications
                        vertexParticles = f

            # Add the vertex particle content to the dictionary vertexDict
            if str(vertexParticles) in vertexDict.keys():
                vertexDict[str(vertexParticles)] += 1
            else:
                vertexDict[str(vertexParticles)] = 1

        # Check that all of the constraints are met
        for filt in filterOptions:
            matchesAnyInterval = False
            filtcount = 0
            # If the filter particles are not in vertexDict, then there is no such vertex as specified in the filter, and we add a {filterParticleList: '0'} to the dictionary
            for f in filt[2]:
                if not str(f) in vertexDict.keys():
                    vertexDict[str(f)] = 0
                filtcount += vertexDict[str(f)]

            # Now we need to check that the vertex count for this specific vertex is within one of the given intervals
            for interval in filt[3]:
                if filtcount >= interval[0] and filtcount <= interval[1]:
                    matchesAnyInterval = True
                    break

            # If for a filter no interval was matched, discard the diagram
            if not matchesAnyInterval:
                return False

        # If we reach this point, all filters are matched and the diagram is kept
        return True
