## @package diagramDrawer
# This module is supposed to draw the Feynman diagrams and topologies of modules.diagram.Diagram instances into a TikZ-Feynman. [J. Ellis: TikZ-Feynman: Feynman diagrams with TikZ, Computer Physics Comm. 210, p.103-123, 2017]
# The main class is modules.diagramDrawer.DiagramDrawer.

from modules.ediaGenerator import EdiaGenerator
from modules.feynmanRulesReader import FeynmanRulesReader
import logging
import re


## This class creates a .tex representation of modules.diagram.Diagram instances. 
# 
# The such created .tex file must be inserted into a tikz-feynman template and build using luatex.
# The main methods are diagramDrawer::drawDiagrams() and diagramDrawer::drawTopologies()
# Here we also use the `block_size` option to split the amount of diagrams into smaller chunks
class DiagramDrawer:
    ## Constructor
    # @param config Overall used modules.config.Conf instance 
    # @param latexFileName The output filename of the diagram latex file  
    def __init__(self, config, latexFileName):
        ## Local modules.config.Conf pointer
        self.config = config
        ## File name where the final output shall be written to. The default name is diagrams.tex
        # If the file does not end with .tex, it is added.
        # For `block_size` option set, we insert a dot number to the name (as in diagrams.3.tex) 
        self.latexFileName = latexFileName
        if not self.latexFileName.endswith(".tex"):
            self.latexFileName = self.latexFileName + ".tex"
        ## Propagators from .prop file defined in #config. Needed to draw propagators correctly.
        # This is optional and not necessary needed
        self.propagators = []
        if config.vrtxFile != "":
            feynRulesReader = FeynmanRulesReader(config)
            self.propagators = feynRulesReader.feynmanRules.propLorentz.keys()

        self.head = r"""\documentclass[11pt]{article}
\usepackage[top=1in, bottom=1.25in, left=0.8in, right=0.8in]{geometry}
\usepackage{tikz-feynman}

\begin{document}

"""

        self.tail = r"""
\end{document}
"""


    ## This method translates modules.diagram.Diagram instances tu tikz-feynman objects and writes the to the #latexFileName file
    #
    # The entries that are generated are of the form
    # ```
    # \feynmandiagram[large]{
    #   i1 [particle=e] -- [fermion] a -- [fermion] i2[particle=e], 
    #   a -- [photon, edge label=W] b,
    #   b -- [fermion, half left, in=90, out=90] c,
    #   c --  [fermion, half left, in=90, out=90] b,
    #   c -- [photon] d,
    #   i3 -- [fermion] d -- [fermion] i4, 
    # };
    # ```
    #
    # The output is split to multiple files if module.config.Config.blockSize was specified.
    # The main work is done via #getDiagramDrawingStrings()
    # @param diagrams List of modules.diagram.Diagram instances we want to draw
    # @param useCachedTopologyLineList Boolean value whether modules.diagram.Diagram.cachedTopologyLineList should be used for drawing rather than modules.diagram.Diagram.topologyLineList
    def drawDiagrams(self, diagrams, onlyTopology=False, useCachedTopologyLineList=False):
        # write entries to a single .tex file if module.config.Config.blockSize is not set do make multiple blocks.
        EdiaGenerator.generatePath(self.latexFileName)

        if self.config.blockSize < 1:
            try:
                with open(self.latexFileName, 'w') as tex:
                    tex.write(self.head)
                    tex.write(self.getDiagramDrawingStrings(diagrams, onlyTopology, useCachedTopologyLineList))
                    tex.write(self.tail)

            except:
                logging.error("Not able to write '" + self.latexFileName + "'")
                exit(1)

            if onlyTopology:
                print("Topology pictures are ready for compilation. Run\n\n\tlualatex " + self.latexFileName + "\n")
            else:
                print("Feynman diagram pictures are ready for compilation. Run\n\n\tlualatex " + self.latexFileName + "\n")


        # If multiple blocks should be generated, create a list of multiple entry lists and write to multiple files.
        else:
            diagramSubLists = [diagrams[i:i+self.config.blockSize] for i in range(0, len(diagrams), self.config.blockSize)]
            for i in range(0,len(diagramSubLists)):
                try:
                    with open(self.latexFileName.replace(".tex","") + "." + str(i+1) + ".tex",'w') as tex:
                        tex.write(self.head)
                        tex.write(self.getDiagramDrawingStrings(diagramSubLists[i], onlyTopology, useCachedTopologyLineList))
                        tex.write(self.tail)

                except:
                    logging.error("Not able to write '" + self.latexFileName.replace(".tex","") + "." + str(i+1) + ".tex" + "'")
                    exit(1)

            if onlyTopology:
                print(F"Topology pictures are ready for compilation. To do so for all pictures, run\n\n\tfor i in `seq 1 {len(diagramSubLists)}`; do lualatex " + self.latexFileName.replace(".tex","") + ".$i.tex; done\n")
            else:
                print(F"Feynman diagram pictures are ready for compilation. To do so for all pictures, run\n\n\tfor i in `seq 1 {len(diagramSubLists)}`; do lualatex " + self.latexFileName.replace(".tex","") + ".$i.tex; done\n")
        


    ## Generate the actual tikz-feynman representation of each diagram for #drawDiagrams()
    # @param diagrams List of modules.diagram.Diagram instances we want to draw
    # @param useCachedTopologyLineList Boolean value whether modules.diagram.Diagram.cachedTopologyLineList should be used for drawing rather than modules.diagram.Diagram.topologyLineList
    # \returns String representing all diagrams in tikz-feynman
    def getDiagramDrawingStrings(self, diagrams, onlyTopology=False, useCachedTopologyLineList=False):
        drawingString = ""
        for dia in diagrams:
            # Use cached topological line list in favor of the current one
            if useCachedTopologyLineList:
                topoLineList = dia.cachedTopologicalLineList
            else:
                topoLineList = dia.topologicalLineList

            # First, find all lines that have the same start and end points
            similarLines = {line.number : sorted([line.start, line.end]) for line in topoLineList if not line.external}
            # wrap all line numbers with same start and end points together [[1,2], [3,5,6]]
            similarLines = [[key for key, val1 in similarLines.items() if val1 == val2 ]
                                for val2 in [list(x) for x in set(tuple(x) for x in similarLines.values())]]
            similarLines = [l for l in similarLines if len(l) > 1]

            # line content
            content = []
            # save the number of external lines
            externalPointIndex = 0
            for line in topoLineList:
                # check if it is known how the particle shall be drawn
                particle = None
                # check for particle drawing options
                if line.incomingParticle in self.config.drawingObjects:
                    particle = self.config.drawingObjects[line.incomingParticle]
                # check antiparticle in prop file (if it is given)
                elif line.incomingParticle in [p for prop in self.propagators for p in prop]:
                    for prop in self.propagators:
                        if line.incomingParticle == prop[0] and prop[1] in self.config.drawingObjects:
                            particle = self.config.drawingObjects[prop[1]]
                        elif line.incomingParticle == prop[1] and prop[0] in self.config.drawingObjects:
                            particle = self.config.drawingObjects[prop[0]]
                # apply the same procedure for the outgoing particle
                elif line.outgoingParticle in self.config.drawingObjects:
                    particle = self.config.drawingObjects[line.outgoingParticle]
                # check antiparticle in prop file (if it is given)
                elif line.outgoingParticle in [p for prop in self.propagators for p in prop]:
                    for prop in self.propagators:
                        if line.outgoingParticle == prop[0] and prop[1] in self.config.drawingObjects:
                            particle = self.config.drawingObjects[prop[1]]
                        elif line.outgoingParticle == prop[1] and prop[0] in self.config.drawingObjects:
                            particle = self.config.drawingObjects[prop[0]]

                # apply a default drawing option for unknown particles
                if particle == None:
                    if not onlyTopology:
                        logging.warning("Not specified drawing option for particle " + line.incomingParticle +" ("+ line.outgoingParticle + ")")
                    # take scalar or fermion for massless or massive particles
                    if line.incomingParticle != "":
                        if line.mass != "":
                           particle = "fermion"
                        else:
                           particle = "scalar"
                    else:
                        if line.massive:
                            particle = "fermion"
                        else:
                            particle = "scalar"
                    # save new particle drawing option for next iteration
                    if line.incomingParticle != "":
                        self.config.drawingObjects[line.incomingParticle] = particle
                    elif line.outgoingParticle != "":
                        self.config.drawingObjects[line.outgoingParticle] = particle

                # Check for the particle name
                particleName = None
                # check for particleName drawing options
                if line.incomingParticle in self.config.drawingNames:
                    particleName = self.config.drawingNames[line.incomingParticle]
                # check antiparticle in prop file (if it is given)
                elif line.incomingParticle in [p for prop in self.propagators for p in prop]:
                    for prop in self.propagators:
                        if line.incomingParticle == prop[0] and prop[1] in self.config.drawingNames:
                            particleName = self.config.drawingNames[prop[1]]
                        elif line.incomingParticle == prop[1] and prop[0] in self.config.drawingNames:
                            particleName = self.config.drawingNames[prop[0]]
                # apply the same procedure for the outgoing particle
                elif line.outgoingParticle in self.config.drawingNames:
                    particleName = self.config.drawingNames[line.outgoingParticle]
                # check antiparticle in prop file (if it is given)
                elif line.outgoingParticle in [p for prop in self.propagators for p in prop]:
                    for prop in self.propagators:
                        if line.outgoingParticle == prop[0] and prop[1] in self.config.drawingNames:
                            particleName = self.config.drawingNames[prop[1]]
                        elif line.outgoingParticle == prop[1] and prop[0] in self.config.drawingNames:
                            particleName = self.config.drawingNames[prop[0]]

                # if no name was found, the default particle name is the incoming/external particle identifier 
                if particleName == None:
                    if line.incomingParticle != "":
                        particleName = line.incomingParticle
                    else:
                        particleName = line.outgoingParticle

                # external lines
                if line.external:
                    externalPointIndex += 1

                    # draw external momenta for topologies
                    if onlyTopology:
                        if line.mass == "":
                            particle = "charged scalar"
                        else:
                            particle = "fermion"

                        # show momentum name on line 
                        particleName = re.sub(r'^(\-*[a-zA-Z])([0-9]+)$', r'$\1_{\2}$', line.momentum)
                    
                    # draw external particles with their momentum
                    elif particleName and particleName != "" and not particleName.isspace():
                        particleName = particleName + "(" + re.sub(r'^(\-*[a-zA-Z])([0-9]+)$', r'$\1_{\2}$', line.momentum) + ")"

                    # check for the external point of the line
                    if line.start <= 0:
                        startPoint = "e" + str(externalPointIndex) + "[particle = {" + particleName +  "}]"
                        endPoint = "i" + str(line.end)
                    else:
                        startPoint = "i" + str(line.start)
                        endPoint = "e" + str(externalPointIndex) + "[particle = {" + particleName +  "}]"

                    content.append("\t\t\t\t{} -- [{}] {}".format(
                        startPoint,
                        particle, 
                        endPoint))

                # internal lines
                else:
                    # set a line bending if the line is part of similarLines
                    angleString = ""
                    for l in similarLines:
                        if line.number in l:
                            # define middle point of list
                            mid = (len(l)-1)/2
                            # define looseness depending on how many lines there are connecting the same vertices
                            looseness = str(abs(l.index(line.number)-mid) * 1.5)
                            # lower parts
                            if l.index(line.number) < mid:
                                if line.start > line.end:
                                    angleString += ", half left, looseness = " + looseness
                                else:
                                    angleString += ", half right, looseness = " + looseness
                            # upper parts
                            elif l.index(line.number) > mid:
                                if line.start > line.end:
                                    angleString += ", half right, looseness = " + looseness
                                else:
                                    angleString += ", half left, looseness = " + looseness

                    # self-loops always have an angle
                    if line.isSelfLoop():
                        angleString += ", half left, looseness = 1.5"
                    
                    vertex1 = ""
                    vertex2 = ""
                    preface = ""

                    # topologies only consist of massive and massless particles
                    if onlyTopology:
                        if line.mass == "":
                            particle = "charged scalar"
                        else:
                            particle = "fermion"

                        # show momentum name and potentially mass on line 
                        particleName = re.sub(r'^([a-zA-Z])([0-9]+)$', r'$\1_{\2}$', line.momentum)

                        if line.mass != "":
                            particleName = particleName + F",{line.mass}"

                        # show vertex names
                        # vertex1 = "[particle = ${}$]".format(line.start) 
                        # vertex2 = "[particle = ${}$]".format(line.end) 

                        # show dots in topologies
                        preface = r"\tikzfeynmanset{every vertex/.style={dot}}"

                    # Add for self-loops an additional vertex to form a circle
                    if line.isSelfLoop():
                        content.append(F"\t\t\t\ti{line.start} {vertex1} -- [{particle}, edge label = { r'{' + particleName + r'}'}{angleString}] s{line.end} {vertex2}")
                        content.append(F"\t\t\t\ts{line.start} {vertex1} -- [{particle}, edge label = { r'{' + particleName + r'}'}{angleString}] i{line.end} {vertex2}")
                    else:
                        content.append(F"\t\t\t\ti{line.start} {vertex1} -- [{particle}, edge label = { r'{' + particleName + r'}'}{angleString}] i{line.end} {vertex2}")

            # head of the diagram
            # orient diagram horizontally from external point 1 to 2
            if externalPointIndex > 1:
                horizontality = ", horizontal = e1 to e2 "
            else:
                horizontality = ""

            diaString = \
r"""\begin{minipage}{0.33 \textwidth}
    \centering
    \resizebox{0.8 \textwidth}{!}{
        """ + preface + r"""
        \feynmandiagram[large""" + horizontality + r"""]{
"""
            diaString += ",\n".join(content)
            diaString += r"""
        };
    }\\
""" + dia.name + r"""
\end{minipage}
"""
            drawingString += diaString

        return drawingString
            

    ## This method draws the topological structure of modules.diagram.Diagram instances.
    # It basically does the same as #drawDiagrams() only using different styles
    # @param diagrams List of modules.diagram.Diagram instances we want to draw
    def drawTopologies(self, diagrams):
        self.drawDiagrams(diagrams, onlyTopology=True)