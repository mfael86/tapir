## @package integralFamily
# This package contains the class modules.integralFamily.IntegralFamily which contains the analytic information of a Feynman integral.


import copy
from modules.topoAnalyzer import TopologyAnalyzer
from modules.topoMinimizer import TopoMinimizer


## Ancillary plain sub-class that wraps topology information in an easy accessable form  
class IntegralFamily:
    ## Simple constructor class
    # @param topoName The name of the family 
    # @param analyticTopologyExpression A list of terms that define the denominators/numerators of the family. The same as #modules.momentumAssigner.MomentumAssigner.analyticTopologyExpression
    # @param loopMomenta A list of strings that correspond to the loop momentum names in \p analyticTopologyExpression
    # @param indices A list which describe with which power each \p analyticTopologyExpression term contributes
    def __init__(self, topoName="", analyticTopologyExpression=[], loopMomenta=[], indices=[]):
        ## Name of the topology
        self.topoName = topoName
        ## List of terms that define the denominators/numerators of the topology. 
        # This is the same as #modules.momentumAssigner.MomentumAssigner.analyticTopologyExpression
        self.analyticTopologyExpression = analyticTopologyExpression
        ## List of strings that correspond to the loop momentum names in #analyticTopologyExpression
        self.loopMomenta = loopMomenta
        ## Indices of the family
        # This is a list which describe with which power each #analyticTopologyExpression term contributes
        # E.g. `[1,1,0,2]` says the topology is 1/D1*1/D2*1/D4^2
        self.indices = indices
        ## First Symanzik polynomial
        # This is computed via AnalyticTopoMapper::initializeTopology()
        self.U = ""
        ## Second Symanzik polynomial
        # This is computed via AnalyticTopoMapper::initializeTopology()
        self.F = ""
        ## List indices, that defines how the line (Feynman) paramenters in #U and #F have to be renamed to get a canonical ordering
        # This is the same as modules.topoAnalyzer.TopologyAnalyzer::permutations since we use the same method to compute them.
        # This is computed via AnalyticTopoMapper::initializeTopology()
        # Example: <code>[1,4,3,2] => x1->x1, x2->x4, x3->x3, x4->x2</code>\n
        # (i.e. x1 is associated with #analyticTopologyExpression[0], x4 with #analyticTopologyExpression[1], x3 with #analyticTopologyExpression[2] and x2 with #analyticTopologyExpression[3])
        self.permutations = list(range(1,len(analyticTopologyExpression)+1))
        ## List of lexicographic Line-symmetries.
        # This is the same as modules.topoAnalyzer.TopologyAnalyzer::lineSymmetries
        # This is computed via AnalyticTopoMapper::initializeTopology()
        # Example: [[1,2], [1,3]] means the renaming of x1 <=> x2 and/or x1 <=> x3 in #U and #F leads to equal lexicographic weight.\n
        self.lineSymmetries = [] 

    
    ## Remove zeroes from #indices and shorten #analyticTopologyExpression accordingly
    # Note: #U, #F and #permutations are in general not canonically ordered afterwards!
    def stripIndices(self):
        self.indices = [int(i) for i in self.indices]
        while 0 in self.indices:
            # extract where the zero is and remove the appropriate index from the other fields
            zeroIndex = self.indices.index(0)
            del self.indices[zeroIndex]
            del self.analyticTopologyExpression[zeroIndex]
            # re-initialize permutations since removing zeroes destroys the canonical ordering whatsoever
            self.permutations = list(range(1,len(self.analyticTopologyExpression)+1))


    ## Change the #indices field according to #permutations and a possible symmetry transformation
    # Starting with a #permutations field [1,3,2,4] and an transformation \p symmetryTransformations of [(1,2)], we first apply the latter: [3,1,2,4] (see #getSymmetricalPermutations). 
    # Then, if the topology has the indices (a1,a2,a3,a4) we swap them around according to the new \p permutations to get (a2,a3,a1,a4) (x1 has the index a2, x2 has a3, x3 has a1 and x4 has a4)
    # @param symmetryTransformations: List of transformations that shall be applied to #permutations. E.g. [(1,2),(3,4)] means x1<->x2, x3<->x4 or [1,2,3,4] -> [2,1,4,3]  
    # \return Permutated #indices that corresponds to the given symmetry and #permutations.
    def getPermutatedIndices(self, symmetryTransformations=[]):
        newPermutations = self.getSymmetricalPermutations(symmetryTransformations)
        # change the indices according to the new permutations
        if self.indices == []:
            return []
        return [self.indices[newPermutations.index(i+1)] for i in range(len(newPermutations))]


    ## Get #permutations with applied symmetry transformations
    # A #permutations field [1,3,2,4] and an transformation \p symmetryTransformations of [(1,2)] leads to [3,1,2,4]
    # @param symmetryTransformations: List of transformations that shall be applied to #permutations. E.g. [(1,2),(3,4)] means x1<->x2, x3<->x4 or [1,2,3,4] -> [2,1,4,3]  
    # \return Swapped #permutations that corresponds to the given symmetry
    def getSymmetricalPermutations(self, symmetryTransformations=[]):
        newPermutations = self.permutations.copy()

        # apply the symmetryTransformations
        for lineSwap in symmetryTransformations:
            if lineSwap != []:
                newPermutations[lineSwap[0]-1], newPermutations[lineSwap[1]-1] = newPermutations[lineSwap[1]-1], newPermutations[lineSwap[0]-1]

        return newPermutations


    # Apply a bunch of line symmetry transformations on #U
    # @param symmetryTransformations: List of transformations that shall be applied to #U. E.g. [(1,2),(3,4)] with #permutations [1,3,2,4] means to swap x1<->x3, x2<->x4
    # \return #U with applied symmetry transformation
    def getPermutatedU(self, symmetryTransformations=[]):
        u = copy.deepcopy(self.U)
        # apply the symmetryTransformations
        return TopoMinimizer.symbolSwap(u, [("x" + str(self.permutations[swap[0]-1]), "x" + str(self.permutations[swap[1]-1])) for swap in symmetryTransformations])


    # Apply a bunch of line symmetry transformations on #F
    # @param symmetryTransformations: List of transformations that shall be applied to #F. E.g. [(1,2),(3,4)] with #permutations [1,3,2,4] means to swap x1<->x3, x2<->x4
    # \return #F with applied symmetry transformation
    def getPermutatedF(self, symmetryTransformations=[]):
        f = copy.deepcopy(self.F)
        # apply the symmetryTransformations
        return TopoMinimizer.symbolSwap(f, [("x" + str(self.permutations[swap[0]-1]), "x" + str(self.permutations[swap[1]-1])) for swap in symmetryTransformations])


    ## Override string representation of IntegralFamily
    def __str__(self):
        return "{ " + self.topoName + ": [" + ", ".join([str(i) for i in self.analyticTopologyExpression]) + "], [" +", ".join([str(i) for i in self.loopMomenta]) + F"],\n {self.U},\n {self.F},\n{self.permutations}, {self.lineSymmetries}" +" }"