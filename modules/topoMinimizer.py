## @package topoMinimizer
# Here, we minimize a set of given topologies in terms of modules.diagram.Diagram instances.
# The main actor is the modules.topoMinimizer.TopoMinimizer class with its allrounding method modules.topoMinimizer.TopoMinimizer::filter().


import logging
from modules.diagram import Diagram
from modules.topoAnalyzer import TopologyAnalyzer
import sympy as sy
import copy
import time
import multiprocessing as mp
import itertools as it
from modules.filter import Filter
from typing import List
import signal


## This class offers the possibility to analyze and reduce a number of diagram.Diagram's by finding topological transformations.
#
# To reduce a bunch of diagrams, the main method #filter() is used.
class TopoMinimizer(Filter):
    ## Constructor
    # @param config The global config.Conf instance
    # @param verbose Print additional information during minimization, default is True
    # @param fine Use #fineFilter() after #coarseFilter() in #filter()
    # @param kernels Define on how many cores to run on
    def __init__(self, config, fine=True, verbose=True, kernels=1):
        logging.debug("TopoMinimizer started")
        ## config.Conf instance 
        self.config = config
        ## List of unique topoAnalyzer.TopologyAnalyzer representing the diagrams that cannot be mapped any further on each other
        # They are found by #filter().
        self.uniqueDiagramsAnalyzers = []
        ## Dict of diagram.Diagram::name's that describe which diagrams are mapped on each other.
        # It is computed both by #coarseFilter() and #fineFilter() (more precisely in #mapsOn()).
        # Note that if the fine filter is used, this dict is not minimized #TODO
        # E.g. `{"d1l1":"d1l2", "d1l3":"topo2"}` means that diagram "d1l1" can be mapped on "d1l2" and "d1l3" can be mapped on diagram "topo2".
        self.diagramMappings = {} 
        ## Dict that describes how the internal diagram.Line's momenta must be changed to achieve the mapping given by #diagramMappings.
        # It is computed both by #coarseFilter() and #fineFilter() (more precisely in #mapsOn()).
        # Example: `{"d1l1": {"p1":"p2", "p2":"-p4"}, ...}` together with `diagramMapping = {"d1l1":"d1l2"}` means that diagram d1l1 maps on d1l2 by replacing `p1` by `p2` and `p2` by `-p3` etc.
        # Note that the momenta mappings from #mapsOn() do not provide a sign since only Symanzik polynomials are compared! #TODO
        # Also note that if the fine filter is used, this dict is not minimized #TODO
        self.internalMomentumMappingConfigurations = {}
        ## verbose flag to print progress during minimization.
        self.verbose = verbose
        ## Flag to use #fineFilter() after #coarseFilter() in #filter().
        self.fine = fine
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels
        ## Name of the FORM readable file to which the found mapping of #filter() is written.
        self.mappingFileName = config.mappingFile
        ## Name of the YAML file to which the found mapping of #filter() is written.
        self.yamlMappingFileName = config.yamlMappingFile


    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)


    ## Main method of finding a topological minimal set of modules.diagram.Diagram's in a two stage process.
    # Here we call #coarseFilter() and #fineFilter().
    # Both have their advantages and disadvantages, the #coarseFilter() is fast but not capable to find line symmetries and intrinsic graph properties, like for example the Symanzik polynomials.
    # Whereas, the #fineFilter() really finds all possible isomorphisms for Feynman graphs, but it is incredibly slow.
    # @param origDiagrams List of modules.diagram.Diagram Instance we want to minimize
    # @param presetDiagrams List of known diagram.Diagram's which are added to the filtered diagrams and are definitely included in the final minimized diagram.Diagram list from #filter(). This option is used to update previous results and minimizations.
    def filter(self, origDiagrams: List[Diagram], presetDiagrams=[]) -> List[Diagram]:
        # compute Symanzik polynomials also for the known diagrams
        diagrams = presetDiagrams + origDiagrams

        # rename diagrams with the same name, starting from the end of the list
        diagrams.reverse()
        for diagram in diagrams:
            while len([d for d in diagrams if d.name == diagram.name]) > 1:
                diagram.name = diagram.name + "x"

        diagrams.reverse()

        # build a bare topo analyzer for every diagram but do not apply the externalMomenta replacements to preset diagrams
        analyzers = [TopologyAnalyzer(diagram, self.config.externalMomenta) for diagram in diagrams[:len(presetDiagrams)]] \
            + [TopologyAnalyzer(diagram, self.config.externalMomenta) for diagram in diagrams[len(presetDiagrams):]]

        # apply coarse filter (without mapping external momenta)
        self.uniqueDiagramsAnalyzers = self.coarseFilter(analyzers)
        # apply fine filter, if requested in the constructor
        if self.fine:
            self.uniqueDiagramsAnalyzers = self.fineFilter(self.uniqueDiagramsAnalyzers)

        # print result of findings
        self.vprint(F" Found {len(self.uniqueDiagramsAnalyzers)} unique topologies:")
        for a in self.uniqueDiagramsAnalyzers:
            self.vprint(F"  {a.diagram.name}: Nickel index: {a.nickelIndex};")
        
        # write found mappings to a FORM readable mapping file
        if self.mappingFileName:
            self.writeMappingFile(origDiagrams, analyzers)

        # write found mappings to a YAML mapping file
        if self.yamlMappingFileName:
            self.writeYamlMappingFile(origDiagrams, analyzers)

        # strip known preset diagrams from the unique analyzers
        newDiagramNames = [analyzer.diagram.name for analyzer in self.uniqueDiagramsAnalyzers if analyzer.diagram.name not in [d.name for d in presetDiagrams]]
        # return minimal set of diagrams
        return [dia for dia in diagrams if dia.name in newDiagramNames]


    ## Write diagram mappings after minimization to FORM file
    # This method prints all found mappings of #diagramMappings using the information from the original diagrams and the Topology analyzers.
    # @param origDiagrams List of all diagram.Diagram's that were included in the mapping excluding the preset diagrams.
    # @param analyzers List of known all topoAnalyzer.TopologyAnalyzer's including the preset analyzers.
    def writeMappingFile(self, origDiagrams, analyzers):
        try:
            with open(self.mappingFileName, 'w') as mappingFile:
                mappingFile.write("* This file contains the mapping of input diagrams on other diagram topologies\n\n")
                for diagram in origDiagrams:
                    mappingFile.write(F"\n*--#[ {diagram.name} :\n\n")
                    if diagram.name in self.diagramMappings and self.diagramMappings[diagram.name] in analyzers: # i.e. mapping was found by fineFilter
                        mappingFile.write(F"* The mapping {diagram.name} : {self.diagramMappings[diagram.name]} with {self.internalMomentumMappingConfigurations[diagram.name]} was found by Symanzik-Analysis. Relative signs could not be reconstructed!\n")
                    else:
                        # Specify the target topology
                        mappingFile.write(F"   #define INT1 \"{self.diagramMappings.get(diagram.name, diagram.name)}\"\n")
                        # Specify the momentum replacement to agree with target topology
                        mappingFile.write(F"   #define MOMREPLACEMENT \"multiply replace_(")
                        # Either map momenta on another topology
                        if diagram.name in self.diagramMappings:
                            mappingFile.write( ", ".join([ F"{p},{pSub}" for p,pSub in sorted(self.internalMomentumMappingConfigurations[diagram.name].items()) ]) )
                        # ... or make an identity mapping
                        else:
                            mappingFile.write( ", ".join([ F"{p[0]},{p[0]}" for p in diagram.internalMomenta ]) )
                        mappingFile.write(F");\"\n")
                    mappingFile.write(F"\n*--#] {diagram.name} :\n\n")
        except:
            logging.error("Not able to write '" + self.mappingFileName + "'")

        self.vprint(" Mappings can be found in " + self.mappingFileName)


    ## Write diagram mappings after minimization to YAML file
    # This method prints all found mappings of #diagramMappings using the information from the original diagrams and the Topology analyzers.
    # @param origDiagrams List of all diagram.Diagram's that were included in the mapping excluding the preset diagrams.
    # @param analyzers List of known all topoAnalyzer.TopologyAnalyzer's including the preset analyzers.
    def writeYamlMappingFile(self, origDiagrams, analyzers):
        try:
            with open(self.yamlMappingFileName, 'w') as mappingFile:
                mappingFile.write("# This file contains the mapping of input diagrams on other diagram topologies\n\n")
                mappingFile.write("topologyMap:\n")
                for diagram in origDiagrams:
                    if diagram.name in self.diagramMappings and self.diagramMappings[diagram.name] in analyzers: # i.e. mapping was found by fineFilter
                        mappingFile.write(F"# The mapping {diagram.name} : {self.diagramMappings[diagram.name]} with {self.internalMomentumMappingConfigurations[diagram.name]} was found by Symanzik-Analysis. Relative signs could not be reconstructed!\n")
                    else:
                        # Specify the target topology
                        mappingFile.write(F"  - name: {diagram.name}\n    mapsOn: {self.diagramMappings.get(diagram.name, diagram.name)}\n")

                        # Either map momenta on another topology
                        if diagram.name in self.diagramMappings:
                            mappingFile.write("    momentumMapping: {" + ", ".join(
                                [F"{p}: {pSub}" for p,pSub in sorted(self.internalMomentumMappingConfigurations[diagram.name].items())]) + "}"
                            )
                        # ... or make an identity mapping
                        else:
                            mappingFile.write("    momentumMapping: {" + ", ".join(
                                [F"{p[0]}: {p[0]}" for p in diagram.internalMomenta]
                                ) + "}"
                            )
                    mappingFile.write("\n")

        except:
            logging.error("Not able to write '" + self.yamlMappingFileName + "'")

        self.vprint(" Mappings can be found in " + self.yamlMappingFileName)


    ## Parallel callable function to compute the Nickel-Index 
    # This is called in parallel in #coarseFilter().
    # Additionally, the found Nickel index is saved in the underlying diagrams.
    # @param analyzer topoAnalyzer.TopologyAnalyzer of which the index shall be computed
    # \return the same topoAnalyzer.TopologyAnalyzer with invoked topoAnalyzer.TopologyAnalyzer::findNickelIndex()
    def computeNickelIndex(self, analyzer):
        analyzer.findNickelIndex(sorted(self.config.mass.values()))
        analyzer.diagram.nickelIndex = analyzer.nickelIndex
        return analyzer


    ## Apply a coarse filtering on a topoAnalyzer.TopologyAnalyzer list
    # Here, we simply create the topoAnalyzer.TopologyAnalyzer.nickelIndex for each diagram and treat it as a hash.
    # With this it is possible to quickly identify topoAnalyzer.TopologyAnalyzer graphs without digging deeper into details about its analytic structure.
    # It is only possible to find graph theoretically identical graphs, i.e. Feynman graphs are not fully minimized.
    # This is already sufficient for most applications, but for a deeper minimization, see #fineFilter()
    #
    # @param analyzers List of topoAnalyzer.TopologyAnalyzer instances we want to minimize
    # @param diagramPreset diagram.Diagram list that are treated as "known"
    # \return Minimized list of topoAnalyzer.TopologyAnalyzer instances
    def coarseFilter(self, analyzers: List[TopologyAnalyzer]) -> List[TopologyAnalyzer]:
        logging.debug("\n"+"-"*10 +  "Coarse Minimization" + "-"*10)
        # save the unique analyzers in a dict with the Nickel-index as key. The value is a list of analyzer objects
        uniqueAnalyzers = {}

        # compute the Nickel-index of each analyzer in the main thread
        if self.kernels == 1:
            newAnalyzers = []
            for i,analyzer in enumerate(analyzers):
                self.vprint(" Compute Nickel-Indices: {0:^7}/{1:^7}\r".format(i, len(analyzers)), end = "")
                newAnalyzers.append(self.computeNickelIndex(analyzer))
            self.vprint(" Compute Nickel-Indices: Done                  ")
            analyzers = newAnalyzers
        # compute the Nickel-index of each analyzer in parallel
        else:
             # initializer function for each worker in the multithread pool
            def initializer():
                # ignore CTRL+C in the worker process
                signal.signal(signal.SIGINT, signal.SIG_IGN)

            if self.kernels == 0:
                pool = mp.Pool(mp.cpu_count(),initializer=initializer)
            else:
                pool = mp.Pool(self.kernels,initializer=initializer)
            
            # start asynchronous workers
            result = pool.map_async(self.computeNickelIndex, [analyzer for analyzer in analyzers], chunksize=1)

            # print progress with actualization rate of 10 Hz
            while not result.ready():
                self.vprint(" Compute Nickel-Indices: {0:^7}/{1:^7}\r".format(len(analyzers)-result._number_left, len(analyzers)), end = "")
                time.sleep(0.1)
            self.vprint(" Compute Nickel-Indices: Done                  ")
            pool.close()
            pool.join()

            analyzers = result.get()
        
        # compare Nickel-indices with the others
        for i, analyzer in enumerate(analyzers):
            self.vprint(" Coarse Minimization : {0:^7}/{1:^7}\r".format(i+1, len(analyzers)), end = "")

            # same Nickel-index was already set by another analyzer
            if analyzer.nickelIndex in uniqueAnalyzers.keys():
                # check if any of the analyzer fit to the restriction of same external momenta
                uniqueAnalyzer = uniqueAnalyzers[analyzer.nickelIndex]

                # specify mapping
                self.diagramMappings[analyzer.diagram.name] = uniqueAnalyzer.diagram.name

                # define how to change internal momenta to apply the mapping using topoAnalyzer.nickelOrderedMomenta
                internalMomentaReplacements = {}
                for p1, p2 in zip(analyzer.nickelOrderedMomenta, uniqueAnalyzer.nickelOrderedMomenta):
                    if p1[0] == "-":
                        if p2[0] == "-":
                            internalMomentaReplacements[p1[1:]] = p2[1:]
                        else:
                            internalMomentaReplacements[p1[1:]] = "-" + p2
                    else:
                        internalMomentaReplacements[p1] = p2
                self.internalMomentumMappingConfigurations[analyzer.diagram.name] = internalMomentaReplacements

            # Nickel-index is new
            else:
                uniqueAnalyzers[analyzer.nickelIndex] = analyzer

        self.vprint(" Coarse Minimization :  Done                          ")

        # extract filtered diagram names
        diagramNames = [analyzer.diagram.name for analyzer in uniqueAnalyzers.values()]

        self.vprint(F" Coarse minimization reduced {len(analyzers)} to {len(diagramNames)} diagrams")

        # return minimized set of diagram analyzers
        return [analyzer for analyzer in analyzers if analyzer.diagram.name in diagramNames]
            
    
    ## Main method of finding a topological minimal set of diagram.Diagram's.
    # To minimize the number of diagrams (called topology in the following),
    # We first build the Symanzik-polynomials topoAnalyzer.TopologyAnalyzer::U and topoAnalyzer.TopologyAnalyzer::F of each diagram.Diagram 
    # using the topoAnalyzer.TopologyAnalyzer, and compare them by removing and/or renaming their line weights and external momenta to find relations between them.
    # The process of renaming of variables is an isomorphic operation between Feynman integrals in Feynman parameter representation.
    #
    # We follow the conventions and rules of [C. Bogner and S. Weinzierl, Int. J. of mod. Phys. A, Vol. 25 No. 13 (2010) 2585-2618 (DOI: 10.1142/S0217751X10049438)]
    # 
    # To improve performance the topology analysis (see #analyzeDiagram()) runs on multiple cores, using the "multiprocessing" library of python 3.
    #
    # After the analysis, we call #findDiagramMappings() to find possible mappings between topoAnalyzer.TopologyAnalyzer's.
    #
    # If #diagramPreset is specified, topologies, that map on this preset are filtered out as well.
    #
    # @param diagrams List of diagram.Diagram instances we want to minimize
    # @param diagramPreset diagram.Diagram list that are treated as "known"
    # \return Minimized list of diagram.Diagram instances
    def fineFilter(self, analyzers: List[TopologyAnalyzer], presetDiagrams=[]) -> List[TopologyAnalyzer]:
        logging.debug("\n"+"-"*10 +  "Fine Minimization" + "-"*10)
        logging.debug("Parallel computation of all diagram polynomials...")

        # calculation of Symanzik polynomials and their canonical ordering
        if self.kernels == 1:
            # compare Symanzik polynomials with the fully analyzed diagrams
            uniqueAnalyzers = self.findDiagramMappings([self.analyzeDiagram(a) for a in analyzers], presetDiagrams)
        # parallel calculation of Symanzik polynomials and their canonical ordering
        else:
             # initializer function for each worker in the multithread pool
            def initializer():
                # ignore CTRL+C in the worker process
                signal.signal(signal.SIGINT, signal.SIG_IGN)

            if self.kernels == 0:
                pool = mp.Pool(mp.cpu_count(),initializer=initializer)
            else:
                pool = mp.Pool(self.kernels,initializer=initializer)
            
            # start asynchronous workers
            result = pool.map_async(self.analyzeDiagram, analyzers, chunksize=1)

            # print progress with actualization rate of 10 Hz
            while not result.ready():
                self.vprint(" Analyze diagrams: {0:^7}/{1:^7}\r".format(len(analyzers)-result._number_left, len(analyzers)), end = "")
                time.sleep(0.1)
            self.vprint(" Analyze diagrams: Done           ")
            pool.close()
            pool.join()
            
            # compare Symanzik polynomials with the fully analyzed diagrams
            uniqueAnalyzers = self.findDiagramMappings(result.get(), presetDiagrams)

        # extract filtered diagram names
        diagramNames = [analyzer.diagram.name for analyzer in uniqueAnalyzers]

        self.vprint(F" Fine filtering reduced {len(analyzers)} to {len(uniqueAnalyzers)} diagrams\n")

        # return minimal set of diagram analyzers
        return [analyzer for analyzer in analyzers if analyzer.diagram.name in diagramNames]


    ## Evoke the spectrum of analysis in topoAnalyzer.TopologyAnalyzer on each individual diagram.Diagram to find all topological properties we need for #fineFilter()
    #
    # The list of calls is
    # * topoAnalyzer.TopologyAnalyzer::constructSymanzikPolynomials2()
    # * topoAnalyzer.TopologyAnalyzer::simplifyExternalMomentaInF()
    # * topoAnalyzer.TopologyAnalyzer::canonicallyOrderSymanzikPolynomials()
    # 
    # @param analyzer topoAnalyzer.TopologyAnalyzer instance we want to analyze further
    # \return TopologyAnalyzer representation of \p diagram with full analysis
    def analyzeDiagram(self, topoAnalyzer):
        # compute U and F polynomials
        topoAnalyzer.constructSymanzikPolynomials2()
        # use momentum replacement rules and momentum conservation equation to simplify F
        topoAnalyzer.simplifyExternalMomentaInF()
        # apply Pak's algorithm to order U and F
        topoAnalyzer.canonicallyOrderSymanzikPolynomials()

        return topoAnalyzer

    
    ## Find out if a bunch of topoAnalyzer.TopologyAnalyzer's can be mapped on each other.
    # Used for #fineFilter().
    #
    # For a list of topoAnalyzer.TopologyAnalyzer's, we first sort the topologies by their maximal number of diagram.Line's in descending order.
    # We then iterate through the list and see if a topology "fits" into one of the unique topoAnalyzer.TopologyAnalyzer's. 
    # If that's not the case, the corresponding topoAnalyzer.TopologyAnalyzer is saved.
    #
    # The initial ordering gives us the possibility to fill the unique topoAnalyzer.TopologyAnalyzer list with topologies prefferably with more lines.
    # It is thus more likely to minimize the amount of unique topoAnalyzer.TopologyAnalyzer's, since we can map topologies with fewer line onto larger ones.
    #
    # This is achieved with line cuttings of (copies of) the unique diagrams. 
    # If at some point the to diagrams we want to compare have the same number of lines, we can find a possible mapping relation with the help of #mapsOn().
    #
    # @param topoAnalyzers List of topoAnalyzer.TopologyAnalyzer instances we want to find mapping relations on
    # @param diagramPreset diagram.Diagram list that are treated as "known"
    # \return uniqueAnalyzers List of unique topoAnalyzer.TopologyAnalyzer instances
    def findDiagramMappings(self, topoAnalyzers, presetDiagrams):
        # initialize the list of empty analyzers
        uniqueAnalyzers = []
        # First, sort diagrams by their number of lines to make sure that we can map small diagrams on larger ones.
        # But ensure that preset diagrams are in front 
        presetAnalyzers = []
        regularAnalyzers = []
        presetDiagramNames = [diagram.name for diagram in presetDiagrams]

        for analyzer in topoAnalyzers:
            if analyzer.diagram.name in presetDiagramNames:
                presetAnalyzers.append(analyzer)
            else:
                regularAnalyzers.append(analyzer)
        
        presetAnalyzers.sort(key = lambda ta: len(ta.diagram.internalMomenta), reverse = True)
        regularAnalyzers.sort(key = lambda ta: len(ta.diagram.internalMomenta), reverse = True)

        topoAnalyzers = presetAnalyzers + regularAnalyzers
        
        # compare each diagram with the ones from uniqueDiagrams list
        counter = 0
        for analyzer in topoAnalyzers: 
            counter += 1
            self.vprint(" Fine Minimization: {0:^7}/{1:^7}\r".format(counter, len(topoAnalyzers)), end = "")
            found = False
            for uniqueAnalyzer in uniqueAnalyzers:
                # first check for number of propagators
                numberLinesToCut = len(uniqueAnalyzer.diagram.internalMomenta) - len(analyzer.diagram.internalMomenta)
                # diagrams have not the same amount of lines
                if numberLinesToCut > 0:
                    # For topologies of unequal size, we have to kick-out lines to see if the diagrams agree.
                    # This must be applied in such a way, that the lineMasses agree.
                    massesInLinesA = analyzer.getMassInLines()
                    deletableLines = []
                    for key, val in uniqueAnalyzer.getMassInLines().items():
                        # masses cannot be found in diagram -> all lines with this mass must possibly be deleted
                        if key not in massesInLinesA:
                            deletableLines.extend(val)
                        # if there are less lines of the mass in the diagram, take each single line as deletable
                        elif len(val) > len(massesInLinesA[key]):
                            deletableLines.extend(val)
                        # there is more lines of a mass in the diagram with less lines -> deleting does not work
                        elif len(val) < len(massesInLinesA[key]):
                            deletableLines = []
                            break

                    # delete lines iteratively until the number of lines agree and compare the diagrams
                    for cutLines in it.combinations(deletableLines, numberLinesToCut):
                        # make a copy of the unique analyzer to compare to 
                        cutUniqueAnalyzer = copy.deepcopy(uniqueAnalyzer)
                        # change the number of lines, the line names in the polynomials and the line mass list

                        cutUniqueAnalyzer.deleteLines(cutLines)
                        
                        # print("before ordering ",cutUniqueAnalyzer.permutations,"\n", cutUniqueAnalyzer.F, "\nsymmetries:", cutUniqueAnalyzer.lineSymmetries)
                        # order canonically in lines and compare diagrams again
                        
                        cutUniqueAnalyzer.canonicallyOrderSymanzikPolynomials()

                        if self.mapsOn(analyzer, cutUniqueAnalyzer):
                            found = True
                            break

                # diagrams have same amount of lines
                else:
                    massesInLinesA = analyzer.getMassInLines()
                    massesInLinesB = uniqueAnalyzer.getMassInLines()     
                    massesMatch = True
                    # check if mass composition matches
                    for m in massesInLinesA:
                        if m not in massesInLinesB:
                            massesMatch = False
                        elif len(massesInLinesA[m]) != len(massesInLinesB[m]):
                            massesMatch = False
                    
                    if massesMatch:
                        found = self.mapsOn(analyzer, uniqueAnalyzer)

                if found: 
                    break

            # add diagram to unique list if no match was found
            if not found:
                uniqueAnalyzers.append(analyzer)
        self.vprint(" Fine Minimization: Done                    ")

        return uniqueAnalyzers


    ## Find out if two topoAnalyzer.TopologyAnalyzer's with the same amount of diagram.Line's can be mapped on each other.
    # Called by #findDiagramMappings()
    #
    # The mapping restricts to a simple comparison of topoAnalyzer.TopologyAnalyzer.U and topoAnalyzer.TopologyAnalyzer.F polynomials with canonically ordered line weights.
    # One only has to iterate over topoAnalyzer.TopologyAnalyzer.lineSymmetries to compare \f$\mathcal{U}\f$'s.
    #
    # For the \f$\mathcal{F}\f$ comparison, we need to iterate over all external momentum renamings 
    # and also over their substitution, that occurs due to topoAnalyzer.TopologyAnalyzer.externalMomentumConservationSum.
    #
    # More specifically, The goal of this method is to find an isomorphism \f$f\f$ such that a potentially cutted \p analyzerA (topology \f$A\f$)
    # can be mapped on an unchanged \p analyzerB (topology \f$B\f$), i.e. find a \f$f\f$ such that \f$B = f(A)\f$
    #
    # @param analyzerA First topoAnalyzer.TopologyAnalyzer, we want to find a mapping for
    # @param analyzerB The second topoAnalyzer.TopologyAnalyzer, we want to map on from \p analyzerA
    # \return Boolean value if te mapping was successful or not
    def mapsOn(self, analyzerA, analyzerB):
        # check first all line symmetries (i.e. same ordering hierarchy for xi <-> xj) of A and B and determine all possible renaming combinations arising from them
        lineSymmetryCombinationsA = self.allCombinationsOfVariableLength(analyzerA.lineSymmetries)
        lineSymmetryCombinationsB = self.allCombinationsOfVariableLength(analyzerB.lineSymmetries)

        # iterate through the combinatorical product of both combination lists
        for (lineSymmetriesA, lineSymmetriesB) in it.product(lineSymmetryCombinationsA, lineSymmetryCombinationsB):
            # create copies of U's with renamed x's, check for cases where only 1 symbol appears, in which case sympy does nonsense
            if not isinstance(analyzerA.U, sy.core.symbol.Symbol):
                uA = self.symbolSwap(analyzerA.U.copy(), [["x" + str(sym[0]), "x" + str(sym[1])] for sym in lineSymmetriesA if len(sym) != 0])
            else:
                uA = 1*analyzerA.U
            if not isinstance(analyzerB.U, sy.core.symbol.Symbol):
                uB = self.symbolSwap(analyzerB.U.copy(), [["x" + str(sym[0]), "x" + str(sym[1])] for sym in lineSymmetriesB if len(sym) != 0])
            else:
                uB = 1*analyzerB.U
                
            # if U is already unequal, skip to next line symmetry combination
            if uA - uB != 0:
                continue

            # make copies of F's
            fA = analyzerA.F.copy()
            fB = analyzerB.F.copy()

            # rename x's in F
            xSwapsA = [["x" + str(sym[0]), "x" + str(sym[1])] for sym in lineSymmetriesA if len(sym) != 0]
            xSwapsB = [["x" + str(sym[0]), "x" + str(sym[1])] for sym in lineSymmetriesB if len(sym) != 0]
            fA = self.symbolSwap(fA, xSwapsA)
            fB = self.symbolSwap(fB, xSwapsB)

            # now check for F equality
            if fA - fB != 0:
                continue

            # form here a match was found! now save the line and external momentum renaming we applied to get here
            # save mapping of diagram A to diagram B
            self.diagramMappings[analyzerA.diagram.name] = analyzerB.diagram.name

            # return to uncut line permutations and save the renaming. For a renaming function f we found f_a(A) = f_b(B). Thus, B = f_b^{-1}(f_a(A)) 
            uncutLinePermutationsA = analyzerA.getUncutPermutations() # f_a
            for sym in lineSymmetriesA:
                if len(sym) != 0:
                    i1 = uncutLinePermutationsA.index(sym[0])
                    i2 = uncutLinePermutationsA.index(sym[1])
                    uncutLinePermutationsA[i1], uncutLinePermutationsA[i2] = uncutLinePermutationsA[i2], uncutLinePermutationsA[i1]

            uncutLinePermutationsB = analyzerB.getUncutPermutations() # f_b
            for sym in lineSymmetriesB:
                if len(sym) != 0:
                    i1 = uncutLinePermutationsB.index(sym[0])
                    i2 = uncutLinePermutationsB.index(sym[1])
                    uncutLinePermutationsB[i1], uncutLinePermutationsB[i2] = uncutLinePermutationsB[i2], uncutLinePermutationsB[i1]
            
            # B = f_b^{-1}(f_a(A)) 
            lineMappingConfigurations = [uncutLinePermutationsB.index(i)+1 for i in uncutLinePermutationsA]

            # build the momentum replacement rules 
            self.internalMomentumMappingConfigurations[analyzerA.diagram.name] = {"p"+str(i+1) : "p"+str(n) for i,n in enumerate(lineMappingConfigurations)}
            
            return True

        return False
        

    ## Find all possibilities of grouping and permuting elements of a list without strict length (with elements appearing only ones)
    #
    # Example: For [1,2,3] we get [(), (1,), (2,), (3,), (1, 2), (1, 3), (2, 1), (2, 3), (3, 1), (3, 2), (1, 2, 3), (1, 3, 2), (2, 1, 3), (2, 3, 1), (3, 1, 2), (3, 2, 1)]
    #
    # @param inputList Source list of arbitrary elements
    # \return List of combinations as sublists
    @staticmethod
    def allCombinationsOfVariableLength(inputList):
        combinations = []
        for n in range(len(inputList)+1):
            combinations.extend(list(it.permutations(inputList,n)))

        return combinations


    ## Swap symbol strings in sympy expressions
    # 
    # @param expr Sympy expression
    # @param listOfRenames List of symbols as strings we want to swap in \p expr. 
    # Like <code>[["x", "y"], ["z", "z1"]]</code> -> replace x by y, z by z1 and y by x and z1 by z at the same time.
    # \return \p expr with swapped symbols
    @staticmethod
    def symbolSwap(expr, listOfSwaps = [[]]):
        # this ancillary function swaps symbols in the given expression according to a swap list
        expr = expr.subs(
            [ (sy.Symbol(swap[0]), sy.Symbol("SWAPPLACEHOLDER" + str(swap[1]))) for swap in listOfSwaps ]
        )
        expr = expr.subs(
            [ (sy.Symbol(swap[1]), sy.Symbol(swap[0])) for swap in listOfSwaps ]
        )
        expr = expr.subs(
            [ (sy.Symbol("SWAPPLACEHOLDER" + str(swap[1])), sy.Symbol(swap[1])) for swap in listOfSwaps ]
        )

        return expr
