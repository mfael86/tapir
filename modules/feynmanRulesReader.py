## @package feynmanRulesReader
# Here, we provide the class modules.feynmanRulesReader.FeynmanRulesReader which is able to parse .vrtx and .prop files and generates a modules.feynmanRules.FeynmanRules object
# that contains the information


import logging
import re
from modules.feynmanRules import FeynmanRules

## The FeynmanRulesReader class reads in .vrtx and .prop files.
#
#  After the constructor has read in the .vrtx and .prop files, the information can be returned via #returnRules
class FeynmanRulesReader:
    ## The constructor, initializes all member and reads in config file.
    #  @param self The object pointer.
    #  @param conf A modules.config.Conf object. Has to contain paths to .prop and .vrtx files. Otherwise an error occurs.
    def __init__(self, conf, verbose=False):
        ## modules.config.Config object
        self.config = conf
        ## Verbose flag to print progress during file reading.
        self.verbose = verbose
        ## FeynmanRules object to hold the read-in Feynman Rules
        self.feynmanRules = FeynmanRules(self.config)
        
        # Now we read in .vrtx and .prop files
        self.readPropFile()
        self.readVertexFile()
        

    ## Returns a modules.feynmanRules.FeynmanRules object containing all information
    #  @param self The object pointer.
    def returnRules(self):
        self.feynmanRules.gaugeProps = self.config.gaugeProps
        return self.feynmanRules


    ## Create a dict which returns for every particle its antiparticle according to the propagator file
    # This method scans through the modules.feynmanRules.FeynmanRules::propLorentz field to extract the particle content of the propagator
    # \return Dictionary containing keys and values which connect names of particles and anit-particles
    def getAntiParticleDict(self):
        antiParticleDict = {}
        # Scan Feynman rules for propagating particles
        for particles in self.feynmanRules.propLorentz.keys():
            antiParticleDict[particles[0]] = particles[1]
            antiParticleDict[particles[1]] = particles[0]

        return antiParticleDict
        
        
    ## Reads in propagator Feynman rules from .prop file.
    #  @param self The object pointer.
    #
    #  If #q2eConfig does not contain a path to a valid .prop File we abort.
    def readPropFile(self):
        try:
            propHandle = open(self.config.propFile,'r')
            propEntries = ''.join(propHandle.readlines())
            propHandle.close()
            propEntries = ''.join(propEntries.split())

            # Get lines in which a new entry starts and ends
            starts = [n for n,char in enumerate(propEntries) if char == "{"]
            ends = [n for n,char in enumerate(propEntries) if char == "}"]
          
            # Make sure syntax is correct
            if len(starts) > len(ends):
                assert False, F"{len(starts)-len(ends)} missing {'}'}"
            if len(starts) < len(ends):
                assert False, F"{len(starts)-len(ends)} missing {'{'}"

            # Now we match rules of the form {p1,p2:lorentz|QCD|QED|EW}.
            # We throw an error if we have more than 2 particles in a rule.
            for i in range(0,len(starts)):
                match = re.match(r'(\S+):(\S*?)\|(\S*?)\|(\S*?)\|(\S*?)$',propEntries[starts[i]+1:ends[i]])
                if match:
                    assert len(match.group(1).split(",")) == 2, "Too many particles in propagator rule: " + match.group(1)
                    particles = match.group(1).split(",")
                    # Create key from particle content of Feynman rule
                    particles = tuple(particles)

                    # Append Feynman rules to known ones for this specific particle content
                    if particles in self.feynmanRules.propLorentz:
                        self.feynmanRules.propLorentz[particles].append(match.group(2))
                        self.feynmanRules.propQCD[particles].append(match.group(3))
                        self.feynmanRules.propQED[particles].append(match.group(4))
                        self.feynmanRules.propEW[particles].append(match.group(5))
                    else:
                        self.feynmanRules.propLorentz[particles] = [match.group(2)]
                        self.feynmanRules.propQCD[particles] = [match.group(3)]
                        self.feynmanRules.propQED[particles] = [match.group(4)]
                        self.feynmanRules.propEW[particles] = [match.group(5)]

        except FileNotFoundError:
            logging.error(".prop file not found!")
            exit(1)

        except Exception as e:
            logging.error(e)
            exit(1)


    ## Reads in propagator Feynman rules from .vrtx file.
    #  @param self The object pointer.
    #
    #  If #q2eConfig does not contain a path to a valid .vrtx File we abort.
    def readVertexFile(self):
        try:
            vrtxHandle = open(self.config.vrtxFile,'r')
            vrtxEntries = ''.join(vrtxHandle.readlines())
            vrtxHandle.close()
            vrtxEntries = ''.join(vrtxEntries.split())

            # get lines in which a new entry starts and ends
            starts = [n for n,char in enumerate(vrtxEntries) if char == "{"]
            ends = [n for n,char in enumerate(vrtxEntries) if char == "}"]
        
            # Now we match rules of the form {p1,p2,...,pN:lorentz|QCD|QED|EW}, they should not contain whitespaces.
            for i in range(0,len(starts)):
                match = re.match(r'(\S+):(\S+?)\|(\S*?)\|(\S*?)\|(\S*?)$',vrtxEntries[starts[i]+1:ends[i]])
                if match:
                    particles = match.group(1).split(",")
                    # Create key from particle content of Feynman rule
                    particles = tuple(particles)

                    # Append Feynman rules to known ones for this specific particle content
                    if particles in self.feynmanRules.vrtxLorentz:
                        self.feynmanRules.vrtxLorentz[particles].append(match.group(2))
                        self.feynmanRules.vrtxQCD[particles].append(match.group(3))
                        self.feynmanRules.vrtxQED[particles].append(match.group(4))
                        self.feynmanRules.vrtxEW[particles].append(match.group(5))
                    else:
                        self.feynmanRules.vrtxLorentz[particles] = [match.group(2)]
                        self.feynmanRules.vrtxQCD[particles] = [match.group(3)]
                        self.feynmanRules.vrtxQED[particles] = [match.group(4)]
                        self.feynmanRules.vrtxEW[particles] = [match.group(5)]

        except FileNotFoundError:
            logging.error(".vrtx file not found!")
            exit(1)
