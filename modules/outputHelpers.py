## @package outputHelpers
#
# The methods described here are helper functions for riding FORM/Mathematica code

import logging
import os

## Write a string to a file
# This method initializes a file and checks if directory has write access. Then we write the given content into it.
# @param path String that describes the path where the file shall be written to
# @param filename The name of the outgoing file
# @param content The String content of the file
def writeCodeFile(path, filename, content):
    logging.debug("\n\n\nGenerate Topology file for: " + filename)
    # open file and fill it
    try:
        with open(os.path.join(path, filename), 'w') as topoFile:
            topoFile.write(content)

    except FileNotFoundError:
        logging.error("Not able to write " + os.path.join(path, filename))
        exit(1)


## Transform a list with possible sublists to a single flat list
# E.g. [[1], 32, [2,[2]]] -> [1, 32, 2, 2]
# Non-lists are returned as they are
# @param boxedList List with possible sublists
# \return Flattened version of \p boxedList
def flatten(boxedList):
    # Return non-lists
    if type(boxedList)!=list:
        return boxedList
        
    # Repeat deboxing elements which are itself lists until the list is flat
    while [e for e in boxedList if type(e)==list] != []:
        newList = []
        for element in boxedList:
            if type(element)==list:
                newList.extend(element)
            else:
                newList.append(element)
        boxedList = newList
    return boxedList