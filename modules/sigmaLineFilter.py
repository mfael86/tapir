## @package sigmaLineFilter
# The here described filter class modules.bridgeRemover.SigmaLineFilter just removes lines of so-called sigma particles, that do not carry momentum.


import logging
from typing import List
from modules.diagram import Diagram
from modules.filter import Filter
import multiprocessing as mp
import time
import signal


## A simple Filter class which removes lines of sigma particles from diagrams destructively.
# The main method is #filter().
# Sigma particles are mathematical objects without physical propagator.
# Hence one can remove their lines safely from the topology to save some space.
# Note: This filter is only available on qlist inputs.
class SigmaLineFilter(Filter):
    ## Constructor
    # @param conf modules.config.Conf instance where modules.config.Conf::topoIgnoreBridges is (possibly) specified
    def __init__(self, conf, topselIn, verbose=False, kernels=1):
        ## Local modules.config.Conf instance
        self.conf = conf
        ## Boolean value whether the input comes from a topsel file or not
        # If the input does come from a topsel file, no particle information is provided and this filter cannot be applied
        self.topselIn = topselIn
        ## Flag whether the input is from a topsel file or a qlist file
        ## verbose flag to print progress
        self.verbose = verbose
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels


    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)


    ## Basic pipe method that changes the topological structure of an input list of modules.diagram.Diagram's to the specifications of modules.config.Conf::sigmaParticles. 
    # We call #contractSigmaLinesOfDiagram() from here which does the main job.
    # It is run in parallel for best performance result.
    # As a result, get the modules.diagram.Diagram's with sigma particle lines contracted but properly initialized (i.e. the line and vertex numbers are ascending without gaps)
    # Be aware, that this mehtod is destructive! I.e. The diagrams are intrinsically changed.
    # Note: Spinor indices are not contracted!
    # Also note that tree level diagrams are completely removed from \p diagrams
    # @param diagrams List of modules.diagram.Diagram instances we want to filter
    # \return \p diagrams where the sigma lines are contracted
    def filter(self, diagrams: List[Diagram]) -> List[Diagram]:
        # continue with main program when there's nothing to remove
        if self.conf.sigmaParticles == []:
            return diagrams

        # check if the input is valid
        if self.topselIn:
            logging.warning("The option 'sigma_particles' cannot be used from a topsel input! Skipping this filter.")
            return diagrams

        # remove unwanted lines from the diagrams in main thread
        if self.kernels == 1:
            dias = []
            for i,d in enumerate(diagrams):
                self.vprint(" Remove sigma particle lines: {0:^7}/{1:^7}\r".format(i, len(diagrams)), end = "")
                dias.append(self.contractSigmaLinesOfDiagram(d))
            self.vprint(" Remove sigma particle lines: Done                  ")
            return dias
        
        # remove unwanted lines from the diagrams in parallel
        else:
             # initializer function for each worker in the multithread pool
            def initializer():
                # ignore CTRL+C in the worker process
                signal.signal(signal.SIGINT, signal.SIG_IGN)

            if self.kernels == 0:
                pool = mp.Pool(mp.cpu_count(),initializer=initializer)
            else:
                pool = mp.Pool(self.kernels,initializer=initializer)

            # start asynchronous workers
            result = pool.map_async(self.contractSigmaLinesOfDiagram, diagrams, chunksize=1)

            # print progress with actualization rate of 10 Hz
            while not result.ready():
                self.vprint(" Remove sigma particle lines: {0:^7}/{1:^7}\r".format(len(diagrams)-result._number_left, len(diagrams)), end = "")
                time.sleep(0.1)
            self.vprint(" Remove sigma particle lines: Done                  ")
            pool.close()
            pool.join()

            return result.get()

    
    ## Contract all sigma particles from a given diagram.
    # Here, we find all lines with propagating sigma particles, contract them and rename the vertices and lines.
    # Note: Spinor indices are not contracted!
    # The method simply finds lines with sigma particles and evokes modules.diagram.Diagram.contractLines().
    # @param diagrams List of modules.diagram.Diagram instances of which we want to remove the sigma lines. Note that this list is altered afterwards!
    # \return List of diagrams with changed topology
    def contractSigmaLinesOfDiagram(self, diagram):
        # find all sigma lines 
        sigmaLineNumbers = [ line.number for line in diagram.topologicalLineList if line.incomingParticle in self.conf.sigmaParticles or line.outgoingParticle in self.conf.sigmaParticles ]
        diagram.contractLines(sigmaLineNumbers)
        return diagram

