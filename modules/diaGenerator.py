## @package diaGenerator
# Here, we provide the class modules.diaGenerator.diaGenerator which is able to generate the FORM code for each diagram.

import logging
import re
from modules.ediaGenerator import EdiaGenerator
import multiprocessing as mp
import time
import signal
import itertools as it
import copy


## The DiaGenerator class reads in .vrtx and .prop files and generates FORM output for each diagram.
#
#  After the constructor has read in the .vrtx and .prop files, the .dia output can be generated with #generateEntriesAndExtendDiagrams() and written using #writeDia()
class DiaGenerator:
    ## The constructor, initializes all member and reads in config file.
    # @param self The object pointer.
    # @param conf A modules.config.Conf object. Has to contain paths to .prop and .vrtx files. Otherwise an error occurs.
    # @param feynmanRules A modules.feynmanRules.FeynmanRules object. Has to contain the Feynman rules for all occuring propagators and vertices.
    def __init__(self, conf, feynmanRules, verbose=False, kernels=1):
        ## modules.config.Config object
        self.config = conf
        ## modules.feynmanRules.FeynmanRules object
        self.feynmanRules = feynmanRules
        ## Verbose flag to print progress during file reading.
        self.verbose = verbose
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels
        ## List which is filled with diagram code by #generateEntriesAndExtendDiagrams()
        self.entryList = []



    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)


    ## Writes generates diagram code to a file.
    # #generateEntriesAndExtendDiagrams() must be called first to generate the actual code.
    # @param self The object pointer.
    # @param diaFile Path to the output file.
    def writeDia(self,diaFile):
        # write entries to a single .dia file if module.config.Config.blockSize is not set do make multiple blocks.
        EdiaGenerator.generatePath(diaFile)

        if self.config.blockSize < 1:
            try:
                with open(diaFile,'w') as diaHandle:
                    for entry in self.entryList:
                        diaHandle.write(entry)

            except FileNotFoundError:
                logging.error("Not able to write .dia")
                exit(1)
        # If multiple blocks should be generated, create a list of multiple entry lists and write to multiple files.
        else:
            listofentryLists = [self.entryList[i:i+self.config.blockSize] for i in range(0, len(self.entryList), self.config.blockSize)]
            for i in range(0,len(listofentryLists)):
                try:
                    with open(diaFile + "." + str(i+1),'w') as diaHandle:
                        for entry in listofentryLists[i]:
                            diaHandle.write(entry)

                except FileNotFoundError:
                    logging.error("Not able to write .dia")
                    exit(1)


    ## Parallel call of the code generation routine #generateEntry() and extension of diagrams according to overlapping Feynman rules.
    # This is a precomputation method that generate FORM code in form of .dia folds.
    # The folds are generated in memory and printed with a call of #writeDia()
    # If the .prop or .vrtx files include multiple Feynman rules with the same particle content, the feeded modules.diagram.Diagram list is extended by copies of the affected diagrams.
    # The original list itself is not changed.
    # To have the correct diagram counting, the diagrams in the newly created list have different names than the original ones.
    # @param self The object pointer.
    # @param diagrams A list of modules.diagram.Diagram objects.
    # \return List of modules.diagram.Diagram which is the same (reference) as the input, if no overlapping Feynman Rules ocurred. Otherwise the list is decoupled and extended from th input. 
    def generateEntriesAndExtendDiagrams(self, diagrams):
        numDias = len(diagrams)
        # match all vertex and propagator rules. Also extend diagrams list if multiple rules have the same particle content
        diagrams, matchedPropagators, matchedVertices = self.matchRulesAndExtendDiagrams(diagrams)
        
        # rename (and copy) diagrams if their number has increased
        if len(diagrams) > numDias:
            if self.verbose:
                logging.warning("Found two or more Feynman rules with same particle content. Create all combinations for dia/edia output.")
            newDiagrams = []
            for i,diagram in enumerate(diagrams):
                d = copy.deepcopy(diagram)
                d.diaComment = F"Generated from qlist entry {d.diagramNumber} with .prop entries {matchedPropagators[i]} and .vrtx entries {matchedVertices[i]}"
                d.diagramNumber = i+1
                d.setName()
                newDiagrams.append(d)
            diagrams = newDiagrams

        # run on main thread
        if self.kernels == 1:
            for i in range(len(diagrams)):
                self.vprint(" Generate dia entries: {0:^7}/{1:^7}\r".format(i, len(diagrams)), end = "")
                self.entryList.append(self.generateEntry(diagrams[i],matchedPropagators[i], matchedVertices[i]))
            self.vprint(" Generate dia entries: Done                  ")
        # generate entries in parallel
        else:
            # initializer function for each worker in the multithread pool
            def initializer():
                # ignore CTRL+C in the worker process
                signal.signal(signal.SIGINT, signal.SIG_IGN)

            if self.kernels == 0:
                pool = mp.Pool(mp.cpu_count(),initializer=initializer)
            else:
                pool = mp.Pool(self.kernels,initializer=initializer)
            
            # start asynchronous workers
            result = pool.starmap_async(self.generateEntry, [[diagrams[i],matchedPropagators[i], matchedVertices[i]] for i in range(len(diagrams))], chunksize=1)

            # print progress with actualization rate of 10 Hz
            while not result.ready():
                self.vprint(" Generate dia entries: {0:^7}/{1:^7}\r".format(len(diagrams)-result._number_left, len(diagrams)), end = "")
                time.sleep(0.1)
            self.vprint(" Generate dia entries: Done                  ")
            pool.close()
            pool.join()

            # add result to the entry list
            self.entryList += result.get()

        return diagrams


    ## Match vertex and propagator Feynman rules to the given diagrams.
    # The rules for the matching are given by #feynmanRules.
    # We basically iterate over every diagram and look for appropriate feynman rules steming from .prop and .vrtx files.
    # For the special case that two rules have the same particle content, we extend the diagram list by each rule combination inserted.
    # 
    # @param diagrams List of modules.diagram.Diagram instances we want to find matching Feynman rules for
    # \return - List of diagrams with potentially more content due to overlapping Feynman rules
    #         - Dict with propagator numbers as keys and (particleNamesTuple, ruleIndex) keys of one of modules.feynmanRules.FeynmanRules propagator dicts as values
    #         - Dict with vertex numbers as keys and (particleNamesTuple, ruleIndex) tuples of the matching rule as values
    def matchRulesAndExtendDiagrams(self, diagrams):
        # list of dicts describing for each diagram which propagator number is matched to a Feynman rule (by the index of FeynmanRules.propList)
        matchedPropagators = []
        # list of dicts describing for each diagram which vertex number is matched to a Feynman rule (by the index of FeynmanRules.vrtxList)
        matchedVertices = []
        # in case we find more than one rule for a propagator or a vertex, we add new diagrams covering all possibilities
        actualizedDiagrams = []

        # check the rules for every line and vertex of each diagram and find a proper mapping
        for diagram in diagrams:
            diagramMatchedPropagators = {}
            diagramMatchedVertices = {}

            # match rules for propagators
            for line in diagram.topologicalLineList:
                # we're only interested in internal lines
                if not line.external:
                    diagramPropKeys = self.feynmanRules.getPropRuleKeys(line)
                    # no matching propagator rule found
                    if len(diagramPropKeys) == 0:
                        logging.error(F"Diagram {diagram.diagramNumber}: No Feynman rule for propagator: "+line.incomingParticle+" "+line.outgoingParticle)
                        exit(1)
                    else:
                        diagramMatchedPropagators[line.number] = diagramPropKeys

            # match rules for vertices
            for vertex in diagram.topologicalVertexList:
                diagramVertexKeys = self.feynmanRules.getVertexRuleKeys(vertex)
                # no matching vertex rule found
                if len(diagramVertexKeys) == 0:
                    logging.error(F"Diagram {diagram.diagramNumber}: No Feynman rule for vertex: "+str(vertex.getOutgoingParticles()))
                    exit(1)
                else:
                    diagramMatchedVertices[vertex.number] = diagramVertexKeys

            # if now multiple propagator or vertex rules are found, find all possibilities
            # propagators
            singlePropRules = {propI: ruleKeys[0] for propI,ruleKeys in diagramMatchedPropagators.items() if len(ruleKeys)==1}
            multiplePropRules = [[{propI: num} for num in ruleKeys] for propI,ruleKeys in diagramMatchedPropagators.items() if len(ruleKeys)>1]
            # all combinations
            multiplePropRules = list(it.product(*multiplePropRules))
            # extend this list with all combinations of propagators
            propRuleCombinations = []
            if len(multiplePropRules) == 0:
                propRuleCombinations = [singlePropRules]
            else:
                for propRules in multiplePropRules:
                    newCombination = singlePropRules.copy()
                    for rule in propRules:
                        newCombination.update(rule)
                    propRuleCombinations.append(newCombination)

            # vertices
            singleVrtxRules = {vrtxI: ruleKeys[0] for vrtxI,ruleKeys in diagramMatchedVertices.items() if len(ruleKeys)==1}
            multipleVrtxRules = [[{vrtxI: num} for num in ruleKeys] for vrtxI,ruleKeys in diagramMatchedVertices.items() if len(ruleKeys)>1]
            # all combinations
            multipleVrtxRules = list(it.product(*multipleVrtxRules))
            # extend this list with all combinations of vertices
            vrtxRuleCombinations = []
            if len(multipleVrtxRules) == 0:
                vrtxRuleCombinations = [singleVrtxRules]
            else:
                for vrtxRules in multipleVrtxRules:
                    newCombination = singleVrtxRules.copy()
                    for rule in vrtxRules:
                        newCombination.update(rule)
                    vrtxRuleCombinations.append(newCombination)

            # fill the methods return variables
            for propCombination in propRuleCombinations:
                for vrtxCombination in vrtxRuleCombinations:
                    matchedPropagators.append(propCombination)
                    matchedVertices.append(vrtxCombination)
                    # we have to copy the diagrams since the diagram numbering is changed afterwards
                    actualizedDiagrams.append(diagram)

        return actualizedDiagrams, matchedPropagators, matchedVertices


    ## Generate a single entry for #entryList
    # @param self The object pointer.
    # @param diagram A modules.diagram.Diagram objects.
    # @param matchedPropagators Dict with propagator numbers as keys and (particleNamesTuple, ruleIndex) keys of one of modules.feynmanRules.FeynmanRules propagator dicts as values. Returned by #matchRulesAndExtendDiagrams()
    # @param matchedVertices Dict with vertex numbers as keys and (particleNamesTuple, ruleIndex) tuples of the matching rule as values. Returned by #matchRulesAndExtendDiagrams()
    #  \return String Corresponding to the entry of a .dia file
    def generateEntry(self, diagram, matchedPropagators, matchedVertices):
        entry =  self.makeFold("DIA", diagram, matchedPropagators, matchedVertices) + "\n"
        entry += self.makeFold("QCD", diagram, matchedPropagators, matchedVertices) + "\n"
        entry += self.makeFold("QED", diagram, matchedPropagators, matchedVertices) + "\n" 
        entry += self.makeFold("EW", diagram, matchedPropagators, matchedVertices) + "\n"
        return entry


    ## Generates a fold for one diagram.
    # @param self The object pointer.
    # @param fold The fold to be generated, options are "DIA", "QCD", "QED", "EW"
    # @param diagram The diagram for which to generate the code 
    # @param matchedPropagators Dict with propagator numbers as keys and (particleNamesTuple, ruleIndex) keys of one of modules.feynmanRules.FeynmanRules propagator dicts as values. Returned by #matchRulesAndExtendDiagrams()
    # @param matchedVertices Dict with vertex numbers as keys and (particleNamesTuple, ruleIndex) tuples of the matching rule as values. Returned by #matchRulesAndExtendDiagrams()
    #
    #  First the proper Feynman rules are picked, depending on the fold. Afterwards
    #  we write the code for the fermion lines, like it is done in q2e. Followed by
    #  non-fermionic propagators and then the non-fermionic vertices.
    #  Note that the special code is necessary            
    def makeFold(self, fold, diagram, matchedPropagators, matchedVertices):
        diaName = "d" + str(diagram.numLoops) + "l" + str(diagram.diagramNumber)
        # number of independent momenta
        foldStr = "*--#[ "
        if fold == "QCD":
            foldName = "fqcd" + re.sub(r'^d', r'', diaName)
            lineBreak = "\n\t"
            foldStr += foldName + " :\n\n\t1" + lineBreak
        elif fold == "QED":
            foldName = "fqed" + re.sub(r'^d', r'', diaName)
            lineBreak = ""
            foldStr += foldName + " :\n\t1" + lineBreak
        elif fold == "EW":
            foldName = "few" + re.sub(r'^d', r'', diaName)
            lineBreak = ""
            foldStr += foldName + " :\n\t1" + lineBreak
        elif fold == "DIA":
            foldName = diaName
            lineBreak = "\n\t"
            fermionLabels = ""
            # look which fermion loops are present, at the corresponding labels (nl,nh,...)
            if not self.config.spinorIndices:
                for fl in diagram.fermionLines:
                    for f in fl.flavours:
                        if f in self.config.closedFermionLoop:
                            fermionLabels += ("*" + self.config.closedFermionLoop[f])
            # Finally, check whether any vertices or propagators should receive a custom prefactor
            # string as per the * tapir.prefactor configuration option
            matchedPropStringList = [",".join(matchedPropagators[k][0]) for k in matchedPropagators]
            matchedVertexStringList = [",".join(matchedVertices[k][0]) for k in matchedVertices]
            customPrefactor = ""
            customPrefactor += "".join([str("*") + self.config.prefactorStrings[p] if p in self.config.prefactorStrings else "" for p in matchedPropStringList])
            customPrefactor += "".join([str("*") + self.config.prefactorStrings[v] if v in self.config.prefactorStrings else "" for v in matchedVertexStringList])
            if customPrefactor != "":
                customPrefactor = "\n\t" + customPrefactor

            # Now assemble the string for the DIA fold
            foldStr += foldName + " :\n\n\t" + str(diagram.preFactor) + customPrefactor + fermionLabels + lineBreak

        # Process fermion lines one by one
        if not self.config.spinorIndices:
            numFermionLine = 0
            for fl in diagram.fermionLines:
                numFermionLine += 1
                fVertList = []
                fPropList = []

                # Read fermion lines in the order of fl.vertices if they are flipped or external.
                # If they are non-flipped loops, process them from last to first item.
                if fl.isFlipped == True:
                    orderedVertices = fl.vertices
                elif fl.isFlipped == False and fl.loop == False:
                    orderedVertices = fl.vertices
                else:
                    orderedVertices = fl.vertices[::-1]

                # First, process all vertices of the fermion Line
                for v in orderedVertices:
                    # Skip apparent tadpole vertices (they may actually be connected to an (cut) external line)
                    if len(v.lines) == 1:
                        continue
                    # Find appropriate feynman rule
                    m = self.feynmanRules.matchVertexByRuleKey(v,len(diagram.externalMomenta),matchedVertices[v.number])
                    fVertList.append(self.feynmanRules.returnFORMStringVrtx(m, matchedVertices[v.number], fold, v.number, len(diagram.externalMomenta), True, numFermionLine))

                # Read fermion loops clockwise if flipped, see comment for fermion vertices.
                if fl.isFlipped == True:
                    orderedLines = fl.lines
                elif fl.isFlipped == False and fl.loop == False:
                    orderedLines = fl.lines
                else:
                    orderedLines = fl.lines[::-1]

                for l in orderedLines: 
                    # Only take propagators with positive line momentum to avoid double counting
                    # check if there is a match
                    p = self.feynmanRules.matchPropByRuleKey(l,len(diagram.externalMomenta),matchedPropagators[l.number])
                    fPropList.append(self.feynmanRules.returnFORMStringProp(p, matchedPropagators[l.number], fold, l.number, l.incomingParticle, l.outgoingParticle, True, numFermionLine))

                # This is where the magic happens.
                lenString = min(len(fVertList),len(fPropList))
                resultString = [None]*(lenString*2)
                # If we have an unflipped loop, we start with the first propagator in the list, followed by a vertex.
                # To match q2e's ordering, we than have to shift the chain, such that we start with the last vertex,
                # followed by the first propagator, then the first vertex and so on.
                if fl.isFlipped == False and fl.loop == True:
                    resultString[1::2] = fVertList[:lenString]
                    resultString[0::2] = fPropList[:lenString]
                    resultString.extend(fVertList[lenString:])
                    resultString.extend(fPropList[lenString:])
                    # Rotate to match q2e
                    resultStringRot = resultString[-1:]+resultString[:-1]
                # If we have a flipped loop, we start with the first vertex in the list, followed by a propagator.
                # To match q2e's ordering, we than have to shift the chain, such that we start with the last vertex,
                # followed by the last propagator, then the first vertex and so on.
                elif fl.isFlipped == True and fl.loop == True:
                    resultString[0::2] = fVertList[:lenString]
                    resultString[1::2] = fPropList[:lenString]
                    resultString.extend(fVertList[lenString:])
                    resultString.extend(fPropList[lenString:])
                    # Rotate to match q2e
                    resultStringRot = resultString[2:]+resultString[:2]
                # If we have a fermion line, we start with the first vertex followed by a propagator etc.
                elif fl.loop == False:
                    resultString[0::2] = fVertList[:lenString]
                    resultString[1::2] = fPropList[:lenString]
                    resultString.extend(fVertList[lenString:])
                    resultString.extend(fPropList[lenString:])
                    resultStringRot = resultString

                for s in resultStringRot:
                    foldStr += s

        # Now that we are done with fermions, we can proceed with the "regular" propagators.
        propList = []
        for v in diagram.topologicalVertexList:
            for l in v.lines:
                # Only take propagators with positive line momentum to avoid double counting
                if (l.external == False and (l.fermionLine == False or self.config.spinorIndices == True) and l.momentum[0] != "-"):
                    p = self.feynmanRules.matchPropByRuleKey(l,len(diagram.externalMomenta),matchedPropagators[l.number])
                    propList.append([int(l.momentum[1::]),self.feynmanRules.returnFORMStringProp(p, matchedPropagators[l.number], fold, l.number, l.incomingParticle, l.outgoingParticle, False)])

        propList.sort(key=lambda entry: entry[0])
        for p in propList:
            foldStr += p[1]

        # Now the remaining vertices
        for v in diagram.topologicalVertexList:
            # Skip fermions, since they are already done in case of q2e mode.
            if v.fermionic == True and self.config.spinorIndices == False:
                continue
            # Skip apparent tadpole vertices (they may actually be connected to an (cut) external line)
            if len(v.lines) == 1:
                continue
            # Find appropriate feynman rule
            m = self.feynmanRules.matchVertexByRuleKey(v,len(diagram.externalMomenta),matchedVertices[v.number])
            foldStr += self.feynmanRules.returnFORMStringVrtx(m, matchedVertices[v.number], fold, v.number, len(diagram.externalMomenta), False)
                        
        # The QCD fold needs a ';' at the end, while the DIA fold needs a generic placeholder for defining the topology
        if fold == "QCD":
            foldStr += ";\n"
        elif fold == "DIA":
            foldStr += ";\n\n" + "\t#define TOPOLOGY \"arb\"\n" + "\t#define INT1 \"arb\"\n"    
            # Add comment 
            if diagram.diaComment != "":
                foldStr += "\n*\t" + diagram.diaComment + "\n"
        foldStr += "\n*--#] " + foldName + " :\n"
        return foldStr
