## @package diagramFilter
#
# The here described class modules.DiagramFilter.diagramFilter is used to extract diagrams with specific topological features due to the options set in modules.config.Conf::filter.
# Hence, all filters defined here are non-destructive, i.e. they do not change the topologies itself but rather just removes the diagrams that do not match the filter conditions.

import copy
from typing import List
from modules.diagram import Diagram
from modules.filter import Filter
from modules.topoAnalyzer import BridgeFinder, TopologyAnalyzer
from modules.feynmanRulesReader import FeynmanRulesReader
from modules.sigmaLineFilter import SigmaLineFilter
from modules.cutFilter import CutFilter
from modules.vertexFilter import VertexFilter
from modules.momentumAssigner import MomentumAssigner
from modules.partialFractionDecomposer import AnalyticTopoMapper
import itertools as it
import logging 
import re


## This class is used to #filter() diagrams with specific topological attributes.
# To make use of it, specify 
# ```
# * tapir.filter [attribute] : [true/t/1/false/f/0] {: [filter arguments]... }
# ```
#
# Here, true and false means that either all diagrams matching to the filter criterion are kept (true) or discarded (false).
class DiagramFilter(Filter):
    ## Constructor
    # @param conf modules.config.Conf instance where modules.config.Conf::filter is (possibly) specified
    def __init__(self, conf, verbose=False, kernels=1):
        ## Class reference of the modules.config.Conf instance
        self.conf = conf
        ## verbose flag to print progress during partial fraction decomposition.
        self.verbose = verbose
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels


    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)


    ## Basic pipe method that gives a copy of an input list of modules.diagram.Diagram instances with their topological structure adjusted to the specifications of #filters.
    # Each filter is applied on the result of the previous filtering. Thus the ordering may be relevant for performance or destructive diagram manipulations.
    # The filtering is defined here as non-destructive, i.e. The diagrams itselves are not changed, only removed from the input list.
    # An exception is the diagram name. It is changed to have a continiously increasing name index.
    #
    # \return A copy of \p diagrams where the specified filters are applied
    def filter(self, diagrams: List[Diagram]) -> List[Diagram]:
        if self.conf.filters:
            self.vprint(F" Apply filters to {len(diagrams)} diagrams:")
        # apply each filter on the result of the previous filtering
        # the filters must be non-destructive to avoid unwanted behavior
        for filter in self.conf.filters:
            # self_energy_bridge_mixing
            if filter[0] == "self_energy_bridge_mixing":
                diaIndices = self.selfEnergyBridgeMixFilter(diagrams, mixing=True, onlyExternalLines=False)
                if filter[1] in ["true", "t", "1"]:
                    diagrams = [d for i,d in enumerate(copy.deepcopy(diagrams)) if i in diaIndices]
                elif filter[1] in ["false", "f", "0"]:
                    diagrams = [d for i,d in enumerate(copy.deepcopy(diagrams)) if i not in diaIndices]

            # self_energy_bridge
            elif filter[0] == "self_energy_bridge":
                diaIndices = self.selfEnergyBridgeMixFilter(diagrams, mixing=False, onlyExternalLines=False)
                if filter[1] in ["true", "t", "1"]:
                    diagrams = [d for i,d in enumerate(copy.deepcopy(diagrams)) if i in diaIndices]
                elif filter[1] in ["false", "f", "0"]:
                    diagrams = [d for i,d in enumerate(copy.deepcopy(diagrams)) if i not in diaIndices]

            # external_self_energy_bridge_mixing
            elif filter[0] == "external_self_energy_bridge_mixing":
                diaIndices = self.selfEnergyBridgeMixFilter(diagrams, mixing=True, onlyExternalLines=True)
                if filter[1] in ["true", "t", "1"]:
                    diagrams = [d for i,d in enumerate(copy.deepcopy(diagrams)) if i in diaIndices]
                elif filter[1] in ["false", "f", "0"]:
                    diagrams = [d for i,d in enumerate(copy.deepcopy(diagrams)) if i not in diaIndices]

            # external_self_energy_bridge
            elif filter[0] == "external_self_energy_bridge":
                diaIndices = self.selfEnergyBridgeMixFilter(diagrams, mixing=False, onlyExternalLines=True)
                if filter[1] in ["true", "t", "1"]:
                    diagrams = [d for i,d in enumerate(copy.deepcopy(diagrams)) if i in diaIndices]
                elif filter[1] in ["false", "f", "0"]:
                    diagrams = [d for i,d in enumerate(copy.deepcopy(diagrams)) if i not in diaIndices]

            # cut filter
            elif filter[0] == "cuts":
                diagrams = CutFilter(self.conf, verbose=self.verbose, kernels=self.kernels).filter(diagrams, filter[1:])

            # vertex filter
            elif filter[0] == "vertex":
                diagrams = VertexFilter(self.conf, verbose=self.verbose, kernels=self.kernels).filter(diagrams, filter[1:])

            # scaleless diagram filter
            elif filter[0] == "scaleless":
                if self.conf.sigmaParticles != []:
                    diagramscopy = copy.deepcopy(diagrams)
                    diagramscopy = SigmaLineFilter(self.conf, False, self.verbose, self.kernels).filter(diagramscopy)
                    diaIndices = self.scalelessDiagramFilter(diagramscopy)
                else:
                    diaIndices = self.scalelessDiagramFilter(diagrams)
                
                if filter[1] in ["true", "t", "1"]:
                    diagrams = [d for i,d in enumerate(copy.deepcopy(diagrams)) if i in diaIndices]
                elif filter[1] in ["false", "f", "0"]:
                    diagrams = [d for i,d in enumerate(copy.deepcopy(diagrams)) if i not in diaIndices]

            # Custom filters may be implemented here the same way:
            # elif filter[0] == "my_filter":
            #     diaIndices = self.myFilterFunction(diagrams)
            #     if filter[1] in ["true", "t", "1"]:
            #         diagrams = [d for i,d in enumerate(copy.deepcopy(diagrams)) if i in diaIndices]
            #     elif filter[1] in ["false", "f", "0"]:
            #         diagrams = [d for i,d in enumerate(copy.deepcopy(diagrams)) if i not in diaIndices]
                    
            # exit when filter is not known
            else:
                self.vprint("")
                logging.error(F"Unknown filter option \"{filter[0]}\"")
                exit(1)

            self.vprint(F"  > {filter[0]}: {len(diagrams)} diagrams left")
            
            # rename the diagrams to have an continiously increasing index
            self.renameDiagrams(diagrams)
        
        return diagrams


    # Define your custom filter function
    # def myFilterFunction(self, diagrams: List[Diagram]) -> List[int]:
    #     matchingDiagramIndices = []
    #     for i, diagram in diagrams:
    #         if ... :
    #             matchingDiagramIndices.append(i)
    #     return matchingDiagramIndices


    ## Rename all diagrams such that exp can handle them properly
    # The names of the diagrams must be e.g. d1l1, d1l2,..., d1l100 without missing ones
    # @param diagrams List of diagram.Diagram's to rename
    def renameDiagrams(self, diagrams):
        for i,diagram in enumerate(diagrams):
            diagram.diagramNumber = i+1

            if self.conf.topologyName.isspace():
                diagram.setName()
            else:
                diagram.setName(self.conf.topologyName)


    ## Match diagrams with bridges (no loop associated lines) that corresponds to different particles on external lines.
    # @param diagrams The diagram.Diagram list on which the filtering applies
    # This method makes use of the .prop file to ensure correct propagator behavior
    #
    # \return set of indices of \p diagrams, that match the filter condition (starting with 0!)
    def selfEnergyBridgeMixFilter(self, diagrams, mixing, onlyExternalLines):
        # read the prop file to 
        feynmanRulesReader = FeynmanRulesReader(self.conf)
        feynmanRules = feynmanRulesReader.returnRules()
        propList = feynmanRules.propLorentz.keys()
        matchingDiaIndices = set()

        for index, diagram in enumerate(diagrams):
            # copy lines of the diagram
            lines = copy.deepcopy(diagram.topologicalLineList)
            # get all bridges of the diagram
            bridges = BridgeFinder().findBridges(lines)

            # collapse non-bridge lines in the topology to a point to have a tree graph only
            for line in lines:
                if line.number in bridges or line.external:
                    continue
                # replace the end point of the current line with its startpoint in the complete lines list
                s = line.start
                e = line.end
                for l in lines:
                    if l.start == e:
                        l.start = s
                    if l.end == e:
                        l.end = s

            # remove the collapsed lines
            lines = [l for l in lines if l.start != l.end]
                        
            # now find all remaining vertices
            verticesLeft = set()
            for line in lines:
                if line.number in bridges or line.external:
                    verticesLeft.add(line.start)
                    verticesLeft.add(line.end)

            
            # Iterate over all remaining vertices and match adjacent line pairs
            # Those pairs that leave the remaining adjacent lines to the actual vertex unconnected to external points, are of interest or not (depending on onlyExternalLines).
            # This means, by removing a remaining adjacent line, the bunch of connected vertices that remains thereafter must (not) contain an external point.
            for vertex in verticesLeft:
                linesAttached = [l for l in lines if l.start == vertex or l.end == vertex]

                # take out always two lines
                for linePair in it.combinations(linesAttached,2):
                    # take only external lines for mixing/non-mixing into account
                    if onlyExternalLines:
                        if not (linePair[0].external or linePair[1].external):
                            continue

                    pair1PI = True
                    # remaining attached lines
                    for remainderLine in [l for l in linesAttached if l.number not in [linePair[0].number, linePair[1].number]]:
                        # go on when the line is external, because then the pair cannot be 1PR whatsoever
                        if remainderLine.external:
                            pair1PI = False
                            break
                        # check if an external vertex (vertex number < 0 (=0 in tests)) is in the same partition as the second vertex of remainderLine
                        partitions = self.lineCutTreePartitioning(lines, remainderLine)
                        for p in partitions:
                            if remainderLine.to(vertex) in p and min(p) <= 0:
                                pair1PI = False
                                break
                    # check if the line pair are the external edges of a 1PI subgraph, and if the flavor is changed between them
                    if pair1PI:
                        # extract standard particle/antiparticle propagation from propagator file
                        particleNumbers1 = [i for i,prop in enumerate(propList) if (linePair[0].incomingParticle in prop) or (linePair[0].outgoingParticle in prop)]
                        particleNumbers2 = [i for i,prop in enumerate(propList) if (linePair[1].incomingParticle in prop) or (linePair[1].outgoingParticle in prop)]

                        # make sure the needed particles have a propagator defined
                        if len(particleNumbers1) == 0:
                            logging.error(F"No propagator for particle {linePair[0].incomingParticle} ({linePair[0].outgoingParticle}) in prop file! Cannot apply self_energy_bridge/self_energy_bridge_mixing filter without it.")
                            exit(1)
                        if len(particleNumbers2) == 0:
                            logging.error(F"No propagator for particle {linePair[1].incomingParticle} ({linePair[1].outgoingParticle}) in prop file! Cannot apply self_energy_bridge/self_energy_bridge_mixing filter without it.")
                            exit(1)

                        # save the diagram whether mixing or non-mixing applies
                        if (mixing and particleNumbers1[0] != particleNumbers2[0]) or (not mixing and particleNumbers1[0] == particleNumbers2[0]):
                            matchingDiaIndices.add(index)
                            break

        return matchingDiaIndices
    
    ## Match diagrams with only scaleless integrals.
    # @param diagrams The diagram.Diagram list on which the filtering applies
    # This method makes use of the .prop file to ensure correct propagator behavior
    #
    # \return set of indices of \p diagrams, that match the filter condition (starting with 0!)
    def scalelessDiagramFilter(self, diagrams: List[Diagram]) -> List[int]:
        matchingDiagramIndices = []
        for i, diagram in enumerate(diagrams):
            assigner = MomentumAssigner(self.conf,diagram)
            assigner.generateLineMassesDict()
            assigner.calculateMomentumRelations()
            U, F = AnalyticTopoMapper.UF(assigner.loopMomenta,assigner.analyticTopologyExpression[:len(diagram.internalMomenta)-len(assigner.equalMomentumDict)])      
            if TopologyAnalyzer.isScaleless(U,F,assigner.analyticTopologyExpression[:len(diagram.internalMomenta)-len(assigner.equalMomentumDict)]):
                matchingDiagramIndices.append(i)
        return matchingDiagramIndices                

    ## Split a tree graph in two partitions by removing a single bridge
    # @param allLines List of diagram.Line objects corresponding to a tree graph
    # @param cutLine diagram.Line object which will be cut from \p allLines
    # \return partitions: List of sets of vertex numbers that correspond to each end of the cut line
    def lineCutTreePartitioning(self, allLines, cutLine):
        # start with a single partition for each line, except the the line
        partitions = [set([l.start, l.end]) for l in allLines if l.number != cutLine.number]
        # get a set of vertex numbers
        vertexNumbers = set([l.start for l in allLines] + [l.end for l in allLines])

        # update partitions for each vertex 
        for i in vertexNumbers:
            # the new partition is a combination of all partitions containing the vertex number i
            newPartition = set()
            # the remainder of the above partitions
            remainder = []
            # now update newPartition or fill the remainder
            for partition in partitions:
                if i in partition:
                    newPartition.update(partition)
                else:
                    remainder.append(partition)

            # update partitions with the remainder and the new,larger partition
            partitions = remainder + [newPartition]

        return partitions 




        





