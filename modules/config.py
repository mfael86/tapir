## @package config
# This module parses q2e/tapir configuration files.
#
# The main class is modules.config.Conf


import logging
import re
import os
import yaml
from modules.outputHelpers import flatten

## The Conf class is capable of reading q2e/tapir config files and storing the relevant information.
#
# It is designed to be backwards compatible, meaning if the -q2e switch is not used,
# it still can read old q2e style config files but also accepted newer tapir options.
# If -q2e is set, it ignores tapir options and will not complain.
class Conf:
    ## The constructor, initializes all member and reads in config file.
    #  @param self The object pointer.
    #  @param confPath Path to the configuration file.
    #  @param argsObj Object returned by parser_args()
    #  @param oldq2e If True, we are operating in q2e compatibility mode.
    #  @param onlySkeleton If True no config file is read and one gets only a bare config instance
    def __init__(self, confPath, argsObj = None, oldq2e = False, onlySkeleton = False):
        ## Path to the configuration file.
        self.confFile = confPath
        ## Path to the file containing the Feynman rules for propagators.
        self.propFile = ""
        ## Path to the file containing the Feynman rules for vertices.
        self.vrtxFile = ""
        ## Ordered list containing the relevant scales in the problem.
        self.scales = []
        ## Ordered list containing the scales in which we want to perform a naive Taylor expansion.
        self.expandNaive = []
        ## Dictionary containing all massive fields and the respective masses.
        # Example: <code>{"h": "M1", "ft1": "M2"}</code>
        self.mass = {}
        ## Dictionary containing fermion fields whose closed loops should be labeled and the respective label.
        # Example: `{"fq": "nl*3"}`
        self.closedFermionLoop = {}
        ## Dictionary containing fermion fields and the corresponding anti fermions, unused in tapir, since we recognize this differently.
        # Example: <code>{"f1" : "f1bar", "ft" : "fT"}</code>
        self.antiFermion = {}
        ## Tells tapir to split .dia and .edia files into blocks of size blockSize, such that they can be processed independently.
        # The default value of "0" does not lead to a splitting.
        self.blockSize = 0
        ## A dict containing momenta to be set to different values.
        #  This is usefull for specifying forward scattering kinematics or similar things.
        #  Not present in q2e.
        # Example: <code>{"q1" : "q", "q3": "-q", "q2": "qSomething"}</code>
        self.externalMomenta = {}
        ## If True, we are operating in q2e compatibility mode.
        self.q2e = oldq2e
        ## If True, we use spinor indices instead of anti-commuting functions for fermionic propagators and vertices.
        self.spinorIndices = not self.q2e
        ## If True, we use \f$(M^2 + P^2)^{-1}\f$ for propagators instead of \f$(M^2 - p^2)^{-1}\f$
        self.euclideanMomenta = False
        ## Define how scalar topologies are named
        # E.g. `tapir.topology_name INT1` results for two-loop topologies in `INT12l1`,...,`INT12lN`
        # 
        # See modules.topoFileGenerator.TopoFileGenerator
        self.topologyName = ""
        ## A dict that explains how specific particles are drawn in the sense of tikz-feynman particles.
        # E.g. `{"fq" : "fermion", "A": "photon", "g": "gluon", "c" : "ghost"}`. 
        # The allowed particle names are given in #allowedParticles.
        self.drawingObjects = {}
        ## These are the allowed particle names of #drawingObjects 
        self.allowedParticles = ["fermion", "gluon", "photon", "scalar",
                                         "boson", "charged boson", "charged scalar", "ghost", "majorana"]
        ## Define the name of a particle (in TeX form) that is shown in drawings.
        # E.g. `{"fq" : "$q_1$", "A": "$\gamma$", "g": "g"}`
        self.drawingNames = {}
        ## Apply additional filter to the qgraf generated diagrams
        # The structure is [["filtername", "true/t/1/false/f/0", ...]`
        self.filters = []
        ## Add the possibility to have a .sort statement after every id statement in the generated topology files
        self.topoExtraSort = False
        ## Directory in which UFO model files are stored
        try:
            self.ufo_indir = argsObj.ufo_indir if argsObj.ufo_indir != None else ""
        except:
            self.ufo_indir = ""
        ## Directory in which Feynman rules files should be stored
        try:
            self.ufo_outdir = argsObj.ufo_outdir if argsObj.ufo_outdir != None else ""
        except:
            self.ufo_outdir = ""
        ## If True, include a tag of the fermion flavour in the fermion propagators extracted from the UFOReader.
        self.ufoTagFermionFlavour = False
        ## Dictionary to hold information on gauge parameters
        self.gaugeDict = {}
        ## Dictionary to hold information on gauge boson propagators
        self.gaugeProps = {}
        ## Add the option to specify a function, e.g. ACCU, to call instead of sorting in the topology files
        self.topoSortFunc = ""
        ## Ignore bridges in the generated topology files and their corresponding topsel entry in the topsel generation
        self.topoIgnoreBridges = False
        ## Also include all combinations where massive lines are absent in the generation of the topsel file 
        self.topoAllowAbsentMassiveLines = False
        ## Remove lines of in topologies carrying the same momentum and mass
        self.topoRemoveDuplicateLines = False
        ## Add to every massive propagator line a massless pendent in the the analytic topology files
        self.extendAnalyticalTopos = False
        ## Replace numerator momentum products by real propagators in the topology files
        self.topoCompleteMomentumProducts = False
        ## Names of sigma particles, that do not carry momenta and can thus be ignored in topologies
        self.sigmaParticles = []
        ## Assume eikonal propagators with internal times internal momenta in the topologies
        self.topoEikonalInternal = False
        ## Assume eikonal propagators with internal times external momenta in the topologies
        # The first list entry says if eikonal propagators are regarded.
        # Further list entries specify which external momenta shall be used in the eikonal propagators.
        # No further list entries, on the other hand, mean to take all external momenta of the problem.
        self.topoEikonalExternal = [False]
        ## Define replacements of external momentum products
        self.kinematicRelations = {}
        ## Output the line momentum mapping after minimization to a dedicated FORM readable file
        self.mappingFile = ""
        ## Output the line momentum mapping after minimization to a YAML file
        self.yamlMappingFile = ""
        ## Switch for compatibility with exp, specifically the wrapping of external momenta in L,-1, qX, -1, L structures when inserting Feynman Rules
        self.expCompat = True
        ## Only write numerator momentum replacement rules into topology files
        self.numrepOnly = False
        ## Dictionary of prefactors per vertex or propagator (can be used e.g. to tag certain vertices)
        self.prefactorStrings = dict()

        if not onlySkeleton:
            if self.confFile.split(".")[-1].lower() in ["yml", "yaml"]:
                self.readYAML(argsObj)
            else:
                self.read(argsObj)


    ## Reads the configuration file.
    #  @param self The object pointer.
    #  @param argsObj Object returned by parser_args()
    #
    #  If the file specified in #confFile is not readable, an error is printed and the program terminates.
    #  Should an option be set twice a DuplicateOptionError is raised, the duplicate option is printed
    #  and the program terminates.
    #  In case an unknown option is encountered an UnknownOptionError is raised and a warning printed.
    def read(self, argsObj):
        # try to open the config file
        try:
            confHandle = open(self.confFile, 'r')
        except FileNotFoundError:
            logging.error("Configuration file not found!")
            exit(1)
                
        # read every line of config file
        for i,line in enumerate(confHandle):
            try:
                # Valid options always start with a * at the beginning of the line, followed by whitespace and the option as well as arguments
                match = re.match(r'^\*\s+(\S+)\s*(?:\s*(.*\S)\s*|\s*)$', line)
                if match:
                    if match.group(1) == "q2e.propagator_file" or (match.group(1) == "tapir.propagator_file" and not self.q2e):
                        assert match.lastindex == 2, "Empty Propagator file option"
                        assert self.propFile == "", "Duplicate option: " + match.group(1)
                        # check for full path name, if not use relative one
                        if match.group(2)[0] == "/":
                            self.propFile = match.group(2)
                        else:
                            self.propFile = os.path.join(os.path.dirname(self.confFile), match.group(2))

                    elif match.group(1) == "q2e.vertex_file" or (match.group(1) == "tapir.vertex_file" and not self.q2e):
                        assert match.lastindex == 2, "Empty Vertex file option"
                        assert self.vrtxFile == "", "Duplicate option: " + match.group(1)
                        # check for full path name, if not use relative one
                        if match.group(2)[0] == "/":
                            self.vrtxFile = match.group(2)
                        else:
                            self.vrtxFile = os.path.join(os.path.dirname(self.confFile), match.group(2))

                    elif match.group(1) == "q2e.block_size" or (match.group(1) == "tapir.block_size" and not self.q2e):
                        assert match.lastindex == 2, "Empty block size option"
                        assert self.blockSize == 0, "Duplicate option: " + match.group(1)
                        assert isinstance(self.blockSize, int), "Non-numeric input for" + match.group(1)
                        self.blockSize = int(match.group(2))

                    elif match.group(1) == "q2e.scales" or (match.group(1) == "tapir.scales" and not self.q2e):
                        assert match.lastindex > 1, "Empty scales option"
                        assert self.scales == [], "Duplicate option: " + match.group(1)
                        self.scales = [re.sub(r'\s', r'', s) for s in match.group(2).split(',')]

                    elif match.group(1) == "q2e.expand_naive" or (match.group(1) == "tapir.expand_naive" and not self.q2e):
                        assert match.lastindex > 1, "Empty scales option for naive expansion"
                        assert self.expandNaive == [], "Duplicate option: " + match.group(1)
                        self.expandNaive = [re.sub(r'\s', r'', s) for s in match.group(2).split(',')]

                    elif match.group(1) == "q2e.mass" or (match.group(1) == "tapir.mass" and not self.q2e):
                        assert match.lastindex == 2, "Empty particle mass option"
                        particle = [re.sub(r'\s', r'', s) for s in match.group(2).split(':')]
                        assert particle[0] not in self.mass, "Duplicate option: " + match.group(1)+" "+match.group(2)
                        self.mass[particle[0]] = particle[1]

                    elif match.group(1) == "q2e.closed_fermion_loop" or (match.group(1) == "tapir.closed_fermion_loop" and not self.q2e):
                        assert match.lastindex == 2, "Empty closed fermion loop option"
                        particle = [re.sub(r'\s', r'', s) for s in match.group(2).split(':')]
                        assert particle[0] not in self.closedFermionLoop, "Duplicate option: " + match.group(1)+" "+match.group(2)
                        self.closedFermionLoop[particle[0]] = particle[1]

                    elif match.group(1) == "q2e.anti_fermion" or (match.group(1) == "tapir.anti_fermion" and not self.q2e):
                        assert match.lastindex == 2, "Empty anti-fermion option"
                        particle = [re.sub(r'\s', r'', s) for s in match.group(2).split(':')]
                        assert particle[0] not in self.antiFermion, "Duplicate option: " + match.group(1)+" "+match.group(2)
                        self.antiFermion[particle[0]] = particle[1]

                    elif match.group(1) == "tapir.external_momentum" and not self.q2e:
                        assert match.lastindex == 2, "Empty momentum replacements option"
                        momenta =[re.sub(r'\s', r'', s) for s in match.group(2).split(':')]
                        assert momenta[0] not in self.externalMomenta, "Duplicate option: " + match.group(1)+" "+match.group(2)
                        self.externalMomenta[momenta[0]] = momenta[1]

                    elif match.group(1) == "tapir.kinematic_relation" and not self.q2e:
                        assert match.lastindex == 2, "Empty kinematic relation option"
                        replacement = [re.sub(r'\s', r'', s) for s in match.group(2).split(':')]
                        assert re.match(r'^[a-zA-Z0-9]+\.[a-zA-Z0-9]+$', replacement[0]), F"Left-hand side of Kinematic relation \"{match.group(2)}\" not valid syntax as in \"q1.q2\""
                        self.kinematicRelations[replacement[0]] = replacement[1]

                    elif match.group(1) == "tapir.contract_fermion_lines" and not self.q2e:
                        self.spinorIndices = False

                    elif match.group(1) == "tapir.euclidean" and not self.q2e:
                        if self.euclideanMomenta:
                            logging.warning("Duplicate option: " + match.group(1))
                        self.euclideanMomenta = True

                    elif match.group(1) == "tapir.topo_extrasort" and not self.q2e:
                        if self.topoExtraSort:
                            logging.warning("Duplicate option: " + match.group(1))
                        self.topoExtraSort = True
                        
                    elif match.group(1) == "tapir.topo_sortfunc" and not self.q2e:
                        assert match.lastindex == 2, "Empty topology sort function"
                        assert self.topoSortFunc == "", "Duplicate option: " + match.group(1)
                        self.topoSortFunc = match.group(2)

                    elif match.group(1) == "tapir.topo_ignore_bridges" and not self.q2e:
                        if self.topoIgnoreBridges:
                            logging.warning("Duplicate option: " + match.group(1))
                        self.topoIgnoreBridges = True

                    elif match.group(1) == "tapir.topo_allow_absent_massive_lines" and not self.q2e:
                        if self.topoAllowAbsentMassiveLines:
                            logging.warning("Duplicate option: " + match.group(1))
                        self.topoAllowAbsentMassiveLines = True

                    elif match.group(1) == "tapir.topo_remove_duplicate_lines" and not self.q2e:
                        if self.topoRemoveDuplicateLines:
                            logging.warning("Duplicate option: " + match.group(1))
                        self.topoRemoveDuplicateLines = True

                    elif match.group(1) == "tapir.extend_analytical_topos" and not self.q2e:
                        if self.extendAnalyticalTopos:
                            logging.warning("Duplicate option: " + match.group(1))
                        self.extendAnalyticalTopos = True

                    elif match.group(1) == "tapir.topo_eikonal_internal" and not self.q2e:
                        if self.topoEikonalInternal:
                            logging.warning("Duplicate option: " + match.group(1))
                        self.topoEikonalInternal = True

                    elif match.group(1) == "tapir.topo_eikonal_external" and not self.q2e:
                        if self.topoEikonalExternal[0]:
                            logging.warning("Duplicate option: " + match.group(1))
                        self.topoEikonalExternal = [True]
                        if match.lastindex == 2:
                            self.topoEikonalExternal += [re.sub(r'\s', r'', s) for s in match.group(2).split(',')]

                    elif match.group(1) == "tapir.topo_complete_momentum_products" and not self.q2e:
                        if self.topoCompleteMomentumProducts:
                            logging.warning("Duplicate option: " + match.group(1))
                        self.topoCompleteMomentumProducts = True

                    elif match.group(1) == "tapir.topology_name" and not self.q2e:
                        assert match.lastindex == 2, "Empty topology name option"
                        assert self.topologyName == "", "Duplicate option: " + match.group(1)
                        self.topologyName = match.group(2)

                    elif match.group(1) == "tapir.mapping_file" and not self.q2e:
                        assert self.mappingFile == "", "Duplicate option: " + match.group(1)
                        if match.lastindex == 1:
                            self.mappingFile = "mapping.inc"
                        else:
                            self.mappingFile = match.group(2)

                    elif match.group(1) == "tapir.yaml_mapping_file" and not self.q2e:
                        assert self.yamlMappingFile == "", "Duplicate option: " + match.group(1)
                        if match.lastindex == 1:
                            self.yamlMappingFile = "mapping.yaml"
                        else:
                            self.yamlMappingFile = match.group(2)

                    elif match.group(1) == "tapir.filter" and not self.q2e:
                        assert match.lastindex == 2, "Unspecified filter option"
                        filterSpec = [re.sub(r'(\S?)\s*$', r'\1', re.sub(r'^\s*(\S?)', r'\1', s)) for s in match.group(2).split(':')]
                        # Remove whitespaces
                        filterSpec = [s.replace(" ", "") for s in filterSpec]
                        assert len(filterSpec) >= 2, F"Not specified if filter {match.group(1)} is applied including (true) or excluding (false) the matching diagrams"
                        assert filterSpec[1].lower() in ["true", "t", "1", "false", "f", "0"], "Unknown filter specifier " + filterSpec[1]
                        # combine cut filters
                        if filterSpec[0] == "cuts":
                            if "cuts" in [f[0] for f in self.filters]:
                                self.filters[[f[0] for f in self.filters].index("cuts")].append(filterSpec[1:])
                            else:
                                self.filters.append([ filterSpec[0], filterSpec[1:] ])
                        # combine vertex filters
                        elif filterSpec[0] == "vertex":
                            if "vertex" in [f[0] for f in self.filters]:
                                self.filters[[f[0] for f in self.filters].index("vertex")].append(filterSpec[1:])
                            else:
                                self.filters.append([ filterSpec[0], filterSpec[1:] ])
                        else:
                            self.filters.append(filterSpec)

                    elif match.group(1) == "tapir.prefactor" and not self.q2e:
                        assert match.lastindex == 2, "No vertex/propagator given for option: " + match.group(1)
                        # Catch exceptions here
                        try:
                            self.prefactorStrings[match.group(2).split(":")[0].replace(" ", "")] = match.group(2).split(":")[1].replace(" ", "")
                        except:
                            logging.error('Correct syntax: * tapir.prefactor p1,p2(,...) : "FORMSTRING"')
                            exit(1)

                    elif match.group(1) == "tapir.draw_particle" and not self.q2e:
                        assert match.lastindex == 2, "Empty drawing options"
                        particleSpec = [re.sub(r'(\S?)\s*$', r'\1', re.sub(r'^\s*(\S?)', r'\1', s)) for s in match.group(2).split(':')]
                        assert particleSpec[0] != "", "Unknown particle"
                        assert particleSpec[1].lower() in self.allowedParticles,\
                                 "Unknown particle " + particleSpec[1] + "\nAllowed are: " + ", ".join(self.allowedParticles)
                        if len(particleSpec) > 2:
                            self.drawingNames[particleSpec[0]] = ":".join(particleSpec[2:])
                        self.drawingObjects[particleSpec[0]] = particleSpec[1].lower()

                    elif match.group(1) == "tapir.ufo_indir" and not self.q2e:
                        if argsObj.commands=="ufo":
                            if argsObj.ufo_indir != None:
                                logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                            else:
                                assert match.lastindex == 2, "Empty UFO input directory option"
                                assert self.ufo_indir == "", "Duplicate option: " + match.group(1)
                                self.ufo_indir = match.group(2)
                        else:
                            logging.error("It seems you are trying to use UFO files, but are not using the correct subcommand (ufo). Please use 'tapir ufo -c UFO_CONFIG_FILE' instead.")
                            raise Exception("Unknown option: " + match.group(1))

                    elif match.group(1) == "tapir.ufo_outdir" and not self.q2e:
                        if argsObj.commands=="ufo":
                            if argsObj.ufo_outdir != None:
                                logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                            else:
                                assert match.lastindex == 2, "Directory to write UFO Feynman rules is missing."
                                assert self.ufo_outdir == "", "Duplicate option: " + match.group(1)
                                self.ufo_outdir = match.group(2)
                        else:
                            logging.error("It seems you are trying to use UFO files, but are not using the correct subcommand (ufo). Please use 'tapir ufo -c UFO_CONFIG_FILE' instead.")
                            raise Exception("Unknown option: " + match.group(1))

                    elif match.group(1) == "tapir.ufo_tag_fermion_flavour" and not self.q2e:
                        if self.ufoTagFermionFlavour:
                            logging.warning("Duplicate option: " + match.group(1))
                        self.ufoTagFermionFlavour = True

                    elif match.group(1) == "tapir.gaugeparam" and not self.q2e:
                        assert match.lastindex == 2, "Empty gauge parameter option"
                        particle = [re.sub(r'\s', r'', s) for s in match.group(2).split(':')]
                        assert particle[0] not in self.gaugeDict, "Duplicate option: " + match.group(1)+" "+match.group(2)
                        self.gaugeDict[particle[0]] = particle[1]
                        
                    elif match.group(1) == "tapir.gaugeprop" and not self.q2e:
                        assert match.lastindex == 2, "Empty field pair option"
                        particle = [re.sub(r'\s', r'', s) for s in match.group(2).split(':')]
                        assert particle[0] not in self.gaugeProps, "Duplicate option: " + match.group(1)+" "+match.group(2)
                        self.gaugeProps[particle[0]] = particle[1]
                        
                    elif match.group(1) == "tapir.kernels" and not self.q2e and argsObj != None:
                        if argsObj.kernels != 1:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            assert match.lastindex == 2, "Empty or overdefined Kernel number option"
                            assert int(match.group(2)) > 0, "Kernel number has to be positive"
                            argsObj.kernels = int(match.group(2))
                    
                    elif match.group(1) == "tapir.qlist" and not self.q2e and argsObj != None:
                        if argsObj.qlist != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            assert match.lastindex == 2, "Empty or overdefined qlist file option"
                            argsObj.qlist = match.group(2)

                    elif match.group(1) == "tapir.qlist_out" and not self.q2e and argsObj != None:
                        if argsObj.qlist_out != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            assert match.lastindex == 2, "Empty or overdefined qlist output option"
                            argsObj.qlist_out = match.group(2)
                            
                    elif match.group(1) == "tapir.diaout" and not self.q2e and argsObj != None:
                        if argsObj.diaout != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            assert match.lastindex == 2, "Empty or overdefined dia file option"
                            argsObj.diaout = match.group(2)
                            
                    elif match.group(1) == "tapir.ediaout" and not self.q2e and argsObj != None:
                        if argsObj.ediaout != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            assert match.lastindex == 2, "Empty or overdefined edia file option"
                            argsObj.ediaout = match.group(2)

                    elif match.group(1) == "tapir.topselin" and not self.q2e and argsObj != None:
                        if argsObj.topselin != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            assert match.lastindex == 2, "Empty or overdefined topsel file input option"
                            argsObj.topselin = match.group(2)
                            
                    elif match.group(1) == "tapir.topselout" and not self.q2e and argsObj != None:
                        if argsObj.topselout != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            assert match.lastindex == 2, "Empty or overdefined topsel file output option"
                            argsObj.topselout = match.group(2)
                            
                    elif match.group(1) == "tapir.diagram_yaml_out" and not self.q2e and argsObj != None:
                        if argsObj.diagram_yaml_out != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            assert match.lastindex == 2, "Empty or overdefined diagram yaml file output option"
                            argsObj.diagram_yaml_out = match.group(2)

                    elif match.group(1) == "tapir.topology_yaml_out" and not self.q2e and argsObj != None:
                        if argsObj.topology_yaml_out != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            assert match.lastindex == 2, "Empty or overdefined topology yaml file output option"
                            argsObj.topology_yaml_out = match.group(2)

                    elif match.group(1) == "tapir.diagram_yaml_in" and not self.q2e and argsObj != None:
                        if argsObj.diagram_yaml_in != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            assert match.lastindex == 2, "Empty or overdefined diagram yaml file input option"
                            argsObj.diagram_yaml_in = match.group(2)

                    elif match.group(1) == "tapir.topology_yaml_in" and not self.q2e and argsObj != None:
                        if argsObj.topology_yaml_in != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            assert match.lastindex == 2, "Empty or overdefined topology yaml file input option"
                            argsObj.topology_yaml_in = match.group(2)

                    elif match.group(1) == "tapir.ordertopsel" and not self.q2e and argsObj != None:
                        if argsObj.ordertopsel != False:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            argsObj.ordertopsel = True
                            
                    elif match.group(1) == "tapir.topologyfolder" and not self.q2e and argsObj != None:
                        if argsObj.topologyfolder != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            assert match.lastindex == 2, "Empty topology folder option"
                            argsObj.topologyfolder = match.group(2)
                            
                    elif match.group(1) == "tapir.minimize" and not self.q2e and argsObj != None:
                        if argsObj.minimize != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            if match.lastindex >= 2:
                                argsObj.minimize = match.group(2)
                            else:
                                argsObj.minimize = ""

                    elif match.group(1) == "tapir.minimize_fine" and not self.q2e and argsObj != None:
                        if argsObj.minimize_fine != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            if match.lastindex >= 2:
                                argsObj.minimize_fine = match.group(2)
                            else:
                                argsObj.minimize_fine = ""
                            
                    elif match.group(1) == "tapir.partfrac" and not self.q2e and argsObj != None:
                        if argsObj.partfrac != False:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            argsObj.partfrac = True

                    elif match.group(1) == "tapir.partfrac_minimize" and not self.q2e and argsObj != None:
                        if argsObj.partfrac_minimize != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            if match.lastindex >= 2:
                                argsObj.partfrac_minimize = match.group(2)
                            else:
                                argsObj.partfrac_minimize = ""
                            
                    elif match.group(1) == "tapir.diagramart" and not self.q2e and argsObj != None:
                        if argsObj.diagramart != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            if match.lastindex >= 2:
                                argsObj.diagramart = match.group(2)
                            else:
                                argsObj.diagramart = "diagrams.tex"

                    elif match.group(1) == "tapir.repart" and not self.q2e and argsObj != None:
                        if argsObj.repart != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            if match.lastindex >= 2:
                                argsObj.repart = match.group(2)
                            else:
                                argsObj.repart = "representatives.tex"

                    elif match.group(1) == "tapir.topologyart" and not self.q2e and argsObj != None:
                        if argsObj.topologyart != None:
                            logging.warning("Option: " + match.group(1) + " set via CLI and config file. CLI overwrites config file entry.")
                        else:
                            if match.lastindex >= 2:
                                argsObj.topologyart = match.group(2)
                            else:
                                argsObj.topologyart = "topologies.tex"

                    elif match.group(1) == "tapir.sigma_particles" and not self.q2e and argsObj != None:
                        assert match.lastindex > 1, "Empty sigma_particles option"
                        assert self.sigmaParticles == [], "Duplicate option: " + match.group(1)
                        self.sigmaParticles = [re.sub(r'\s', r'', s) for s in match.group(2).split(',')]
                        
                    elif match.group(1) == "tapir.no_exp_compat" and not self.q2e:
                        if not self.expCompat:
                            logging.warning("Duplicate option: " + match.group(1))
                        self.expCompat = False
                        
                    elif match.group(1) == "tapir.topo_numrep_only" and not self.q2e:
                        if self.numrepOnly:
                            logging.warning("Duplicate option: " + match.group(1))
                        self.numrepOnly = True
                                                                                    
                    else:
                        raise Exception("Unknown option: " + match.group(1))

            except Exception as e:
                logging.error(F"{self.confFile}, Line {i}: {e}")
                exit(1)

        confHandle.close()

        # Check if eikonal propagators and numerator product replacement is used at the same time, which is not allowed
        if (self.topoEikonalInternal or self.topoEikonalExternal[0]) and self.topoCompleteMomentumProducts:
            logging.warning(F"tapir.topo_eikonal_internal/tapir.topo_eikonal_external and tapir.topo_complete_momentum_products used at the same time! Ignore the latter option.")
            self.topoCompleteMomentumProducts = False
   
        # In case we are running in spinor index mode, add anti-fermions to closedFermionLoop
        if self.spinorIndices == True:
            keylist = self.closedFermionLoop.keys()
            tmplist = []
            for k in keylist:
                if k[1].islower():
                    antik = k[0] + k[1].upper()
                elif k[1].isupper():
                    antik = k[0] + k[1].lower()
                if len(k) >= 3:
                    antik += k[2::]
                if antik not in keylist:
                    tmplist.append([antik,self.closedFermionLoop[k]])
            for p in tmplist:
                self.closedFermionLoop[p[0]] = p[1]

    ## Reads a YAML configuration file.
    #  @param self The object pointer.
    #  @param argsObj Object returned by parser_args()
    #
    #  If the file specified in #confFile is not readable, an error is printed and the program terminates.
    #  After reading in the YAML config file, the ordinary "read(argsObj)" is called, but with an auxiliary
    #  config file produced here.
    def readYAML(self, argsObj):
        # try to open the config file
        logging.info("Reading in YAML-formatted config file.")
        try:
            with open(self.confFile, "r") as f:
                try:
                    confDict = yaml.safe_load(f)['config']
                except yaml.YAMLError as e:
                    logging.error(F"A YAML error occured:\n {e}")
        except FileNotFoundError:
            logging.error("Configuration file not found!")
            exit(1)
        except Exception as e:
            logging.error(F"An error occured while reading the YAML config file:\n {e}")
            exit(1)
        confString = ""

        for key in confDict:
            try:
                if type(confDict[key]) is not list:
                    # non-list-type values are simply translated to "* tapir.key value"
                    if type(confDict[key]) != type(True):
                        confString += "* tapir." + key + " " + str(confDict[key]) + "\n"
                    else:
                        # the option "tapir.key" is only a flag, so the "True" should not be printed
                        # if the value is false, then "tapir.key" is not written at all
                        confString += "* tapir." + key + "\n" if confDict[key] == True else ""
                else:
                    # list-type values are translated into comma- or colon-separated strings, depending on their nature
                    if type(confDict[key][0]) is not dict:
                        # non-dicts result in comma-separated strings
                        confString += "* tapir." + key + " " + ", ".join(str(s) for s in flatten(confDict[key])) + "\n"
                    elif key == "filter":
                        # translate the cut filter option in the correct order
                        for option in confDict[key]:
                            filterType = option.get('type', '')
                            filterValue = option.get('value', '')
                            filterSources = option.get('sources', '')
                            if type(filterSources) is list:
                                filterSources = ",".join([str(s) for s in flatten(filterSources)])
                            filterParticles = option.get('particles', '')
                            if type(filterParticles) is list:
                                if type(filterParticles[0]) is list and filterType=='vertex':
                                    # <- The second condition can be dropped once there are other vertices in which filterParticles[0] can be a list as well;
                                    # For now, this is only the vertex filter.
                                    # Check if multiple vertices are given in the vertex filter
                                    filterParticles = ";".join([str(",".join(flatten(f))) for f in filterParticles])
                                else:
                                    filterParticles = ",".join([str(s) for s in flatten(filterParticles)])
                            filterIntervals = option.get('intervals', '')
                            if type(filterIntervals) is list:
                                filterIntervals = ",".join([str(s) for s in flatten(filterIntervals)])
                            
                            # ignore unnecessary filter options
                            if filterIntervals != "":
                                confString += F"* tapir.{key} {filterType} : {filterValue} : {filterSources} : {filterParticles} : {filterIntervals}\n"
                            elif filterParticles != "":
                                confString += F"* tapir.{key} {filterType} : {filterValue} : {filterSources} : {filterParticles}\n"
                            elif filterSources != "":
                                confString += F"* tapir.{key} {filterType} : {filterValue} : {filterSources}\n"
                            elif filterValue != "":
                                confString += F"* tapir.{key} {filterType} : {filterValue}\n"
                    elif key == "prefactor":
                        # Here we have a list of dictionaries, each being
                        # {"particles": list of vertex lists, "factor": "some string"}
                        for d in confDict[key]:
                            for particleList in d["particles"]:
                                try:
                                    confString += F"* tapir.{key} {','.join(particleList)} : {d['factor']}\n"
                                except:
                                    logging.error("""Error: Please make sure you use the following syntax:
                                    prefactor:
                                      - particles:
                                        - [p1,p2,...] # vertex 1 or propagator 1
                                        - [p1prime,p2prime,...] # vertex 2 or propagator 2
                                        - ...
                                        factor: "FORMSTRING"
                                      - particles:
                                        - [p1,p2,...] # vertex or propagator 1 for next factor
                                        - ...
                                        factor: "FORMSTRING2"
                                      - particles:
                                        ...""")
                                    exit(1)
                    else:
                        # ":" separated list for each dict in the list, e.g.
                        # * tapir.draw_particle fu : fermion : $u$
                        # * tapir.draw_particle fd : fermion : $d$
                        flattenedDict = {k: (v if type(v) is list else [str(v)]) for d in confDict[key] for k,v in d.items()}
                        confString += "".join(["* tapir." + key + " " + k + " : " + " : ".join(flattenedDict[k]) + "\n" for k in flattenedDict.keys()])
            except:
                logging.error(F"An error occured while interpreting the following option: {key}")
                exit(1)
        fileBaseName = "".join(self.confFile.split(".")[:-1])
        oldConfigFile = self.confFile
        self.confFile = fileBaseName + "FromYAML.conf"
        logging.info("Producing old-school config file from YAML file: {self.confFile} and proceeding...")
        with open(self.confFile, "w") as f:
            f.write(confString)
        self.read(argsObj)


    ## Function for debug output. Returns all configuration values.
    def __str__(self):
        string = "{\n"
        for key,val in vars(self).items():
            string += F"    {key}: {val}\n"
        string += "}"

        return string
