## @package topoFileGenerator
#
# The classes described here are responsible for generating topology files that are FORM readable. 
#
# modules.topoFileGenerator.TopoFileGenerator converts a list of Diagram instances to topology files
# 
# In the references of the described classes, we will go further into detail with the folowing example diagram:
# \image html box.png "Example box diagram" height=300px
# \image latex box.eps "Example box diagram" width=10cm


from modules.momentumAssigner import MomentumAssigner
from modules.partialFractionDecomposer import PartialFractionDecomposer
from modules.yamlGenerator import YamlGenerator
from modules.outputHelpers import writeCodeFile
from modules.mathematicaTopoListGenerator import MathematicaTopoListGenerator
from modules.integralFamily import IntegralFamily
import logging
import re
import os
import multiprocessing as mp
import time
import signal


## Main class which converts a list of modules.diagram.Diagram instances to topology files 
#
# To generate topo files, one has to call #generateTopoFiles() after evoking the constructor.
#
# With this class we are able to extract all relevant momentum relations to construct a scalar integral expression from a diagram like
# \image html box.png "Example box diagram" height=300px
# \image latex box.eps "Example box diagram" width=10cm
#
# which corresponds to a topology (see modules.diagram.Diagram::topologicalVertexList) given by
# \image html box-topo.png "Topology description of box diagram" height=300px
# \image latex box-topo.eps "Topology description of box diagram"
# or as a topsel entry (see ediaGenerator and topselReader):
#
# <code> {boxExample;4;1;3;1;;(q1:1,4)(q2:3,4)(q3:2,4)(p1:1,2)(p2:1,3)(p3:2,4)(p4:4,3); 1001} </code>.
#
# The generated, FORM-readable topology file for this example looks like this:
#
# <code>
# id p4 = p3 - q1 - q2 - q3;\n
# id p1 = p3 - q3;\n
# id p2 = -p3 + q1 + q3;
#
# .sort
#
# id p3.q1 = p1.p1/2 - p2.p2/2 + q1.q1/2 + q3.q1;\n
# id p3.q2 = p2.p2/2 - p4.p4/2 + q2.q1 + q2.q2/2 + q3.q2;\n
# id p3.q3 = -p1.p1/2 + p3.p3/2 + q3.q3/2;
#
# .sort
#
# id p1.p1 = -1/s1m1 + M1^2;\n
# id p4.p4 = -1/s4m1 + M1^2;
#
# .sort
#
# id s4m1^n0? * s1m1^n1? * 1/p2.p2^n2? * 1/p3.p3^n3? = 
#    (-1)^n2 * (-1)^n3 * 
#    boxExample(n0,n1,n2,n3);
#
# .sort
# </code>
#
# If in #modules.config.Conf::externalMomenta external momentum replacement rules of the form <code>{"q1": "q", "q2": "-q"}</code> are defined, the topology file will start with:
#
# <code>
# multiply replace_( q1,q , q2,-q );
# </code>
#
# For more details on how this is generated, have a look at modules.momentumAssigner.MomentumAssigner.
#
# Additionally, partial fraction decomposition can be applied using modules.partialFractionDecomposer.PartialFractionDecomposer
class TopoFileGenerator:
    ## Constructor
    # @param config modules.config.Conf instance that describes the meta data of current diagrams
    # @param verbose Print additional information during partial fraction decomposition, default is True
    # @param kernels Define on how many cores to run on
    def __init__(self, config, verbose=True, kernels=1):
        ## modules.config.Conf instance that describes the meta data of current diagrams
        self.config = config
        ## verbose flag to print progress during partial fraction decomposition.
        self.verbose = verbose
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels


    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)


    ## This method generates topology files from a list of modules.diagram.Diagram instances and additionally two list files with all integral family topologies
    # We first compute the modules.momentumAssigner.MomentumAssigner representation for each diagram, then (possibly) compute the  partial fraction decomposition.
    # Afterwards, we use and #getTopoString() #getPartFracString() for the formating. Also, we use modules.outputHelpers.writeCodeFile() for the file writing.
    # 
    # We also create two list files of all minimal topologies (minimal only if partial fractioning is applied). They are called integral family files.
    # The first file is Mathematica readible and can be used for a reduction with FIRE. Its entries have the structure `{topo Name, list of propagators, list of loop momenta}`
    # The second one follows the conventions of Kira and uses modules.yamlGenerator.YamlGenerator::integralFamilyToYaml().
    # Both are written in the same folder as the topology files (\p path)
    # @param path Folder string the topology files are written to
    # @param mathematicaListFileName Name of the topology output list file in Mathematica redable format
    # @param yamlListFileName Name of the topology output list file in YAML format
    # @param diagrams List of modules.diagram.Diagram instances from which we generate the topology file
    # @param partFrac Name of the input list file that we want to compare to. None if no partial fractioning shall be applied, and empty String with no input file
    # @param partFracMinimizationFile If this option is privided with a valid integral family file, the minimization according to the given families is triggered
    def generateTopoFiles(self, path, mathematicaListFileName, yamlListFileName, diagrams, applyPartFrac=False, partFracMinimizationFile=None):
        # create path if it does not exist already
        if not os.path.exists(path) and path != "":
            try:
                os.makedirs(path)
            except:
                logging.error("Cannot create path of " + str(path))
                exit(1)

        if partFracMinimizationFile != None and partFracMinimizationFile != "" and not os.path.isfile(partFracMinimizationFile):
            logging.error("Cannot find file " + partFracMinimizationFile)
            exit(1)

        # apply minimization if partFracMinimizationFile is not None
        minimizeTopologies = partFracMinimizationFile != None
        
        # compute the momentum configuration for every diagram in the main thread only
        if self.kernels == 1:
            momentumAssigners = []
            for i,d in enumerate(diagrams):
                self.vprint(" Assign momenta: {0:^7}/{1:^7}\r".format(i, len(diagrams)), end = "")
                momentumAssigners.append(self.getMomentumAssigner(d))
            self.vprint(" Assign momenta: Done                        ")
        
        # compute the momentum configuration for every diagram in parallel
        else:
             # initializer function for each worker in the multithread pool
            def initializer():
                # ignore CTRL+C in the worker process
                signal.signal(signal.SIGINT, signal.SIG_IGN)

            if self.kernels == 0:
                pool = mp.Pool(mp.cpu_count(),initializer=initializer)
            else:
                pool = mp.Pool(self.kernels,initializer=initializer)

            # start asynchronous workers
            result = pool.map_async(self.getMomentumAssigner, diagrams, chunksize=1)

            # print progress with actualization rate of 10 Hz
            while not result.ready():
                self.vprint(" Assign momenta: {0:^7}/{1:^7}\r".format(len(diagrams)-result._number_left, len(diagrams)), end = "")
                time.sleep(0.1)
            self.vprint(" Assign momenta: Done                  ")
            pool.close()
            pool.join()

            momentumAssigners = result.get()
            
        # Keep track of the relevant integral families
        # The information is written afterwards to the analytic topology list files
        families = []

        # generate file iteratively for all diagrams
        # with partial fractioning
        if applyPartFrac:
            partialFractionDecomposer = PartialFractionDecomposer(self.config, partFracMinimizationFile, self.verbose, self.kernels)
            # apply partial fractioning 
            for i,mA in enumerate(momentumAssigners):
                if self.config.topoEikonalExternal[0]:
                    externalFourVectors = mA.externalMomenta + self.config.topoEikonalExternal[1:]
                else:
                    externalFourVectors = mA.externalMomenta
                self.vprint(" Apply partial fraction decomposition: {0:^7}/{1:^7}\r".format(i, len(momentumAssigners)), end = "")
                partialFractionDecomposer.decompose(mA.diagram.name, mA.analyticTopologyExpression, mA.loopMomenta, externalFourVectors=externalFourVectors)
            self.vprint(" Apply partial fraction decomposition: Done                        ")

            # minimize the amount of topologies
            if minimizeTopologies:
                partialFractionDecomposer.minimizeTopologies()

            for i,mA in enumerate(momentumAssigners):
                self.vprint(" Generate topology files: {0:^7}/{1:^7}\r".format(i, len(momentumAssigners)), end = "")
                topoFileContent = self.getTopoString(mA)
                topoFileContent += partialFractionDecomposer.getPartialFractioningString(mA.diagram.name, mA.analyticTopologyExpression, mA.loopMomenta) 
                writeCodeFile(path, mA.diagram.name, topoFileContent)
            self.vprint(" Generate topology files: Done                        ")

            # extract relevant partfrac topologies
            if minimizeTopologies:
                families = partialFractionDecomposer.getNewUniqueTopologies()
            else:
                families = partialFractionDecomposer.getAllNewTopologies()
            
        # or without partial fractioning
        else:
            for i, mA in enumerate(momentumAssigners):
                self.vprint(" Generate topology files: {0:^7}/{1:^7}\r".format(i, len(momentumAssigners)), end = "")
                # generate topology file
                topoFileContent = self.getTopoString(mA)
                writeCodeFile(path, mA.diagram.name, topoFileContent)
                # and save information about the the relevant topologies
                families.append(IntegralFamily(
                    topoName=mA.diagram.name,
                    analyticTopologyExpression=[str(prop).replace("**", "^") for prop in mA.analyticTopologyExpression],
                    loopMomenta=mA.loopMomenta
                ))
            self.vprint(" Generate topology files: Done                        ")

        # Write topology list files
        # Mathematica
        MathematicaTopoListGenerator(self.config).writeIntegralFamilies(os.path.join(path, mathematicaListFileName), families)
        # YAML
        YamlGenerator(self.config).writeIntegralFamilies(os.path.join(path, yamlListFileName), families)


    ## Wrapper function that creates a modules.momentumAssigner.MomentumAssigner instance for every diagram
    # @param diagram modules.diagram.Diagram instance of which we want to assign the momenta according to a specific routing
    # \return modules.momentumAssigner.MomentumAssigner instance for \p diagram
    def getMomentumAssigner(self, diagram):
        mA = MomentumAssigner(self.config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        return mA


    ## This method uses the modules.momentumAssigner.MomentumAssigner to analyze the momentum routing and then builds a FORM readable string from the result
    # Here, basically, is all formatting of the topology files defined.
    # @param mA modules.momentumAssigner.MomentumAssigner representation of the diagram the topology string shall be computed
    # \return String that correxponds to scalar topology creation in FORM
    def getTopoString(self, mA):
        # Check if we should call a function instead of ".sort"
        if self.config.topoSortFunc != "":
            sort = F"#call {self.config.topoSortFunc}\n"
        else:
            sort = ".sort\n"
        
        # print header
        content = "*"*80 + "\n"
        content += "* Topology file " + mA.diagram.name + " generated by TAPIR\n"

        # indicate in the topology file if we use Euclidean or Minkowski momenta
        if self.config.euclideanMomenta:
            content += "* Using Euclidean momenta\n"
        else:
            content += "* Using Minkowski momenta\n"

        # message about eikonal propagators
        if self.config.topoEikonalInternal or self.config.topoEikonalExternal[0]:
            content += "* Assuming eikonal propagators\n"

        content += "*\n"

        # print also how the scalar topology function is defined
        content += "*\t" + mA.diagram.name + ":\t{" + ", ".join(mA.analyticTopologyExpression) + "}\n"
        content += "*\tLoop momenta:\t" + ", ".join(mA.loopMomenta) + "\n"
        content += "*"*80 + "\n\n"

        # rename momenta according to config.externalMomenta
        if self.config.externalMomenta != {}:
            for key, val in self.config.externalMomenta.items():
                content += F"id {key} = {val};\n"
            content += F"\n{sort}\n"

        content += "* Reducible numerator momentum replacements\n"

        # replacement of linearly dependent momenta
        for p, pSub in mA.momentumDict.items():
            content += "id " + p + " = " + pSub + ";\n"
            # since these statements usually involve several terms on the RHS, we should .sort here
            if self.config.topoExtraSort:
                content += sort

        # Replace same momenta in possible eikonal denominators
        if self.config.topoEikonalInternal or self.config.topoEikonalExternal[0]:
            for p, pSub in mA.equalMomentumDict.items():
                content += F"multiply replace_({p}, {pSub});\n"
                # since these statements usually involve several terms on the RHS, we should .sort here
                if self.config.topoExtraSort:
                    content += sort

        # do not .sort here if topoExtraSort is enabled, otherwise we have 2 .sort statements right after each other
        if self.config.topoExtraSort:
            content += "\n\n"
        else:
            content += F"\n{sort}\n"

        # Jump out if we only want the numerator replacements
        if self.config.numrepOnly:
            return content

        content += "* Numerator momentum product replacements\n"

        # replacement of linearly dependent momentum products
        # First for equalMomentumSquaredProducts (treat the specially to match also denominators)
        for pp, ppSub in mA.equalMomentumSquaredProducts.items():
            content += "id {}^n0? = ({})^n0;\n".format(pp, ppSub)
            # since these statements usually involve several terms on the RHS, we should .sort here
            if self.config.topoExtraSort:
                content += sort

        if len(mA.equalMomentumSquaredProducts.items()) > 0:
            content += "\n"

        # then for the rest
        for pp, ppSub in mA.momentumProducts.items():
            if pp not in mA.equalMomentumSquaredProducts.keys():
                content += "id {} = {};\n".format(pp, ppSub)
                # since these statements usually involve several terms on the RHS, we should .sort here
                if self.config.topoExtraSort:
                    content += sort

        # do not .sort here if topoExtraSort is enabled, otherwise we have 2 .sort statements right after each other
        if self.config.topoExtraSort:
            content += "\n\n"
        else:
            content += F"\n{sort}\n"

        # regex to extract p1 from p1.p1
        squarematch = re.compile(r'([a-zA-Z]+[0-9]*)\.\1')

        content += "* Define massive propagators\n"

        # replacement of squared massive line momenta with massive propagator
        for pi, m in mA.lineMasses.items():
            if m == 0:
                continue
            # set s1m1 equal to s2m1 if p1=p2 and both are massive
            if (pi+"."+pi) in mA.equalMomentumSquaredProducts.keys():
                pRepl = squarematch.findall(mA.equalMomentumSquaredProducts[(pi+"."+pi)])[0]
                assert pRepl in mA.lineMasses.keys(), "{} is not massive but {} is. The exact replacement ignores the simj term".format(pRepl, pi)
                content += "id {} = {};\n".format(mA.simj(pi), mA.simj(pRepl))
            else:
                if self.config.euclideanMomenta:
                    # construct p2.p2 = 1/s2m1 - M1^2
                    content += "id " + pi + "." + pi + " = 1/" + mA.simj(pi)\
                        + " - " + mA.lineMasses[pi] + "^2"  + ";\n"
                else:
                    # construct p2.p2 = -1/s2m1 + M1^2
                    content += "id " + pi + "." + pi + " = -1/" + mA.simj(pi)\
                        + " + " + mA.lineMasses[pi] + "^2"  + ";\n"

            # since these statements usually involve several terms on the RHS, we should .sort here
            if self.config.topoExtraSort:
                content += sort

        # insert the kinematic replacements here
        if len(self.config.kinematicRelations) != 0:
            content += "\n* Apply kinematic relations\n"
            for lhs, rhs in self.config.kinematicRelations.items():
                if rhs != "0":
                    content += F"id {lhs}^n0? = ({rhs})^n0;\n"
                else:
                    content += F"id {lhs} = {rhs};\n"

        # do not .sort here if topoExtraSort is enabled, otherwise we have 2 .sort statements right after each other
        if self.config.topoExtraSort:
            content += "\n\n"
        else:
            content += F"\n{sort}\n"

        # now replace all non further reducible scalar factors to a topology function with appropriate indices as arguments
        logging.debug("Independent scalars: " + str(mA.topoFactors))
        content += "* Combine to scalar topology function\n"

        lhs = "id "
        rhsSigns = ""
        rhsTopo = mA.diagram.name + "("
        for i, factor in enumerate(mA.topoFactors):
            # left-hand side
            lhs += factor +"^n" + str(i) + "?"

            # right-hand side
            if mA.topoFactorSigns[i] == "-" and not self.config.euclideanMomenta:
                rhsSigns += "(-1)^n" + str(i) + " * "
            rhsTopo += "n" + str(i)

            if i != len(mA.topoFactors) - 1:
                lhs += " * "
                rhsTopo += ","
            
        content += lhs + " = \n   " + rhsSigns + "\n   " + rhsTopo + ");\n\n"
        content += sort

        # check whether all topology factors are inserted in the scalar function
        content += "\n* Check for unmatched topology factors\n"
        content += "if( occurs("
        # momenta
        content += ", ".join([F"p{i+1}" for i in range(len(mA.diagram.internalMomenta))]) 
        # denominators with masses
        for m in sorted({i for i in mA.lineMasses.values() if i != 0}):
            content += ", " 
            content += ", ".join([ F"s{i+1}m{m[1:]}" for i in range(len(mA.diagram.internalMomenta)) ])
        content += ") );\n   exit \">> Error: Unmatched topology factors found!\";\nendif;\n\n"

        content += sort

        return content
