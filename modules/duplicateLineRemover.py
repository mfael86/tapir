## @package duplicateLineRemover
# The here described filter class modules.duplicateLineRemover.duplicateLineRemover removes lines with the same momentum and mass from diagrams.


from typing import List
from modules.diagram import Diagram
from modules.filter import Filter
from modules.momentumAssigner import MomentumAssigner
import re
import multiprocessing as mp
import time
import signal


## A simple Filter class which removes lines with the same line momentum flowing through them.
# The main method is #filter().
# It is used to minimize the number of lines in topsel file entries, allowing for proper minimization of topologies in later steps.
class DuplicateLineRemover(Filter):
    ## Constructor
    # @param conf modules.config.Conf instance
    def __init__(self, conf, verbose=False, kernels=1):
        ## Local modules.config.Conf instance
        self.conf = conf
        ## verbose flag to print progress
        self.verbose = verbose
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels


    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)


    ## Basic pipe method that removes duplicates of lines with the same momentum.
    # We call #removeDuplicateLines() from here which does the main job.
    # It is run in parallel for best performance result.
    # As a result, get the modules.diagram.Diagram's with duplicate lines removed but properly initialized (i.e. the line and vertex numbers are ascending without gaps)
    # Be aware, that this mehtod is destructive! I.e. The diagrams are intrinsically changed.
    # Note: Spinor indices are not contracted!
    # @param diagrams List of modules.diagram.Diagram instances we want to filter
    # \return \p diagrams where the duplicates of lines are removed
    def filter(self, diagrams: List[Diagram]) -> List[Diagram]:
        # do nothing if option is unwanted
        if not self.conf.topoRemoveDuplicateLines:
            return diagrams

        # remove unwanted lines from the diagrams in main thread
        if self.kernels == 1:
            dias = []
            for i,d in enumerate(diagrams):
                self.vprint(" Remove duplicate lines: {0:^7}/{1:^7}\r".format(i, len(diagrams)), end = "")
                dias.append(self.removeDuplicateLines(d))
            self.vprint(" Remove duplicate lines: Done                  ")
            return dias
        
        # remove unwanted lines from the diagrams in parallel
        else:
             # initializer function for each worker in the multithread pool
            def initializer():
                # ignore CTRL+C in the worker process
                signal.signal(signal.SIGINT, signal.SIG_IGN)

            if self.kernels == 0:
                pool = mp.Pool(mp.cpu_count(),initializer=initializer)
            else:
                pool = mp.Pool(self.kernels,initializer=initializer)

            # start asynchronous workers
            result = pool.map_async(self.removeDuplicateLines, diagrams, chunksize=1)

            # print progress with actualization rate of 10 Hz
            while not result.ready():
                self.vprint(" Remove duplicate lines: {0:^7}/{1:^7}\r".format(len(diagrams)-result._number_left, len(diagrams)), end = "")
                time.sleep(0.1)
            self.vprint(" Remove duplicate lines: Done                  ")
            pool.close()
            pool.join()

            return result.get()

    
    ## Remove all duplicated lines from a given diagram.
    # I.e. lines which have the same momentum and mass as another line.
    # We contract them and rename the vertices and lines.
    # Note: Spinor indices are not contracted!
    # @param diagrams List of modules.diagram.Diagram instances of which we want to remove the duplicates. Note that this list is altered afterwards!
    # \return List of diagrams with changed topology
    def removeDuplicateLines(self, diagram):
        # Find all duplicate lines
        mA = MomentumAssigner(self.conf,diagram)
        mA.generateLineMassesDict()
        mA.calculateMomentumRelations()
        
        toRemove = []

        # Group identical momenta (from momentumAssigner.equalMomentumSquaredProducts)
        momentumGroups = {}
        for pp, ppRepl in mA.equalMomentumSquaredProducts.items():
            # Cut string of the form 'pXY.pXY' before the dot
            p = pp[:pp.index(".")]
            pRepl = ppRepl[:ppRepl.index(".")]

            # Group the momentum into momentumGroups with the replacement momentum as key 
            if pRepl in momentumGroups:
                momentumGroups[pRepl].append(p)
            else:
                momentumGroups[pRepl] = [pRepl,p]

        # Build subgroups of momentumGroups with momenta which have the same mass
        massMomentumGroups = []
        for momentumGroup in momentumGroups.values():
            # Build dict of momenta according to their masses, eg. {mass1 : [p1,p3]} 
            massMomentumGroup = {}
            for p in momentumGroup:
                if mA.lineMasses[p] in massMomentumGroup:
                    massMomentumGroup[mA.lineMasses[p]].append(p)
                else:
                    massMomentumGroup[mA.lineMasses[p]] = [p]

            # add mass-subgroup to massMomentumGroups
            massMomentumGroups.extend(massMomentumGroup.values())

        # Remove momentum from each subgroup (the unique momentum of each group) and mark the rest as removable
        for group in massMomentumGroups:
            if len(group) > 1:
                toRemove.extend(sorted(group)[1:])
        
        # Remember the vertices attached to duplicate lines
        vertexRename = [ [l[1],l[2]] for l in diagram.internalMomenta if l[0] in toRemove ]
        
        # remove the lines
        diagram.internalMomenta = [ l for l in diagram.internalMomenta if l[0] not in toRemove ]
        
        # set the line vertices as equal, line start -> line end
        while len(vertexRename) > 0:
            currentRename = vertexRename.pop()
            # discard the case of self-loop contraction
            if currentRename[0] == currentRename[1]:
                continue

            # rename the vertices in the diagram
            for p in diagram.internalMomenta:
                if currentRename[0] == p[1]:
                    p[1] = currentRename[1]
                if currentRename[0] == p[2]:
                    p[2] = currentRename[1]

            for q in diagram.externalMomenta:
                if currentRename[0] == q[1]:
                    q[1] = currentRename[1]

            # rename vertices in vertexRename also
            for v in vertexRename:
                if currentRename[0] == v[0]:
                    v[0] = currentRename[1]
                if currentRename[0] == v[1]:
                    v[1] = currentRename[1]

        # re-label vertices in ascending order
        vertexLabels = set([p[1] for p in diagram.internalMomenta] + [p[2] for p in diagram.internalMomenta] + [q[1] for q in diagram.externalMomenta])
        vertexLabels = list(vertexLabels)
        # [2,4,7,8] means, re-label 2->1, 4->2, 7->3, 8->4
        vertexLabels.sort()

        # also re-label line momenta in ascending order
        for i,p in enumerate(diagram.internalMomenta):
            p[0] = re.sub(r'p[0-9]+', F"p{i+1}", p[0])
            p[1] = vertexLabels.index(p[1]) + 1
            p[2] = vertexLabels.index(p[2]) + 1

        # relabel the vertices in the external momenta field
        for q in diagram.externalMomenta:
            q[1] = vertexLabels.index(q[1]) + 1

        # re-calculate diagram.Diagram.topologicalLineList and diagram.Diagram.topologicalVertexList
        diagram.createTopologicalVertexAndLineLists()
        return diagram

