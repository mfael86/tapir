## @package yamlReader
# This module reads yaml files and generates according diagrams, topologies or integral families.


import logging
from modules.diagram import Diagram
from modules.qgrafReader import QgrafReader
from modules.feynmanRulesReader import FeynmanRulesReader
from modules.partialFractionDecomposer import IntegralFamily
import yaml
import multiprocessing as mp
import time
import signal
import re


## The YamlReader class reads in a yaml files which represent either diagrams, topologies or integral families.
class YamlReader:
    ## Constructor
    #  @param conf An initialized config object
    #  @param needFRs Read in Feynman rules
    #  @param verbose Increase verbosity level if set to true
    #  @param kernels Run certain parts of the module in parallel using the given number of cores
    def __init__(self, conf, needFRs=True, verbose=False, kernels=1):
        ## config.Conf object reference.
        self.config = conf
        ## Verbose flag to print progress during file reading.
        self.verbose = verbose
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels
        ## Load a local QgrafReader instance
        self.qgrafReader = QgrafReader(conf, verbose, kernels)
        ## Dictionary which contains information about all particle-antiparticle relations
        # Load Feynman rules to get information about particle-antiparticle relations
        if needFRs == True:
            self.antiparticleDict = FeynmanRulesReader(conf).getAntiParticleDict()
        else:
            self.antiparticleDict = {}


    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)

    
    ## Read a YAML diagram file and generate modules.diagram.Diagram instances from it
    # @param yamlDiagramFile File name (including path) to the YAML diagram file we want to read
    # \return List of modules.diagram.Diagram objects representing the read diagrams
    def readDiagrams(self, yamlDiagramFile):
        return self.readListFromYaml(yamlFile=yamlDiagramFile, topLevelKey="diagrams", yamlTranslationFunction=self.yamlToDiagram)


    ## Read a YAML topology file and generate modules.diagram.Diagram instances from it
    # @param yamlTopologyFile File name (including path) to the YAML topology file we want to read
    # \return List of modules.diagram.Diagram objects representing the read topologies
    def readTopologies(self, yamlTopologyFile):
        return self.readListFromYaml(yamlFile=yamlTopologyFile, topLevelKey="topologies", yamlTranslationFunction=self.yamlToTopology)


    ## Read a YAML topology list file and generate a modules.integralFamily.IntegralFamily instance from it
    # @param yamlTopologyListFile File name (including path) to the YAML topology list file we want to read
    # \return List of modules.integralFamily.IntegralFamily objects representing the read integral families
    def readIntegralFamilies(self, yamlTopologyListFile):
        return self.readListFromYaml(yamlFile=yamlTopologyListFile, topLevelKey="integralfamilies", yamlTranslationFunction=self.yamlToIntegralFamily)


    ## Read a YAML file and generate a list of diagram, topology or integral family instances from it
    # @param yamlFile File name (including path) which shall be read
    # @param topLevelKey String representing the top level key of the list which is included in the read \p yamlFile. I.e. "diagrams", "topologies" or "integralfamilies"
    # @param yamlTranslationFunction Callable function which translates a yaml dict to the wished object class
    # \return List of wished objects according to the return values of \p yamlTranslationFunction()
    def readListFromYaml(self, yamlFile, topLevelKey, yamlTranslationFunction):
        # Try to extract yaml dict from the given file
        try:
            with open(yamlFile, "r") as openFile:
                yamlDict = yaml.safe_load(openFile)
        except FileNotFoundError:
            logging.error(openFile + " not found!")
            exit(1)
        except Exception as e:
            logging.error(F"An error occured while reading {yamlFile}:")
            logging.error(str(e))
            exit(1)

        # Check for the top level key
        if topLevelKey not in yamlDict:
            logging.error(F"No \"{topLevelKey}\" key found in {yamlFile}")
            exit(1)

        # Save number of entries
        numYamlEntries = len(yamlDict[topLevelKey])

        # Read single entry in main thread
        if self.kernels == 1:
            entriesList = []
            for i in range(0,numYamlEntries):
                self.vprint(" Read YAML input: {0:^7}/{1:^7}\r".format(i, numYamlEntries), end = "")
                entriesList.append(yamlTranslationFunction(yamlDict[topLevelKey][i], i+1))
            self.vprint(" Read YAML: Done                  ")
            return entriesList
        # Read single entries in parallel
        else:
            # Initializer function for each worker in the multithread pool
            def initializer():
                # ignore CTRL+C in the worker process
                signal.signal(signal.SIGINT, signal.SIG_IGN)

            if self.kernels == 0:
                pool = mp.Pool(mp.cpu_count(),initializer=initializer)
            else:
                pool = mp.Pool(self.kernels,initializer=initializer)

            # start asynchronous workers
            result = pool.map_async(yamlTranslationFunction, yamlDict[topLevelKey], chunksize=1)

            # print progress with actualization rate of 10 Hz
            while not result.ready():
                self.vprint(" Read YAML input: {0:^7}/{1:^7}\r".format(numYamlEntries-result._number_left, numYamlEntries), end = "")
                time.sleep(0.1)
            self.vprint(" Read YAML input: Done                  ")
            pool.close()
            pool.join()

            return result.get()
    

    ## Translate a YAML dict to a modules.diagram.Diagram format
    # This method is the inversion of modules.yamlGenerator.YamlGenerator::diagramToYaml().
    # After the diagram instance is created, also modules.diagram.Diagram::createTopologicalVertexAndLineLists() and modules.diagram.Diagram::groupFermions() is called.
    # @param yamlDict Dict object with the yaml input
    # @param entryNumber This number is used as the diagram number if \p diagramNumber is not defined in \p yamlDict
    # \return Initialized modules.diagram.Diagram instance which corresponds to the yaml input
    def yamlToDiagram(self, yamlDict, entryNumber=0):
        diagram = Diagram()

        # diagramNumber
        diagram.diagramNumber = yamlDict.get("diagramNumber", entryNumber)

        # preFactor
        diagram.preFactor = yamlDict.get("preFactor")

        # numLoops
        diagram.numLoops = yamlDict.get("loops")

        # numLegsIn (is increased iteratively during external line extraction)
        diagram.numLegsIn = 0

        # numLegsOut (is increased iteratively during external line extraction)
        diagram.numLegsOut = 0

        # externalMomenta
        # Count the number of external momenta
        externalMomentumCounter = 1
        for externalLine in yamlDict.get("externalLines", []):
            # Incoming momentum (no change)
            if "incomingMomentum" in externalLine:
                diagram.numLegsIn += 1
                diagram.externalMomenta.append([
                    externalLine["incomingMomentum"], # q1
                    externalLine["vertex"], # 2
                    externalLine["incomingParticle"], # ft
                    F"i{externalMomentumCounter}", # i1
                ])
            # Outgoing momentum (change particle by anti-particle)
            else:
                diagram.numLegsOut += 1
                diagram.externalMomenta.append([
                    externalLine["outgoingMomentum"], # q1
                    externalLine["vertex"], # 2
                    self.antiparticleDict[externalLine["outgoingParticle"]], # ft -> fT
                    F"i{externalMomentumCounter}", # i1
                ])
            externalMomentumCounter += 1

        # internalMomenta
        # save number of independent external momenta
        numIndependentExtMom = len(diagram.externalMomenta) - 1
        for internalLine in yamlDict.get("internalLines", []):
            line = [
                internalLine["momentum"], # p1
                internalLine["vertices"][0],  # 2
                internalLine["vertices"][1],  # 3
                internalLine["incomingParticle"], # ft
                internalLine["outgoingParticle"], # fT
                2 * ( int(re.match(r'p([0-9]+)', internalLine["momentum"]).group(1)) + numIndependentExtMom) + 1,  # i1
                2 * ( int(re.match(r'p([0-9]+)', internalLine["momentum"]).group(1)) + numIndependentExtMom) + 2,  # i2
                internalLine.get("mass")
            ]

            # Look for mass in config file if none is given in the yaml file
            if line[7] == None:
                line[7] = self.qgrafReader.getMass(internalLine["incomingParticle"], internalLine["outgoingParticle"])

            diagram.internalMomenta.append(line)

        # Initialize the topological graph data structures
        diagram.createTopologicalVertexAndLineLists()

        # Group the fermions accordingly if the "spinorIndices" option is used
        if not self.config.spinorIndices:
            try:
                diagram.groupFermions()
            except Exception as e:
                logging.error(e)
                exit(1)
        # Give the diagram a unique name
        if self.config.topologyName.isspace():
            diagram.setName()
        else:
            diagram.setName(self.config.topologyName)

        return diagram


    ## Translate a YAML dict to a modules.diagram.Diagram format which includes only the topological infrmation
    # This method is the inversion of modules.yamlGenerator.YamlGenerator::topologyToYaml().
    # After the topology instance is created, also modules.diagram.Diagram::createTopologicalVertexAndLineLists() is called.
    # @param yamlDict Dict object with the yaml input
    # @param entryNumber This number is used as the diagram number
    # \return Initialized modules.diagram.Diagram instance which corresponds to the yaml input
    def yamlToTopology(self, yamlDict, entryNumber):
        diagram = Diagram()

        # diagramNumber
        diagram.diagramNumber = entryNumber

        # numLoops
        diagram.numLoops = yamlDict.get("loops")

        # Set name according to "name" field which is not mandatory. An empty name is replaced be the d3l101 type according to number of loops and the entryNumber
        diagram.setName(yamlDict.get("name", F"d{diagram.numLoops}l{diagram.diagramNumber}"), appendNumbering=False)

        # numLegsIn (is increased iteratively during external line extraction)
        diagram.numLegsIn = 0

        # numLegsOut (is increased iteratively during external line extraction)
        diagram.numLegsOut = 0

        # externalMomenta
        # Count the number of external momenta
        externalMomentumCounter = 1
        for externalLine in yamlDict.get("externalLines", []):
            # Incoming momentum (no change)
            if "incomingMomentum" in externalLine:
                diagram.numLegsIn += 1
                diagram.externalMomenta.append([
                    externalLine["incomingMomentum"], # q1
                    externalLine["vertex"], # 2
                    "dummyMasslessParticle", # dummy particle name
                    externalLine["vertex"], # 2
                ])
            # Outgoing momentum (change particle by anti-particle)
            else:
                diagram.numLegsOut += 1
                diagram.externalMomenta.append([
                    externalLine["outgoingMomentum"], # q1
                    externalLine["vertex"], # 2
                    "dummyMasslessParticle", # dummy particle name
                    externalLine["vertex"], # 2
                ])
            externalMomentumCounter += 1

        # internalMomenta
        for internalLine in yamlDict.get("internalLines", []):
            # Build dummy particle name according to its mass
            if internalLine.get("mass", None) == None:
                particleName = "dummyMasslessParticle"
                massName = ""
            else:
                particleName = F"dummy{internalLine['mass']}particle"
                massName = internalLine['mass']

            line = [
                internalLine["momentum"], # p1
                internalLine["vertices"][0],  # 2
                internalLine["vertices"][1],  # 3
                particleName, # dummy particle name
                particleName, # dummy particle name
                internalLine["vertices"][0],  # 2
                internalLine["vertices"][1],  # 3
                massName
            ]

            diagram.internalMomenta.append(line)

        # Initialize the topological graph data structures
        diagram.createTopologicalVertexAndLineLists()

        return diagram


    ## Translate a YAML dict to a modules.integralFamily.IntegralFamily instance
    # This method is the inversion of modules.yamlGenerator.YamlGenerator::integralFamilyToYaml().
    # @param yamlDict Dict object with the yaml input
    # @param entryNumber This number is used as the replacement if no \p topoName was provided. The substitution is called "Family0",...,"Family42",...
    # \return Initialized modules.integralFamily.IntegralFamily instance which corresponds to the yaml input
    def yamlToIntegralFamily(self, yamlDict, entryNumber=0):
        # Initialize name if none was provided
        if yamlDict.get('name', '') == '':
            topoName = F"Family{entryNumber}"
        else:
            topoName = yamlDict.get('name')

        return IntegralFamily(
            topoName=topoName, 
            analyticTopologyExpression=[p[0] for p in yamlDict['propagators']],
            loopMomenta=yamlDict['loop_momenta']
        )
