# distutils: language = c++

from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.algorithm cimport sort


cdef extern from "nickel.hpp":
    string cFindNickelIndex(
        vector[vector[int]] lineList, 
        vector[int] &nickelVertexPermutations,
        vector[int] &nickelOrderedMomenta,
        vector[int] &nickelMomentaSigns,
        vector[int] &nickelOrderedExternalMomenta
        )

    vector[string] cFindNickelIndices(
        vector[vector[vector[int]]] lineLists, 
        vector[vector[int]] &allNickelVertexPermutations,
        vector[vector[int]] &allNickelOrderedMomenta,
        vector[vector[int]] &allNickelMomentaSigns,
        vector[vector[int]] &allNickelOrderedExternalMomenta
        )

    string cTest()


# Compute Nickel index, the vertices orders which appear in the index, the signed momenta in the specific order according to the index and the external momenta
cpdef findNickelIndex(lineList):
    cdef vector[int] nickelVertexPermutations 
    cdef vector[int] nickelOrderedMomenta 
    cdef vector[int] nickelMomentaSigns
    cdef vector[int] nickelOrderedExternalMomenta 
    nickelIndex = cFindNickelIndex(lineList, nickelVertexPermutations, nickelOrderedMomenta, nickelMomentaSigns, nickelOrderedExternalMomenta)

    nickelOrderedMomentaString = []
    # Change signs of momenta according to #nickelMomentaSigns
    for i in range(len(nickelOrderedMomenta)):
        if nickelMomentaSigns[i] == 1:
            nickelOrderedMomentaString.append(F"p{nickelOrderedMomenta[i]}")
        else:
            nickelOrderedMomentaString.append(F"-p{nickelOrderedMomenta[i]}")

    nickelOrderedExternalMomentaString = [F"q{i}" for i in nickelOrderedExternalMomenta]

    return nickelIndex, nickelVertexPermutations, nickelOrderedMomentaString, nickelOrderedExternalMomentaString


# Compute #findNickelIndex() in a c++ for loop for multiple graphs
cpdef findNickelIndices(lineLists):
    cdef vector[vector[int]] allNickelVertexPermutations 
    cdef vector[vector[int]] allNickelOrderedMomenta 
    cdef vector[vector[int]] allNickelMomentaSigns 
    cdef vector[vector[int]] allNickelOrderedExternalMomenta
    nickelIndices = cFindNickelIndices(lineLists, allNickelVertexPermutations, allNickelOrderedMomenta, allNickelMomentaSigns, allNickelOrderedExternalMomenta)

    nickelOrderedMomentaStrings = []
    for i in range(len(allNickelOrderedMomenta)):
        nickelOrderedMomentaString = []
        # Change signs of momenta according to #nickelMomentaSigns
        for j in range(len(allNickelOrderedMomenta[i])):
            if allNickelMomentaSigns[i][j] == 1:
                nickelOrderedMomentaString.append(F"p{allNickelOrderedMomenta[i][j]}")
            else:
                nickelOrderedMomentaString.append(F"-p{allNickelOrderedMomenta[i][j]}")
        nickelOrderedMomentaStrings.append(nickelOrderedMomentaString)

    nickelOrderedExternalMomentaStrings = []
    for nickelOrderedExternalMomenta in allNickelOrderedExternalMomenta:
        nickelOrderedExternalMomentaStrings.append([F"p{i}" for i in nickelOrderedExternalMomenta])

    return nickelIndices, allNickelVertexPermutations, nickelOrderedMomentaStrings, nickelOrderedExternalMomentaStrings


def test():
    return test()