/**
 * Note: C++ 20 standard is used here. 
 * Hence, compile with option `--std=c++20`
 **/

#include <iostream>
#include <vector>

using namespace std;


#ifndef DEBUG
    #define DEBUG 0
#endif


char nickelNumeration[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                'a', 'b', 'c', 'd', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
                                's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};


string cFindNickelIndex(vector< vector<int> > lineList, vector<int> &nickelVertexPermutations, vector<int> &nickelOrderedMomenta, vector<int> &nickelMomentaSigns, vector<int> &nickelOrderedExternalMomenta){
    string nickelIndex = "~";

    // Extract number of vertices
    int numVertices = 0;
    for(vector<int> v : lineList){
        if(v[0] > numVertices)
            numVertices = v[0];
        if(v[1] > numVertices)
            numVertices = v[1];
    }

    // Initialize vector with numbers range from 1 to numVertices
    vector<int> vertexNames;
    for(int i=1; i<=numVertices; i++)
        vertexNames.push_back(i);
    
    // Iterate over all permutations of different vertex names
    do{
        // Compute Nickel notation

        // Initialize vector with signs of momenta for which the Nickel index is minimal
        vector<int> momentumSigns(lineList.size(), 1);

        // Sort lines according to their start and end point w.r.t. the new vertex naming
        sort(lineList.begin(), lineList.end(), [naming=vertexNames](vector<int> v1, vector<int> v2){
            int v1Min, v2Min, v1Max, v2Max;

            // viMin is always the lesser vertex number, unless the line is external, then it is the only real vertex number
            // viMax is always the bigger vertex number, unless the line is external, then it is more negative according to a lower external vertex number.
            if(v1[0] < 0){
                v1Min = naming[v1[1]-1];
                v1Max = -1000-v1[0];
            }
            else if(v1[1] < 0){
                v1Min = naming[v1[0]-1];
                v1Max = -1000-v1[1];
            }
            else{
                v1Min=min(naming[v1[0]-1], naming[v1[1]-1]);
                v1Max=max(naming[v1[0]-1], naming[v1[1]-1]);
            }

            if(v2[0] < 0){
                v2Min = naming[v2[1]-1];
                v2Max = -1000-v2[0];
            }
            else if(v2[1] < 0){
                v2Min = naming[v2[0]-1];
                v2Max = -1000-v2[1];
            }
            else{
                v2Min=min(naming[v2[0]-1], naming[v2[1]-1]);
                v2Max=max(naming[v2[0]-1], naming[v2[1]-1]);
            }

            if(v1Min == v2Min) return v1Max < v2Max;
            return v1Min < v2Min;
        });

        // Construct Nickel notation

        // The first part of the Nickel notation describing which vertices are connected by a line
        string nickelLines = "";
        // The second part of the Nickel notation remembering the masses and the external momenta
        string nickelMasses = "";
        // Count which vertex we are currently describing
        int vrtxCounter = 1;
        // Remember the first line to place the separator correctly
        bool firstLine = true;


        for(vector<int> v : lineList){
            // vMin is the actual vertex number
            // vMax is the on that is needed for the Nickel notation
            int vMin, vMax;

            if(v[0] < 0){
                vMin = vertexNames[v[1]-1];
                vMax = v[0];
            }
            else if(v[1] < 0){
                vMin = vertexNames[v[0]-1];
                vMax = v[1];
            }
            else if(vertexNames[v[0]-1] < vertexNames[v[1]-1]){
                momentumSigns[v[3]-1] = -1;
                vMin=vertexNames[v[0]-1];
                vMax=vertexNames[v[1]-1];
            }
            else{
                vMin=vertexNames[v[1]-1];
                vMax=vertexNames[v[0]-1];
            }

            #if DEBUG == 1
                cout << "{";
                for(vector<int> v:lineList)
                    cout << "(" << v[0] << "," << v[1] << "," << v[2] << "," << v[3] << ")";
                cout << "}";
                cout << endl;
            #endif

            // New vertex -> add divider to Nickel notation until vrtxCounter is equal to the new vertex number
            bool newVertex = firstLine;
            firstLine = false;

            while(vMin > vrtxCounter){
                nickelLines += "|";
                nickelMasses += "|";
                vrtxCounter++;
                newVertex = true;
            }

            // The entries of the Nickel notations for one vertex are the adjacent vertices
            // External vertices get an "e"
            if(vMax < 0){
                nickelLines += "e";
                nickelMasses += (newVertex ? "q" : "_q") + to_string(-vMax);
            }
            // Internal vertices get the index number
            else{
                nickelLines += nickelNumeration[vMax-1];
                if(v[2] == 0)
                    nickelMasses += (newVertex ? "" : "_");
                else
                    nickelMasses += (newVertex ? "M" : "_M") + to_string(v[2]);
            }
        }

        // Combine both parts
        string nickelNotation = nickelLines + "| : " + nickelMasses + "|";

        #if DEBUG == 1
            cout << nickelNotation << endl;
        #endif

        // Compare current Nickel notation to the best yet found
        if(nickelNotation < nickelIndex){
            nickelIndex = nickelNotation;
            nickelVertexPermutations = vertexNames;
            nickelOrderedMomenta = vector<int>{};
            nickelMomentaSigns = vector<int>{};
            nickelOrderedExternalMomenta = vector<int>{};
            for(vector<int> v : lineList){
                if(v[3] < 0)
                    nickelOrderedExternalMomenta.push_back(-v[3]);
                else{
                    nickelOrderedMomenta.push_back(v[3]);
                    nickelMomentaSigns.push_back(momentumSigns[v[3]-1]);
                }
            }
        }

    }
    while(next_permutation(vertexNames.begin(), vertexNames.end()));

    return nickelIndex;
}


// Implementation of #cFindNickelIndex() with a c++ loop
vector<string> cFindNickelIndices(vector< vector< vector<int> > > lineLists, vector< vector<int> > &allNickelVertexPermutations, vector< vector<int> > &allNickelOrderedMomenta, vector< vector<int> > &allNickelMomentaSigns, vector< vector<int> > &allNickelOrderedExternalMomenta){
    vector<string> nickelIndices {};

    for(int i=0; i<(int)lineLists.size(); i++){
        allNickelOrderedMomenta.push_back({});
        allNickelMomentaSigns.push_back({});
        allNickelOrderedExternalMomenta.push_back({});
        nickelIndices.push_back(
            cFindNickelIndex(lineLists[i], allNickelVertexPermutations[i], allNickelOrderedMomenta[i], allNickelMomentaSigns[i], allNickelOrderedExternalMomenta[i])
        );
    }

    return nickelIndices;
}


// This is a test function to see if the program executes as expected
int main(){
    vector< vector<int> > lineList {{1,2,2,1},{1,2,2,2},{2,3,0,3},{3,4,0,4},{4,6,2,5},{4,6,2,6},{4,5,1,7},{6,5,0,8},{1,-1,0,-1},{1,-2,0,-2},{5,-4,0,-4},{3,-3,0,-3}};
    vector<int> nickelVertexPermutations {};
    vector<int> nickelOrderedMomenta {};
    vector<int> nickelMomentaSigns {};
    vector<int> nickelOrderedExternalMomenta {};

    cout << cFindNickelIndex(lineList, nickelVertexPermutations, nickelOrderedMomenta, nickelMomentaSigns, nickelOrderedExternalMomenta) << endl;
    for(int i : nickelVertexPermutations)
        cout << i << " ";
    cout << endl;
    for(int i : nickelOrderedMomenta)
        cout << "p" << i << " ";
    cout << endl;
    for(int i : nickelOrderedExternalMomenta)
        cout << "q" << i << " ";
    cout << endl;
    return(0);
}