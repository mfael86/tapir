## @package UFOReader
# Here, we provide the class modules.UFOReader.UFOReader which allows to
# generate .vrtx and .prop files from the Universal FeynRules Output (UFO)
# format of the Mathematica package FeynRules.

import importlib
import sys
import os
import logging
import re
import itertools as itt
from collections import Counter

from modules.feynmanRules import FeynmanRules

## A reader/parser for FeynRules UFO output files.
# The main functions are #getVertices(), #getPropagators(), #exportLagFile() and #exportCouplingsFile().

class UFOReader:
    ##  The constructor initializes all class members.
    #   @param self     object pointer
    #   @param conf     config object
    #   @return         -
    def __init__(self, conf, verbose=False):
        self.verbose = verbose
        self.conf = conf
        self.ufoDir = self.conf.ufo_indir
        self.prefix = self.conf.ufo_outdir
        self.tagFermionFlavour = self.conf.ufoTagFermionFlavour

        # Create prefix directory if necessary and add trailing '/'.
        if (self.prefix != '' and self.prefix[-1] != '/'):
            self.prefix == self.prefix + '/'
        if not os.path.exists(self.prefix):
            if (self.prefix != ''):
                os.makedirs(self.prefix)

        # Set the path to the UFO directory and remove the last character if it
        # is a '/'.
        if (self.ufoDir[-1] == '/'):
            self.ufoDir = self.ufoDir.rsplit('/', 1)[0]

        # Temporarily include ufoDir in the PATH.
        path_prefix, module_name = self.ufoDir.rsplit(
            '/', 1)[0] + '/', self.ufoDir.rsplit('/', 1)[1]
        sys.path.insert(0, path_prefix)

        # Try to import the UFO module.
        try:
            self.UFO = importlib.import_module(module_name)
            logging.info(
                ' Successfully loaded the module {} from location {}.'.format(
                    module_name, path_prefix))
        except ModuleNotFoundError:
            logging.error(
                (' {} is not a valid module. Please make sure that you '
                 'specified the correct path to your UFO module.').format(
                    self.ufoDir))
            sys.exit(1)

        # Remove the path that was temporarily added to sys.path.
        sys.path.pop(0)

        # Read the vertices, couplings, Lorentz structures, ....
        self.vertices = self.UFO.all_vertices
        self.couplings = self.UFO.all_couplings
        self.lorentz = self.UFO.all_lorentz
        self.particles = self.UFO.all_particles
        self.massiveGaugeBosons = self.__getMassiveGaugeBosons()
        # A dictionary of the form {W+: xiW, G-: xiW, a: xia}.
        self.gaugeParameters = self.__getGaugeParameters()

        # Define lists and dictionaries for writeOut.
        self.vrtx = []
        self.vrtx_lorentz = dict()
        self.vrtx_qcd = dict()
        self.vrtx_ew = dict()
        self.vrtx_qed = dict()

        self.prop = []
        self.prop_lorentz = dict()
        self.prop_qcd = dict()
        self.prop_ew = dict()
        self.prop_qed = dict()

        # Feynman Rules.
        self.FR = FeynmanRules(self.conf)

        # Counter for non-factorizing vertices (e.g. 4-gluon vertex).
        self.nonfactct = 0

        # Create a dictionary with replacements, e.g. {'W__minus__':
        # ('WL__minus__', 'WT__minus__'), 'W__plus__': ...} of "longitudinal" and
        # "transversal" components.
        self.auxBosons = self.__getAuxBosons()

    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)

    ## This extracts vertices in tapir/q2e compatible-format.
    #  @param self      object pointer
    #  @return          -
    def getVertices(self):
        logging.info('Found {} vertices.'.format(len(self.vertices)))
        self.vprint('Found {} vertices.'.format(len(self.vertices)))

        for v in self.vertices:
            fieldsUnparsed = v.particles
            color = v.color
            couplings = v.couplings
            lorentz = [l.get_all()['structure']
                       for l in self.lorentz if l in v.lorentz]

            numMassiveBosons = self.__getNumOfMassiveVectorBosons(
                fieldsUnparsed)
            fields = self.__parseFields(v.particles)
            translationDict = dict()

            # Some vertices depend on the gauge parameter xi, namely the
            # ghost-ghost-Higgs and ghost-ghost-Goldstone vertices, but
            # FeynRules does not give results for a general R_xi gauge.
            # The gauge-dependent vertices are multiplied with the following
            # variable: gaugeDependenceString.
            gaugeDependenceString = ''
            # Determine if the vertex is gauge-dependent.
            if self.__isGaugeDependent(v):
                # Determine the outgoing ghost particle.
                gaugeDependenceString = '*<gauge_parameter_{}> '.format(
                        self.__getGaugeString(v))

            for i in range(len(fieldsUnparsed)):
                translationDict[str(fieldsUnparsed[i])] = fields[i]

            if not numMassiveBosons:
                lorentz, qcd, ew, qed = self.__getVertexString(
                    fields, lorentz, color, couplings)

                # Restore the xi dependence of the gauge-dependent vertices.
                lorentz = gaugeDependenceString + lorentz

                self.__writeOutVertex(fields, lorentz, qcd, ew, qed)
            else:
                # For each massive gauge boson, create two auxiliary bosons
                # instead.
                numAuxVertices = 2**int(numMassiveBosons)

                # Generate auxiliary vertices with the longitudinal and
                # transversal components respectively.
                listOfBosons = [self.auxBosons[p.get_all()['name']]
                                for p in fieldsUnparsed if p
                                in self.massiveGaugeBosons]

                # Get all numMassiveBosons-tuples of auxiliary fields involved
                # in the vertex.
                # The itt.chain(*list) flattens the list of lists to a list.
                allCombinations = list(
                    itt.product(
                        itt.chain(
                            *listOfBosons),
                        repeat=numMassiveBosons))
                allCombinations = [list(x) for x in allCombinations]

                # Check if the auxiliary particles can be matched into the
                # vertex in the order they are appearing, e.g. for (W+ - W- -
                # Z) the combination (ZT - ZL - ZT) is not possible.
                allowedVertices = []
                for c in allCombinations:
                    vertex = []
                    allowed = True
                    indexcounter = 0
                    for field in fieldsUnparsed:
                        if field in self.massiveGaugeBosons:
                            if (c[indexcounter]
                                    in self.auxBosons[field.get_all()['name']]):
                                vertex.append(c[indexcounter])
                            else:
                                allowed = False
                            indexcounter += 1
                        else:
                            vertex.append(field)
                    if allowed:
                        allowedVertices.append(vertex)

                # Remove combinations that differ only by a permutation of the
                # auxiliary fields.
                auxDict = dict()
                for a in allowedVertices:
                    auxDict[str(sorted(Counter([str(i) for i in a])))] = a
                allAuxVertices = [auxDict[i] for i in auxDict]

                # All auxiliary vertices get the same interaction structure.
                lorentz, qcd, ew, qed = self.__getVertexString(
                        fields, lorentz, color, couplings)

                # Restore the xi dependence of the gauge-dependent vertices.
                lorentz = gaugeDependenceString + lorentz

                # Now parse the fields in each auxiliary vertex, depending on
                # whether they are auxiliary fields or not.
                for av in allAuxVertices:
                    fieldList = []
                    for f in av:
                        if str(f) in translationDict:
                            # Not an auxiliary field.
                            fieldList.append(translationDict[str(f)])
                        else:
                            # Auxiliary field.
                            fieldList.append(self.__parseElemFieldString(f))

                    self.__writeOutVertex(fieldList, lorentz, qcd, ew, qed)

        logging.info(('Expanded massive gauge bosons; there are {} vertices in '
               'total.').format(len(self.vrtx)))
        self.vprint(('Expanded massive gauge bosons; there are {} vertices in '
               'total.').format(len(self.vrtx)))

    ## This extracts the propagators in tapir/q2e-compatible format.
    #  @param self      object pointer
    #  @return          -
    def getPropagators(self):
        # Determine which particles are propagating at all.
        propagatingParticles = [
            p for p in self.particles if p.get_all()['propagating']]

        propagatorList = self.__getPropagatorList()
        logging.info(
            ('Found {} propagating particles. Propagators of massive '
             'gauge bosons will be expanded into longitudinal '
             'and transversal components.').format(
                len(propagatorList)))
        self.vprint(
            ('Found {} propagating particles. Propagators of massive '
             'gauge bosons will be expanded into longitudinal '
             'and transversal components.').format(
                len(propagatorList)))

        lorentz = ''
        qcd = ''
        ew = ''
        qed = ''

        for p in propagatorList:
            # This voodoo with strings instead of particles is necessary
            # because ['antiname'] is a string and not a reference to a
            # particle.
            lorentz, qcd, ew, qed = self.__getPropagatorStrings(p)

            if not self.__isMassiveGaugeBoson(p):
                pParsed = self.__parseFields(p[:-1]) + self.__parseSign(p[-1])
                self.__writeOutProp(pParsed, lorentz, qcd, ew, qed)
            else:
                # In this case, generate exactly two auxiliary propagators
                # [W+,W-] -> [[WT+,WT-], [WL+,WL-]].
                pNames = [[[self.auxBosons[p[i].get_all()['name']] for i in range(
                    len(p) - 1)][i][j] for i in range(2)] for j in range(2)]

                # Replace trailing +/- by p/m.
                pNames = [[self.__parseElemFieldString(f) for f in n] for n in pNames]
                # These auxiliary fields receive longitudinal and
                # transversal propagators, respectively.

                # Add the pair of fields to the FeynmanRules.gaugeProps dict.
                self.FR.gaugeProps[pNames[0][0]] = pNames[1][0]

                # First comes the longitudinal one.
                lorentz = ('*Dlong(<lorentz_index_vertex_1>,'
                           '<lorentz_index_vertex_2>,<momentum>,'
                           '<gauge_parameter_{}>'.format(
                               self.gaugeParameters[str(p[0].get_all()['name'])]))
                lorentz += (',<physical_mass_{}>)'.format(pNames[1][0]))
                self.__writeOutProp(
                    pNames[0] + self.__parseSign(p[-1]), lorentz, qcd, ew, qed)
                lorentz = ('*Dtran(<lorentz_index_vertex_1>,'
                           '<lorentz_index_vertex_2>,<momentum>,'
                           '<gauge_parameter_{}>'.format(
                               self.gaugeParameters[str(p[0].get_all()['name'])]))
                lorentz += (',<physical_mass_{}>)'.format(pNames[1][0]))
                self.__writeOutProp(
                    pNames[1] + self.__parseSign(p[-1]), lorentz, qcd, ew, qed)
                # Assume that the massive propagators do not carry color here.

    ## This writes a .lag file containing the possible vertices and
    #  propagators of the model, for use with qgraf.
    #  @param self      object pointer
    def exportLagFile(self):
        # Write the .lag file for use with qgraf
        fName = self.prefix + 'UFO.lag'
        with open(fName, 'w') as outFile:
            outFile.write('* Propagators\n')
            outFile.write('*' * 40 + '\n\n')
            for p in self.prop:
                outFile.write('[%s]\n' % ', '.join(map(str, p)))
            outFile.write('\n\n' + '*' * 40 + '\n')
            outFile.write('* Vertices\n')
            outFile.write('*' * 40 + '\n\n')
            for v in self.vrtx:
                outFile.write('[%s]\n' % ', '.join(map(str, v)))
            outFile.write('\n\n' + '*' * 40 + '\n')
        logging.info("UFO.lag file written.")
        self.vprint("UFO.lag file written.")

    ## Routine for writing the vertices and propagators to stdout.
    #  @param self      object pointer
    def writeStdOut(self):
        print('*' * 60)
        print('Propagators')
        print('*' * 60 + '\n')
        for p in self.prop:
            try:
                outString = '{' + self.__concatStringList(p[:-1]) + ':' + \
                    self.prop_lorentz[str(p)] + '|' \
                    + self.prop_qcd[str(p)] + '|' + self.prop_ew[str(p)] \
                    + '|' + self.prop_qed[str(p)] + '}'
            except KeyError:
                logging.warning(' {} could not be processed'.format(p))
                print('{} could not be processed.'.format(p))
            print(outString)

        print('*' * 60)
        print('Vertices')
        print('*' * 60 + '\n')
        for v in self.vrtx:
            try:
                outString = '{' + self.__concatStringList(v) + ':' + \
                    self.vrtx_lorentz[str(v)] + '|' \
                    + self.vrtx_qcd[str(v)] + '|' + self.vrtx_ew[str(v)] \
                    + '|' + self.vrtx_qed[str(v)] + '}'
            except KeyError:
                logging.warning(' {} could not be processed'.format(v))
                print('{} could not be processed.'.format(v))
            print(outString)

    ## This exports the couplings into two files.
    #  @param self      object pointer
    #
    #  The file UFOdecl.inc contains the declarations of all coupling names
    #  and some additional functions for use with FORM.
    #  The file UFOrepl.inc contains the final replacements of the couplings
    #  by symbols and cfunctions for use with FORM.
    def exportCouplingsFile(self):
        # Create a dict for all items that have to be declared when using FORM.
        symbolsDict = dict()
        cfunctionsDict = dict()
        couplingsDict = dict()

        # Add CF nonfactag (used for non-factorizing vertices) in any case;
        # we only need the key, not the values.
        cfunctionsDict['nonfactag'] = None

        for c in self.couplings:
            # Split the string and parse the individual bits.
            stringList = list(
                filter(
                    None,
                    re.split(
                        r'((?<=\D)\)|(?<=\W)\(|^\(|\)$|\)(?=\W)|\*\*|\*|\/| \- |\-| \+ |\+)',
                        c.get_all()['value'])))

            # Fill the dictionaries with symbols and cfunctions.
            for tinybit in stringList:
                if tinybit not in [
                        '', '(', ')', '*', '+', '-', '/', ' - ', ' + ', '^']:
                    # Determine whether it is a symbol or a cfunction.
                    tinybitParsed = self.__parseElementaryCoupling(tinybit)
                    if re.match(r'^\w*\(', tinybitParsed):
                        # Extract the function name.
                        cfName = re.split(r'\(', tinybitParsed)[0]
                        # cfunctions (we only need the keys, not the values).
                        cfunctionsDict[str(cfName)] = None
                    elif re.match(r'^\d', tinybitParsed):
                        # Ignore numbers.
                        pass
                    elif re.match(r'^\w+', tinybitParsed):
                        # Symbols (we only need the keys, not the values).
                        symbolsDict[str(tinybitParsed)] = None

            # Concatenate the list back into one single string.
            cParsedList = [
                    self.__parseElementaryCoupling(c) for c in stringList]
            renderedCoupling = ''.join(cParsedList)

            # Write to the dictionary.
            cName = self.__parseCouplingName(c.get_all()['name'])
            couplingsDict[str(cName)] = renderedCoupling

        # Open file for writing declarations.
        fNamedecl = self.prefix + 'UFOdecl.inc'
        with open(fNamedecl, 'w') as outFile:
            # Declare some CFunctions.
            for cf in cfunctionsDict:
                outFile.write('CFunction {};\n'.format(cf))
            outFile.write('\n\n')
            # Declare couplings.
            for coupling in couplingsDict:
                outFile.write('Symbol {};\n'.format(coupling))

        # Open file for writing symbol replacements.
        fNamerepl = self.prefix + 'UFOrepl.inc'
        with open(fNamerepl, 'w') as outFile:
            # Write Symbols.
            for s in symbolsDict:
                outFile.write('Symbol {};\n'.format(s))
            outFile.write('\n\n')

            # Write CFunctions.
            for cf in cfunctionsDict:
                outFile.write('CFunction {};\n'.format(cf))
            outFile.write('\n\n')

            # Replace couplings.
            for coupling in couplingsDict:
                outFile.write('id {} = {};\n'.format(coupling,
                                                    couplingsDict[coupling]))
        logging.info("Auxiliary files UFOdecl.inc and UFOrepl.inc written.")
        self.vprint("Auxiliary files UFOdecl.inc and UFOrepl.inc written.")

    ## Returns the FeynmanRules object.
    #  @param self      object pointer
    #  @return          Feynman rules (FeynmanRules object)
    def returnRules(self):
        return self.FR

    ## This routine returns a dictionary of the gauge parameter strings
    #  associated to each vector boson for the propagators, e.g.
    #  {'W-': 'xiW', 'ghG~': 'xig', 'G0': 'xiZ'}.
    #  @param self          object pointer
    #  @return              dictionary of the form described above
    def __getGaugeParameters(self):
        xiDictVBosons = dict()

        # Find all vector bosons.
        vBosons = [p for p in self.particles
                if str(p.get_all()['spin']) == '3']
        for b in vBosons:
            # For each vector boson, get the tuple (mass, color).
            combination = (b.get_all()['mass'], b.get_all()['color'])
            name = b.get_all()['name']
            for c in ['+','-','~']:
                name = name.replace(c, '')
            xiDictVBosons[str(combination)] = 'xi' + name

        # Now collect all vector bosons, Goldstones and ghosts into the dict.
        xiDict = dict()
        for p in self.particles:
            # Vector bosons.
            if (str(p.get_all()['spin']) == '3'):
                tup = (p.get_all()['mass'], p.get_all()['color'])
                xiDict[str(p.get_all()['name'])] = xiDictVBosons[str(tup)]
            # Goldstone bosons.
            # Associate the Goldstone boson to the correct vector boson.
            if (str(p.get_all()['spin']) == '1'):
                try:
                    goldstoneFlag = p.get_all()['goldstone']
                    if (str(p.get_all()['goldstone']) != '0'):
                        # Find the corresponding vector boson.
                        for part in self.particles:
                            if (str(part.get_all()['mass'])
                                    == str(p.get_all()['mass'])
                                    and str(part.get_all()['spin']) == '3'):
                                tup = (p.get_all()['mass'],
                                        p.get_all()['color'])
                                # Same gauge parameter as the vector boson.
                                xiDict[str(p.get_all()['name'])] = xiDictVBosons[str(tup)]
                except KeyError:
                    pass
            # Ghosts.
            # Associate the ghost to the correct vector boson.
            if (str(p.get_all()['spin']) == '-1'
                    and str(p.get_all()['GhostNumber']) != '0'):
                # Find the corresponding vector boson.
                for part in self.particles:
                    if (str(part.get_all()['mass']) == str(p.get_all()['mass'])
                            and str(part.get_all()['spin']) == '3'):
                        tup = (p.get_all()['mass'], p.get_all()['color'])
                        # Get the same gauge parameter as the vector boson.
                        xiDict[str(p.get_all()['name'])] = xiDictVBosons[str(tup)]
        return xiDict

    ## This checks whether a vertex is gauge-dependent, that is, whether
    #  it consists of two ghosts and one Higgs or Goldstone boson.
    #  @param self      object pointer
    #  @param v         vertex object
    #  @return          (bool) whether the vertex is gauge-dependent
    def __isGaugeDependent(self, v):
        gaugeDependent = False
        gaugeString = ''
        # Check if the vertex has exactly three fields.
        if len(v.particles) == 3:
            # Check if two particles are ghosts and one is a Higgs/Goldstone.

            # Check if there is one ghost, one anti-ghost, and one particle
            # without ghost number.
            ghostNumbers = [int(p.get_all()['GhostNumber']) for
                    p in v.particles]
            if (sorted(ghostNumbers) == [-1, 0, 1]):
                # Check if the particle without ghost number is a Higgs or
                # Goldstone particle with non-zero mass.
                for p in v.particles:
                    if (str(p.get_all()['GhostNumber']) == '0' and
                            str(p.get_all()['mass']) != 'ZERO' and
                            str(p.get_all()['spin']) == '1'):
                        # In this case, we need the gauge parameter of the
                        # outgoing ghost particle.
                        gaugeDependent = True
        return gaugeDependent

    ## This returns the gauge-parameter string for a gauge-dependent vertex.
    #  @param self      object pointer
    #  @param v         vertex object
    #  @return          (string) gauge string (e.g. 'xiW' or 'xiZ')
    def __getGaugeString(self, v):
        for p in v.particles:
            if (str(p.get_all()['GhostNumber']) == '1'):
                gaugeString = self.gaugeParameters[str(p)]
        return gaugeString

    ## This parses a string '-1' -> '-' and '+1' -> '+' for use in the .lag file.
    #  @param self          object pointer
    #  @param signString    String containing the sign
    #  @return              parsed string, i.e. '+' or '-'
    def __parseSign(self, signString):
        if (str(signString) == '-1'):
            return ['-']
        elif (str(signString) == '1'):
            return ['+']
        else:
            logging.error(' Unknown sign: {}'.format(signString))
            sys.exit(1)

    ## This checks whether a gauge boson propagator is massive.
    #  @param self      object pointer
    #  @param prop      list of objects of type particle (from UFO)
    #  @return          bool
    def __isMassiveGaugeBoson(self, prop):
        massless = True
        for f in self.particles:
            if (prop[0].get_all()['name'] == str(f.get_all()['name'])
                    and str(f.get_all()['spin']) == '3'):
                return (str(f.get_all()['mass']) != 'ZERO')

    ## This returns a string corresponding to a propagator.
    #  @param self      object pointer
    #  @param p         list of objects of type particle (from UFO)
    #  @return          tapir/q2e string of the propagator
    #
    #  The string that is returned depends on the spin (scalar, fermion,...)
    #  Massive vector bosons are dealt with separately.
    def __getPropagatorStrings(self, p):
        propStringLorentz = ''
        propStringQCD = ''
        propStringEW = ''
        propStringQED = ''

        for f in self.particles:
            if (str(p[0].get_all()['name']) == str(f.get_all()['name'])):

                # Lorentz fold first
                # Check for spin (spin number is (2s + 1), while -1 for ghosts).
                if (str(f.get_all()['spin']) == '2'):
                    # Fermions (the momentum is negative because we go opposite to the fermion line).
                    propStringLorentz = '*auxSlash(-<momentum>,<spinor_index_vertex_2>,<spinor_index_vertex_1>)'
                    # If fermion flavours should be tagged
                    if self.tagFermionFlavour:
                        fermionFlavour = self.__getFermionFlavour(f)
                        propStringLorentz += '*flavourTag(' + fermionFlavour + ',<spinor_index_vertex_2>,<spinor_index_vertex_1>)'
                elif (str(f.get_all()['spin']) == '-1'):
                    # Ghosts.
                    gaugeString = '<gauge_parameter_{}>'.format(
                        self.gaugeParameters[
                            str(f.get_all()['name'])])
                    propStringLorentz = '*Dghost(<momentum>,{})'.format(
                            gaugeString)
                elif (str(f.get_all()['spin']) == '1'):
                    # Scalar particles.

                    # The key 'goldstoneboson' is False for all particles,
                    # regardless of whether they are Goldstone bosons or not.
                    # The key 'goldstone' exists only for Goldstone bosons,
                    # for other particles it is not defined.
                    try:
                        goldstoneflag = f.get_all()['goldstone']
                        if (str(goldstoneflag) != '0'):
                            # Goldstone bosons.
                            gaugeString = '<gauge_parameter_{}>'.format(
                                self.gaugeParameters[
                                    str(f.get_all()['name'])])
                            propStringLorentz = '*Dgoldst(<momentum>,{})'.format(
                                    gaugeString)
                    except KeyError:
                        # Scalars with gauge-independent mass.
                        propStringLorentz = '*DH(<momentum>)'
                elif (str(f.get_all()['spin']) == '3'):
                    if (str(f.get_all()['mass']) == 'ZERO'):
                        # Do the massless bosons first.
                        gaugeString = '<gauge_parameter_{}>'.format(
                            self.gaugeParameters[
                                str(f.get_all()['name'])])
                        if (str(f.get_all()['color']) == '1'):
                            # Photons etc.
                            propStringLorentz = ('*Dph('
                                                 '<lorentz_index_vertex_2>,'
                                                 '<lorentz_index_vertex_1>,'
                                                 '<momentum>,{})'.format(
                                                     gaugeString))
                        else:
                            # Gluons etc.
                            propStringLorentz = ('*Dgl('
                                                 '<lorentz_index_vertex_2>,'
                                                 '<lorentz_index_vertex_1>,'
                                                 '<momentum>,{})'.format(
                                                     gaugeString))
                    else:
                        # Massive vector bosons are treated in
                        # getMassivePropagatorStrings().
                        pass
                else:
                    # Spin > 1 -> not implemented yet -> throw error.
                    logging.error(
                        (' Propagators for particles with spin (2s+1) >= 3 '
                         'have not yet been implemented.'))
                    sys.exit(1)

                # Then the colour part
                if (str(f.get_all()['color']) == '3'):
                    propStringQCD = ('*d_(<spinor_index_vertex_2>,'
                                     '<spinor_index_vertex_1>)')
                elif (str(f.get_all()['color']) == '1'):
                    propStringQCD = ''
                elif (str(f.get_all()['color']) == '8'):
                    propStringQCD = ('*prop(<colour_index_vertex_2>,'
                                     '<colour_index_vertex_1>)')
                else:
                    logging.warning(' Only colour singlet, triplet and octet propagators have been implemented.')
                    sys.exit(1)

        return propStringLorentz, propStringQCD, propStringEW, propStringQED

    ## This returns a list such as ['ftopq', 'fTopq, '-1'].
    #  for all propagating particles
    #  @param self      object pointer
    #  @return          list of propagators of all propagating particles
    #
    #  Propagators are always in the form [particle, antiparticle, spinflag].
    def __getPropagatorList(self):
        propagatorListFull = []

        for p in self.particles:
            if p.get_all()['propagating']:
                spinFlag = 0
                if (p.get_all()['spin'] > 0):
                    # Resolve the 2s+1 notation.
                    spinFlag = (-1)**(p.get_all()['spin'] - 1)
                elif (str(p.get_all()['spin']) == '-1'):
                    # Ghosts.
                    spinFlag = -1
                else:
                    logging.error(
                        (' Encountered propagating particle with spin (2s+1) '
                         '= {}. Do not know what to do.').format(
                            p.get_all()['spin']))
                    sys.exit(1)
                # In both cases, get the name of the particle and antiparticle.
                pName = p.get_all()['name']
                apName = p.get_all()['antiname']

                # If the particles have a PDG code, move the one with positive
                # PDG code to the left, so that the propagator looks like
                # [particle, antiParticle, +-1].
                try:
                    pdg = p.get_all()['pdg_code']
                    if pdg < 0:
                        # Switch the particles if the antiparticle is the first
                        # particle of the propagator.
                        temp = pName
                        pName = apName
                        apName = temp
                except BaseException:
                    pass

                # Translate this into particles again.
                particle = 0
                antiparticle = 0
                for f in self.particles:
                    if (pName == str(f.get_all()['name'])):
                        particle = f
                    if (apName == str(f.get_all()['name'])):
                        antiparticle = f

                propagatorListFull.append([particle, antiparticle, spinFlag])

        # Remove duplicates (e.g. ['W+', 'W-', 1], ['W-', 'W+', 1]).
        propDict = dict()
        for prop in propagatorListFull:
            propDict[str(sorted(Counter([str(i) for i in prop])))] = prop
        propagatorList = [propDict[p] for p in propDict]

        return propagatorList

    ## This counts how many massive vector bosons enter a vertex.
    #  @params self     object pointer
    #  @params fields   list of elements of type particle (UFO)
    #  @return          number of massive vector bosons of the vertex (int)
    def __getNumOfMassiveVectorBosons(self, fields):
        # Count the number of massive gauge bosons in the vertex.
        numMassive = len(
            fields) - len([f for f in fields if
                           f not in self.massiveGaugeBosons])
        return numMassive

    ## This generates the Lorentz, QCD, EW, and QED parts of the vertex string.
    #  @param self          object pointer
    #  @param fields        a list of strings (result of parsing particles)
    #  @param lorentz       the lorentz structure extracted from the UFO vertex
    #  @param qcd           the color structure extracted from the UFO vertex
    #  @param couplings     the couplings extracted from the UFO vertex
    #  @return              tapir/q2e string of the vertex
    def __getVertexString(self, fields, lorentz, color, couplings):
        # By default, ewString and qedString are not used.
        lorentzString = ''
        colorString = ''
        ewString = ''
        qedString = ''

        if len(color) != 1:
            # The case where color does not factorize needs special care (this
            # should only be the four-gluon vertex in the Standard Model).
            logging.info(
                (' Color does not factorize for the following vertex: {}'
                 '.').format(fields))
            # Tag the vertices where color does not factorize.
            colorStrings = [self.__parseColor(color[i]) + '*nonfactag({},{},<local_index_F>)'.format(self.nonfactct, i)
                    for i in range(len(color))]
            colorString = '*('
            for col in colorStrings:
                colorString += (' ' + col + ' + ')
            # Remove the trailing '+'.
            colorString = colorString.rstrip('+ ')
            colorString += ')'

            lorentzString = '*('
            for c in couplings:
                # The couplings c are tuples, indicating which element of color
                # should be multiplied by which element from lorentz.
                i, j = c
                # IMPORTANT: Do not remove the spaces surrounding the '('
                # and ')' in the strings here!
                lorentzPart = self.__parseLorentz(
                    ' ( ' + str(lorentz[j]) + ' ) ')
                lorentzString += (' ' + self.__parseCouplingName(str(
                    couplings[c])) + ' *' + lorentzPart + '*nonfactag({},{},<local_index_F>)'.format(self.nonfactct, i)
                    + ' + ')
            # Remove the trailing '+'.
            lorentzString = lorentzString.rstrip('+ ')
            lorentzString += ')'
            # Increase counter of nonfactorizing vertices.
            self.nonfactct += 1

        else:
            # For all other vertices, color can be factorized out.
            colorStringr = self.__parseColor(color[0])
            if (colorStringr != ''):
                colorString = '*(' + colorStringr + ')'
            lorentzString = '*('
            for c in couplings:
                # The couplings c are tuples, indicating which element of color
                # should be multiplied by which element from lorentz.
                i, j = c
                if i != 0:
                    logging.error(
                        (' Encountered an index != 0 where the index should '
                         'be 0. This should not have occured. Handle '
                         'the output with care! Vertex: {}.').format(fields))
                    sys.exit(1)
                # IMPORTANT: Do not remove the spaces surrounding the '('
                # and ')' in the strings here!
                lorentzPart = self.__parseLorentz(
                    ' ( ' + str(lorentz[j]) + ' ) ')
                lorentzString += ' ' + self.__parseCouplingName(str(
                    couplings[c])) + ' *' + lorentzPart + ' + '
            # Remove the trailing '+'.
            lorentzString = lorentzString.rstrip('+ ')
            lorentzString += ')'
        return lorentzString, colorString, ewString, qedString

    ## This parses a list of fields (particles).
    #  @param self          object pointer
    #  @param fieldList     list of particle objects
    #  @return              list of parsed fields (list of string)
    def __parseFields(self, fieldList):
        fieldListParsed = [self.__parseElemField(field) for field in fieldList]
        return fieldListParsed

    ## This parses a single field (particle).
    #  @param self      object pointer
    #  @param field     particle (UFO) object
    #  @return          parsed elementary field (string)
    #
    #  This routine replaces trailing +/- by p/m and removes trailing ~
    #  (indicating antiparticles) in favour of a capital first letter.
    #  If the particle is a fermion, an 'f' will be prepended, and a
    #  'q' is appended if the particle is a quark.
    def __parseElemField(self, field):
        fieldprops = field.get_all()

        retString = ''
        prefix = ''
        fieldName = ''
        suffix = ''

        # First, replace all special characters by their spelt-out version except
        # if they occur in the last position
        specialChars = {'+': 'plus', '-': 'minus', '~': 'tilde'}
        fieldName = fieldprops['name']
        for c in specialChars.keys():
            fieldName = fieldName[:-1].replace(c, specialChars[c]) + fieldName[-1]

        # Determine if it is a fermionic field.
        if (str(fieldprops['spin']) == '2'):
            # Fermions receive a prefix 'f'.
            prefix = 'f'
            if re.search('~$', fieldName):
                if fieldName[0].upper() != fieldName[0]:
                    fieldName = fieldName[0].upper(
                    ) + fieldName[1:-1]
                else:
                    fieldName = fieldName[:-1] + 'tilde'
            elif re.search(r'\+$', fieldName):
                if fieldName[0].upper() != fieldName[0]:
                    fieldName = fieldName[0].upper(
                    ) + fieldName[1:-1]
                else:
                    fieldName = fieldName[:-1] + 'plus'
            elif re.search(r'\-$', fieldName):
                fieldName = fieldName[:-1]
            elif re.search('^v*[^ ~]$', fieldName):
                fieldName = fieldName
            else:
                # Anything else is just taken as is.
                fieldName = fieldName

        # Determine if it is a scalar particle.
        elif (str(fieldprops['spin']) == '1'):
            # In this case, only replace '+' and '-' at the end of the name
            # string by letters.
            if re.search(r'\+$', fieldName):
                fieldName = fieldName[:-1] + 'p'
            elif re.search(r'\-$', fieldName):
                fieldName = fieldName[:-1] + 'm'
            elif re.search('~$', fieldName):
                if fieldName[0].upper() != fieldName[0]:
                    fieldName = fieldName[0].upper(
                    ) + fieldName[1:-1]
                else:
                    fieldName = fieldName[:-1] + 'tilde'
            elif re.search('^v*[^ ~]$', fieldName):
                fieldName = fieldName
            else:
                fieldName = fieldName

        # Determine if it is a ghost.
        elif (str(fieldprops['spin']) == '-1'):
            if re.search('~$', fieldName):
                # If the 'name' ends with ~, put a prefix 'C' in front and
                # remove the ~.
                prefix = 'C'
                fieldName = fieldName[:-1]
            else:
                # Otherwise, prefix 'c' and do not remove the last character.
                prefix = 'c'
                fieldName = fieldName

        # Determine if it is a vector particle.
        elif (str(fieldprops['spin']) == '3'):
            if (str(fieldprops['mass']) == 'ZERO'):
                # Massless vector bosons.
                if re.search('~$', fieldName):
                    if fieldName[0].upper() != fieldName[0]:
                        fieldName = fieldName[0].upper(
                        ) + fieldName[1:-1]
                    else:
                        fieldName = fieldName[:-1] + 'tilde'
                elif re.search(r'\+$', fieldName):
                    fieldName = fieldName[:-1] + 'p'
                elif re.search(r'\-$', fieldName):
                    fieldName = fieldName[:-1] + 'm'
                else:
                    fieldName = fieldName
            else:
                # Massive vector bosons will be treated separately.
                pass
        else:
            if re.search('~$', fieldName):
                if fieldName[0].upper() != fieldName[0]:
                    fieldName = fieldName[0].upper(
                    ) + fieldName[1:-1]
                else:
                    fieldName = fieldName[:-1] + 'tilde'
            elif re.search(r'\+$', fieldName):
                fieldName = fieldName[:-1] + 'p'
            elif re.search(r'\-$', fieldName):
                fieldName = fieldName[:-1] + 'm'
            else:
                fieldName = fieldName
            # In any case, issue a warning.
            logging.warning(
                (' Encountered field {} with spin {}, which was parsed, but '
                 'the output might be incorrect.').format(
                    fieldName, fieldprops['spin']))

        retString = prefix + fieldName + suffix
        return retString

    ## This parses a Lorentz string as extracted from UFO.
    #  @param self              object pointer
    #  @param lorentzString     Lorentz string from UFO
    #  @return                  list of parsed Lorentz strings
    #
    #  The string is decomposed into the smallest bits and each tiny bit
    #  is parsed separately.
    def __parseLorentz(self, lorentzString):
        stringList = list(filter(None, re.split(
            r'( \( | \) |\*| \+ | \- |/|(?<=\W)\((?=\w))|(?<=\))(\))|(\()(?=\()',
            lorentzString)))
        # Parse the individual pieces.
        stringListParsed = [str(self.__parseElementaryLorentzString(part))
                            for part in stringList]
        # Concatenate the pieces again to obtain the parsed string.
        parsedString = ''.join(stringListParsed)
        return parsedString

    ## This parses a color string as extracted from UFO.
    #  @param self              object pointer
    #  @param colorString       color string from UFO
    #  @return                  list of parsed color strings
    #
    #  The string is decomposed into the smallest bits and each tiny bit
    #  is parsed separately.
    def __parseColor(self, colorString):
        stringList = list(filter(None, re.split(
            r'( \( | \) |\*| \+ | \- |/|(?<=\W)\((?=\w))|(?<=\))(\))|(\()(?=\()',
            colorString)))
        # Parse the individual pieces.
        stringListParsed = [str(self.__parseElementaryColorString(part))
                            for part in stringList]
        # Concatenate the pieces again to obtain the parsed string.
        parsedString = ''.join(stringListParsed)
        return parsedString

    ## This parses a single bit of a Lorentz string.
    #  @param self          object pointer
    #  @param elemString    part of a Lorentz string, e.g. '(', 'Metric(1,2)'
    #  @return              parsed string
    def __parseElementaryLorentzString(self, elemString):
        retString = ''

        if elemString in ['*', ' * ', ' + ', ' - ',
                          ' ( ', ' ) ', '(', ')', '/']:
            # Leave the binary operators unchanged.
            retString = elemString
        elif elemString[0].isdigit():
            # If the first character is a digit, then probably the whole string
            # is a number -> leave it unchanged except for trailing .
            retString = elemString.replace('.', '')
        elif re.match(r'Identity\(', elemString):
            # Spinorial Kronecker.
            retString += 'd_('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j = re.search(r'\((.*?)\)', elemString).group(1).rsplit(',')
            retString += '<spinor_index_particle_' + str(i) + '>,'
            retString += '<spinor_index_particle_' + str(j) + '>'
            retString += ')'
        elif re.match(r'Metric\(', elemString):
            # Metric tensor.
            retString += 'd_('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j = re.search(r'\((.*?)\)', elemString).group(1).rsplit(',')
            retString += '<lorentz_index_particle_' + str(i) + '>,'
            retString += '<lorentz_index_particle_' + str(j) + '>'
            retString += ')'
        elif re.match(r'Gamma\(', elemString):
            # Gamma matrices.
            retString += 'auxGamma('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            mu, i, j = re.search(r'\((.*?)\)', elemString).group(1).rsplit(',')
            retString += '<lorentz_index_particle_' + str(mu) + '>,'
            retString += '<spinor_index_particle_' + str(i) + '>,'
            retString += '<spinor_index_particle_' + str(j) + '>'
            retString += ')'
        elif re.match(r'Gamma5\(', elemString):
            # Gamma 5 matrix.
            retString += 'auxGamma5('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j = re.search(r'\((.*?)\)', elemString).group(1).rsplit(',')
            retString += '<spinor_index_particle_' + str(i) + '>,'
            retString += '<spinor_index_particle_' + str(j) + '>'
            retString += ')'
        elif re.match(r'Epsilon\(', elemString):
            logging.warning(
                (' Encountered a Levi-Civita tensor with four indices. It '
                 'will be parsed as auxEps(...). Be careful with this!'))
            # Four-index epsilon tensor.
            retString += 'auxEps('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            mu, nu, rho, sigma = re.search(
                r'\((.*?)\)', elemString).group(1).rsplit(',')
            retString += '<lorentz_index_particle_' + str(mu) + '>,'
            retString += '<lorentz_index_particle_' + str(nu) + '>,'
            retString += '<lorentz_index_particle_' + str(rho) + '>,'
            retString += '<lorentz_index_particle_' + str(sigma) + '>'
            retString += ')'
        elif re.match(r'P\(', elemString):
            # Momentum component j of particle N.
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            j, N = re.search(r'\((.*?)\)', elemString).group(1).rsplit(',')
            retString += 'Mom(<lorentz_index_particle_' + str(j) + '>,<outgoing_momentum_' + \
                str(N) + '>)'
        elif re.match(r'ProjP\(', elemString):
            # Chiral projector 1/2 * (1 + gamma_5).

            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j = re.search(r'\((.*?)\)', elemString).group(1).rsplit(',')
            retString += 'auxPR('

            retString += '<spinor_index_particle_' + str(i) + '>,'
            retString += '<spinor_index_particle_' + str(j) + '>'
            retString += ')'
        elif re.match(r'ProjM\(', elemString):
            # Chiral projector 1/2 * (1 - gamma_5).

            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j = re.search(r'\((.*?)\)', elemString).group(1).rsplit(',')
            retString += 'auxPL('

            retString += '<spinor_index_particle_' + str(i) + '>,'
            retString += '<spinor_index_particle_' + str(j) + '>'
            retString += ')'
        elif re.match(r'Sigma\(', elemString):
            # Sigma matrices.
            retString += 'auxSigma('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            mu, nu, i, j = re.search(
                r'\((.*?)\)', elemString).group(1).rsplit(',')
            retString += '<lorentz_index_particle_' + str(mu) + '>,'
            retString += '<lorentz_index_particle_' + str(nu) + '>,'
            retString += '<spinor_index_particle_' + str(i) + '>,'
            retString += '<spinor_index_particle_' + str(j) + '>'
            retString += ')'
        elif re.match(r'C\(', elemString):
            # Charge conjugation matrix.
            retString += 'auxCC('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j = re.search(r'\((.*?)\)', elemString).group(1).rsplit(',')
            retString += '<spinor_index_particle_' + str(i) + '>,'
            retString += '<spinor_index_particle_' + str(j) + '>'
            retString += ')'
        elif re.match(r'-', elemString):
            retString += '-'
        else:
            logging.error(
                ' Encountered unknown Lorentz structure: {}'.format(
                    elemString))
            sys.exit(1)
        return retString

    ## This parses the UFO color part of a vertex.
    #  @param self          object pointer
    #  @param colorString   UFO color string
    #  @return              parsed color string
    def __parseElementaryColorString(self, colorString):
        # Parse the color strings from FeynRules to tapir/q2e format.
        retString = ''

        if colorString in ['*', ' * ', ' + ', ' - ',
                          ' ( ', ' ) ', '(', ')', '/']:
            # Leave the binary operators unchanged.
            retString = colorString
        elif colorString[0].isdigit():
            # If the first character is a digit, then probably the whole string
            # is a number -> leave it unchanged except for trailing .
            retString = colorString.replace('.', '')
        elif (colorString == '1'):
            # Color singlets.
            pass
        elif re.match(r'Identity\(', colorString):
            # Couplings of color fundamentals to singlets (e.g. ubar-u-W).
            retString += 'd_('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j = re.search(r'\((.*?)\)', colorString).group(1).rsplit(',')
            retString += '<spinor_index_particle_' + str(i) + '>,'
            retString += '<spinor_index_particle_' + str(j) + '>'
            retString += ')'
        elif re.match(r'T\(', colorString):
            # Coupling of color fundamentals to color adjoints.
            retString += 'GM('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j, k = re.search(r'\((.*?)\)', colorString).group(1).rsplit(',')
            retString += '<colour_index_particle_' + str(i) + '>,'
            retString += '<spinor_index_particle_' + str(k) + '>,'
            retString += '<spinor_index_particle_' + str(j) + '>'
            retString += ')'
        elif re.match(r'f\(', colorString):
            # Couplings of three color adjoints.
            retString += 'ufocomplex(0,1)*V3g('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j, k = re.search(r'\((.*?)\)', colorString).group(1).rsplit(',')
            retString += '<colour_index_particle_' + str(i) + '>,'
            retString += '<colour_index_particle_' + str(j) + '>,'
            retString += '<colour_index_particle_' + str(k) + '>'
            retString += ')'
        elif re.match(r'd\(', colorString):
            # Symmetric color tensors.
            retString += 'Vsymmg('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j, k = re.search(r'\((.*?)\)', colorString).group(1).rsplit(',')
            retString += '<colour_index_particle_' + str(i) + '>,'
            retString += '<colour_index_particle_' + str(j) + '>,'
            retString += '<colour_index_particle_' + str(k) + '>'
            retString += ')'
        elif re.match(r'Epsilon(', colorString):
            retString += 'ColEps('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j, k = re.search(r'\((.*?)\)', colorString).group(1).rsplit(',')
            retString += '<spinor_index_particle_' + str(i) + '>,'
            retString += '<spinor_index_particle_' + str(j) + '>,'
            retString += '<spinor_index_particle_' + str(k) + '>'
            retString += ')'
        elif re.match(r'EpsilonBar(', colorString):
            retString += 'ColEpsBar('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j, k = re.search(r'\((.*?)\)', colorString).group(1).rsplit(',')
            retString += '<spinor_index_particle_' + str(i) + '>,'
            retString += '<spinor_index_particle_' + str(j) + '>,'
            retString += '<spinor_index_particle_' + str(k) + '>'
            retString += ')'
        elif re.match(r'T6(', colorString):
            retString += 'Tsix('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j, k = re.search(r'\((.*?)\)', colorString).group(1).rsplit(',')
            retString += '<colour_index_particle_' + str(i) + '>,'
            retString += '<colour_index_particle_' + str(j) + '>,'
            retString += '<colour_index_particle_' + str(k) + '>'
            retString += ')'
        elif re.match(r'K6(', colorString):
            retString += 'Ksix('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j, k = re.search(r'\((.*?)\)', colorString).group(1).rsplit(',')
            retString += '<colour_index_particle_' + str(i) + '>,'
            retString += '<spinor_index_particle_' + str(j) + '>,'
            retString += '<spinor_index_particle_' + str(k) + '>'
            retString += ')'
        elif re.match(r'K6Bar(', colorString):
            retString += 'KsixBar('
            # Extract the arguments between parentheses (there should only be
            # one pair of parentheses).
            i, j, k = re.search(r'\((.*?)\)', colorString).group(1).rsplit(',')
            retString += '<colour_index_particle_' + str(i) + '>,'
            retString += '<spinor_index_particle_' + str(j) + '>,'
            retString += '<spinor_index_particle_' + str(k) + '>'
            retString += ')'
        else:
            logging.error(
                ' Encountered unknown color combination: {}.'.format(
                    colorString))
            sys.exit(1)
        return retString

    ## This parses the UFO couplings (removes underscores and prefixes them with 'ufo').
    #  @param self          object pointer
    #  @param couplingName  string containing the name of the coupling
    #  @return              the same string with '_' removed
    def __parseCouplingName(self, couplingName):
        # Remove underscores from coupling names to avoid problems with FORM
        # Also prefix 'ufo' to avoid name clashes.
        retString = 'ufo' + couplingName.replace('_', '')
        return retString

    def __getFermionFlavour(self, p):
        pName = p.get_all()['name']
        pNameParsed = self.__parseElemField(p)
        return pNameParsed

    ## This generates the auxiliary bosons for massive vector bosons.
    #  @param self      object pointer
    #  @return          a dictionary containing the mapping of
    #                   massive vector bosons to auxiliary bosons
    def __getAuxBosons(self):
        auxBosons = dict()
        for GB in self.massiveGaugeBosons:
            name = GB.get_all()['name']
            aux1 = name
            aux2 = name
            if re.search(r'\+$', name):
                # W+ -> (WL+, WT+).
                aux1 = re.sub(r'\+$', 'L+', aux1)
                aux2 = re.sub(r'\+$', 'T+', aux2)
            elif re.search(r'\-$', name):
                # W- -> (WL-, WT-).
                aux1 = re.sub(r'\-$', 'L-', aux1)
                aux2 = re.sub(r'\-$', 'T-', aux2)
            else:
                # Uncharged bosons simply get a suffix.
                aux1 += 'L'
                aux2 += 'T'
            auxBosons[name] = [aux1, aux2]
        return auxBosons

    ## This parses the string of an elementary field.
    #  @param self          object pointer
    #  @param fieldString   string containing the field name
    #  @return              parsed string
    #
    #  This only applies to the name strings of the auxiliary bosons.
    def __parseElemFieldString(self, fieldString):
        # Replaces '+', '-' at the end of the name of the auxiliary fields.
        retString = ''
        if re.search(r'\+$', fieldString):
            retString += fieldString[:-1] + 'p'
        elif re.search(r'\-$', fieldString):
            retString += fieldString[:-1] + 'm'
        else:
            retString = fieldString
        return retString

    ## This returns a list containing the massive vector bosons.
    #  @param self      object pointer
    #  @return          list containing particle objects
    def __getMassiveGaugeBosons(self):
        massiveList = []
        # Returns a list of all massive gauge bosons (those have
        # gauge-dependent propagators that need to be split).
        for particle in self.particles:
            if (str(particle.get_all()['spin']) == '3' and str(
                    particle.get_all()['mass']) != 'ZERO'):
                massiveList.append(particle)
                logging.info('Found massive vector boson {} which will be'
                        ' expanded in transversal and longitudinal components:'
                        ''.format(particle.get_all()['name']))
                self.vprint('Found massive vector boson {} which will be'
                        ' expanded in transversal and longitudinal components:'
                        ''.format(particle.get_all()['name']))
        return massiveList

    ## This writes a vertex into dicts and FeynmanRules.
    #  @param self      object pointer
    #  @param fields    list containing field strings, e.g. ['fe','fe','ZT']
    #  @param lorentz   the Lorentz string of the vertex
    #  @param qcd       the QCD/color string of the vertex
    #  @param ew        the electroweak string of the vertex
    #  @param qed       the QED string of the vertex
    #  @return          -
    def __writeOutVertex(self, fields, lorentz, qcd, ew, qed):
        fields_tuple = tuple(fields)
        self.vrtx.append(fields_tuple)
        self.vrtx_lorentz[fields_tuple] = []
        self.vrtx_qcd[fields_tuple] = []
        self.vrtx_ew[fields_tuple] = []
        self.vrtx_qed[fields_tuple] = []
        self.vrtx_lorentz[fields_tuple].append(lorentz)
        self.vrtx_qcd[fields_tuple].append(qcd)
        self.vrtx_ew[fields_tuple].append(ew)
        self.vrtx_qed[fields_tuple].append(qed)
        # Write to FeynmanRules.
        self.FR.vrtxLorentz[fields_tuple] = []
        self.FR.vrtxQCD[fields_tuple] = []
        self.FR.vrtxEW[fields_tuple] = []
        self.FR.vrtxQED[fields_tuple] = []
        self.FR.vrtxLorentz[fields_tuple].append(lorentz)
        self.FR.vrtxQCD[fields_tuple].append(qcd)
        self.FR.vrtxEW[fields_tuple].append(ew)
        self.FR.vrtxQED[fields_tuple].append(qed)

    ## This writes a propagator into dicts and FeynmanRules.
    #  @param self      object pointer
    #  @param fields    a list of the form ['part', 'antipart', '+/-']
    #  @param lorentz   the Lorentz string of the propagator
    #  @param qcd       the QCD/color string of the propagator
    #  @param ew        the electroweak string of the propagator
    #  @param qed       the QED string of the propagator
    #  @return          -
    def __writeOutProp(self, fields, lorentz, qcd, ew, qed):
        fields_tuple = tuple(fields)
        self.prop.append(fields_tuple)
        self.prop_lorentz[fields_tuple] = []
        self.prop_qcd[fields_tuple] = []
        self.prop_ew[fields_tuple] = []
        self.prop_qed[fields_tuple] = []
        self.prop_lorentz[fields_tuple].append(lorentz)
        self.prop_qcd[fields_tuple].append(qcd)
        self.prop_ew[fields_tuple].append(ew)
        self.prop_qed[fields_tuple].append(qed)
        # Write to the Feynman Reader.
        fields_tuple_without_pm = tuple(fields[:-1])
        self.FR.propLorentz[fields_tuple_without_pm] = []
        self.FR.propQCD[fields_tuple_without_pm] = []
        self.FR.propEW[fields_tuple_without_pm] = []
        self.FR.propQED[fields_tuple_without_pm] = []
        self.FR.propLorentz[fields_tuple_without_pm].append(lorentz)
        self.FR.propQCD[fields_tuple_without_pm].append(qcd)
        self.FR.propEW[fields_tuple_without_pm].append(ew)
        self.FR.propQED[fields_tuple_without_pm].append(qed)

    ## This concatenates the string objects of a list to one single string.
    #  @param self      object pointer
    #  @param l         list of strings
    #  @return          the concatenated string
    #
    #  e.g. ['fe','fE','WTp'] -> 'fe,fE,WTp'.
    def __concatStringList(self, l):
        retString = ''
        for elem in l:
            retString += str(elem) + ','
        retString = retString[:-1]
        return retString

    ## This parses a tiny bit of a coupling string.
    #  @param self          object pointer
    #  @param elemString    elementary String, e.g. '(', 'complex(0,1)',...
    def __parseElementaryCoupling(self, elemString):
        retString = ''
        if re.match(r'^cmath\.', elemString):
            # Discard cmath prefix for use with FORM.
            retString = elemString.rsplit('.')[-1]
        elif re.match(r'^(\*\*)', elemString):
            # Replace ** by ^.
            retString = '^'
        elif elemString[0].isdigit():
            # Replace trailing dots: 2. -> 2
            retString = elemString.replace('.', '')
        else:
            # Other strings are returned unchanged.
            retString = elemString
        # Create ufo 'namespace'.
        if re.match(r'^[a-zA-Z]', retString):
            retString = 'ufo' + retString
        # If it is a function, e.g. complexconjugate(CKM1x1), replace
        # CKM1x1 -> ufoCKM1x1.
        retString = re.sub(r'\(([a-zA-Z]\w*)', r'(ufo\1', retString)
        return retString
