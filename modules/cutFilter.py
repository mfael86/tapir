## @package cutFilter
# This module contains the modules.cutFilter.CutFilter class.
# The filter is used to extract diagrams with specific features due to their possible "cut" properties following the so-called Cutkosky rules.
# This class is mainly an implementation of the algorithm of Chapter 3.2. of [[1]](@ref refs) with different data-structures and in more generalized form.
# #modules.cutFilter.CutFilter::filter() is the main method, where
# #modules.cutFilter.CutFilter::parseFilterOptions() is called to bring the filter options in a testable form. Afterwards,
# #modules.cutFilter.CutFilter::matchesCutConstraints() is evoked where the main part from Hoff's algorithm resides.
# Especially interesting might be the ancillary static method #modules.cutFilter.CutFilter::findNumberOfConnectedComponents() which returns the number of unconnected sub-graphs (connected components) of a combination of edges.
#
# References: \anchor refs
# -----------
# * [[1]](@ref refs) Jens Hoff, PhD thesis, 2015 : "Methods for Multiloop Calculations and Higgs Boson Production at the LHC"
# * [[2]](@ref refs) https://www.geeksforgeeks.org/program-to-count-number-of-connected-components-in-an-undirected-graph/ (accessed 07.09.2021)


import logging
import signal
import re
import multiprocessing as mp
import time
import itertools
from typing import List
from modules.diagram import Diagram
from modules.filter import Filter
from modules.topoAnalyzer import BridgeFinder


## A Filter class derivative which filters Cutkosky cut allowed or disallowed diagrams from a modules.diagram.Diagram list.
# The main function is #filter(), where #parseFilterOptions() is called first and #matchesCutConstraints() is used
# to check for matching filter conditions subsequently for every diagram.
# The algorithms used here stem from [[1]](@ref refs) Chapter 3.2.
class CutFilter(Filter):
    ## Constructor
    def __init__(self, conf, verbose=False, kernels=1):
        ## Local modules.config.Conf instance
        self.conf = conf
        ## verbose flag to print progress
        self.verbose = verbose
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels


    ## Verbose printing method
    def vprint(self, *printStuff, **etc):
        if self.verbose:
            print(*printStuff, **etc)


    ## Apply the cut filter to the given diagrams and sort the ones out that do not comply with the filter conditions
    # @param diagrams List of modules.diagram.Diagram instances we want to filter
    # \return \p diagrams where the appropriate cut restrictions match
    def filter(self, diagrams: List[Diagram], filterOptions=[]) -> List[Diagram]:
        # Skip trivial diagrams list
        if len(diagrams) == 0:
            return []
        # Bring filter options to a correct form. 
        # Use first diagram's external momenta since it is assumed that all diagrams have the same external states.
        filterOptions = self.parseFilterOptions(filterOptions=filterOptions, externalMomenta=[mom[0] for mom in diagrams[0].externalMomenta])

        # List of boolean values indicating for each diagram in diagrams if the cut filter conditions were met
        constraintsValidList = []

        # Apply the cut filter in the main thread for all diagrams
        if self.kernels == 1:
            for i,d in enumerate(diagrams):
                self.vprint("  > cuts: {0:^7}/{1:^7}\r".format(i, len(diagrams)), end = "")
                # Check for valid cut constraints
                constraintsValidList.append(self.matchesCutConstraints(d,filterOptions))
        
        # Apply the cut filter for all diagrams in parallel
        else:
             # Initializer function for each worker in the multithread pool
            def initializer():
                # Ignore CTRL+C in the worker process
                signal.signal(signal.SIGINT, signal.SIG_IGN)

            if self.kernels == 0:
                pool = mp.Pool(mp.cpu_count(),initializer=initializer)
            else:
                pool = mp.Pool(self.kernels,initializer=initializer)

            # Start asynchronous workers
            result = pool.starmap_async(self.matchesCutConstraints, [[d,filterOptions] for d in diagrams], chunksize=1)

            # Print progress with actualization rate of 10 Hz
            while not result.ready():
                self.vprint("  > cuts: {0:^7}/{1:^7}\r".format(len(diagrams)-result._number_left, len(diagrams)), end = "")
                time.sleep(0.1)
            pool.close()
            pool.join()
            # Read results
            constraintsValidList = result.get()

        # Return only those diagrams with valid cut conditions
        return [diagrams[i] for i,cutConditionMet in enumerate(constraintsValidList) if cutConditionMet]


    ## Bring a bunch of filter options to a further usable form and exit with an error for bad input.
    # The argument filterOptions is a list of multiple entries of the following type:
    #  ["1" , "q1,q2" , "ft" , "0,0, 1,14"]
    # The arguments are described in the user manual.
    # Note that exclusive interval options are translated to inclusive ones.
    # Also, empty cut intervals are expanded (i.e. replaced by their numeric representation).
    #
    # @param filterOptions List of filter options with the four defined entries
    # @param externalMomenta List of all external momenta defined as incoming, e.g. ["q1", "-q2"]
    # \return parsed and checked version of \p filterOptions
    def parseFilterOptions(self, filterOptions, externalMomenta):
        # Check for valid filter options and straighten the filterOptions entries
        for option in filterOptions:
            # Check first argument
            if re.sub(r'\s', r'', option[0].lower()) in ["1", "true", "t"]:
                option[0] = True
            elif re.sub(r'\s', r'', option[0].lower()) in ["0", "false", "f"]:
                option[0] = False
            else:
                logging.error(F"Invalid filter option \"{option}\". First argument must be 1/true/t/0/false/f.")
                exit(1)
            # Check second argument
            option[1] = re.sub(r'\s', r'', option[1]).split(",")
            # Check for incoming external momenta if no argument was given
            if option[1] == [""]:
                option[1] = [q for q in externalMomenta if q[0] != "-"]
            for q in option[1]:
                # Check for q in external momenta of diagrams
                if q not in [re.sub(r'-', r'', mom) for mom in externalMomenta]:
                    logging.error(F"Invalid filter option \"{option}\". Second argument must be a comma separated list of external momenta. \"{q}\" is invalid.")
                    exit(1)
            # Straighten third argument
            option[2] = re.sub(r'\s', r'', option[2]).split(",")
            if option[2] == [""]:
                option[2] = []
            # Check fourth argument
            option[3] = re.sub(r'\s', r'', option[3]).split(",")
            # Invert selection if empty range (i.e. [1,Inf) ) was given
            if option[3] == [""]:
                option[0] = not option[0]
                option[3] = [[0,0]]
            # Otherwise, split the ranges accordingly in pairs
            else:
                # Check for odd amount of range numbers
                if(len(option[3])%2 == 1):
                    logging.error(F"Invalid filter option \"{option}\". Fourth argument must be a comma separated list of numeric ranges \"from,to,...\". The specified input must be even.")
                    exit(1)
                for i, rangeVal in enumerate(option[3]):
                    # Check for non-numeric range numbers
                    if not rangeVal.isnumeric():
                        logging.error(F"Invalid filter option \"{option}\". Fourth argument must be a comma separated list of numeric ranges \"from,to,...\". \"{rangeVal}\" is invalid.")
                        exit(1)
                    # Left bound is greater than right bound
                    if (i%2 == 0) and int(option[3][i]) > int(option[3][i+1]):
                        logging.error(F"Invalid filter option \"{option}\". Interval [{option[3][i]},{option[3][i+1]}] is not allowed.")
                        exit(1)

                # Combine range pairs and sort them correctly
                option[3] = sorted(  [ [int(option[3][2*i]), int(option[3][2*i+1])] for i in range(int(len(option[3])/2)) ]  )

                # Combine overlapping intervals. Start with first interval
                intervals = [option[3][0]] 
                for i in range(1,len(option[3])):
                    # Right bound of the last interval piece lies in the range of the next interval, e.g. [1,3] and [2,5]
                    if intervals[-1][1] >= option[3][i][0]:
                        # Extend last interval if the next interval to add extends the last one
                        if intervals[-1][1] < option[3][i][1]:
                            intervals[-1] = [ intervals[-1][0], option[3][i][1] ]
                    # If no overlapping happens, just add the interval
                    else:
                        intervals.append(option[3][i])
                option[3] = intervals

            # Exclusive intervals are translated to inclusive ones by inversion
            if option[0] == False:
                option[3] = self.invertIntervals(option[3])
                option[0] = True
                        
        return filterOptions


    ## Check whether a diagram matches the cut filter conditions
    # The argument filterOptions is a list of multiple entries of the following type:
    #  [ True , ["q1","q2"] , ["ft"] , [[0,0], [1,14]] ]
    # \p filterOptions are given by #parseFilterOptions(). See also the reference there to see what all entries mean.
    #
    # @param diagram modules.diagram.Diagram instance which we want to check
    # @param filterOptions List of above mentioned single filter options
    # \return Boolean value whether filter condition was met
    def matchesCutConstraints(self, diagram, filterOptions):
        # Split filters with different source vertices and try to match the constraints for each source definition independently
        differentSources = {tuple(option[1]) for option in filterOptions}
        if len(differentSources) > 1:
            # Final result is combination of subsequent results
            result = True
            for sources in differentSources:
                result = result and self.matchesCutConstraints(diagram, [option for option in filterOptions if tuple(option[1])==sources])
                if result == False:
                    return False
            return True
                
        # Check if it is at all possible for the diagram to fulfill the cut constraints solely from the number of particles involved
        if not self.diagramMatchesParticleNumberConstraints(diagram.topologicalLineList, filterOptions):
            return False

        # Dict which maps one line index onto others if all have the same start and end vertex
        multipleLineIndices = self.createMultipleEdgeClusters(diagram.topologicalLineList)

        # List of all line indices which corresponds to self-loops
        selfLoopIndices = {l.number for l in diagram.topologicalLineList if l.isSelfLoop()}

        # Get indices of lines which must not be cut a priori 
        typeNLineIndices = self.getTypeNLines(diagram.topologicalLineList, filterOptions)
        # Multiple lines with one N-line are all N-lines
        newNLineIndices = set()
        for nLineIndex in typeNLineIndices:
            if nLineIndex in multipleLineIndices:
                newNLineIndices.update(multipleLineIndices[nLineIndex])
        typeNLineIndices.update(newNLineIndices)

        # Get indices of lines which must be cut a priori 
        typeMLineIndices = self.getTypeMLines(diagram.topologicalLineList, filterOptions)
        # Multiple lines with one M-line are all M-lines
        newMLineIndices = set()
        for mLineIndex in typeMLineIndices:
            if mLineIndex in multipleLineIndices:
                newMLineIndices.update(multipleLineIndices[mLineIndex])
        typeMLineIndices.update(newMLineIndices)

        # Contradicting M- and N-lines cannot have valid cuts
        if not typeNLineIndices.isdisjoint(typeMLineIndices):
            return False

        # M-line self-loops cannot be cut
        if not selfLoopIndices.isdisjoint(typeMLineIndices):
            return False

        # List of index numbers which corresponds to external vertices which act as source and as sink
        sourceVertexNumbers = []
        sinkVertexNumbers = []
        for q in diagram.externalMomenta:
            qName = re.sub(r'-*', r'', q[0])
            if qName in filterOptions[0][1]:
                sourceVertexNumbers.append(-int(re.sub(r'q', r'', qName)))
            else:
                sinkVertexNumbers.append(-int(re.sub(r'q', r'', qName)))

        # Build vertex coloring heuristics by extracting relations between vertices.
        # E.g. {1: {}, 2:{(1,True)}, 3:{(-2,True),(2,False)}} means that vertex 2 is in the same group as vertex 1. Vertex 3 is in the same class as -2 and in a different class as 2
        # The structure of this object is a tree, i.e. higher vertex numbers reference only to lower ones
        vertexRelationTrees = {v.number:set() for v in diagram.topologicalVertexList}
        for line in diagram.topologicalLineList:
            # N-type edges between two vertices says both are in the same group
            if line.number in typeNLineIndices:
                # Self-loops do not contribute to the later vertex coloring and can be ignored
                if line.isSelfLoop():
                    continue
                vertexRelationTrees[max(line.start, line.end)].add((min(line.start, line.end), True))
            # M-type edges between two vertices says both are in different groups
            if line.number in typeMLineIndices:
                vertexRelationTrees[max(line.start, line.end)].add((min(line.start, line.end), False))

        # Now we try to split all vertices in two groups.
        # We thus try to colorize all vertices according to their relations, and try assign them to one of the main groups in the end.
        # We start with two colors of the groups source (0) and sink (1).
        # E.g. {-1:0, -2:1, 2:1} means that vertex 2 is in the same color group as (sink) vertex -2
        colorGroups = {}
        for qIndex in sourceVertexNumbers:
            colorGroups[qIndex] = 0
        for qIndex in sinkVertexNumbers:
            colorGroups[qIndex] = 1

        # vertexRelationTrees also gives information which color combinations are not allowed.
        # E.g. {3:{2}, 4:{2,3}} says that color "3" is not allowed to be the same as color 2, and color 4 is not allowed to be colored as 2 or 3.
        # Larger colors reference only to lower ones.
        colorRestrictions = {0:set(), 1:set()}

        # Now fill colorGroups and colorRestrictions according to vertexRelationTrees
        colorCounter = 2
        for vertex in diagram.topologicalVertexList:
            # Vertex is connected to another one (has the same color)
            connectedVertices = [rel[0] for rel in vertexRelationTrees[vertex.number] if rel[1] == True]
            for connectedVertex in connectedVertices:
                # Connection is already established
                if vertex.number in colorGroups:
                    # Continue if connection is known
                    if colorGroups[vertex.number] == colorGroups[connectedVertex]:
                        continue
                    # If two vertices must have the same color but are already fixed to different classes 0 or 1, a coloring is impossible
                    if colorGroups[vertex.number] in (0,1) and colorGroups[connectedVertex] in (0,1):
                        return False
                    # If connection is not known, re-color connected group
                    if colorGroups[connectedVertex] in (0,1):
                        # Do not rename the sink/source colors but rename the color of the current vertex
                        colorGroups[vertex.number] = colorGroups[connectedVertex]
                    else:
                        for alreadyColoredVertex in [v for v,col in colorGroups.items() if col == colorGroups[connectedVertex]]:
                            colorGroups[alreadyColoredVertex] = colorGroups[vertex.number]
                    # Rename key in colorRestrictions
                    if colorGroups[connectedVertex] in colorRestrictions:
                        colorRestrictions[colorGroups[connectedVertex]] = colorRestrictions[colorGroups[vertex.number]]
                    # Rename values in colorRestrictions
                    for restrictedCols in colorRestrictions.values():
                        if colorGroups[connectedVertex] in restrictedCols:
                            restrictedCols.remove(colorGroups[connectedVertex])
                            restrictedCols.add(colorGroups[vertex.number])

                else:
                    colorGroups[vertex.number] = colorGroups[connectedVertex]
            
            # If no known color could be assigned, apply a new one
            if len(connectedVertices) == 0:
                colorGroups[vertex.number] = colorCounter
                colorRestrictions[colorCounter] = set()
                colorCounter += 1

            # Assign colorRestrictions due to disconnected vertices
            disconnectedVertices = [rel[0] for rel in vertexRelationTrees[vertex.number] if rel[1] == False]
            for disconnectedVertex in disconnectedVertices:
                colorRestrictions[colorGroups[vertex.number]].add(colorGroups[disconnectedVertex])

        # Check for unresolvable cuts. I.e. color restrictions which disallow themselves
        for keyCol, restrictedCols in colorRestrictions.items():
            if keyCol in restrictedCols:
                return False
            
        logging.debug(F"vertexRelationTrees: {vertexRelationTrees}")
        logging.debug(F"colorGroups: {colorGroups}")
        logging.debug(F"colorRestrictions: {colorRestrictions}")

        # Get number of unassigned colors
        unassignedColors = [col for col in colorGroups.values() if col != 0 and col != 1]

        # Build iterator for different colorings or a trivial one
        if len(unassignedColors) == 0:
            coloringCombinatorics = [[]]
        else:
            coloringCombinatorics = itertools.product([0,1], repeat=len(unassignedColors))

        # Iterate over all possible vertex colorings of unassigned vertices
        for coloringList in coloringCombinatorics:
            # Translate list of colors to a dict describing which color will be 0 or 1
            coloring = {unassignedColors[i]: coloringList[i] for i in range(len(unassignedColors))}
            # Local colorGroups instance with current coloring
            newColorGroups = {v : coloring.get(col, col) for v, col in colorGroups.items()}

            # Skip disallowed colorings due to colorRestrictions. I.e. the key color is not allowed by the value colors
            if True in [ coloring.get(col,col) in map(lambda c:coloring.get(c,c),rCols) \
              for col, rCols in colorRestrictions.items() ]:
                continue

            # Now, we translate vertex colors to line cuts
            # Extract particle content of cut lines
            particlesOfCutLines = []
            cutLines = set()
            connectedEdges = set()
            for line in diagram.topologicalLineList:
                # N-line -> connected
                if newColorGroups[line.start] == newColorGroups[line.end]:
                    connectedEdges.add(tuple(sorted([line.start, line.end])))
                # M-line -> cut (disconnected)
                else:
                    cutLines.add(line.number)
                    particlesOfCutLines.append([line.incomingParticle, line.outgoingParticle])
            # If there are more than two connected components, cuts are not allowed
            if self.findNumberOfConnectedComponents(connectedEdges) > 2:
                continue

            # Do not allow isolated vertices
            if self.findIsolatedVertices(connectedEdges,len(diagram.topologicalVertexList)):
                continue

            # Check if cut lines are in accordance with the cut filter constraint
            if self.cutLinesMatchFilterConstraints(particlesOfCutLines, filterOptions):
                logging.debug(F"Found valid cut of lines: {cutLines}")
                return True
                
        return False


    ## Find multiple lines of a graph (as modules.Diagram.Line list), i.e. edges which make the graph non-simple.
    # Create a Dict which maps one line index onto others if all have the same start and end vertex
    # E.g. {1:[2,3], 2:[1,3], 3:[1,2], 5:[6], 6:[5]} if lines 1,2 & 3 as well as 5 & 6 have same start and end vertices.
    # Thus, one gets a simple graph by taking the lines which are not part of this dict and only one per multiple line cluster. 
    # @param topologicalLineList Instance of modules.Diagram.diagram.topologicalLineList
    # \return Dict telling which lines are not simple and mapping on lines with same start and end vertex
    def createMultipleEdgeClusters(self, topologicalLineList):
        # Cluster lines with same start and end vertex in dict with vertices as keys and list of line numbers as values
        lineCluster = {}
        for line in topologicalLineList:
            key = tuple(sorted([line.start,line.end]))
            if key in lineCluster:
                lineCluster[key].append(line.number)
            else:
                lineCluster[key] = [line.number]

        # Now build all combinations of indices as keys and values from the clusters
        nonSimpleLineIndices = {}
        for cluster in lineCluster.values():
            # Skip single edges
            if len(cluster) == 1:
                continue
            # Add every index as key and the cluster list without the key as value
            for index in cluster:
                nonSimpleLineIndices[index] = cluster.copy()
                nonSimpleLineIndices[index].remove(index)
        
        return nonSimpleLineIndices


    ## Check whether the particle content of a whole diagram is in accordance with the cut filter constraints.
    # This is a naive check whether a diagram is at all capable of meeting the filter conditions.
    #
    # @param topologicalLineList Instance of modules.Diagram.diagram.topologicalLineList we want to check
    # @param filterOptions List of filter options as returned by #parseFilterOptions()
    # \return Boolean value whether particle numbers comply the filter conditions
    def diagramMatchesParticleNumberConstraints(self, topologicalLineList, filterOptions):
        # All constraints must be complied simultaneously
        for option in filterOptions:
            # Sum of relevant particles of the constraint. 
            # Note that empty option[2] matches any particle
            # Note also that topologicalLineList contains external lines too, need to filter them out
            significantParticleAmount = len([l for l in topologicalLineList \
                if (not l.external) and ( (option[2] == []) or (l.incomingParticle in option[2]) or (l.outgoingParticle in option[2]) )
            ])

            # Check if particle number can lie in the interval, i.e. number is larger/equal the lowest allowed bound
            # Note that "intervals" is non-empty and ordered at this point when comming from #parseFilterOptions()
            if sorted(option[3])[0][0] > significantParticleAmount:
                return False

        return True


    ## Check whether the given particle content of cut lines is in accordance with the cut filter constraints
    # If a cut is specified it must be checked whether its allowed by the filter options.
    #
    # @param particles List of lists with particle names of cut lines. The sublists have always two entries to indicate incoming and outgoing particles. E.g. [["ft","fT"], ["fT","ft"], ["g","g"]]
    # @param filterOptions List of filter options as returned by #parseFilterOptions()
    # \return Boolean value whether the particles in the cut comply the filter conditions
    def cutLinesMatchFilterConstraints(self, particles, filterOptions):
        # All constraints must be complied simultaneously
        for option in filterOptions:
            # Sum of relevant particles of the constraint. 
            # Note that empty option[2] matches any particle
            significantParticleAmount = len([p for p in particles \
                if (option[2] == []) or p[0] in option[2] or p[1] in option[2]
            ])

            # Check if particle number lies in the allowed intervals
            matchesIntervals = False
            for interval in option[3]:
                matchesIntervals = matchesIntervals or (interval[0] <= significantParticleAmount <= interval[1])
            
            # The current interval does not match
            if not matchesIntervals:
                return False

        return True


    ## Find all lines which must not be cut by construction of the filter option
    # We go through the options and check if cuts on particles of some line are restricted to zero.
    #
    # @param topologicalLineList Instance of modules.Diagram.diagram.topologicalLineList we want to check
    # @param filterOptions List of filter options as returned by #parseFilterOptions()
    # \return Set of line indices which must not be checked
    def getTypeNLines(self, topologicalLineList, filterOptions):
        NLineIndices = set()
        # Check for every line if it is restraint from cutting
        for line in topologicalLineList:
            # External lines are uncutable
            if line.external:
                NLineIndices.add(line.number)
                continue

            # Go through all filter options which restrict to exactly zero cuts and check if the current line applies to that filter
            for option in filterOptions:
                if option[3] == [[0,0]]:
                    if option[2] == [] or line.incomingParticle in option[2] or line.outgoingParticle in option[2]:
                        NLineIndices.add(line.number)

        return NLineIndices


    ## Find all lines which must be cut by construction of the filter option
    # We go through the options and check if any line must be cut.
    # This is the case if at least the maximum number of that available particle is allowed.
    #
    # @param topologicalLineList Instance of modules.Diagram.diagram.topologicalLineList we want to check
    # @param filterOptions List of filter options as returned by #parseFilterOptions()
    # \return Set of line indices which must not be checked
    def getTypeMLines(self, topologicalLineList, filterOptions):
        MLineIndices = set()

        # Go through all filter options which restrict the cuts to be at least the actual amount of that particle type, and pick those lines
        for option in filterOptions:
            # Sum of relevant particles of the constraint. 
            # Note that empty option[2] matches any particle
            # Note also that topologicalLineList contains external lines too, need to filter them out
            significantParticleIndices = [l.number for l in topologicalLineList \
                if (not l.external) and ( (option[2] == []) or (l.incomingParticle in option[2]) or (l.outgoingParticle in option[2]) )
            ]

            # Check if we need to cut all available particles, i.e. lowest bound on cuts is just what is available
            if len(significantParticleIndices) == option[3][0][0]:
                MLineIndices.update(significantParticleIndices)

        return MLineIndices


    ## Invert a bunch of one-dimensional intervals respective to [0,limit].
    # For an interval `A`, we compute its inverse `Abar` on `G = [0,limit]` as their difference `Abar = G\A`.
    # @param intervalsA List of one-dimensional intervals between [0,limit]. E.g. [[1,3], [6,10]]. The intervals are assumed to be limited by [0,limit] and non-overlapping
    # @param limit Right limit of the underlying set `G`
    # \return List of intervals which describe the difference between `G` and `A`
    def invertIntervals(self, intervalsA, limit=1000):
        # Start from full G interval from which we cut out the chunks of A
        intervalsG = [[0,limit]]
        # Cut out every interval chunk of A one by one
        for intervalA in sorted(intervalsA):
            # Check from which leftover chunk of G the actual chunk of A lies in
            for i,partition in enumerate(intervalsG):
                # To see if one interval lies in another is easy since all intervals in A are non-overlapping
                if intervalA[0] >= partition[0] and intervalA[1] <= partition[1]:
                    # Split the interval in two if boundaries are not hit, e.g. [1,8]\[3,4] = [1,2] & [5,8]
                    if intervalA[1] != partition[1]:
                        # Insert right interval first
                        intervalsG.insert(i+1, [intervalA[1]+1, partition[1]])
                    if intervalA[0] != partition[0]:
                        # Insert left interval in front of the (possible) right one
                        intervalsG.insert(i+1, [partition[0], intervalA[0]-1])
                    # Remove old part of interval G and move to next interval to be removed from intervalsG
                    del intervalsG[i]
                    break
                    
        return intervalsG
        
        
    ## Check if there are isolated vertices
    # It is possible for a cut to leave a vertex with all attached lines to be cut
    # This can not be a valid cut, as even for 1 -> 1 amplitudes source and sinks are always connected to at least one other vertex
    # @param edges List of edges given as their start vertex number and end vertex number. E.g. [(1,2), (2,3)] describe edges from 1 to 2 and 2 to 3
    # @param numVertices Number of vertices
    # \return True in case there are isolated vertices, False otherwise
    def findIsolatedVertices(self,edges,numVertices):
        # Obtain list of all internal vertices appearing in connected pieces
        connectedVertexList = list(set([vertex for edge in edges for vertex in edge if vertex > 0]))
        # If there is a different number of vertices in the connected pieces, return True
        if len(connectedVertexList) != numVertices:
            return True
        else:
            return False
        

    ## Count all connected components from a list of edges.
    # Here, we follow the basic depth first search approach to walk through the graph without skipping to another vertex which is not connected to the already  visited graph.
    # See for example: [[2]](@ref refs)
    # @param edges List of edges given as their start vertex number and end vertex number. E.g. [[1,2], [2,3]] describe edges from 1 to 2 and 2 to 3
    # \return Number of connected components of the grapg described by \p edges
    @staticmethod
    def findNumberOfConnectedComponents(edges):
        visitedVertices = set()
        # Build dict with adjacent vertices
        adjacentVertices = {}
        for edge in edges:
            adjacentVertices[edge[0]] = {edge[1]}.union(adjacentVertices.get(edge[0], set()))
            adjacentVertices[edge[1]] = {edge[0]}.union(adjacentVertices.get(edge[1], set()))

        numberOfConnectedComponents = 0
        for vertex in adjacentVertices:
            if vertex in visitedVertices:
                continue
            numberOfConnectedComponents += 1
            CutFilter.dfs(vertex, adjacentVertices, visitedVertices)
        
        return numberOfConnectedComponents


    ## Ancillary depth-first search method to be used in #findNumberOfConnectedComponents()
    # @param vertex Number of the current vertex
    # @param adjacentVertices Dict of vertices as key and their neighbors as values
    # @param visitedVertices Set of vertex numbers which were visited before. This parameter is changed as part of the walk through the graph.
    @staticmethod
    def dfs(vertex, adjacentVertices, visitedVertices):
        visitedVertices.add(vertex)
        for adjacentVertex in adjacentVertices[vertex]:
            if adjacentVertex in visitedVertices:
                continue
            CutFilter.dfs(adjacentVertex, adjacentVertices, visitedVertices)
