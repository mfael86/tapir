from modules.partialFractionDecomposer import PartialFractionDecomposer, GroebnerRelationIndices, AnalyticTopoMapper, IntegralFamily
from modules.topoAnalyzer import TopologyAnalyzer 
from modules.config import Conf
from sympy import Matrix
import sympy as sy
import unittest
import logging
import re
import time


class testPartialFractionDecomposer(unittest.TestCase):
    def test_eikonalTopology(self): 
        # logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.DEBUG)
        config = Conf("test/qqqq.conf",False)
        pf = PartialFractionDecomposer(config=config, verbose=False)

        props = ['M1^2 - p5^2', '-p5^2', 'M1^2 - p5^2 + 2*p5*q1 - q1^2', '-p5^2 + 2*p5*q1 - q1^2', 'M3^2 - p5^2', 'M3^2 - p5^2']
        pf.decompose("Int1", props, ["p5"], True)
        partFracString = pf.getPartialFractioningString("Int1", props, ["p5"])

        self.assertNotIn("w5", partFracString)
        self.assertNotIn("w1w4", partFracString)
        self.assertNotIn("w0w4", partFracString)
        self.assertNotIn("w2w3", partFracString)
        self.assertNotIn("w0w1", partFracString)


    def test_doubleReducibleTopology(self):
        config = Conf("test/qqqq.conf",False)
        pf = PartialFractionDecomposer(config=config, verbose=False)

        props = ['-p4^2 - 2*p4*q1 - q1^2', 'M2^2 - p4^2', 'M2^2 - p4^2 - 2*p4*q1 - q1^2', '-p4^2']
        pf.decompose("Int1", props, ["p4"], False)
        partFracString = pf.getPartialFractioningString("Int1", props, ["p4"])
        
        self.assertEqual(partFracString.count("Int1"), 5)
        # [[2, 3], [1, 2], [0, 3], [0, 2], [0, 1]]
        self.assertIn("Int1w2w3", partFracString)
        self.assertIn("Int1w1w2", partFracString)
        self.assertIn("Int1w0w3", partFracString)
        self.assertIn("Int1w0w1", partFracString)



    def test_numericalPartfracTest_2loop(self):
        # logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.DEBUG)
        config = Conf("test/qqqq.conf",False)
        pf = PartialFractionDecomposer(config=config, verbose=False)

        props = ["-2*p*k-k^2", "2*p*k-k^2", "-k^2", "-l^2", "-(l+k)^2", "q*k", "2*l*k", "M^2-k^2", "M^2 - l^2"]
        pf.decompose("Int1", props, ["k", "l"], True)
        partFracString = pf.getPartialFractioningString("Int1", props, ["k", "l"])

        self.numTest(props, partFracString)



    def test_numericalPartfracTest_trivial(self):
        config = Conf("test/qqqq.conf",False)
        pf = PartialFractionDecomposer(config=config, verbose=False)

        props = ["(p+q)^2", "(p-q)^2", "p^2"]
        pf.decompose("Int1", props, ["p"], True)
        partFracString = partFracString = pf.getPartialFractioningString("Int1", props, ["p"])

        self.numTest(props, partFracString)



    def test_IfEndif(self):
        # A cheap attempt at checking whether the FORM code can be valid or not by comparing numbers of if and
        # endif statements, reaction to issue #87
        config = Conf("test/qqqq.conf",False)
        pf = PartialFractionDecomposer(config=config, verbose=False)
        analyticTopologyExpression = ['M1^2 - p3^2', '-(q1 + q2)^2', '-(p3 - q1 - q2 - q3)^2', '-p3*q3', '-p3*q2']
        topoName = 'd1l1'
        loopMomenta = ['p3']
        pf.decompose(topoName, analyticTopologyExpression, loopMomenta, aggregate=True)
        partFracString = pf.getPartialFractioningString(topoName, analyticTopologyExpression, loopMomenta)
        numEndif = partFracString.count("endif;")
        numIf = partFracString.count("\nif")
        self.assertEqual(numIf, numEndif)

    def numTest(self, props, partFracString):
        groebnerRelations = re.findall(r'(?<=id )[^=\?]* = [^;]*(?=;)', partFracString)
        lhs = []
        rhs = []
        for e in groebnerRelations:
            match = re.search(r'^([^=]*)=([^=]*)$', e).groups()
            lhs.append(sy.sympify(match[0]))
            rhs.append(sy.sympify(match[1]))

        propReplace = {sy.sympify("D"+ str(i)) : sy.sympify(p).replace(sy.Symbol("p"), 3).replace(sy.Symbol("q"), 5).replace(sy.Symbol("k"), 7).replace(sy.Symbol("M"), 11).replace(sy.Symbol("l"), 13) for i,p in enumerate(props)}
        propReplace[sy.sympify("p")] = 3
        propReplace[sy.sympify("q")] = 5
        propReplace[sy.sympify("M")] = 11

        self.assertNotEqual(lhs, [], "Empty input!")
        self.assertNotEqual(rhs, [], "Empty input!")

        for i in range(len(lhs)):
            l = lhs[i]
            r = rhs[i]
            #print(l, " = ", r)
            for key, val in propReplace.items():
                l=l.replace(key, val)
                r=r.replace(key, val)

            #print(" => ", l, " = ", r)
            
            self.assertEqual(l,r)



    def test_simpleSingleRelationPartfracTestGroebner(self):
        config = Conf("test/qqqq.conf",False)
        pf = PartialFractionDecomposer(config=config, verbose=False)
        pf.decompose("Int1", ["(p+q)^2", "(p-q)^2", "p^2"], ["p"], False, externalFourVectors=['q'])
        partFracString = pf.getPartialFractioningString("Int1", ["(p+q)^2", "(p-q)^2", "p^2"], ["p"])

        partFracString = partFracString.replace(" ","")

        self.assertIn("D2=D0/2+D1/2-q.q", partFracString)
        self.assertIn("D1/D2=-D0/D2+2/D2*q.q+2", partFracString)
        self.assertIn("D0/D1/D2=2/D1/D2*q.q+2/D1-1/D2", partFracString)
        self.assertIn("1/D0/D1/D2=(-2/D0/D1+1/D0/D2+1/D1/D2)/(2*q.q)", partFracString)
        self.assertIn("Int1w0w1", partFracString)
        self.assertIn("Int1w1w2", partFracString)
        self.assertIn("Int1w0w2", partFracString)


    def test_expressionListToCoeffMatrix_physical(self):
        testTopology = ["-2*p*k-k^2", "2*p*k-k^2", "-k^2", "k*l", "l^2", "M1^2-(l+k)^2", "2*l*q"]
        config = Conf("test/qqqq.conf",False)
        pf = PartialFractionDecomposer(config=config, verbose=False)
        M, v = pf.expressionListToCoeffMatrix(testTopology, ["k", "l"])

        vBar = M*v
        testTopology = sy.Matrix([sy.sympify(t).expand() for t in testTopology])
        self.assertEqual(vBar,testTopology)

        v = [str(s) for s in v]
        self.assertListEqual(sorted(v), sorted(["1", "k*p", "k**2", "k*l", "l**2", "l*q"]))


    def test_expressionListToCoeffMatrix_arbitrary(self):
        testTopology = ["x^2 + 2*M1^3*x + x^4*y^2 - 34 + x - y^2 - 82*y + 3*x + M1^2"]
        config = Conf("test/qqqq.conf",False)
        pf = PartialFractionDecomposer(config=config, verbose=False)
        M, v = pf.expressionListToCoeffMatrix(testTopology, ["x", "y"])

        vBar = M*v
        testTopology = sy.Matrix([sy.sympify(t).expand() for t in testTopology])
        self.assertEqual(vBar,testTopology)

        v = [str(s) for s in v]
        self.assertListEqual(sorted(v), sorted(["1", "x**2", "M1**3*x", "x**4*y**2", "x", "y**2", "y"]))


    def test_sympyMonomials(self):
        monomials = PartialFractionDecomposer.sympyMonomials(sy.Poly("x^2 + 2*M1^3*x + x^4*y^2 - 34 + x - y^2 - 82*y + 3*x"))
        monomials = [str(m) for m in monomials]

        self.assertListEqual(sorted(monomials), sorted(["x**2", "M1**3*x", "x**4*y**2", "x", "y**2", "y"]))


    def test_getAllRemainingPropagators(self):
        g = [GroebnerRelationIndices({1},[{2}]), GroebnerRelationIndices({3,4},[{3},{4}]), GroebnerRelationIndices({6},[{5}])]

        config = Conf("test/qqqq.conf",False)
        props = PartialFractionDecomposer(config=config, verbose=False).getAllRemainingPropagators(g, 7)

        self.assertListEqual(sorted(props), 
            sorted([ {0,2,3,5}, {0,2,4,5} ]))

    def test_AnalyticTopoMapper_UF(self):
        mapper = AnalyticTopoMapper()

        t1 = IntegralFamily("t1", ["-p1^2", "-p3^2", "M1^2-(q1-p1-p3)^2"], ["p1", "p3"])
        t2 = IntegralFamily("t2", ["-(p1+q1)^2", "-(p3+q1)^2", "M1^2-(-q1-p1-p3)^2"], ["p1", "p3"])
        t3 = IntegralFamily("t3", [ "M1^2-(-q1-p1-p3)^2", "-(p1+q1)^2", "-(p3+q1)^2",], ["p1", "p3"])

        t4 = IntegralFamily("t4", ["-p1*q1", "-p3^2", "M1^2-(p1+p3)^2"], ["p1", "p3"])
        t5 = IntegralFamily("t5", ["-(p1+q1)*q1", "-(p3+q1)^2", "M1^2-((p1+q1)+(p3+q1))^2"], ["p1", "p3"])
        t6 = IntegralFamily("t6", ["M1^2-((p1+q1)+(p3+q1))^2", "-(p1+q1)*q1", "-(p3+q1)^2"], ["p1", "p3"])

        mapper.initializeTopology(t1)
        mapper.initializeTopology(t2)
        mapper.initializeTopology(t3)
        mapper.initializeTopology(t4)
        mapper.initializeTopology(t5)
        mapper.initializeTopology(t6)

        self.assertEqual((t1.U-t2.U).simplify(), 0)
        self.assertEqual((t1.F-t2.F).simplify(), 0)
        self.assertEqual((t1.U-t3.U).simplify(), 0)
        self.assertEqual((t1.F-t3.F).simplify(), 0)

        self.assertEqual((t4.U-t5.U).simplify(), 0)
        self.assertEqual((t4.F-t5.F).simplify(), 0)
        self.assertEqual((t4.U-t6.U).simplify(), 0)
        self.assertEqual((t4.F-t6.F).simplify(), 0)

        self.assertEqual(mapper.findTopologyMapping(t1, aggregate=True), (t1.topoName, {0:0, 1:1, 2:2}))
        self.assertEqual(mapper.findTopologyMapping(t2, aggregate=True), (t1.topoName, {0:0, 1:1, 2:2}))
        self.assertEqual(mapper.findTopologyMapping(t3, aggregate=True), (t1.topoName, {0:2, 1:0, 2:1}))
        
        self.assertEqual(mapper.findTopologyMapping(t4, aggregate=True), (t4.topoName, {0:0, 1:1, 2:2}))
        self.assertEqual(mapper.findTopologyMapping(t5, aggregate=True), (t4.topoName, {0:0, 1:1, 2:2}))
        self.assertEqual(mapper.findTopologyMapping(t6, aggregate=True), (t4.topoName, {0:2, 1:0, 2:1}))

        self.assertListEqual([t.topoName for t in mapper.uniqueTopologies], ["t1", "t4"])


    def test_mappingInTopoString(self):
        config = Conf("test/qqqq.conf",False)
        pf = PartialFractionDecomposer(config=config, verbose=False)
        pf.decompose("Int1", ["M1^2-(p+q)^2", "p^2", "(p+q)^2"], ["p"], True)
        pf.decompose("Int2", ["(p-q)^2", "M1^2-p^2", "(p)^2"], ["p"], True)
        pf.minimizeTopologies()
        partFracString1 = pf.getPartialFractioningString("Int1", ["M1^2-(p+q)^2", "p^2", "(p+q)^2"], ["p"])
        partFracString2 = pf.getPartialFractioningString("Int2", ["(p-q)^2", "M1^2-p^2", "(p)^2"], ["p"])

        # print(partFracString1)
        # print(" --- ")
        # print(partFracString2)

        partFracString1 = partFracString1.replace(" ","")
        partFracString2 = partFracString2.replace(" ","")
        
        self.assertIn("Int1w0w1", partFracString1)
        self.assertIn("Int1w1w2", partFracString1)
        self.assertNotIn("Int1w0w2", partFracString1)

        self.assertIn(re.sub(r'\s', r'', "id D0^n0? * D1^n1? = Int1w0w1(-n1, -n0);"), partFracString2)
        self.assertIn(re.sub(r'\s', r'', "id D0^n0? * D2^n1? = Int1w1w2(-n0, -n1);"), partFracString2)
        self.assertNotIn("Int2w", partFracString2)


    def test_simpleSingleRelationPartfracWithReducibleSubTopos(self):
        config = Conf("test/qqqq.conf",False)
        pf = PartialFractionDecomposer(config=config, verbose=False)
        pf.decompose("Int1", ["(p+q)^2", "(p-q)^2", "p^2"], ["p"], True)
        pf.minimizeTopologies()
        partFracString = pf.getPartialFractioningString("Int1", ["(p+q)^2", "(p-q)^2", "p^2"], ["p"])

        partFracString = partFracString.replace(" ","")
        
        self.assertIn("Int1w0w1", partFracString)
        self.assertIn("Int1w0w2", partFracString)
        self.assertNotIn("Int1w1w2", partFracString)


    def test_complexUFcreation(self):
        mapper = AnalyticTopoMapper()

        t1 = IntegralFamily("t1", ['-k1*k2', '-k1^2 - 2*k1*q1 - q1^2', 'M2^2 - k2^2 + 2*k2*q1 - q1^2', '-k1^2', '-k2^2'], ["k1", "k2"]) 

        mapper.initializeTopology(t1)
        self.assertEqual(t1.U - sy.sympify("-x1**2/4 + x2*x3 + x2*x4 + x3*x5 + x4*x5"), 0)
        self.assertEqual((t1.F - sy.sympify("-M2**2*x1**2*x5/4 + M2**2*x2*x3*x5 + M2**2*x2*x4*x5 + M2**2*x3*x5**2 + M2**2*x4*x5**2 + q1**2*x1**2*x4/4 + q1**2*x1**2*x5/4 + q1**2*x1*x4*x5 - q1**2*x2*x3*x4 - q1**2*x2*x3*x5 - q1**2*x2*x4*x5 - q1**2*x3*x4*x5")).simplify(), 0)


    def test_mapOnNonPartialFractionedTopology(self):
        config = Conf("test/qqqq.conf",False)
        pf = PartialFractionDecomposer(config=config, verbose=False)
        pf.decompose("Int1", ["(p+q)^2", "(p-q)^2", "p^2"], ["p"], True)
        pf.decompose("Int2", ["(p+q)^2", "p^2"], ["p"], True)
        pf.minimizeTopologies()

        partFracString = pf.getPartialFractioningString("Int1", ["(p+q)^2", "(p-q)^2", "p^2"], ["p"])

        partFracString = partFracString.replace(" ","")
        
        self.assertIn("Int2", partFracString)
        self.assertNotIn("Int1w0w2", partFracString)
        self.assertNotIn("Int1w1w2", partFracString)

    
    def test_complex2lTopoMapping(self):
        mapper = AnalyticTopoMapper()

        props1 = "-p1^2 + 2*p1*p4 + 2*p1*q1 - p4^2 - 2*p4*q1 - q1^2, -p4^2 - 2*p4*q1 - q1^2, -p1^2 + 2*p1*q1 - q1^2, -p4^2, -p1^2".split(",")
        props2 = "-p1^2 - 2*p1*p4 - p4^2, -p1^2 - 2*p1*p4 + 2*p1*q1 - p4^2 + 2*p4*q1 - q1^2, -p1^2 + 2*p1*q1 - q1^2, -p4^2, -p1^2".split(",")
        props3 = "-p7^2 - 2*p7*q1 - q1^2, -p1^2 - 2*p1*p7 - p7^2, -p1^2 + 2*p1*q1 - q1^2, -p1^2, -p7^2".split(",")
        t1 = IntegralFamily("Int1", props1, ["p1", "p4"])
        t2 = IntegralFamily("Int2", props2, ["p1", "p4"])
        t3 = IntegralFamily("Int3", props3, ["p7", "p1"])

        mapper.findTopologyMapping(t1,True)

        mapping2 = mapper.findTopologyMapping(t2,False)
        mapping3 = mapper.findTopologyMapping(t3,False)

        # tested by hand
        self.assertEqual(mapping2[0], "Int1")
        self.assertDictEqual(mapping2[1], {3: 0, 1: 1, 4: 2, 2: 4, 0: 3})

        self.assertEqual(mapping3[0], "Int1")
        self.assertIn(mapping3[1], [
            {0:1, 1:0, 2:4, 3:2, 4:3},
            {0:4, 1:0, 2:1, 3:3, 4:2},
            {0:2, 1:0, 2:3, 3:1, 4:4},
            {0:3, 1:0, 2:2, 3:4, 4:1}
        ])


    def test_mappingOn2lTopology(self):
        props1 = "-p1^2 + 2*p1*p4 + 2*p1*q1 - p4^2 - 2*p4*q1 - q1^2, -p4^2 - 2*p4*q1 - q1^2, -p1^2 + 2*p1*q1 - q1^2, -p4^2, -p1^2".split(",")
        props2 = "-p7^2 - 2*p7*q1 - q1^2, -p1^2 - 2*p1*p7 - p7^2, -p1^2 + 2*p1*q1 - q1^2, -p1^2, M1^2-p7^2, -p7^2".split(",")

        config = Conf("test/qqqq.conf",False)
        pf = PartialFractionDecomposer(config=config, verbose=False)
        pf.decompose("Int1", props1, ["p1", "p4"])
        pf.decompose("Int2", props2, ["p1", "p7"])
        pf.minimizeTopologies()

        partFracString = pf.getPartialFractioningString("Int2", props2, ["p1", "p7"])

        partFracString = partFracString.replace(" ","")
        self.assertIn(re.sub(r'\s', r'', "D0^n0? * D1^n1? * D2^n2? * D3^n3? * D5^n4? = Int1(-n1, -n0, -n3, -n4, -n2)"), partFracString)
        self.assertIn(re.sub(r'\s', r'', "D0^n0? * D1^n1? * D2^n2? * D3^n3? * D4^n4? = Int2w0w1w2w3w4(-n0, -n1, -n2, -n3, -n4)"), partFracString)

    
    def test_IntegralFamily_stripIndices(self):
        t = IntegralFamily("t1", ["k1^2", "k2^2", "k1^2-M1^2", "(k2-k1)^2"], ["k1", "k2"], [0,1,2,0])
        t.stripIndices()

        self.assertListEqual(t.indices, [1,2])
        self.assertListEqual(t.analyticTopologyExpression, ["k2^2", "k1^2-M1^2"])
        self.assertListEqual(t.permutations, [1,2])

    
    def test_dotedLineMapping(self):
        mapper = AnalyticTopoMapper()

        props1 = ["k1^2","k2^2", "(k1+q)^2", "(k2+q)^2", "(k1-k2)^2-M1^2"]
        props2 = ["(k1-k2)^2-M1^2", "k1^2","k2^2", "(k1+q)^2", "(k2+q)^2", "2*q*k1"]
        loops = ["k1", "k2"]

        t1 = IntegralFamily("Int1", props1, loops, [1,1,1,1,2])
        t2 = IntegralFamily("Int2", props1, loops, [2,1,1,1,1])
        t3 = IntegralFamily("Int3", props2, loops, [2,1,1,1,1,0])

        mapper.findTopologyMapping(t1,True)

        t2MappingName, t2Conf = mapper.findTopologyMapping(t2,False)
        t3MappingName, t3Conf = mapper.findTopologyMapping(t3,False)

        self.assertEqual(t2MappingName, "Int2")
        self.assertEqual(t3MappingName, "Int1")


    def test_dotedSymmetricLineMapping(self):
        mapper = AnalyticTopoMapper()

        props1 = ["k1^2","k2^2", "(k1+q)^2", "(k2+q)^2", "(k1-k2)^2-M1^2"]
        props2 = ["k1^2","k2^2", "k3^2","k4^2"]
        loops1 = ["k1", "k2"]
        loops2 = ["k1", "k2", "k3", "k4"]

        t1 = IntegralFamily("Int1", props1, loops1, [2,1,1,1,1])
        t2 = IntegralFamily("Int2", props1, loops1, [1,2,1,1,1])
        t3 = IntegralFamily("Int3", props2, loops2, [1,1,2,1])
        t4 = IntegralFamily("Int4", props2, loops2, [1,2,1,1])
        t5 = IntegralFamily("Int5", props2, loops2, [2,1,1,1])
        t6 = IntegralFamily("Int6", props2, loops2, [1,1,1,2])

        mapper.findTopologyMapping(t1,True)
        mapper.findTopologyMapping(t3,True)

        t2MappingName, t2Conf = mapper.findTopologyMapping(t2,False)
        self.assertEqual(t2MappingName, "Int1")

        t4MappingName, t4Conf = mapper.findTopologyMapping(t4,False)
        t5MappingName, t5Conf = mapper.findTopologyMapping(t5,False)
        t6MappingName, t6Conf = mapper.findTopologyMapping(t6,False)
        self.assertEqual(t4MappingName, "Int3")
        self.assertEqual(t5MappingName, "Int3")
        self.assertEqual(t6MappingName, "Int3")


    def test_symmetricLineMapping(self):
        mapper = AnalyticTopoMapper()
        props1 = [" M1^2 -k1^2 - 2*k1*q1 - q1^2", " M3^2 -k1^2 "]
        props2 = [" -k1^2 + M3^2 - 2*k1*q1 - q1^2", " -k1^2 + M1^2 "]
        loops = ["k1"]

        t1 = IntegralFamily("Int1", props1, loops, [1,1])
        t2 = IntegralFamily("Int2", props2, loops, [1,1])

        mapper.findTopologyMapping(t1,True)

        t2MappingName, t2Conf = mapper.findTopologyMapping(t2,False)
        self.assertEqual(t2MappingName, "Int1")


    def test_twoOneLoopMapping(self):
        mapper = AnalyticTopoMapper()
        props1 = [' -k1^2 + 2*k1*q1 - q1^2', ' -k2^2 + M3^2 - 2*k2*q1 - q1^2', ' -k1^2', ' -k2^2 + M1^2 ']
        props2 = [' -k1^2 + 2*k1*q1 - q1^2', ' -k2^2 + M1^2 + 2*k2*q1 - q1^2', ' -k1^2', ' -k2^2 + M3^2 ']
        loops = ["k1", "k2"]

        t1 = IntegralFamily("Int1", props1, loops, [1,1,1,1])
        t2 = IntegralFamily("Int2", props2, loops, [1,1,1,1])

        mapper.findTopologyMapping(t1,True)

        t2MappingName, t2Conf = mapper.findTopologyMapping(t2,False)
        self.assertEqual(t2MappingName, "Int1")


    def test_complexSymmetryMappingConsistency(self):
        # test mapping first
        mapper = AnalyticTopoMapper()
        props1 = ['-k1^2 - 2*k1*k2 - k2^2',' -k1^2 + 2*k1*q1 - q1^2',' M3^2 - k2^2 - 2*k2*q1 - q1^2',' -k1^2',' M1^2 - k2^2']
        props2 = ['-p1^2 - 2*p1*p4 - p4^2',' -p1^2 - 2*p1*p4 - 2*p1*q1 - p4^2 - 2*p4*q1 - q1^2',' M3^2 - p4^2 - 2*p4*q1 - q1^2',' -p4^2 - 2*p4*q1 - q1^2',' -p1^2',' M1^2 - p4^2',' -p4^2']

        t1 = IntegralFamily("Int1", props1.copy(), ["k1", "k2"], [1,1,1,1,1])
        t2 = IntegralFamily("Int2", props2.copy(), ["p1", "p4"], [1,1,1,0,1,1,0])

        mapper.findTopologyMapping(t1,True)
        t2MappingName, t2Conf = mapper.findTopologyMapping(t2,False)

        self.assertEqual(t2MappingName, "Int1")

        # now check out the partfrac string
        config = Conf("test/qqqq.conf",False)
        pf = PartialFractionDecomposer(config=config, verbose=False)
        pf.decompose("Int1", props1, ["k1", "k2"])
        pf.decompose("Int2", props2, ["p1", "p4"])
        pf.minimizeTopologies()

        partFracString = pf.getPartialFractioningString("Int2", props2, ["p1", "p4"])

        self.assertIn("id D0^n0? * D1^n1? * D2^n2? * D4^n3? * D5^n4? = Int1(-n3, -n1, -n2, -n0, -n4);", partFracString)


    def test_UF(self):
        U1,F1 = AnalyticTopoMapper.UF(["k1","k2"], ["M1^2 - k1^2", "M2^2 - (k1 + k2)^2", "-k2^2", "-(k2 + q1)^2"])
        U1alt,F1alt = AnalyticTopoMapper.UF2(["k1","k2"], ["M1^2 - k1^2", "M2^2 - (k1 + k2)^2", "-k2^2", "-(k2 + q1)^2"])
        
        U1mathematica = sy.sympify("x1*x2 + x1*x3 + x2*x3 + x1*x4 + x2*x4")
        F1mathematica = sy.sympify("""M1^2*x1^2*x2 + M2^2*x1*x2^2 + M1^2*x1^2*x3 + M1^2*x1*x2*x3 +
        M2^2*x1*x2*x3 + M2^2*x2^2*x3 + M1^2*x1^2*x4 + M1^2*x1*x2*x4 +
        M2^2*x1*x2*x4 - q1^2*x1*x2*x4 + M2^2*x2^2*x4 - q1^2*x1*x3*x4 -
        q1^2*x2*x3*x4""")
        
        self.assertEqual(U1,U1mathematica)
        self.assertEqual(F1,F1mathematica)
        self.assertEqual(U1alt,U1mathematica)
        self.assertEqual(F1alt,F1mathematica)

        U2,F2 = AnalyticTopoMapper.UF(["k1","k2","k3"], ["-k3**2", "-k2**2", "-k1**2", "-(k2 - q1)**2", "-(-k1 + k2 - k3 - q1)**2", "-(k3 + q1)**2", "-(-k1 + k2)**2", "-(k1 + k3)**2", "-k2*k3"])

        U2mathematica = sy.sympify("""(4*x1*x2*x3 + 4*x1*x3*x4 + 4*x1*x2*x5 + 4*x1*x3*x5 + 4*x2*x3*x5 +
        4*x1*x4*x5 + 4*x3*x4*x5 + 4*x2*x3*x6 + 4*x3*x4*x6 + 4*x2*x5*x6 +
        4*x3*x5*x6 + 4*x4*x5*x6 + 4*x1*x2*x7 + 4*x1*x3*x7 + 4*x1*x4*x7 +
        4*x2*x5*x7 + 4*x3*x5*x7 + 4*x4*x5*x7 + 4*x2*x6*x7 + 4*x3*x6*x7 +
        4*x4*x6*x7 + 4*x1*x2*x8 + 4*x2*x3*x8 + 4*x1*x4*x8 + 4*x3*x4*x8 +
        4*x1*x5*x8 + 4*x3*x5*x8 + 4*x2*x6*x8 + 4*x4*x6*x8 + 4*x5*x6*x8 +
        4*x1*x7*x8 + 4*x2*x7*x8 + 4*x3*x7*x8 + 4*x4*x7*x8 + 4*x5*x7*x8 +
        4*x6*x7*x8 + 4*x3*x5*x9 - 4*x7*x8*x9 - x3*x9^2 - x5*x9^2 - x7*x9^2 -
        x8*x9^2)/4""")

        F2mathematica = sy.sympify("""-(q1^2*x1*x2*x3*x4) - q1^2*x1*x2*x3*x5 - q1^2*x1*x2*x4*x5 -
        q1^2*x2*x3*x4*x5 - q1^2*x1*x2*x3*x6 - q1^2*x1*x3*x4*x6 -
        q1^2*x2*x3*x4*x6 - q1^2*x1*x2*x5*x6 - q1^2*x1*x3*x5*x6 -
        q1^2*x1*x4*x5*x6 - q1^2*x2*x4*x5*x6 - q1^2*x3*x4*x5*x6 -
        q1^2*x1*x2*x4*x7 - q1^2*x1*x3*x4*x7 - q1^2*x1*x2*x5*x7 -
        q1^2*x1*x3*x5*x7 - q1^2*x1*x4*x5*x7 - q1^2*x2*x4*x5*x7 -
        q1^2*x3*x4*x5*x7 - q1^2*x1*x2*x6*x7 - q1^2*x1*x3*x6*x7 -
        q1^2*x1*x4*x6*x7 - q1^2*x2*x4*x6*x7 - q1^2*x3*x4*x6*x7 -
        q1^2*x1*x2*x4*x8 - q1^2*x2*x3*x4*x8 - q1^2*x1*x2*x5*x8 -
        q1^2*x2*x3*x5*x8 - q1^2*x1*x2*x6*x8 - q1^2*x2*x3*x6*x8 -
        q1^2*x1*x4*x6*x8 - q1^2*x2*x4*x6*x8 - q1^2*x3*x4*x6*x8 -
        q1^2*x1*x5*x6*x8 - q1^2*x2*x5*x6*x8 - q1^2*x3*x5*x6*x8 -
        q1^2*x1*x4*x7*x8 - q1^2*x2*x4*x7*x8 - q1^2*x3*x4*x7*x8 -
        q1^2*x1*x5*x7*x8 - q1^2*x2*x5*x7*x8 - q1^2*x3*x5*x7*x8 -
        q1^2*x1*x6*x7*x8 - q1^2*x2*x6*x7*x8 - q1^2*x3*x6*x7*x8 +
        q1^2*x3*x4*x6*x9 + q1^2*x4*x5*x6*x9 + q1^2*x4*x5*x7*x9 +
        q1^2*x4*x6*x7*x9 + q1^2*x4*x6*x8*x9 + q1^2*x5*x6*x8*x9 +
        q1^2*x4*x7*x8*x9 + q1^2*x5*x7*x8*x9 + q1^2*x6*x7*x8*x9 +
        (q1^2*x3*x4*x9^2)/4 + (q1^2*x3*x5*x9^2)/4 + (q1^2*x4*x5*x9^2)/4 +
        (q1^2*x3*x6*x9^2)/4 + (q1^2*x5*x6*x9^2)/4 + (q1^2*x4*x7*x9^2)/4 +
        (q1^2*x5*x7*x9^2)/4 + (q1^2*x6*x7*x9^2)/4 + (q1^2*x4*x8*x9^2)/4 +
        (q1^2*x5*x8*x9^2)/4 + (q1^2*x6*x8*x9^2)/4""")

        self.assertEqual(U2,U2mathematica)
        self.assertEqual(F2,F2mathematica)
