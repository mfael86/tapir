import unittest
import logging
from modules.diagram import Diagram, Vertex, Line
from modules.config import Conf 
from modules.topoMinimizer import TopoMinimizer
from modules.topoAnalyzer import TopologyAnalyzer, BridgeFinder
from modules.momentumAssigner import MomentumAssigner
from modules.partialFractionDecomposer import AnalyticTopoMapper
import sympy as sy
import re
import copy
import itertools as it


class testTopoMinimizer(unittest.TestCase):
    def initDiagram(self, massive = True):
        # test case: two-loop propagator diagram
        #              ___ ___
        #             /   |   \
        #        ----X    |    X----
        #             \___|___/

        diagram = Diagram()
        if massive:
            diagram.internalMomenta = [
                ["p1", 1, 2, "g", "g", 0, 0, ""],
                ["p2", 2, 3, "g", "g", 0, 0, ""],
                ["p3", 4, 3, "g", "g", 0, 0, ""],
                ["p4", 1, 4, "g", "g", 0, 0, ""],
                ["p5", 2, 4, "fq", "fQ", 0, 0, "M1"]
            ]
        else:
            diagram.internalMomenta = [
                ["p1", 1, 2, "g", "g", 0, 0, ""],
                ["p2", 2, 3, "g", "g", 0, 0, ""],
                ["p3", 4, 3, "g", "g", 0, 0, ""],
                ["p4", 1, 4, "g", "g", 0, 0, ""],
                ["p5", 2, 4, "g", "g", 0, 0, ""]
            ]
        diagram.externalMomenta = [
            ["q1", 1, "g", 0],
            ["q2", 3, "g", 0]
        ]
        diagram.diagramNumber = 42
        diagram.createTopologicalVertexAndLineLists()
        diagram.setName()

        return diagram


    def test_TopologyLaplacianAndKirchhoffPolynom(self):
        topology = TopologyAnalyzer(self.initDiagram())
        matrix = sy.sympify([
                            ["x1 + x4",          "-x1",       "0",          "-x4"],
                            [    "-x1", "x1 + x2 + x5",     "-x2",          "-x5"],
                            [      "0",          "-x2", "x2 + x3",          "-x3"],
                            [    "-x4",          "-x5",     "-x3", "x3 + x4 + x5"]])
        matrix = sy.Matrix(matrix)

        # test the laplacian of the two-loop diagram
        laplacian = topology.constructLaplacian()
        self.assertEqual(laplacian, matrix)

        # test the Kirchhoffpolynom also
        kirchhoffPolynom = topology.constructKirchhoffPolynom()
        for i in range(4):
            cutMatrix = matrix.copy()
            cutMatrix.row_del(i)
            cutMatrix.col_del(i)
            polynom = cutMatrix.det()
            self.assertEqual(sy.expand(kirchhoffPolynom), sy.expand(polynom))


    def test_TopologyLaplacianWithExternalLinesAndW(self):
        topology = TopologyAnalyzer(self.initDiagram())
        matrix = sy.sympify([
                            ["x1 + x4 + z1",          "-x1",       "0"     ,          "-x4",    "-z1"  ],
                            [    "-x1"     , "x1 + x2 + x5",     "-x2"     ,          "-x5",     "0"   ],
                            [      "0"     ,          "-x2", "x2 + x3 + z2",          "-x3",    "-z2"  ],
                            [    "-x4"     ,          "-x5",     "-x3"     , "x3 + x4 + x5",     "0"   ],
                            [    "-z1"     ,            "0",     "-z2"     ,            "0",  "z1 + z2"]])
        matrix = sy.Matrix(matrix)

        # test the laplacian of the two-loop diagram
        laplacian = topology.constructLaplacian(externalLinesConnected = True)
        self.assertEqual(laplacian, matrix)

        # now, test the W polynom
        w = topology.constructGraphPolynomW()
        matrix.row_del(4)
        matrix.col_del(4)
        polynom = matrix.det()
        self.assertEqual(sy.expand(w), sy.expand(polynom))


    def test_TopologySymanzikPolynomialsMassless(self):
        topology = TopologyAnalyzer(self.initDiagram(False))

        # test the Symanzik polynomials of the massless two-loop diagram
        U, F = topology.constructSymanzikPolynomials()
        Utest = sy.sympify("(x1+x4)*(x2+x3) + (x1+x2+x3+x4)*x5")
        Ftest = sy.sympify("( (x1+x2)*(x3+x4)*x5 + x1*x4*(x2+x3) + x2*x3*(x1+x4) ) * (q1*q2)")

        Ftest = Ftest.subs(sy.Symbol("q2"), sy.Symbol("-q1"))
        Ftest = sy.sympify(str(Ftest))
        F = F.subs(sy.Symbol("q2"), sy.Symbol("-q1"))
        F = sy.sympify(str(F))

        self.assertEqual(sy.expand(U) - sy.expand(Utest), 0)
        self.assertEqual(sy.expand(F) - sy.expand(Ftest), 0)


    def test_TopologySymanzikPolynomialsMassive(self):

        topology = TopologyAnalyzer(self.initDiagram())

        # test the Symanzik polynomials of the two-loop diagram with lines 5 being massive
        U, F = topology.constructSymanzikPolynomials()
        Utest = sy.sympify("(x1+x4)*(x2+x3) + (x1+x2+x3+x4)*x5")
        Ftest = sy.sympify("( (x1+x2)*(x3+x4)*x5 + x1*x4*(x2+x3) + x2*x3*(x1+x4) ) * (q1*q2) + ( (x1+x4)*(x2+x3) + (x1+x2+x3+x4)*x5 )*x5*M1^2")

        Ftest = Ftest.subs(sy.Symbol("q2"), sy.Symbol("-q1"))
        Ftest = sy.sympify(str(Ftest))
        F = F.subs(sy.Symbol("q2"), sy.Symbol("-q1"))
        F = sy.sympify(str(F))
        
        self.assertEqual(sy.expand(U) - sy.expand(Utest), 0)
        self.assertEqual(sy.expand(F) - sy.expand(Ftest), 0)


    def test_SymanzikPolynomialsWithBothVariantsComparisonOnSimpleOneLoopBox(self):
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, "M1"], ["p2", 1, 3, "g", "g", 0,0, ""], ["p3", 2, 4, "g", "g", 0,0, ""], ["p4", 4, 3, "fq", "fQ", 0,0, "M1"]]
        box.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        box.diagramNumber = 1
        box.createTopologicalVertexAndLineLists()

        topology = TopologyAnalyzer(box)

        # construct and order Symanzik polynomials
        U1, F1 = topology.constructSymanzikPolynomials()
        U2, F2 = topology.constructSymanzikPolynomials2()

        F1 = F1.subs(sy.sympify("q4"), sy.sympify("-q1-q2-q3"))
        F1 = F1.expand()

        Utest = sy.sympify("x1 + x2 + x3 + x4")
        Ftest = sy.sympify("M1**2*x1**2 + M1**2*x1*x2 + M1**2*x1*x3 + 2*M1**2*x1*x4 + M1**2*x2*x4 + M1**2*x3*x4 + M1**2*x4**2 - q1**2*x1*x2 - q1**2*x1*x4 - q1**2*x2*x3 - q1**2*x3*x4 - 2*q1*q2*x1*x4 - 2*q1*q2*x3*x4 - 2*q1*q3*x2*x3 - 2*q1*q3*x3*x4 - q2**2*x1*x4 - q2**2*x2*x4 - q2**2*x3*x4 - 2*q2*q3*x3*x4 - q3**2*x1*x3 - q3**2*x2*x3 - q3**2*x3*x4")
        
        self.assertEqual(sy.expand(U1) - sy.expand(U2), 0)
        self.assertEqual(sy.expand(F1) - sy.expand(F2), 0)
        self.assertEqual(sy.expand(U2) - sy.expand(Utest), 0)
        self.assertEqual(sy.expand(F2) - sy.expand(Ftest), 0)


    def test_SymanzikPolynomialsTopologicalConsistency(self):
        #compare F of both these identical diagrams
        box1 = Diagram()
        box1.internalMomenta = [["p1", 1, 2, "W", "W", 0,0, "MW"], ["p2", 3, 4, "W", "W", 0,0, "MW"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
        box1.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        box1.diagramNumber = 1
        box1.createTopologicalVertexAndLineLists()

        box2 = Diagram()
        box2.internalMomenta = [["p4", 1, 2, "W", "W", 0,0, "MW"], ["p2", 3, 4, "W", "W", 0,0, "MW"], ["p1", 3, 1, "fq", "fQ", 0,0, ""], ["p3", 4, 2, "fq", "fQ", 0,0, ""]]
        box2.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        box2.diagramNumber = 2
        box2.createTopologicalVertexAndLineLists()

        boxAnalyzer1 = TopologyAnalyzer(box1)
        # construct and order Symanzik polynomials
        boxAnalyzer1.constructSymanzikPolynomials2()
        boxAnalyzer1.canonicallyOrderSymanzikPolynomials()

        boxAnalyzer2 = TopologyAnalyzer(box2)
        # construct and order Symanzik polynomials
        boxAnalyzer2.constructSymanzikPolynomials2()
        boxAnalyzer2.canonicallyOrderSymanzikPolynomials()

        lineSymmetryCombinationsA = TopoMinimizer.allCombinationsOfVariableLength(boxAnalyzer1.lineSymmetries)
        lineSymmetryCombinationsB = TopoMinimizer.allCombinationsOfVariableLength(boxAnalyzer2.lineSymmetries)

        conf = Conf("test/qqqq.conf", False)
        conf.mass = {"W" : "MW"}
        conf.scales = ["MW", "q1", "q2", "q3"]

        minimizer = TopoMinimizer(conf)

        self.assertTrue(minimizer.mapsOn(boxAnalyzer2, boxAnalyzer1))


    def test_SymanzikPolynomialsTopologicalConsistencyLastExternalMomentumChanged(self):
        #compare F of both these identical diagrams
        box1 = Diagram()
        box1.internalMomenta = [["p1", 1, 2, "W", "W", 0,0, "MW"], ["p2", 3, 4, "W", "W", 0,0, "MW"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
        box1.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        box1.diagramNumber = 1
        box1.createTopologicalVertexAndLineLists()

        box2 = Diagram()
        box2.internalMomenta = [["p4", 1, 2, "W", "W", 0,0, "MW"], ["p2", 3, 4, "W", "W", 0,0, "MW"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p1", 4, 2, "fq", "fQ", 0,0, ""]]
        box2.externalMomenta = [["-q4", 1, "g", 0], ["-q3", 3, "g", 0], ["-q2", 2,  "g", 0], ["-q1", 4, "g", 0]]
        box2.diagramNumber = 2
        box2.createTopologicalVertexAndLineLists()

        boxAnalyzer1 = TopologyAnalyzer(box1)
        # construct and order Symanzik polynomials
        boxAnalyzer1.constructSymanzikPolynomials2()
        boxAnalyzer1.canonicallyOrderSymanzikPolynomials()

        boxAnalyzer2 = TopologyAnalyzer(box2)
        # construct and order Symanzik polynomials
        boxAnalyzer2.constructSymanzikPolynomials2()
        boxAnalyzer2.canonicallyOrderSymanzikPolynomials()

        conf = Conf("test/qqqq.conf", False)
        conf.mass = {"W" : "MW"}
        conf.scales = ["MW", "q1", "q2", "q3"]

        minimizer = TopoMinimizer(conf)

        self.assertTrue(minimizer.mapsOn(boxAnalyzer2, boxAnalyzer1))


    def test_simplifyExternalMomentaInF(self):
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "W", "W", 0,0, "MW"], ["p2", 3, 4, "W", "W", 0,0, "MW"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
        box.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        box.diagramNumber = 1
        box.createTopologicalVertexAndLineLists()


        boxAnalyzer = TopologyAnalyzer(box, {"q1": "q", "q2": "q", "q3" : "-q"})
        boxAnalyzer.constructSymanzikPolynomials2()
        boxAnalyzer.F = sy.sympify("x1*q1**2 + x2*q2**2 + x3*q3**2 + x4*q4**2 + x5*q1*q4")
        boxAnalyzer.simplifyExternalMomentaInF()

        self.assertEqual(boxAnalyzer.F, sy.sympify("x1*q**2 + x2*q**2 + x3*q**2 + x4*q**2 - x5*q**2"))



    def test_BridgeFinderSimpleExample(self):
        bf = BridgeFinder()

        Lines = [Line(1,1,1), Line(2,1,2), Line(3,2,3)]

        bridges = bf.findBridges(Lines)
        bridges.sort()
        self.assertListEqual(bridges, [2, 3])


    def test_BridgeFinderHardExample(self):
        bf = BridgeFinder()

        Lines = [Line(1,1,3), Line(2,1,2), Line(3,2,3), Line(4,3,7), Line(5,7,8), Line(6,7,8), Line(7,4,5), Line(8,5,6), Line(9,4,6), Line(10,4,8)]

        bridges = bf.findBridges(Lines)
        bridges.sort()
        self.assertListEqual(bridges, [4, 10])


    def test_TopologySymanzikPolynomialsMasslessAlternativeAlgorithm(self):
        topology = TopologyAnalyzer(self.initDiagram(False))

        # test the Symanzik polynomials of the massless two-loop diagram
        U, F = topology.constructSymanzikPolynomials2()

        Utest = sy.sympify("(x1+x4)*(x2+x3) + (x1+x2+x3+x4)*x5")
        Ftest = sy.sympify("( (x1+x2)*(x3+x4)*x5 + x1*x4*(x2+x3) + x2*x3*(x1+x4) ) * (q1*q2)")

        Ftest = Ftest.subs(sy.Symbol("q2"), sy.Symbol("-q1"))
        Ftest = sy.sympify(str(Ftest))
        F = F.subs(sy.Symbol("q2"), sy.Symbol("-q1"))
        F = sy.sympify(str(F))

        self.assertEqual(sy.expand(U) - sy.expand(Utest), 0)
        self.assertEqual(sy.expand(F) - sy.expand(Ftest), 0)
    

    def test_TopologySymanzikPolynomialsMassiveAlternativeAlgorithm(self):
        topology = TopologyAnalyzer(self.initDiagram())

        # test the Symanzik polynomials of the two-loop diagram with Line 5 being massive
        U, F = topology.constructSymanzikPolynomials2()

        Utest = sy.sympify("(x1+x4)*(x2+x3) + (x1+x2+x3+x4)*x5")
        Ftest = sy.sympify("( (x1+x2)*(x3+x4)*x5 + x1*x4*(x2+x3) + x2*x3*(x1+x4) ) * (q1*q2) + ( (x1+x4)*(x2+x3) + (x1+x2+x3+x4)*x5 )*x5*M1^2")

        Ftest = Ftest.subs(sy.Symbol("q2"), sy.Symbol("-q1"))
        Ftest = sy.sympify(str(Ftest))
        F = F.subs(sy.Symbol("q2"), sy.Symbol("-q1"))
        F = sy.sympify(str(F))
        
        self.assertEqual(sy.expand(U) - sy.expand(Utest), 0)
        self.assertEqual(sy.expand(F) - sy.expand(Ftest), 0)


    def test_maximizeOrdering(self):
        topology = TopologyAnalyzer(self.initDiagram())
        topology.numLines = 3

        matrix = sy.Matrix([
            ["-q", 1, 4, 1],
            ["M1", 2, 2, 1],
            ["1",  0, 3, 1],
            ["M2",  1, 1, 1],
        ])

        matrix, permutation, symmetries = topology.maximizeOrdering(matrix, [1,2,3])

        orderedMatrix = sy.Matrix([
            ["1",  0, 1, 3],
            ["M2",  1, 1, 1],
            ["-q", 1, 1, 4],
            ["M1", 2, 1, 2]
        ])

        self.assertListEqual(matrix.tolist(), orderedMatrix.tolist())
        self.assertListEqual(permutation, [1,3,2])

    
    def test_canonicallyOrderSymanzikPolynomials(self):
        diagram = Diagram()
        diagram.internalMomenta = [
            ["p1", 1, 2, "g", "g", 0, 0, ""],
            ["p2", 2, 3, "g", "g", 0, 0, ""],
            ["p3", 4, 3, "g", "g", 0, 0, ""]
        ]
        diagram.externalMomenta = [
            ["q1", 1, "g", 0],
            ["q2", 3, "g", 0]
        ]

        diagram.diagramNumber = 42
        diagram.createTopologicalVertexAndLineLists()

        topology = TopologyAnalyzer(diagram)
        topology.permutations = [1,2,3]

        topology.U = sy.sympify("x3")
        topology.F = sy.sympify("-q*x1*x2**4 + M1*x1**2*x2**2 + x2**3 + M2*x1*x2")

        topology.canonicallyOrderSymanzikPolynomials()

        self.assertEqual(topology.permutations, [1,3,2])
        self.assertEqual(topology.U, sy.sympify("x2"))
        self.assertEqual(topology.F, sy.sympify("-q*x1*x3**4 + M1*x1**2*x3**2 + x3**3 + M2*x1*x3"))

    
    def test_canonicallyOrderSymanzikPolynomialsArbitraryPermutation(self):
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "W", "W", 0,0, "MW"], ["p2", 3, 4, "W", "W", 0,0, "MW"], ["p3", 3, 1, "fq", "fQ", 0,0, ""]]
        box.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]

        boxAnalyzer = TopologyAnalyzer(box)
        boxAnalyzer.F = sy.sympify("MW**2*x1**2 + MW**2*x1*x2 + 2*MW**2*x1*x3 + MW**2*x2*x3 + MW**2*x3**2 - q1**2*x1*x3 - q1**2*x2*x3 - 2*q1*q2*x1*x3 - q2**2*x1*x2 - q2**2*x1*x3")
        boxAnalyzer.U = sy.sympify("x1+x2+x3")
        boxAnalyzer.lineMasses = {2:"MW"}
        boxAnalyzer.permutations = [3,1,2]

        fCorrectOrder = copy.deepcopy(boxAnalyzer.F)

        fCorrectOrder = fCorrectOrder.subs("x1", "X2").subs("x2", "X1").subs("X1", "x1").subs("X2", "x2")

        boxAnalyzer.canonicallyOrderSymanzikPolynomials()

        self.assertEqual(boxAnalyzer.permutations, [3,2,1])
        self.assertEqual(boxAnalyzer.U, sy.sympify("x1+x2+x3"))
        self.assertEqual(boxAnalyzer.F, fCorrectOrder)
        self.assertDictEqual(boxAnalyzer.lineMasses, {1:"MW"})
        

    def test_getMassInLines(self):
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "W", "W", 0,0, "MW"], ["p2", 3, 4, "W", "W", 0,0, "MW"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
        box.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        
        box.createTopologicalVertexAndLineLists()

        boxAnalyzer = TopologyAnalyzer(box)
        boxAnalyzer.lineMasses = {1:"MW", 2:"MW"}
        self.assertDictEqual(boxAnalyzer.getMassInLines(), {"MW": [1,2], 0:[3,4]})


    def test_symbolSwap(self):
        x = sy.sympify("A1 * f + 3*A2 * df2**3")
        x = TopoMinimizer.symbolSwap(x, [["f", "df2"]])
        self.assertEqual(x.expand(), sy.sympify("A1 * df2 + 3*A2 * f**3").expand())


    def test_deleteLinesWithoutExtMomMapping(self):
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "W", "W", 0,0, "MW"], ["p2", 3, 4, "W", "W", 0,0, "MW"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
        box.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        
        box.createTopologicalVertexAndLineLists()

        boxAnalyzer = TopologyAnalyzer(box)
        boxAnalyzer.F = sy.sympify("MW**2*x2**2 + 2*MW**2*x1*x3 - q1**2*x1*x3 - q3**2*x1*x3 - 2*q1*q3*x1*x3 -2*x4*x1")
        boxAnalyzer.U = sy.sympify("x1+x2+x3+x4")
        boxAnalyzer.lineMasses = {1:"MW", 2:"MW"}
        boxAnalyzer.permutations = [2,3,4,1]
        boxAnalyzer.externalMomenta = ["q2", "q3", "q4", "q1"]

        boxAnalyzer.deleteLines([2])
       
        self.assertListEqual(boxAnalyzer.externalMomenta, ["q2", "q3", "q4", "q1"])
        self.assertEqual(boxAnalyzer.F.expand(), sy.sympify("2*MW**2*x1*x3 - q1**2*x1*x3 - q3**2*x1*x3 - 2*q1*q3*x1*x3 -2*x2*x1").expand())
        self.assertEqual(boxAnalyzer.U.expand(), sy.sympify("x1+x2+x3").expand())
        self.assertDictEqual(boxAnalyzer.lineMasses, {1:"MW"})
        self.assertListEqual(boxAnalyzer.permutations, [3,2,1])
        self.assertEqual(len(boxAnalyzer.diagram.internalMomenta), 3)


    def test_getUncutPermutation(self):
        analyzer = TopologyAnalyzer(Diagram())
        analyzer.permutations = [2,3,4,1]
        analyzer.cuts = [1,3]

        self.assertListEqual(analyzer.getUncutPermutations(), [0,2,0,3,4,1])


    def test_mapsOnWithLineSymmetry(self):
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "W", "W", 0,0, "MW"], ["p2", 3, 4, "W", "W", 0,0, "MW"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
        box.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        
        box.createTopologicalVertexAndLineLists()

        box.diagramNumber = 1
        box.name = "d1"
        a1 = TopologyAnalyzer(box)
        a1.F = sy.sympify("MW**2*x2**2 + 2*MW**2*x1*x3- q1**2*x1*x2 - 2*q1**2*x1*x3 -2*x4*x1")
        a1.U = sy.sympify("x1+x2+x3+x4")
        a1.lineMasses = {1:"MW", 2:"MW"}
        a1.permutations = [2,3,4,1]
        a1.numLines = 4
        a1.externalMomentumConservationSum = "qOrd1+qOrd2+qOrd3+qOrd4"
        a1.externalMomenta = ["q1", "q2", "q3", "q4"]

        box1 = copy.deepcopy(box)
        box1.diagramNumber = 2
        box1.name = "d2"
        a2 = TopologyAnalyzer(box1)
        a2.F = sy.sympify("MW**2*x1**2 + 2*MW**2*x2*x3- q1**2*x2*x1 - 2*q1**2*x2*x3 -2*x4*x2")
        a2.U = sy.sympify("x1+x2+x3+x4")
        a2.lineMasses = {1:"MW", 2:"MW"}
        a2.permutations = [1,2,4,3]
        a2.numLines = 4
        a2.externalMomentumConservationSum = "qOrd1+qOrd2+qOrd3+qOrd4"
        a2.externalMomenta = ["q1", "q2", "q3", "q4"]

        conf = Conf("test/qqqq.conf", False)
        conf.mass = {"W" : "MW"}
        conf.scales = ["MW", "q1", "q2", "q3"]

        minimizer = TopoMinimizer(conf)
        minimizer.verbose = False

        self.assertTrue(minimizer.mapsOn(a2, a2))
        self.assertFalse(minimizer.mapsOn(a1, a2))
        a1.lineSymmetries = [[1,2]]
        self.assertTrue(minimizer.mapsOn(a2, a1))
        self.assertDictEqual(minimizer.internalMomentumMappingConfigurations["d2"], {"p1":"p1","p2":"p4","p3":"p3","p4":"p2"})


    def test_contractLines(self):
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "W", "W", 0,0, "MW"], ["p2", 3, 4, "W", "W", 0,0, "MW"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
        box.externalMomenta = [["q1", 1, "g", 0], ["q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        
        box.createTopologicalVertexAndLineLists()

        boxAnalyzer = TopologyAnalyzer(box)
        boxAnalyzer.permutations = [1,2,3,4]

        boxAnalyzer.contractLines((3,4))

        self.assertListEqual([sorted([(l.momentum, l.start, l.end) for l in v.lines]) for v in boxAnalyzer.diagram.topologicalVertexList],
                            [sorted([('p2', 1, 2), ('-q2', 1, -2), ('p1', 1, 2), ('-q1', 1, -1)]), 
                            sorted([('-p2', 2, 1), ('q4', 2, -4), ('-p1', 2, 1), ('q3', 2, -3)])])


    def test_getMassInLines2(self):
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "W", "W", 0,0, "MW"], ["p2", 3, 4, "W", "W", 0,0, "M2"], ["p3", 3, 1, "fq", "fQ", 0,0, "M1"], ["p4", 4, 2, "fq", "fQ", 0,0, ""], ["p5", 1, 3, "fq", "fQ", 0,0, "M1"]]
        box.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        box.diagramNumber = 2
        
        box.createTopologicalVertexAndLineLists()

        boxAnalyzer = TopologyAnalyzer(box)

        boxAnalyzer.permutations = [5,4,3,2,1]

        self.assertDictEqual(boxAnalyzer.getMassInLines(), {"MW" : [5], "M2" : [4], "M1" : [3,1], 0: [2]})


    def test_topologyEmbeddingLastLineCut(self):
        # try to embed a triangle topology (3 lines) into a square topology (4 lines)
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "W", "W", 0,0, "MW"], ["p2", 3, 4, "W", "W", 0,0, "MW"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
        box.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        box.diagramNumber = 2
        box.name = "d2"
        
        box.createTopologicalVertexAndLineLists()

        triangle = Diagram()
        triangle.internalMomenta = [["p1", 1, 3, "fq", "fQ", 0,0, ""], ["p2", 1, 2, "W", "W", 0,0, "MW"], ["p3", 3, 2,  "W", "W", 0,0, "MW"]]
        triangle.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0]]
        triangle.diagramNumber = 1
        triangle.name = "d1"
        
        triangle.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)
        conf.mass = {"W" : "MW"}
        conf.scales = ["MW", "q1", "q2", "q3"]
        conf.externalMomenta = {"q1":"q", "q2":"q", "q3":"-2*q"}

        minimizer = TopoMinimizer(conf)
        minimizer.verbose = False

        minimizer.filter([triangle, box])


        self.assertListEqual([a.diagram.diagramNumber for a in minimizer.uniqueDiagramsAnalyzers], [2])
        self.assertDictEqual(minimizer.diagramMappings, {"d1":"d2"})
        self.assertDictEqual(minimizer.internalMomentumMappingConfigurations["d1"], {"p1":"p3","p2":"p2","p3":"p1"}, "or {'d1': [4,2,1]} or {'d1': [3,1,2]}")


    def test_topologyEmbeddingFirstLineCut(self):
        # try to embed a triangle topology (3 lines) into a square topology (4 lines)
        box = Diagram()
        box.internalMomenta = [["p4", 1, 2, "W", "W", 0,0, "MW"], ["p3", 3, 4, "W", "W", 0,0, "MW"], ["p2", 3, 1, "fq", "fQ", 0,0, ""], ["p1", 4, 2, "fq", "fQ", 0,0, ""]]
        box.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        box.diagramNumber = 2
        box.name = "d2"
        
        box.createTopologicalVertexAndLineLists()

        triangle = Diagram()
        triangle.internalMomenta = [["p1", 1, 3, "fq", "fQ", 0,0, ""], ["p2", 1, 2, "W", "W", 0,0, "MW"], ["p3", 3, 2,  "W", "W", 0,0, "MW"]]
        triangle.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0]]
        triangle.diagramNumber = 1
        triangle.name = "d1"
        
        triangle.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)
        conf.mass = {"W" : "MW"}
        conf.scales = ["MW", "q1", "q2", "q3"]

        minimizer = TopoMinimizer(conf)
        minimizer.verbose = False

        minimizer.filter([triangle, box])

        self.assertListEqual([a.diagram.diagramNumber for a in minimizer.uniqueDiagramsAnalyzers], [2])
        self.assertDictEqual(minimizer.diagramMappings, {"d1":"d2"})
        self.assertDictEqual(minimizer.internalMomentumMappingConfigurations["d1"], {"p1":"p2","p2":"p4","p3":"p3"})


    def test_allCombinationsOfVariableLength(self):
        combinations = TopoMinimizer.allCombinationsOfVariableLength([1,2,3])
        self.assertListEqual(sorted(combinations), sorted([(), (1,), (2,), (3,), (1, 2), (1, 3), (2, 1), (2, 3), (3, 1), (3, 2), (1, 2, 3), (1, 3, 2), (2, 1, 3), (2, 3, 1), (3, 1, 2), (3, 2, 1)]))


    def test_nickelIndexBox(self):
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, "M1"], ["p2", 1, 3, "g", "g", 0,0, ""], ["p3", 2, 4, "g", "g", 0,0, ""], ["p4", 4, 3, "fq", "fQ", 0,0, "M1"]]
        box.externalMomenta = [["q1", 1, "g", 0], ["q2", 3, "g", 0], ["q3", 2,  "g", 0], ["q4", 4, "g", 0]]
        box.diagramNumber = 1
        box.createTopologicalVertexAndLineLists()

        analyzer = TopologyAnalyzer(box)

        self.assertEqual(analyzer.findNickelIndex(["M1"]), "e12|e3|e3|e| : q1_M1_|q3_|q2_M1|q4|")


    def test_nickelIndexBoxWithSameExternalMomenta(self):
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, "M1"], ["p2", 1, 3, "g", "g", 0,0, ""], ["p3", 2, 4, "g", "g", 0,0, ""], ["p4", 4, 3, "fq", "fQ", 0,0, "M1"]]
        box.externalMomenta = [["q1", 1, "g", 0], ["q2", 3, "g", 0], ["q3", 2,  "g", 0], ["q4", 4, "g", 0]]
        box.diagramNumber = 1
        box.createTopologicalVertexAndLineLists()

        analyzer = TopologyAnalyzer(box, {"q1":"q", "q2":"q", "q3":"-q", "q4":"-q"})

        self.assertEqual(analyzer.findNickelIndex(["M1"]), "e12|e3|e3|e| : q_M1_|-q_|q_M1|-q|")


    def test_nickelIndexBoxWithExternalLinesRemovedButReplaced(self):
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, "M1"], ["p2", 1, 3, "g", "g", 0,0, ""], ["p3", 2, 4, "g", "g", 0,0, ""], ["p4", 4, 3, "fq", "fQ", 0,0, "M1"]]
        box.externalMomenta = [["q1", 1, "g", 0], ["q4", 4, "g", 0]]
        box.diagramNumber = 1
        box.createTopologicalVertexAndLineLists()

        analyzer = TopologyAnalyzer(box, {"q4":"q2"})

        self.assertEqual(analyzer.findNickelIndex(["M1"]), "12|e3|e3| : M1_|q1_|q2_M1|")


    def test_nickelOrderedMomentaDifferingSigns(self):
        box1 = Diagram()
        box1.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, "M1"], ["p2", 1, 3, "g", "g", 0,0, ""], ["p3", 2, 4, "g", "g", 0,0, ""], ["p4", 4, 3, "fq", "fQ", 0,0, "M1"]]
        box1.externalMomenta = [["q1", 1, "g", 0], ["q2", 3, "g", 0], ["q3", 2,  "g", 0], ["q4", 4, "g", 0]]
        box1.diagramNumber = 1
        box1.createTopologicalVertexAndLineLists()
        box1.name = "b1"
        analyzer1 = TopologyAnalyzer(box1)
        analyzer1.findNickelIndex(["M1"])

        box2 = Diagram()
        box2.internalMomenta = [["p1", 2, 1, "fq", "fQ", 0,0, "M1"], ["p2", 1, 3, "g", "g", 0,0, ""], ["p3", 2, 4, "g", "g", 0,0, ""], ["p4", 4, 3, "fq", "fQ", 0,0, "M1"]]
        box2.externalMomenta = [["q1", 1, "g", 0], ["q2", 3, "g", 0], ["q3", 2,  "g", 0], ["q4", 4, "g", 0]]
        box2.diagramNumber = 2
        box2.createTopologicalVertexAndLineLists()
        box2.name = "b2"
        analyzer2 = TopologyAnalyzer(box2)
        analyzer2.findNickelIndex(["M1"])

        self.assertEqual(analyzer1.nickelIndex, analyzer2.nickelIndex)
        self.assertNotEqual(analyzer1.nickelOrderedMomenta, analyzer2.nickelOrderedMomenta)
        self.assertEqual(analyzer1.nickelOrderedMomenta[1:], analyzer2.nickelOrderedMomenta[1:])
        self.assertEqual(sy.sympify(analyzer1.nickelOrderedMomenta[0] + "+" + analyzer2.nickelOrderedMomenta[0]), 0)

        conf = Conf("test/qqqq.conf", False)
        conf.mass = {"fq" : "M1"}
        conf.scales = ["M1", "q1", "q2", "q3"]

        minimizer = TopoMinimizer(conf)
        minimizer.verbose = False

        analyzersCoarse = minimizer.coarseFilter([analyzer1, analyzer2])

        self.assertDictEqual(minimizer.diagramMappings, {"b2":"b1"})
        self.assertDictEqual(minimizer.internalMomentumMappingConfigurations["b2"], {"p1":"-p1","p2":"p2","p3":"p3","p4":"p4"})


    def test_nickelIndexMultipleMasses(self):
        d = Diagram()
        d.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, "M2"], ["p2", 1, 2, "fq", "fQ", 0,0, "ms"], ["p3", 2, 2, "fq", "fQ", 0,0, "mb"]]
        d.externalMomenta = [["q1", 1, "fq", 0], ["q2", 3, "fq", 0]]
        d.diagramNumber = 1
        d.createTopologicalVertexAndLineLists()

        analyzer = TopologyAnalyzer(d)

        self.assertEqual(analyzer.findNickelIndex(["mb","M2","ms"]), "011|e|e| : mb_M2_ms|q1|q2|")


    def test_deleteMultipleLines(self):
        d1 = Diagram()
        d1.internalMomenta = [['p1', 1, 2, 'g', 'g', 1, 2, ''],
            ['p2', 1, 5, 'fT1', 'ft1', 1, 5, 'M1'],
            ['p3', 6, 2, 'fT1', 'ft1', 6, 2, 'M1'],
            ['p4', 5, 3, 'g', 'g', 5, 3, ''],
            ['p5', 6, 3, 'g', 'g', 6, 3, ''],
            ['p6', 4, 5, 'g', 'g', 4, 5, ''],
            ['p7', 4, 6, 'fT1', 'ft1', 4, 6, 'M1']]
        d1.externalMomenta = [['q1', 1, 'g', 1],
            ['q2', 4, 'g', 4]]
        d1.diagramNumber = 1
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)
        conf.mass = {"ft1" : "M1"}
        conf.scales = ["M1", "q1", "q2"]

        minimizer = TopoMinimizer(conf)
        minimizer.verbose = False

        a1 = TopologyAnalyzer(d1, conf.externalMomenta)
        a1 = minimizer.analyzeDiagram(a1)

        self.assertListEqual(a1.permutations, [1, 4, 6, 2, 5, 3, 7])

        a1.deleteLines([4, 6, 7])

        self.assertListEqual(a1.permutations, [1, 2, 4, 3])
        

    def test_updatePresetDiagrams(self):
        knownTopo = Diagram()
        knownTopo.internalMomenta = [['p1', 2, 1, 'g', 'g', 9, 10, ''], ['p2', 5, 1, 'fT1', 'ft1', 11, 12, 'M1'], ['p3', 6, 2, 'fT2', 'ft2', 13, 14, 'M1'], ['p4', 3, 5, 'fT1', 'ft1', 15, 16, 'M1'], ['p5', 6, 3, 'g', 'g', 17, 18, ''], ['p6', 5, 4, 'g', 'g', 19, 20, ''], ['p7', 4, 6, 'fT2', 'ft2', 21, 22, 'M1']]
        knownTopo.externalMomenta = [['q1', 3, 'ft1', 1], ['q2', 1, 'fT1', 2], ['q3', 4, 'ft2', 3], ['q4', 2, 'fT2', 4]]
        knownTopo.createTopologicalVertexAndLineLists()
        knownTopo.name = "knownTopo"

        d2l1 = Diagram()
        d2l1.internalMomenta = [['p1', 1, 2, 'fT1', 'ft1', 9, 10, 'M1'], ['p2', 5, 1, 'g', 'g', 11, 12, ''], ['p3', 6, 2, 'g', 'g', 13, 14, ''], ['p4', 3, 5, 'fT2', 'ft2', 15, 16, 'M1'], ['p5', 6, 3, 'g', 'g', 17, 18, ''], ['p6', 5, 4, 'fT2', 'ft2', 19, 20, 'M1'], ['p7', 6, 4, 'g', 'g', 21, 22, '']]
        d2l1.externalMomenta =[['q1', 1, 'ft1', 1], ['q2', 2, 'fT1', 2], ['q3', 3, 'ft2', 3], ['q4', 4, 'fT2', 4]]
        d2l1.createTopologicalVertexAndLineLists()
        d2l1.name = "d2l1"

        d2l6 = Diagram()
        d2l6.internalMomenta = [['p1', 2, 1, 'g', 'g', 9, 10, ''], ['p2', 5, 1, 'fT1', 'ft1', 11, 12, 'M1'], ['p3', 6, 2, 'fT2', 'ft2', 13, 14, 'M1'], ['p4', 3, 5, 'fT1', 'ft1', 15, 16, 'M1'], ['p5', 6, 3, 'g', 'g', 17, 18, ''], ['p6', 5, 4, 'g', 'g', 19, 20, ''], ['p7', 4, 6, 'fT2', 'ft2', 21, 22, 'M1']]
        d2l6.externalMomenta = [['q1', 3, 'ft1', 1], ['q2', 1, 'fT1', 2], ['q3', 4, 'ft2', 3], ['q4', 2, 'fT2', 4]]
        d2l6.createTopologicalVertexAndLineLists()
        d2l6.name = "d2l6"

        d2l7 = Diagram()
        d2l7.internalMomenta = [['p1', 2, 1, 'fT2', 'ft2', 9, 10, 'M1'], ['p2', 5, 2, 'g', 'g', 11, 12, ''], ['p3', 6, 1, 'g', 'g', 13, 14, ''], ['p4', 3, 5, 'fT1', 'ft1', 15, 16, 'M1'], ['p5', 6, 3, 'g', 'g', 17, 18, ''], ['p6', 5, 4, 'fT1', 'ft1', 19, 20, 'M1'], ['p7', 6, 4, 'g', 'g', 21, 22, '']]
        d2l7.externalMomenta = [['q1', 2, 'ft1', 1], ['q2', 1, 'fT1', 2], ['q3', 3, 'ft2', 3], ['q4', 4, 'fT2', 4]]
        d2l7.createTopologicalVertexAndLineLists()
        d2l7.name = "d2l7"

        conf = Conf("test/qqqq.conf")

        minimizer = TopoMinimizer(conf)
        minimizer.verbose = False

        diagrams = minimizer.filter([d2l1, d2l6, d2l7], presetDiagrams=[knownTopo])

        self.assertListEqual([d.name for d in diagrams], ["d2l1"])
        self.assertDictEqual(minimizer.diagramMappings, {"d2l6":"knownTopo", "d2l7":"d2l1"})
        self.assertNotEqual([d.nickelIndex for d in diagrams], ["" for d in diagrams])


    def test_fineCoarseFilterDifference(self):
        # try to embed a triangle topology (3 lines) into a square topology (4 lines)
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "W", "W", 0,0, "MW"], ["p2", 3, 4, "W", "W", 0,0, "MW"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
        box.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        box.diagramNumber = 2
        box.name = "d2"
        box.createTopologicalVertexAndLineLists()

        triangle1 = Diagram()
        triangle1.internalMomenta = [["p1", 1, 3, "fq", "fQ", 0,0, ""], ["p2", 1, 2, "W", "W", 0,0, "MW"], ["p3", 3, 2,  "W", "W", 0,0, "MW"]]
        triangle1.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0]]
        triangle1.diagramNumber = 1
        triangle1.name = "d1"
        triangle1.createTopologicalVertexAndLineLists()

        triangle2 = Diagram()
        triangle2.internalMomenta = [["p2", 1, 2, "fq", "fQ", 0,0, ""], ["p3", 1, 3, "W", "W", 0,0, "MW"], ["p1", 2, 3,  "W", "W", 0,0, "MW"]]
        triangle2.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 2, "g", 0], ["-q3", 3,  "g", 0]]
        triangle2.diagramNumber = 3
        triangle2.name = "d3"
        triangle2.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)
        conf.mass = {"W" : "MW"}
        conf.scales = ["MW", "q1", "q2", "q3"]

        minimizer = TopoMinimizer(conf)
        minimizer.verbose = False

        analyzersCoarse = minimizer.coarseFilter([TopologyAnalyzer(d) for d in [triangle1, triangle2, box]])
        self.assertListEqual([a.diagram.name for a in analyzersCoarse], ["d1", "d2"])

        analyzersCoarse = minimizer.fineFilter([TopologyAnalyzer(d) for d in [triangle1, triangle2, box]])
        self.assertListEqual([a.diagram.name for a in analyzersCoarse], ["d2"])
        
        
    def test_findScalelessIntegral(self):
        # Example 23 from Jens Hoff's PhD Thesis with a 2-loop tadpole
        prop1 = Diagram()
        prop1.internalMomenta = [["p1", 1, 2, "g", "g", 0, 0, ""],["p2", 2, 1, "g", "g", 0, 0, ""],["p3", 2, 3, "g", "g", 0, 0, ""],["p4", 3, 4, "g", "g", 0, 0, ""],["p5", 3, 4, "g", "g", 0, 0, ""],["p6", 4, 2, "g", "g", 0, 0, ""]]
        prop1.externalMomenta = [["q1", 1, "g", 0], ["q2", 2, "g", 0]]
        prop1.diagramNumber = 1
        prop1.name = "prop1"
        prop1.createTopologicalVertexAndLineLists()
        
        # Example 24 from Jens Hoff's PhD Thesis
        prop2 = Diagram()
        prop2.internalMomenta = [["p1", 1, 2, "g", "g", 0, 0, ""],["p2", 2, 1, "g", "g", 0, 0, ""],["p3", 1, 2, "g", "g", 0, 0, ""]]
        prop2.externalMomenta = [["q1", 1, "g", 0], ["q2", 2, "g", 0]]
        prop2.diagramNumber = 2
        prop2.name = "prop2"
        prop2.createTopologicalVertexAndLineLists()
        
        # Example 23 from Jens Hoff's PhD Thesis with a massless 1-loop tadpole
        prop3 = Diagram()
        prop3.internalMomenta = [["p1", 1, 2, "g", "g", 0, 0, ""],["p2", 2, 1, "g", "g", 0, 0, ""],["p3", 2, 2, "g", "g", 0, 0, ""]]
        prop3.externalMomenta = [["q1", 1, "g", 0], ["q2", 2, "g", 0]]
        prop3.diagramNumber = 3
        prop3.name = "prop3"
        prop3.createTopologicalVertexAndLineLists()
        
        # Example 23 from Jens Hoff's PhD Thesis with a massive 1-loop tadpole
        prop4 = Diagram()
        prop4.internalMomenta = [["p1", 1, 2, "g", "g", 0, 0, ""],["p2", 2, 1, "g", "g", 0, 0, ""],["p3", 2, 2, "ft1", "fT1", 0, 0, "M1"]]
        prop4.externalMomenta = [["q1", 1, "g", 0], ["q2", 2, "g", 0]]
        prop4.diagramNumber = 4
        prop4.name = "prop4"
        prop4.createTopologicalVertexAndLineLists()
        
        conf = Conf("test/qqqq.conf", False)
        conf.scales = ["q1","M1"]
        
        assigner1 = MomentumAssigner(conf,prop1)
        assigner2 = MomentumAssigner(conf,prop2)
        assigner3 = MomentumAssigner(conf,prop3)
        assigner4 = MomentumAssigner(conf,prop4)
        
        assigner1.generateLineMassesDict()
        assigner2.generateLineMassesDict()
        assigner3.generateLineMassesDict()
        assigner4.generateLineMassesDict()
        
        assigner1.calculateMomentumRelations()
        assigner2.calculateMomentumRelations()
        assigner3.calculateMomentumRelations()
        assigner4.calculateMomentumRelations()
        
        U1,F1 = AnalyticTopoMapper.UF(assigner1.loopMomenta,assigner1.analyticTopologyExpression[:len(prop1.internalMomenta)-len(assigner1.equalMomentumDict)])
        U2,F2 = AnalyticTopoMapper.UF(assigner2.loopMomenta,assigner2.analyticTopologyExpression[:len(prop2.internalMomenta)-len(assigner2.equalMomentumDict)])
        U3,F3 = AnalyticTopoMapper.UF(assigner3.loopMomenta,assigner3.analyticTopologyExpression[:len(prop3.internalMomenta)-len(assigner3.equalMomentumDict)])
        U4,F4 = AnalyticTopoMapper.UF(assigner4.loopMomenta,assigner4.analyticTopologyExpression[:len(prop4.internalMomenta)-len(assigner4.equalMomentumDict)])

        # Scaleless
        self.assertTrue(TopologyAnalyzer.isScaleless(U1,F1,assigner1.analyticTopologyExpression[:len(prop1.internalMomenta)-len(assigner1.equalMomentumDict)]))
        
        # Scalefull
        self.assertFalse(TopologyAnalyzer.isScaleless(U2,F2,assigner2.analyticTopologyExpression[:len(prop2.internalMomenta)-len(assigner2.equalMomentumDict)]))
        
        # Scaleless
        self.assertTrue(TopologyAnalyzer.isScaleless(U3,F3,assigner3.analyticTopologyExpression[:len(prop3.internalMomenta)-len(assigner3.equalMomentumDict)]))
        
        # Scalefull
        self.assertFalse(TopologyAnalyzer.isScaleless(U4,F4,assigner4.analyticTopologyExpression[:len(prop4.internalMomenta)-len(assigner4.equalMomentumDict)]))
