import unittest
import logging
from modules.diagram import Diagram, Vertex, Line
from modules.config import Conf 
from modules.topselReader import TopselReader
from modules.ediaGenerator import EdiaGenerator
import sympy as sy
import re
import copy


class testTopselReader(unittest.TestCase):
    def test_topselReaderEasyInputOutputComparison(self):
        config = Conf("test/qqqq.conf", False)
        topselReader = TopselReader(config)
        diagrams = topselReader.getDiagrams("test/testInputShort.topsel")

        """
        Test the following fields of Diagram class:

        diagram.diagramNumber 
        #diagram.preFactor 
        diagram.numLoops 
        diagram.externalMomenta 
        diagram.internalMomenta 
        diagram.topologicalVertexList 
        #diagram.fermionLines 
        diagram.name 

        of 
        '''
        {inpt1,poco_scale,copy_scale;5;2;1;0; ;(q1:1,3)(p1:2,3)(p2:1,2)(p3:4,1)(p4:3,4)(p5:4,2);   00100}


        # this is a comment

         # {inpt2,poco_scale,copy_scale;5;2;1;0; ;
         #      (q3:1,4)(p1:3,4)(p2:1,4)(p3:2,3)(p4:2,3)(p5:1,2);
         #     00000}
        
        {inpt2,poco_scale,copy_scale;5;2;1;0; ;
            (q3:1,4)(p1:3,4)(p2:1,4)(p3:2,3)(p4:2,3)(p5:1,2);
            00000}


            
       '''
        """

        self.assertEqual(len(diagrams), 2)

        # first entry
        self.assertEqual(diagrams[0].name, "inpt1")
        self.assertEqual(diagrams[0].diagramNumber, 1)
        self.assertEqual(diagrams[0].numLoops, 2)
        self.assertListEqual(diagrams[0].externalMomenta, [["q1", 1, "dummyMasslessParticle", 1], ["q2", 3, "dummyMasslessParticle", 3]])
        self.assertListEqual(diagrams[0].internalMomenta,
         [    ["p1", 2, 3, "dummyMasslessParticle", "dummyMasslessParticle", 2, 3, ""], 
              ["p2", 1, 2, "dummyMasslessParticle", "dummyMasslessParticle", 1, 2, ""],
              ["p3", 4, 1, "dummyM1particle", "dummyM1particle", 4, 1, "M1"],
              ["p4", 3, 4, "dummyMasslessParticle", "dummyMasslessParticle", 3, 4, ""],
              ["p5", 4, 2, "dummyMasslessParticle", "dummyMasslessParticle", 4, 2, ""]
            ])

        # second entry
        self.assertEqual(diagrams[1].name, "inpt2")
        self.assertEqual(diagrams[1].diagramNumber, 2)
        self.assertEqual(diagrams[1].numLoops, 2)
        self.assertListEqual(diagrams[1].externalMomenta, [["q3", 1, "dummyMasslessParticle", 1], ["q1", 4, "dummyMasslessParticle", 4]])
        self.assertListEqual(diagrams[1].internalMomenta,
         [    ["p1", 3, 4, "dummyMasslessParticle", "dummyMasslessParticle", 3, 4, ""], 
              ["p2", 1, 4, "dummyMasslessParticle", "dummyMasslessParticle", 1, 4, ""],
              ["p3", 2, 3, "dummyMasslessParticle", "dummyMasslessParticle", 2, 3, ""],
              ["p4", 2, 3, "dummyMasslessParticle", "dummyMasslessParticle", 2, 3, ""],
              ["p5", 1, 2, "dummyMasslessParticle", "dummyMasslessParticle", 1, 2, ""]
            ])

        self.assertEqual(topselReader.config.mass["dummyM1particle"], "M1")


    def test_topselReaderLargeInputOutputComparison(self):
        config = Conf("test/qqqq.conf", False)
        topselReader = TopselReader(config)
        diagrams = topselReader.getDiagrams("test/testInputLong.topsel")

        """
        test longer topsel input with more difficulties
        E.g. The first entry looks like
            {ggHHR3L1s1;9;3;3;3; ;
            (q1:1,3)(q2:2,3)(q3:4,3)
            (p1:1,2)(p2:2,6)(p3:6,7)(p4:7,4)(p5:7,4)(p6:4,3)(p7:3,5)(p8:5,1)(p9:6,5);
            003220000} 
        """

        self.assertEqual(len(diagrams), 16)

        # first entry
        self.assertEqual(diagrams[0].name, "ggHHR3L1s1")
        self.assertEqual(diagrams[0].diagramNumber, 1)
        self.assertEqual(diagrams[0].numLoops, 3)
        self.assertListEqual(diagrams[0].externalMomenta, [["q1", 1, "dummyMasslessParticle", 1], ["q2", 2, "dummyMasslessParticle", 2],
                                                           ["q3", 4, "dummyMasslessParticle", 4], ["q4", 3, "dummyMasslessParticle", 3]])
        self.assertListEqual(diagrams[0].internalMomenta,
         [    ["p1", 1, 2, "dummyMasslessParticle", "dummyMasslessParticle", 1, 2, ""], 
              ["p2", 2, 6, "dummyMasslessParticle", "dummyMasslessParticle", 2, 6, ""],
              ["p3", 6, 7, "dummyM3particle", "dummyM3particle", 6, 7, "M3"],
              ["p4", 7, 4, "dummyM2particle", "dummyM2particle", 7, 4, "M2"],
              ["p5", 7, 4, "dummyM2particle", "dummyM2particle", 7, 4, "M2"],
              ["p6", 4, 3, "dummyMasslessParticle", "dummyMasslessParticle", 4, 3, ""],
              ["p7", 3, 5, "dummyMasslessParticle", "dummyMasslessParticle", 3, 5, ""],
              ["p8", 5, 1, "dummyMasslessParticle", "dummyMasslessParticle", 5, 1, ""],
              ["p9", 6, 5, "dummyMasslessParticle", "dummyMasslessParticle", 6, 5, ""]
            ])
        

    def test_topselReaderLargeInputToOutputComparison(self):
        # here we test the integration of the topsel reader with the topsel writer 
        # I.e. we check that the same topsel file as our input is generated 
        config = Conf("test/qqqq.conf", False)
        topselReader = TopselReader(config)
        diagrams = topselReader.getDiagrams("test/testInputLong.topsel")

        ediaGenerator = EdiaGenerator(config)
        ediaGenerator.generateEntries(diagrams)
        ediaGenerator.writeTopsel("test/testInputLongOUT.topsel")

        with open("test/testInputLong.topsel") as iFile:
            inputTopsel = iFile.readlines()

        with open("test/testInputLongOUT.topsel") as oFile:
            outputTopsel = oFile.readlines()

        inputTopsel = "".join(inputTopsel)
        inputTopsel = re.sub(r'\n', r'', inputTopsel)
        inputTopsel = re.sub(r' ', r'', inputTopsel)
        inputTopsel = re.sub(r'{[^;]*;', r'', inputTopsel)
        inputTopsel = re.sub(r'}', r'}\n', inputTopsel)
        inputTopsel = inputTopsel.split("\n")
        
        outputTopsel = "".join(outputTopsel)
        outputTopsel = re.sub(r'\n', r'', outputTopsel)
        outputTopsel = re.sub(r' ', r'', outputTopsel)
        outputTopsel = re.sub(r'{[^;]*;', r'', outputTopsel)
        outputTopsel = re.sub(r'}', r'}\n', outputTopsel)
        outputTopsel = outputTopsel.split("\n")

        self.assertEqual(len(inputTopsel), len(outputTopsel))

        for i in range(len(inputTopsel)):
            self.assertEqual(inputTopsel[i], outputTopsel[i])