from modules.qgrafReader import QgrafReader
from modules.diagram import Diagram
from modules.config import Conf
import unittest

class testQgrafReader(unittest.TestCase):
    def test_readDiagram(self):
        config = Conf("test/qqqq.conf",False)
        # parse qlist file 
        qgrafReader = QgrafReader(config)

        testDiagram = []
        testDiagram.append("diagram            42")
        testDiagram.append("pre_factor         (-1)*1")
        testDiagram.append("")
        testDiagram.append("number_propagators 7")
        testDiagram.append("number_loops       2")
        testDiagram.append("number_legs_in     4")
        testDiagram.append("number_legs_out    0")
        testDiagram.append("")
        testDiagram.append("")
        testDiagram.append("external_leg       q1|4|ft1")
        testDiagram.append("external_leg       q2|3|fT1")
        testDiagram.append("external_leg       q3|1|ft2")
        testDiagram.append("external_leg       q4|2|fT2")
        testDiagram.append("")
        testDiagram.append("")
        testDiagram.append("momentum           p1|1,2|fT2,ft2")
        testDiagram.append("momentum           p2|3,1|g,g")
        testDiagram.append("momentum           p3|4,2|g,g")
        testDiagram.append("momentum           p4|5,3|fT1,ft1")
        testDiagram.append("momentum           p5|4,6|fT1,ft1")
        testDiagram.append("momentum           p6|6,5|fT1,ft1")
        testDiagram.append("momentum           p7|6,5|g,g")

        diagram = qgrafReader.readDiagram(testDiagram)

        self.assertEqual(diagram.diagramNumber, 42)
        self.assertEqual(diagram.preFactor, "(-1)*1")
        self.assertEqual(diagram.numLoops, 2)

        self.assertListEqual(diagram.externalMomenta, [["q1", 4, "ft1",1], ["q2", 3, "fT1",2], ["q3", 1, "ft2",3], ["q4", 2, "fT2",4]])
        self.assertListEqual([v[:5] for v in diagram.internalMomenta], [["p1", 1, 2, "fT2", "ft2"],\
             ["p2", 3, 1, "g", "g"], ["p3", 4, 2, "g", "g"], ["p4", 5, 3, "fT1", "ft1"],\
             ["p5", 4, 6, "fT1", "ft1"], ["p6", 6, 5, "fT1", "ft1"], ["p7", 6, 5, "g", "g"]])


    def test_getDiagram(self):
        config = Conf("test/qqqq.conf",False)
        # parse qlist file 
        qgrafReader = QgrafReader(config)
        diagrams = qgrafReader.getDiagrams("test/qlist.2")

        # check for the right amount of diagrams and for the correct class used
        self.assertEqual(len(diagrams), 63)
        self.assertEqual(str(type(diagrams[8])) , "<class 'modules.diagram.Diagram'>")

    def test_diagram(self):
        diagram = Diagram()
        diagram.diagramNumber = 42
        diagram.preFactor = "(-1)*1"
        diagram.numLoops = 2
        diagram.externalMomenta = [["q1", 4, "ft1",1], ["q2", 3, "fT1",2], ["q3", 1, "ft2",3], ["q4", 2, "fT2",4]]
        diagram.internalMomenta = [["p1", 1, 2, "fT2", "ft2", 9, 10, "M2"],\
             ["p2", 3, 1, "g", "g", 11, 12, ""], ["p3", 4, 2, "g", "g", 13, 14, ""], ["p4", 5, 3, "g", "g", 15, 16, ""],\
             ["p5", 4, 6, "fT1", "ft1", 17, 18, "M1"], ["p6", 6, 5, "g", "g", 19, 20, ""], ["p7", 6, 5, "g", "g", 21, 22, ""]]

        diagram.createTopologicalVertexAndLineLists()
        # print([[v.number for v in fermionLine.vertices] for fermionLine in diagram.fermionLines])
        # test correct class
        self.assertEqual(str(type(diagram.topologicalVertexList[0])) , "<class 'modules.diagram.Vertex'>")
        # test number, fermionic attached lines and number of lines attached to each vertex
        self.assertListEqual([[v.number, v.fermionic, len(v.lines)] for v in diagram.topologicalVertexList] , \
            [[1, True, 3], [2, True, 3], [3, True, 3], [4, True, 3], [5, False, 3], [6, True, 3]])
        # test line list
        lines = diagram.topologicalLineList
        self.assertEqual(
            sorted([[l.external, l.momentum, l.start, l.end, l.incomingParticle, l.outgoingParticle, l.mass] for l in diagram.topologicalLineList]),
            sorted([[True, "q1", -1, 4, "ft1", "", ""], [True, "q2", -2, 3, "fT1", "", ""], [True, "q3", -3, 1, "ft2", "", ""], [True, "q4", -4, 2, "fT2", "", ""], [False, "p1", 1, 2, "fT2", "ft2", "M2"],\
             [False, "p2", 3, 1, "g", "g", ""], [False, "p3", 4, 2, "g", "g", ""], [False, "p4", 5, 3, "g", "g", ""],\
             [False, "p5", 4, 6, "fT1", "ft1", "M1"], [False, "p6", 6, 5, "g", "g", ""], [False, "p7", 6, 5, "g", "g", ""]])
        )
        # test vertex methods
        vert = diagram.topologicalVertexList[0]
        self.assertListEqual(vert.getOutgoingParticles(), ["ft2", "fT2", "g"])
        self.assertListEqual(vert.getOutgoingMomenta(), ["-q3", "p1", "-p2"])
        self.assertListEqual(vert.getIndices(4), [3, 9, 12])

        # test lines of the vertex
        self.assertListEqual([[l.external, l.start, l.end, l.momentum, l.incomingParticle, l.outgoingParticle, l.mass] for l in vert.lines],\
             [[True, 1, -3, "-q3", "", "ft2", ""], [False, 1, 2, "p1", "fT2", "ft2", "M2"], [False, 1, 3, "-p2", "g", "g", ""]])

    def test_get4Fermi1l(self):
        config = Conf("test/fermi.conf",False)
        # parse qlist file 
        qgrafReader = QgrafReader(config)
        diagrams = qgrafReader.getDiagrams("test/fermi1l.qlist")

        self.assertEqual(diagrams[0].diagramNumber, 1)
        self.assertEqual(diagrams[0].preFactor, "(-1)*1")
        self.assertEqual(diagrams[0].numLoops, 2)
        self.assertListEqual(diagrams[0].internalMomenta,[['p1', 2, 1, 'fEl', 'fel', 5, 6, 'M2'], ['p2', 1, 2, 'fNumu', 'fnumu', 7, 8, ''], ['p3', 1, 2, 'fNuel', 'fnuel', 9, 10, '']])
        self.assertListEqual(diagrams[0].externalMomenta,[['q1', 1, 'fmu', 1], ['q2', 2, 'fMu', 2]])



    def test_read4Fermi3l(self):
        config = Conf("test/fermi.conf",False)
        qgrafReader = QgrafReader(config)

        testDiagram = []
        testDiagram.append("diagram            6")
        testDiagram.append("pre_factor         (-1)*1")
        testDiagram.append("")
        testDiagram.append("number_propagators 9")
        testDiagram.append("number_loops       4")
        testDiagram.append("number_legs_in     2")
        testDiagram.append("number_legs_out    0")
        testDiagram.append("")
        testDiagram.append("")
        testDiagram.append("external_leg       q1|1|fmu")
        testDiagram.append("external_leg       q2|2|fMu")
        testDiagram.append("")
        testDiagram.append("")
        testDiagram.append("momentum           p1|3,1|a,a")
        testDiagram.append("momentum           p2|1,5|fMu,fmu")
        testDiagram.append("momentum           p3|4,2|a,a")
        testDiagram.append("momentum           p4|6,2|fMu,fmu")
        testDiagram.append("momentum           p5|3,4|fEl,fel")
        testDiagram.append("momentum           p6|6,3|fEl,fel")
        testDiagram.append("momentum           p7|4,5|fEl,fel")
        testDiagram.append("momentum           p8|5,6|fNumu,fnumu")
        testDiagram.append("momentum           p9|5,6|fNuel,fnuel")

        diagram = qgrafReader.readDiagram(testDiagram)

        self.assertEqual(diagram.diagramNumber, 6)
        self.assertEqual(diagram.preFactor, "(-1)*1")
        self.assertEqual(diagram.numLoops, 4)
        self.assertListEqual(diagram.externalMomenta, [['q1', 1, 'fmu', 1], ['q2', 2, 'fMu', 2]])
        self.assertListEqual(diagram.internalMomenta, [['p1', 3, 1, 'a', 'a', 5, 6, ''], ['p2', 1, 5, 'fMu', 'fmu', 7, 8, 'M1'], ['p3', 4, 2, 'a', 'a', 9, 10, ''],\
                                                       ['p4', 6, 2, 'fMu', 'fmu', 11, 12, 'M1'], ['p5', 3, 4, 'fEl', 'fel', 13, 14, 'M2'], ['p6', 6, 3, 'fEl', 'fel', 15, 16, 'M2'],\
                                                       ['p7', 4, 5, 'fEl', 'fel', 17, 18, 'M2'], ['p8', 5, 6, 'fNumu', 'fnumu', 19, 20, ''], ['p9', 5, 6, 'fNuel', 'fnuel', 21, 22, '']])


    def test_createTopologicalVertexAndLineLists(self):
        diagram = Diagram()
        diagram.internalMomenta = [
            ["p1", 1, 2, "g", "g", 1, 2, ""],
            ["p2", 2, 3, "g", "g", 2, 3, ""],
            ["p3", 4, 3, "g", "g", 4, 3, ""],
            ["p4", 1, 4, "g", "g", 1, 4, ""],
            ["p5", 2, 4, "fq", "fQ", 2, 4, "M1"]
        ]
        diagram.externalMomenta = [
            ["q1", 1, "g", 1],
            ["q2", 2, "g", 2]
        ]

        diagram.createTopologicalVertexAndLineLists()

        self.assertListEqual(
            [{v.number : [[l.momentum, l.start, l.end] for l in v.lines]} for v in diagram.topologicalVertexList], 
            [{1: [['-q1', 1, -1], ['p1', 1, 2], ['p4', 1, 4]]}, {2: [['-q2', 2, -2], ['-p1', 2, 1], ['p2', 2, 3], ['p5', 2, 4]]},
             {3: [['-p2', 3, 2], ['-p3', 3, 4]]}, {4: [['p3', 4, 3], ['-p4', 4, 1], ['-p5', 4, 2]]}]
            )

        self.assertListEqual([[v.number, v.fermionic, len(v.lines)] for v in diagram.topologicalVertexList] ,
            [[1, False, 3], [2, True, 4], [3, False, 2], [4, True, 3]])


    def test_fermionSelfLoopWithSecondFermionLine(self):
        config = Conf("test/qqqq.conf",False)
        config.spinorIndices = False
        qgrafReader = QgrafReader(config)

        testDiagram = []
        testDiagram.append("diagram            99")
        testDiagram.append("pre_factor         (+1)*1")
        testDiagram.append("")
        testDiagram.append("number_propagators 8")
        testDiagram.append("number_loops       2")
        testDiagram.append("number_legs_in     5")
        testDiagram.append("number_legs_out    0")
        testDiagram.append("")
        testDiagram.append("")
        testDiagram.append("external_leg       q1|1|ft1")
        testDiagram.append("external_leg       q2|1|fT1")
        testDiagram.append("external_leg       q3|2|ft2")
        testDiagram.append("external_leg       q4|2|fT2")
        testDiagram.append("external_leg       q5|3|g")
        testDiagram.append("")
        testDiagram.append("")
        testDiagram.append("momentum           p1|5,1|g,g")
        testDiagram.append("momentum           p2|5,7|fT1,ft1")
        testDiagram.append("momentum           p3|7,6|fT1,ft1")
        testDiagram.append("momentum           p4|3,5|fT1,ft1")
        testDiagram.append("momentum           p5|6,3|fT1,ft1")
        testDiagram.append("momentum           p6|4,4|fT1,ft1")
        testDiagram.append("momentum           p7|6,4|g,g")
        testDiagram.append("momentum           p8|7,2|g,g")

        diagram = qgrafReader.readDiagram(testDiagram)

        self.assertEqual(diagram.diagramNumber, 99)
        self.assertEqual(diagram.preFactor, "(+1)*1")
        self.assertEqual(diagram.numLoops, 2)
        self.assertEqual(diagram.externalMomenta,[['q1', 1, 'ft1', 1], ['q2', 1, 'fT1', 2], ['q3', 2, 'ft2', 3], ['q4', 2, 'fT2', 4], ['q5', 3, 'g', 5]])
        self.assertEqual(diagram.internalMomenta,[['p1', 5, 1, 'g', 'g', 11, 12, ''], ['p2', 5, 7, 'fT1', 'ft1', 13, 14, 'M1'], ['p3', 7, 6, 'fT1', 'ft1', 15, 16, 'M1'], ['p4', 3, 5, 'fT1', 'ft1', 17, 18, 'M1'], ['p5', 6, 3, 'fT1', 'ft1', 19, 20, 'M1'], ['p6', 4, 4, 'fT1', 'ft1', 21, 22, 'M1'], ['p7', 6, 4, 'g', 'g', 23, 24, ''], ['p8', 7, 2, 'g', 'g', 25, 26, '']])

        self.assertEqual(len(diagram.fermionLines), 4)
        self.assertTrue(not diagram.fermionLines[0].loop)
        self.assertTrue(not diagram.fermionLines[1].loop)
        self.assertTrue(diagram.fermionLines[2].loop)
        self.assertTrue(diagram.fermionLines[3].loop)
        self.assertSetEqual({l.number for l in diagram.fermionLines[2].lines}, {2,3,4,5})
        self.assertSetEqual({l.number for l in diagram.fermionLines[3].lines}, {6})
        self.assertEqual(len(diagram.fermionLines), 4)


    def test_diagram2Qlist(self):
        config = Conf("test/qqqq.conf",False)
        qgrafReader = QgrafReader(config)

        testDiagram = []
        testDiagram.append("diagram            99")
        testDiagram.append("pre_factor         (+1)*1")
        testDiagram.append("")
        testDiagram.append("number_propagators 8")
        testDiagram.append("number_loops       2")
        testDiagram.append("number_legs_in     5")
        testDiagram.append("number_legs_out    0")
        testDiagram.append("")
        testDiagram.append("")
        testDiagram.append("external_leg       q1|1|ft1")
        testDiagram.append("external_leg       q2|1|fT1")
        testDiagram.append("external_leg       q3|2|ft2")
        testDiagram.append("external_leg       q4|2|fT2")
        testDiagram.append("external_leg       q5|3|g")
        testDiagram.append("")
        testDiagram.append("")
        testDiagram.append("momentum           p1|5,1|g,g")
        testDiagram.append("momentum           p2|5,7|fT1,ft1")
        testDiagram.append("momentum           p3|7,6|fT1,ft1")
        testDiagram.append("momentum           p4|3,5|fT1,ft1")
        testDiagram.append("momentum           p5|6,3|fT1,ft1")
        testDiagram.append("momentum           p6|4,4|fT1,ft1")
        testDiagram.append("momentum           p7|6,4|g,g")
        testDiagram.append("momentum           p8|7,2|g,g")

        diagram = qgrafReader.readDiagram(testDiagram)

        # print(qgrafReader.diagram2Qlist(diagram))
        # print("\n\n vs \n\n")
        # print("\n".join(testDiagram))

        self.assertEqual(qgrafReader.diagram2Qlist(diagram), "\n".join(testDiagram))
        