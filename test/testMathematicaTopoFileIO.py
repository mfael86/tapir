from modules.diagram import Diagram
from modules.partialFractionDecomposer import IntegralFamily
from modules.config import Conf
from modules.mathematicaTopoListGenerator import MathematicaTopoListGenerator
from modules.mathematicaTopoListReader import MathematicaTopoListReader
import unittest
import os

class testMathematicaTopoFileIO(unittest.TestCase):
    def test_writeIntegralFamilies(self):
        testFileName = "test/test_MathematicaWriteIntegralFamilies.m"
        config = Conf("test/qqqq.conf",False)
        MathematicaTopoListGenerator(config).writeIntegralFamilies(
            testFileName, 
            [ 
                IntegralFamily("Int1", ['-k1*k2', '-k1^2 - 2*k1*q1 - q1^2', 'M2^2 - k2^2 + 2*k2*q1 - q1^2', '-k1^2', '-k2^2'], ["k1", "k2"]),
                IntegralFamily("Int2", ['-k1*k2', '-k1^2 - 2*k1*q1 - q1^2', 'M2^2 - k2^2 + 2*k2*q1 - q1^2', '-k1^2', '-k2^2'], ["k1", "k2"]),
                IntegralFamily("Int3", ['-k1*k2', '-k1^2 - 2*k1*q1 - q1^2', 'M2^2 - k2^2 + 2*k2*q1 - q1^2', '-k1^2', '-k2^2'], ["k1", "k2"])
            ]
        )

        listFile = open(testFileName, "r")
        listFileString = "".join(listFile.readlines())
        listFile.close()
        os.remove(testFileName)

        self.assertIn('{"Int1", {-k1*k2, -k1^2 - 2*k1*q1 - q1^2, M2^2 - k2^2 + 2*k2*q1 - q1^2, -k1^2, -k2^2}, {k1, k2}}', listFileString)
        self.assertIn('{"Int2", {-k1*k2, -k1^2 - 2*k1*q1 - q1^2, M2^2 - k2^2 + 2*k2*q1 - q1^2, -k1^2, -k2^2}, {k1, k2}}', listFileString)
        self.assertIn('{"Int3", {-k1*k2, -k1^2 - 2*k1*q1 - q1^2, M2^2 - k2^2 + 2*k2*q1 - q1^2, -k1^2, -k2^2}, {k1, k2}}', listFileString)

    
    def test_readIntegralFamilies(self):
        config = Conf("test/qqqq.conf",False)
        mathematicaTopoListReader = MathematicaTopoListReader(config)

        families = mathematicaTopoListReader.readIntegralFamilies("test/familiesMathematicaTest.m")
        self.assertEqual(len(families), 2)
        self.assertListEqual([f.topoName for f in families], ["Int1", "Int2"])
        self.assertListEqual(families[0].analyticTopologyExpression, ["(k1+q1)**2", "(k1+q1+q2)**2", "k1**2", "k2**2-M1**2", "(k1-k2)**2"])
        self.assertListEqual(families[0].loopMomenta, ["k1", "k2"])


    def test_mathematicaToIntegralFamily(self):
        mathematicaString = '{"topo42", {(k1 + q1)^2, (k1 + q1 + q2)^2, k1^2, k2^2-M1^2, (k1-k2)^2}, {k1, k2}}'

        mathematicaTopoListReader = MathematicaTopoListReader(conf = Conf("test/qqqq.conf",False))

        family = mathematicaTopoListReader.mathematicaToIntegralFamily(mathematicaString)

        self.assertEqual(family.topoName, "topo42")
        self.assertEqual(family.loopMomenta, ["k1", "k2"])
        self.assertEqual(family.analyticTopologyExpression, ["(k1+q1)**2", "(k1+q1+q2)**2", "k1**2", "k2**2-M1**2", "(k1-k2)**2"])