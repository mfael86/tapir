from modules.diagram import Diagram
from modules.partialFractionDecomposer import IntegralFamily
from modules.config import Conf
from modules.yamlGenerator import YamlGenerator
import unittest
import os

class testYamlGenerator(unittest.TestCase):
    def init(self):
        diagram = Diagram()
        diagram.diagramNumber = 42
        diagram.preFactor = "(-1)*1"
        diagram.numLoops = 2
        diagram.externalMomenta = [["q1", 4, "ft1",1], ["q2", 3, "fT1",2], ["q3", 1, "ft2",3], ["q4", 2, "fT2",4]]
        diagram.internalMomenta = [["p1", 1, 2, "fT2", "ft2", 9, 10, ""],\
             ["p2", 3, 1, "g", "g", 11, 12, ""], ["p3", 4, 2, "g", "g", 13, 14, ""], ["p4", 5, 3, "g", "g", 15, 16, ""],\
             ["p5", 4, 6, "fT1", "ft1", 17, 18, ""], ["p6", 6, 5, "g", "g", 19, 20, ""], ["p7", 6, 5, "g", "g", 21, 22, ""]]
        diagram.setName()
        return diagram


    def test_diagramToYaml(self):
        diagram = self.init()
        config = Conf("test/qqqq.conf",False)
        yamlStr = YamlGenerator(config).diagramToYaml(diagram)
        self.assertIn("- {incomingMomentum: q1, vertex: 4, incomingParticle: ft1}", yamlStr)
        self.assertIn("- {incomingMomentum: q4, vertex: 2, incomingParticle: fT2}", yamlStr)
        self.assertIn("- {momentum: p4, vertices: [5, 3], incomingParticle: g, outgoingParticle: g, mass: }", yamlStr)
        self.assertIn("preFactor: (-1)*1", yamlStr)
        self.assertIn("loops: 2", yamlStr)
        self.assertIn("diagramNumber: 42", yamlStr)


    def test_writeDiagrams(self):
        testFileName = "test/test_YamlWriteDiagrams1.yaml"
        diagram = self.init()
        config = Conf("test/qqqq.conf",False)
        YamlGenerator(config).writeDiagrams(testFileName, [diagram, diagram])
        yamlFile = open(testFileName, "r")
        yamlFileString = "".join(yamlFile.readlines())
        yamlFile.close()
        os.remove(testFileName)

        self.assertEqual(yamlFileString.count("- {incomingMomentum: q1, vertex: 4, incomingParticle: ft1}"), 2)
        self.assertEqual(yamlFileString.count("- {incomingMomentum: q4, vertex: 2, incomingParticle: fT2}"), 2)
        self.assertEqual(yamlFileString.count("- {momentum: p4, vertices: [5, 3], incomingParticle: g, outgoingParticle: g, mass: }"), 2)
        self.assertEqual(yamlFileString.count("preFactor: (-1)*1"), 2)
        self.assertEqual(yamlFileString.count("loops: 2"), 2)
        self.assertEqual(yamlFileString.count("diagramNumber: 42"), 2)
        self.assertTrue(yamlFileString.count("incomingParticle:") > 1)
        self.assertTrue(yamlFileString.count("outgoingParticle:") > 1)


    def test_writeDiagramsWithDifferentBlockSize(self):
        testFileName = "test/test_YamlWriteDiagrams2.yaml"
        diagram = self.init()
        config = Conf("test/qqqq.conf",False)
        config.blockSize = 1
        YamlGenerator(config).writeDiagrams(testFileName, [diagram, diagram])

        yamlFile1 = open(testFileName+".1", "r")
        yamlFileString1 = "".join(yamlFile1.readlines())
        yamlFile1.close()
        os.remove(testFileName+".1")

        yamlFile2 = open(testFileName+".2", "r")
        yamlFileString2 = "".join(yamlFile2.readlines())
        yamlFile2.close()
        os.remove(testFileName+".2")

        self.assertEqual(yamlFileString1.count("diagramNumber: 42"), 1)
        self.assertEqual(yamlFileString1, yamlFileString2)


    def test_topologyToYaml(self):
        diagram = self.init()
        diagram.nickelIndex = "123|e4|e4|e5|5|e| : M1_M1_|q3_|q4_|q1_M1||q2|"
        config = Conf("test/qqqq.conf",False)
        yamlStr = YamlGenerator(config).topologyToYaml(diagram)
        
        self.assertIn("- {incomingMomentum: q1, vertex: 4}", yamlStr)
        self.assertIn("- {incomingMomentum: q4, vertex: 2}", yamlStr)
        self.assertIn("- {momentum: p4, vertices: [5, 3], mass: }", yamlStr)
        self.assertIn("loops: 2", yamlStr)
        self.assertIn("name: d2l42", yamlStr)
        self.assertIn("nickelIndex: 123|e4|e4|e5|5|e| : M1_M1_|q3_|q4_|q1_M1||q2|", yamlStr)


    def test_writeTopologies(self):
        testFileName = "test/test_YamlWriteTopologies1.yaml"
        diagram = self.init()
        diagram.nickelIndex = "123|e4|e4|e5|5|e| : M1_M1_|q3_|q4_|q1_M1||q2|"
        config = Conf("test/qqqq.conf",False)
        YamlGenerator(config).writeTopologies(testFileName, [diagram, diagram])
        yamlFile = open(testFileName, "r")
        yamlFileString = "".join(yamlFile.readlines())
        yamlFile.close()
        os.remove(testFileName)

        self.assertEqual(yamlFileString.count("- {incomingMomentum: q1, vertex: 4}"), 2)
        self.assertEqual(yamlFileString.count("- {incomingMomentum: q4, vertex: 2}"), 2)
        self.assertEqual(yamlFileString.count("- {momentum: p4, vertices: [5, 3], mass: }"), 2)
        self.assertEqual(yamlFileString.count("loops: 2"), 2)
        self.assertEqual(yamlFileString.count("name: d2l42"), 2)
        self.assertEqual(yamlFileString.count("incomingParticle:"), 0)
        self.assertEqual(yamlFileString.count("outgoingParticle:"), 0)


    def test_integralFamilyToYaml(self):
        config = Conf("test/qqqq.conf",False)
        yamlString = YamlGenerator(config).integralFamilyToYaml(IntegralFamily("int1", ['-k1*k2', '-k1^2 - 2*k1*q1 - q1^2', 'M2^2 - k2^2 + 2*k2*q1 - q1^2', '-k1^2', '-k2^2'], ["k1", "k2"]))

        self.assertIn('- name: "int1"', yamlString)
        self.assertIn('loop_momenta: [k1, k2]', yamlString)
        self.assertIn('- ["-k1*k2", 0]', yamlString)
        self.assertIn('- ["-k1^2 - 2*k1*q1 - q1^2", 0]', yamlString)
        self.assertIn('- ["M2^2 - k2^2 + 2*k2*q1 - q1^2", 0]', yamlString)
        self.assertIn('- ["-k1^2", 0]', yamlString)
        self.assertIn('- ["-k2^2", 0]', yamlString)
        

    def test_writeIntegralFamilies(self):
        testFileName = "test/test_YamlWriteIntegralFamilies.yaml"
        config = Conf("test/qqqq.conf",False)
        YamlGenerator(config).writeIntegralFamilies(
            testFileName, 
            [
                IntegralFamily("Int1", ['-k1*k2', '-k1^2 - 2*k1*q1 - q1^2', 'M2^2 - k2^2 + 2*k2*q1 - q1^2', '-k1^2', '-k2^2'], ["k1", "k2"]), 
                IntegralFamily("Int2", ['-k1*k2', '-k1^2 - 2*k1*q1 - q1^2', 'M2^2 - k2^2 + 2*k2*q1 - q1^2', '-k1^2', '-k2^2'], ["k1", "k2"]), 
                IntegralFamily("Int3", ['-k1*k2', '-k1^2 - 2*k1*q1 - q1^2', 'M2^2 - k2^2 + 2*k2*q1 - q1^2', '-k1^2', '-k2^2'], ["k1", "k2"])
            ]
        )

        yamlFile = open(testFileName, "r")
        yamlFileString = "".join(yamlFile.readlines())
        yamlFile.close()
        os.remove(testFileName)

        self.assertEqual(yamlFileString.count('- name: "Int1"'), 1)
        self.assertEqual(yamlFileString.count('- name: "Int2"'), 1)
        self.assertEqual(yamlFileString.count('- name: "Int3"'), 1)
        self.assertEqual(yamlFileString.count('loop_momenta: [k1, k2]'), 3)
        self.assertEqual(yamlFileString.count('- ["-k1*k2", 0]'), 3)
        self.assertEqual(yamlFileString.count('- ["-k1^2 - 2*k1*q1 - q1^2", 0]'), 3)
        self.assertEqual(yamlFileString.count('- ["M2^2 - k2^2 + 2*k2*q1 - q1^2", 0]'), 3)
        self.assertEqual(yamlFileString.count('- ["-k1^2", 0]'), 3)
        self.assertEqual(yamlFileString.count('- ["-k2^2", 0]'), 3)