from modules.diagramDrawer import DiagramDrawer
from modules.diagram import Diagram
from modules.config import Conf
import unittest
import re


class testDiagramDrawer(unittest.TestCase):
    def test_diagramDrawing(self):
        config = Conf("test/qqqq.conf",False)

        config.externalMomenta = {}
        config.drawingObjects = {"W": "charged boson", "g":"gluon"}
        config.drawingNames = {"W": "$W^+$"}
        
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "W", "W", 0,0, "M1"], ["p2", 3, 4, "W", "W", 0,0, "M1"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
        box.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        box.diagramNumber = 1
        box.createTopologicalVertexAndLineLists()

        drawer = DiagramDrawer(config, ".diagram.tex.tmp")
        drawingString = drawer.getDiagramDrawingStrings([box])

        drawingString = re.sub(r'\s', r'', drawingString)

        self.assertIn("e1[particle={g($-q_{1}$)}]--[gluon]i1", drawingString)
        self.assertIn("e2[particle={g($-q_{2}$)}]--[gluon]i3", drawingString)
        self.assertIn("e3[particle={g($-q_{3}$)}]--[gluon]i2", drawingString)
        self.assertIn("e4[particle={g($-q_{4}$)}]--[gluon]i4", drawingString)
        self.assertIn("i1--[chargedboson,edgelabel={$W^+$}]i2", drawingString)
        self.assertIn("i3--[chargedboson,edgelabel={$W^+$}]i4", drawingString)
        self.assertIn("i3--[scalar,edgelabel={fq}]i1", drawingString)
        self.assertIn("i4--[scalar,edgelabel={fq}]i2", drawingString)



    def test_topologyDrawing(self):
        config = Conf("test/qqqq.conf",False)

        config.externalMomenta = {}
        config.drawingObjects = {"W": "charged boson", "g":"gluon"}
        config.drawingNames = {"W": "$W^+$"}
        
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "W", "W", 0,0, "M1"], ["p2", 3, 4, "W", "W", 0,0, "M1"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
        box.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        box.diagramNumber = 1
        box.createTopologicalVertexAndLineLists()

        drawer = DiagramDrawer(config, ".diagram.tex.tmp")
        drawingString = drawer.getDiagramDrawingStrings([box], onlyTopology=True)

        drawingString = re.sub(r'\s', r'', drawingString)

        self.assertIn("e1[particle={$-q_{1}$}]--[chargedscalar]i1", drawingString)
        self.assertIn("e2[particle={$-q_{2}$}]--[chargedscalar]i3", drawingString)
        self.assertIn("e3[particle={$-q_{3}$}]--[chargedscalar]i2", drawingString)
        self.assertIn("e4[particle={$-q_{4}$}]--[chargedscalar]i4", drawingString)
        self.assertIn("i1--[fermion,edgelabel={$p_{1}$,M1}]i2", drawingString)
        self.assertIn("i3--[fermion,edgelabel={$p_{2}$,M1}]i4", drawingString)
        self.assertIn("i3--[chargedscalar,edgelabel={$p_{3}$}]i1", drawingString)
        self.assertIn("i4--[chargedscalar,edgelabel={$p_{4}$}]i2", drawingString)


    def test_selfLoopDrawing(self):
        config = Conf("test/qqqq.conf",False)

        config.externalMomenta = {}
        config.drawingObjects = {"g":"gluon", "fq":"fermion", "fQ":"fermion"}
        
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "g", "g", 0,0, ""], ["p2", 2, 2, "fq", "fQ", 0,0, "M1"]]
        box.externalMomenta = [["-q1", 1, "fq", 0], ["-q2", 1, "fQ", 0]]
        box.diagramNumber = 1
        box.createTopologicalVertexAndLineLists()

        drawer = DiagramDrawer(config, ".diagram.tex.tmp")
        drawingString = drawer.getDiagramDrawingStrings([box])

        drawingString = re.sub(r'\s', r'', drawingString)

        self.assertIn("e1[particle={fq($-q_{1}$)}]--[fermion]i1", drawingString)
        self.assertIn("e2[particle={fQ($-q_{2}$)}]--[fermion]i1", drawingString)
        self.assertIn("i1--[gluon,edgelabel={g}]i2", drawingString)
        self.assertIn("i2--[fermion,edgelabel={fq},halfleft,looseness=1.5]s2", drawingString)
        self.assertIn("s2--[fermion,edgelabel={fq},halfleft,looseness=1.5]i2", drawingString)