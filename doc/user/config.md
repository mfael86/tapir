# Config file

The lines of the `tapir` config file have the following structure:

```
* tapir.[option keyword] {: [option argument 1]  {: [option argument 2] {...}}}
```

## Config options

### `q2e` compatible options

The allowed config entries which are compatible with the historical `q2e` are (use can use `q2e.option` or `tapir.option` for all `q2e` compatible options):

* `q2e/tapir.propagator_file [filename]`: Specify the [prop file](rules.md) file, where the Feynman rules for propagators are specified.
* `q2e/tapir.vertex_file [filename]`: Specify the [vrtx file](rules.md) file, where the Feynman rules for vertices are specified.
* `q2e/tapir.block_size [n]`: Split all the generated entries of the .dia, .edia, diagram-art and topology-art LaTeX files into smaller chunks with only `n` entries each. For each chunk a new .edia, .dia or .tex file is produced.
* `q2e/tapir.scales [scales]`: Define the scales hierarchy as a comma separated string, as `M1,q1,M2` etc.
* `q2e/tapir.expand_naive [scales]`: Define which scales have the to be expanded naively as a comma separated string. They will get a special `exp` flag in the .dia file
* `q2e/tapir.mass [particle]:[mass name]`: Give particles (and their antiparticles) a mass. To get the correct behavior for `exp`, the masses must have the names `M<n>`, where <n> is a positive integer. If a massive particle shares a propagator in the [prop file](rules.md) with another particle, the latter is assumed to be the antiparticle of the former and thus have the same mass.
* `q2e/tapir.closed_fermion_loop [particle]:[symbol]`: Closed fermion lines of `[particle]` will get an extra factor `[symbol]`. Only available for `-q2e` or `tapir.contract_fermion_lines`.
* `q2e/tapir.anti_fermion [particle]:[antiparticle]`: Define the antiparticle to a fermion. Only available for `-q2e` or `tapir.contract_fermion_lines`.

### New options
New to `tapir` are the following. Curly brackets mean optional arguments.

#### Diagram options
* `tapir.contract_fermion_lines`: Combine spinor lines and order fermionic Feynman rules to respect non-commuting input. With this option the template parameter `<spin_line>` is available but vertices with more than two fermions are not allowed (See [Feynman rules](rules.md)).
* `tapir.draw_particle [particle]:[TikZ edge]{:[edge name]}`: Specify how tapir will draw the edge of the particle with the `-da` CLI option. The optional `[edge names]` can be whatever LaTeX allows. How the edges are drawn depends on `[TikZ edge]`, allowed are: `fermion`, `gluon`, `photon`, `scalar`, `boson`, `charged boson`, `charged scalar`, `ghost` and `majorana`.
* `tapir.filter [filter name]:[true/t/1/false/f/0]`: Apply the given filter to the diagrams before generating to .dia and .edia including (true) or excluding (false) the matching diagrams, see [filter](filter.md).

#### Topology options
* `tapir.kinematic_relation [momentum product]:[replacement]`: Replace a product of external momenta by other kinematic invariants in the [topology files](topofiles.md). This option is especially useful when partial fraction decomposition is used. Valid `FORM` syntax is assumed for the left- and right-hand side. Examples: `q1.q1:M1^2` or `q1.q2:-3*(t/2+u)`. With this option one avoids relations including a formal division by zero, e.g. $`1/(M_1^2 - q_1^2)`$ whereas $`q_1^2 = M_1^2`$. Multiple `tapir.kinematic_relation` options are allowed.
* `tapir.external_momentum [regular q]:[new q] `: Replace the generic external momentum by a specific one. This can be used to assign multiple external momenta to be equal, or cut an external line completely from the topology by setting its momentum to `0`. The latter is a special case which also applies to ["edia"](diagramAndTopologyFiles.md) files! It is also possible to apply negative momenta, e.g. `q1:-q`. Beware that momentum conservation is regarded **after** specifying the momenta here!
* `tapir.topology_name [name]`: Specify the name the topologies will have when topology files are generated (`-f` option). E.g. `tapir.topology_name INT1` results for two-loop topologies in `INT12l1`,...,`INT12lN`.
* `tapir.euclidean`: Define topology files (`-f` option) with euclidean propagators, i.e. `s1m1` will be $`1/(p_1^2 +M_1^2)`$ instead of $`1/(M_1^2 - p_1^2)`$.
* `tapir.extend_analytical_topos`: Add to every massive propagator $`1/(M_1^2 - p_1^2)`$ a massless pendent: $`1/(-p_1^2)`$ in the the analytic topology definition in the [topology files](topofiles.md). This option accounts for non-standard propagators where both denominators occur, e.g. massive gluon lines.
* `tapir.topo_eikonal_external {[momenta]}` : Add combinations of eikonal propagators which include products of external and internal momenta to the analytic topology in the [topology files](topofiles.md). `[momenta]` is either a comma-separated list of momenta which are part of the eikonal propagators, or if `[momenta]` is left empty all external momenta of the respective diagram are taken into account. E.g. for a topology with two internal momenta $`p_1, p_2`$ and one external momentum $`q`$ the following propagators are assumed to be present in the problem: $`1/(-p_1.q)`$ and $`1/(-p_2.q)`$. Note that `tapir.topo_complete_momentum_products` is ignored when this option is used.
* `tapir.topo_eikonal_internal` : Add all combinations of eikonal propagators which include only products of internal momenta to the analytic topology in the [topology files](topofiles.md). E.g. For a topology with two internal momenta $`p_1, p_2`$ the following propagators are assumed to be present in the problem: $`1/(-p_1.p_2)`$. Note that `tapir.topo_complete_momentum_products` is ignored when this option is used.
* `tapir.topo_extrasort`: Place extra `.sort` commands in the [topology files](topofiles.md). This may increase the performance for large expressions.
* `tapir.topo_sortfunc [func_call]`: Place calls to a function instead of `.sort` in [topology files](topofiles.md).
* `tapir.topo_complete_momentum_products`: Replace numerator momentum products by real propagators in the [topology files](topofiles.md). E.g. replace $`p_1 q_2`$ by $`((p_1+q_2)^2 - p_1^2 - q_2^2)/2`$. Note: It cannot be used with `tapir.topo_eikonal_external` or `tapir.topo_eikonal_internal`.
* `tapir.topo_ignore_bridges`: Remove bridges (lines that do not correspond to a loop) from the topology. This effects how [topsel](diagramAndTopologyFiles.md#tosel-files) entries and [topology files](topofiles.md) are created. This option ensures, `exp` will always map on that topology. This removal applies just before the topology drawing, the [topsel](diagramAndTopologyFiles.md#topsel-files) entry generation and topology file generation.
* `tapir.topo_remove_duplicate_lines`: Remove lines carrying the same momentum and mass in the topology. This step is applied before partial fraction decomposition.
* `tapir.topo_allow_absent_massive_lines`: Extends the "mass distribution string" of topsel entries to also feature combinations where one or more of the massive lines is absent, e.g. instead of `1111` it will read `1111;111x;11x1;1x11;x111;11xx;1x1x;1xx1;x11x;x1x1;xx11;1xxx;x1xx;xx1x;xxx1`. Can only be used if `tapir.topselout` is defined. Intended for use with the program `exp`.
* `tapir.sigma_particles [particles]`: Sigma particles are ancillary, mathematical objects that do not carry momentum. Used, for example, to split the four-gluon interaction into color-factorizable three-particle interactions. Hence, the propagators of these sigma particles can be removed from the topology for simplification. `[particles]` is a comma-separated list of particle names whose propagator lines are removed in topologies, but kept in diagram representation.
* `tapir.mapping_file [file name]`: If the topology minimization routines are used, a `FORM` processable mapping file is produced. It includes the line momentum substitutions necessary to express the topologies in terms of the minimized ones. If the file name is left empty, `mapping.inc` is used as the default value. Note that this file only contains useful information for coarse filtering since relative momentum signs cannot be reconstructed when using the fine filter.
* `tapir.yaml_mapping_file [file name]`: This option triggers the same as `tapir.mapping_file [file name]` but exports the mapping information in a YAML format.
* `tapir.topo_numrep_only`: This option only includes the replacement of numerator momenta in topology files and does not write them into scalar functions.

#### UFOReader options
* `tapir.gaugeparam [particle]:[gauge parameter]`: Assigns a specific gauge parameter to a gauge boson, e.g. `tapir.gaugeparam g:xiG`. See [UFOReader documentation](UFOReader.md) for more details.
* `tapir.gaugeprop [longitudinal boson]:[transversal boson]`: Connects the "longitudinal" and "transversal" parts of the massive gauge-boson fields, e.g. `tapir.gaugeprop ZL:ZT`. See [UFOReader documentation](UFOReader.md) for more details.
* `tapir.ufo_tag_fermion_flavour`: If this flag is set, the `UFOReader` will add a fermion flavour tag to each fermion propagator. This can be used in `FORM` to detect closed fermion loops with the help of spinor indices.

### Compatibility options
* `tapir.no_exp_compat`: Switch off wrapping of external momenta in vertex functions for exp, simply use external momenta

### Miscellaneous options
* `tapir.prefactor p1,p2(,...) : FORMSTRING`: Multiplies the vertex/propagator defined by the particles `p1,...,pn` (if only two particles are given, it is interpreted as propagator, otherwise as vertex) with a factor `"FORMSTRING"`. This can be used e.g. to tag certain vertices for the purpose of debugging, for example by `* tapir.prefactor fT,ft,g : tagGluQuark`.

### CLI options
The [command line options](index.md#Command-line-options) can also be defined in the config file (command-line is prioritized):

* `tapir.kernels [n]`
* `tapir.qlist [qlist file]`
* `tapir.qlist_out [qlist file]`
* `tapir.diaout [dia output filename]`
* `tapir.ediaout [edia output filename]`
* `tapir.topselin [topsel input filename]`
* `tapir.topselout [topsel output filename]`
* `tapir.ordertopsel`
* `tapir.topologyfolder [topology folder output name]`
* `tapir.minimize {[topsel input file]}`
* `tapir.minimize_fine {[topsel input file]}`
* `tapir.partfrac`
* `tapir.partfrac_minimize {[FIRE topology list input file]}`
* `tapir.diagramart {[diagram-art latex output filename]}`
* `tapir.topologyart {[topology-art latex output filename]}`
* `tapir.repart {[representatives-art latex output filename]}`
* `tapir.diagram_yaml_in [yaml input file]`
* `tapir.diagram_yaml_out [yaml output file]`
* `tapir.topology_yaml_in [yaml input file]`
* `tapir.topology_yaml_out [yaml output file]`

For the `tapir ufo` subcommand, the following [command line options](index.md#Command-line-options) can also be defined in the config file (the command-line being prioritized):

* `tapir.ufo_indir [path to UFO directory]`
* `tapir.ufo_outdir [path to output directory]`

For the `tapir partfrac` subcommand, at present all options have to be passed as [command line options](index.md#Command-line-options).


## Notes
* Fermions must indicated by an "f" as the first letter of their name if one uses the CLI argument`-q2e` or the option `tapir.contract_fermion_lines`.
* Some of the options exist in both a q2e and a tapir version, however, `* tapir.XXX` is ignored if `-q2e` is used.
* There must be a whitespace in `* tapir` or `* q2e`, otherwise the line is treated as a comment like a regular line.


## Example config file

In the following we show an example config file 

```
* tapir.propagator_file qcd.prop
* tapir.vertex_file qcd.vrtx
* tapir.block_size n
* tapir.scales M1,p2,p1
* tapir.expand_naive M1
* tapir.mass q:M1
* tapir.closed_fermion_loop q:nf
* tapir.anti_fermion q:Q
* tapir.external_momentum q1:q

* tapir.draw_particle fq : fermion : $q_1$
* tapir.draw_particle g: gluon 
* tapir.draw_particle c : ghost
* tapir.draw_particle sigma : $\sigma$

* tapir.topology_name topo1x
* tapir.topo_ignore_bridges

* tapir.filter self_energy_bridge : true

* tapir.sigma_particles sig1,sig2

* tapir.euclidean
* tapir.topo_extrasort
* tapir.topo_sortfunc ACCU()
```

## YAML-formatted config files
It is possible to read config files that are `YAML`-formatted. For this, the file name extension must be either `.yml` or `.yaml` and one can simply use `./tapir -c CONFFILE.yaml`.
Since the syntax checks of the input are performed only once, the `.yaml` input is parsed into the standard `tapir config` format first. As a by-product, a file `CONFFILEFromYAML.conf` is produced.
This file is not deleted afterwards; rather it is meant as an option for the user to check whether their YAML input has been correctly processed.

The YAML-style is as follows (compared to the conventional `tapir` config format):
* The legacy-conform `* tapir.` is not present in `.yaml` files, e.g. `* tapir.vertex_file qcd.vrtx` becomes `vertex_file: qcd.vrtx`.
* Simple `option:value` options such as `* tapir.vertex_file qcd.vrtx` are written in `key:value` format, i.e. `vertex_file: qcd.vrtx`.
* Simple flags such as `* tapir.euclidean` receive an extra bool, i.e. `euclidean: True`; a value `False` is treated the same way as the line `* tapir.euclidean` being absent (except in `filter`).
* Options with comma-separated arguments, such as `* tapir.scales M1,M2,M3` translate to `scales: [M1, M2, M3]`
* Options with colon-separated arguments (except `* tapir.filter`), such as
```
* tapir.mass ft : M1
* tapir.mass Wp : M2
* tapir.mass Z : M3
```
are written either as a list of key-value mappings
```
mass:
  - ft: M1
  - Wp: M2
  - Z: M3
```
or as a dictionary of key-value mappings (combined syntax is also possible)
```
mass:
  - {ft: M1, Wp: M2}
  - {Z: M3}
```
* `* tapir.filter` has a slightly [different structure](filter.md), see example config file below


The following table shows a YAML-formatted config file and the equivalent `tapir config` file. **Note** that this file is only a list for comparison and contains several options that can not be simultaneously defined.

<table>
<tr>
<th>`tapir config`</th>
<th>`YAML`</th>
</tr>
<tr>
<td>

```
*Input files
* tapir.propagator_file qcd.prop
* tapir.vertex_file qcd.vrtx
* tapir.qlist qlist.3
* tapir.topselin topsel.in
* tapir.partfrac_minimize firetopo.in

*Output files
* tapir.diaout gg3l.dia
* tapir.ediaout gg3l.edia
* tapir.qlist_out qlistout.3
* tapir.topselout gg3l.topsel
* tapir.topologyart topologies.3.tex
* tapir.repart reps.tex
* tapir.diagramart diagrams.3.tex
* tapir.mapping_file testmapping.inc
* tapir.topology_name TESTTOPO
* tapir.yaml_mapping_file testmapping.yaml
* tapir.diagram_yaml_out dias.yaml
* tapir.topology_yaml_out topos.yaml
* tapir.topologyfolder topologies

*Scales, masses, expansions
* tapir.scales M1, M2, M3
* tapir.expand_naive q1, q2, q3
* tapir.mass ft : M1
* tapir.mass WTp : M2
* tapir.mass ZT : M4
* tapir.mass fb : M5

*Fermions
* tapir.closed_fermion_loop fu : nfu
* tapir.closed_fermion_loop fd : nfd
* tapir.closed_fermion_loop fc : nfc
* tapir.closed_fermion_loop fs : nfs
* tapir.closed_fermion_loop fb : nfb
* tapir.closed_fermion_loop ft : nft
* tapir.anti_fermion fu : fU
* tapir.anti_fermion fd : fD
* tapir.anti_fermion fc : fC
* tapir.anti_fermion fs : fS
* tapir.anti_fermion fb : fB
* tapir.anti_fermion ft : fT
* tapir.contract_fermion_lines

*Gauge
* tapir.gaugeparam g : xiG
* tapir.gaugeparam W : xiW
* tapir.gaugeparam Z : xiZ
* tapir.gaugeprop WLp : WTp
* tapir.gaugeprop WLm : WTm
* tapir.gaugeprop ZL : ZT

*Filters
* tapir.filter cuts : True : q1 : fu : 2,2
* tapir.filter cuts : False : q1 : ft : 0,1
* tapir.filter self_energy_bridge_mixing : True
* tapir.filter self_energy_bridge : True
* tapir.filter external_self_energy_bridge_mixing : True
* tapir.filter external_self_energy_bridge : True

*Prefactors
* tapir.prefactor fB,fb,g : tagGluQuark
* tapir.prefactor fT,ft,g : tagGluQuark
* tapir.prefactor g,g,g : tag3Glu

*Miscellaneous
* tapir.kernels 4
* tapir.block_size 100
* tapir.partfrac
* tapir.minimize
* tapir.topo_remove_duplicate_lines
* tapir.topo_ignore_bridges
* tapir.topo_eikonal_external q1, q2
* tapir.topo_eikonal_internal
* tapir.topo_sortfunc ACCU()
* tapir.topo_complete_momentum_products
* tapir.kinematic_relation q1.q1 : 0
* tapir.kinematic_relation q2.q2 : M1^2-M2^2
* tapir.external_momentum q4 : q1
* tapir.external_momentum q3 : q1
* tapir.euclidean
* tapir.sigma_particles sigma1, sigma2
* tapir.ordertopsel
* tapir.minimize_fine
* tapir.draw_particle fu : fermion : $u$
* tapir.draw_particle fd : fermion : $d$
* tapir.draw_particle fs : fermion : $s$
* tapir.draw_particle fc : fermion : $c$
* tapir.draw_particle fb : fermion : $b$
* tapir.draw_particle ft : fermion : $t$
* tapir.draw_particle g : gluon
* tapir.draw_particle c : charged scalar
* tapir.draw_particle sigma : scalar : $\sigma$

*UFO
* tapir.ufo_indir example/UFO/SM/Standard_Model_UFO/
* tapir.ufo_outdir test/smokeTestFiles/tapir/SMUFO2/
* tapir.ufo_tag_fermion_flavour
```

</td>
<td>

```yaml
config:
  # Input files
  propagator_file: qcd.prop
  vertex_file: qcd.vrtx
  qlist: qlist.3
  topselin: topsel.in
  partfrac_minimize: firetopo.in

  # Output files
  diaout: gg3l.dia
  ediaout: gg3l.edia
  qlist_out: qlistout.3
  topselout: gg3l.topsel
  topologyart: topologies.3.tex
  repart: reps.tex
  diagramart: diagrams.3.tex
  mapping_file: testmapping.inc
  topology_name: TESTTOPO
  yaml_mapping_file: testmapping.yaml
  diagram_yaml_out: dias.yaml
  topology_yaml_out: topos.yaml
  topologyfolder: topologies

  # Scales, masses, expansions
  scales: [M1, M2, M3]
  expand_naive: [q1, q2, q3]
  mass:
    - {ft: M1}
    - {WTp: M2, ZT: M4}
    - fb: M5

  # Fermions
  closed_fermion_loop:
    - {fu: nfu, fd: nfd, fc: nfc, fs: nfs}
    - fb: nfb
    - ft: nft
  anti_fermion:
    - {fu: fU, fd: fD, fc: fC, fs: fS, fb: fB}
    - ft: fT
  contract_fermion_lines: True
  # On the other hand:
  # contract_fermion_lines: False
  # is the same as contract_fermion_lines simply being absent

  # Gauge
  gaugeparam:
    - {g: xiG, W: xiW}
    - Z: xiZ
  gaugeprop:
    - {WLp: WTp, WLm: WTm}
    - ZL: ZT

  # Filters
  filter:
    - type: cuts
      value: True
      sources: q1
      particles: fu
      intervals: [2,2]
    - type: cuts
      value: False
      sources: q1
      particles: [ft]
      intervals:
        - [0,1]
        - [4,5]
    - type: self_energy_bridge_mixing
      value: True
    - type: self_energy_bridge
      value: True
    - type: external_self_energy_bridge_mixing
      value: True
    - type: external_self_energy_bridge
      value: True

  # Prefactors
  prefactor:
    - particles:
      - [fB,fb,g]
      - [fT,ft,g]
      factor: "tagGluQuark"
    - particles:
      - [g,g,g]
      factor: "tag3Glu"

  # Miscellaneous
  kernels: 4
  block_size: 100
  partfrac: True
  minimize: True
  topo_remove_duplicate_lines: True
  topo_ignore_bridges: True
  topo_eikonal_external: [q1, q2]
  topo_eikonal_internal: True
  topo_extrasort: False
  topo_sortfunc: ACCU()
  topo_complete_momentum_products: True
  topo_ignore_bridges: True
  topo_remove_duplicate_lines: true
  kinematic_relation:
    - q1.q1: 0
    - q2.q2: M1^2-M2^2
  external_momentum:
    - q4: q1
    - q3: q1
  euclidean: True
  sigma_particles: [sigma1, sigma2]
  ordertopsel: True
  minimize_fine: True
  draw_particle:
    - {fu: [fermion, '$u$'], fd: [fermion, '$d$'], fs: [fermion, '$s$']}
    - {fc: [fermion, '$c$'], fb: [fermion, '$b$'], ft: [fermion, '$t$']}
    - {g: [gluon], c: [charged scalar], sigma: [scalar, '$\sigma$']}
  
  # UFO
  ufo_indir: example/UFO/SM/Standard_Model_UFO/
  ufo_outdir: test/smokeTestFiles/tapir/SMUFOYAML/
  ufo_tag_fermion_flavour: True
```

</td>
</tr>
</table>
