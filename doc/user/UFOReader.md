# The UFOReader

## Converting FeynRules UFO models into `tapir` files
The `UFOReader` module can be used to convert a [FeynRules](https://arxiv.org/abs/1310.1921) model that has been exported into the [UFO](https://arxiv.org/abs/1108.2040) format into `.lag` (Lagrangian input file for `qgraf`), [`.prop` and `.vrtx`](rules.md) files defining propagators and interactions in a `tapir`-compatible format.

## From UFO to `tapir` files
### Extracting propagators
The method `getPropagators()` iterates over all propagating particles and converts the Feynman rules into the `lorentz`, `qcd`, `ew` and `qed` folds.
By default, the `ew` and `qed` folds are not used, i.e. they are empty.
For gauge bosons, the $`\xi`$-dependence of the vector boson propagators is restored, i.e. they are given in a general $`\xi`$ gauge.
A special case applies to massive gauge propagators, which read
```math
D^{\mu \nu} (k, m, \xi) = \frac{\mathrm{i} \left( -g^{\mu \nu} + (1 - \xi) \frac{k^\mu k^\nu}{k^2 - \xi m^2} \right)}{k^2 - m^2} \,.
```
Since these propagators feature two denominators of different masses $`m^2`$ and $`\xi m^2`$ at the same time, they might be cumbersome to deal with in automated setups for calculating the loop integrals.
For this reason, they are split into two "transversal" and "longitudinal" propagators, according to
```math
D^{\mu \nu} (k, m, \xi) = D_T^{\mu \nu} (k, m, \xi) + D_L^{\mu \nu} (k, m, \xi) \,,
```
where the two propagators are given by
```math
D_T^{\mu \nu} (k, m, \xi) = \frac{-\mathrm{i} \left( g^{\mu \nu} - \frac{k^\mu k^\nu}{m^2} \right)}{k^2 - m^2}
```
and
```math
D_L^{\mu \nu} (k, m, \xi) = -\frac{\mathrm{i} k^\mu k^\nu}{m^2} \frac{1}{k^2 - \xi m^2}
```
respectively.
Each of these propagators now only features one mass in the dynamic denominators.
The term "transversal" is used because
```math
k_\mu D_T^{\mu \nu} (k, m, \xi) = 0
```
if $`k^2 = m^2`$.
Internally, these propagators are notated by `Dtran(...)` and `Dlong(...)`, respectively.
In terms of the `.lag` and `.prop` files, the "transversal" and "longitudinal" modes are treated as two separate particles, so two entries in `.lag` and `.prop` will be produced for each massive vector-boson propagator.

For each propagating particle, the `lorentz` and `qcd` parts of the propagators are parsed into `tapir` format. Note that spinor, colour and Lorentz indices are kept.

Additionally, the field name strings are parsed:
- fermions: trailing `~,-,+` are removed or replaced by upper-case letters to indicate anti-particles
- fermions: fermions receive a prefix `f`
- fermions: quarks receive a suffix `q`
- scalars and vector bosons: trailing `+,-` are replaced by `p,m`, trailing `~` are dropped
- ghosts: ghosts receive a prefix `c` or `C` (anti-particle) and trailing `~` are dropped
- massive vector bosons: two fields are created, e.g. `WTp` and `WLp`, corresponding to "transversal" and "longitudinal" parts, respectively.

### Extracting vertices
The method `getVertices()` iterates over all vertices of the UFO model. The fields, `lorentz` and `qcd` parts of the vertices are parsed, just as in the case of the propagators.
In a general $`\xi`$ gauge, certain vertices become gauge-dependent; they carry an overall factor of the corresponding gauge parameter $`\xi`$.
This factor is explicitly added to the `lorentz` string of the corresponding vertex, because the UFO module should be exported in the $`\xi = 1`$ gauge.
Furthermore, since massive gauge-boson propagators are split into "transversal" and "longitudinal" particles, the number of vertices involving massive vector bosons will blow up.
For each occurrence of a massive vector boson in a vertex, there will be two vertices, one with the "transversal" and one with the "longitudinal" field, resulting in up to $`2^n`$ vertices, if $`n`$ is the number of massive gauge bosons in the vertex (duplicates with identical particle content will be removed).

There may be some vertices, where the colour and Lorentz parts do not factorise (e.g. the 4-gluon vertex in QCD), such that the separation into `lorentz` and `qcd` folds is not trivial.
Rather than introducing auxiliary particles, we assign a "tagging" function `nonfactag()` to the `lorentz` and `qcd` folds that allows to formally factorise the two parts, while at the same time being relatively easy to use.

Note that spinor, colour and Lorentz indices are kept.


### Writing the `.lag` file
The method `exportLagFile()` writes a `.lag` file containing all possible propagators and vertices, including the auxiliary "transversal" and "longitudinal" particles that arise when splitting up the massive gauge-boson propagators.
For propagators, a `+` or `-` is added in declaration of the propagator to indicate the spin-statistics of the corresponding particle.
No definitions of the propagators or vertices are given in this file, only the declarations.

### Export couplings
There is an additional method, `exportCouplingsFile()`, that serves to simplify the use of UFO models with a `FORM`-based setup.
This method produces two files, `UFOdecl.inc` and `UFOrepl.inc`.
The `UFOdecl.inc` file contains a list of all couplings and functions that are used in the UFO files and is intended as a declaration file for the use with `FORM`, in order to avoid the laborious fixing of "Undeclared variable" errors in the calculation.
It does not contain any replacements for these couplings etc., as in practical applications this would usually lead to a slowdown.
However, the couplings etc. defined in this file do not contain any Lorentz or colour elements, i.e. they are pure prefactors.
The second file, `UFOrepl.inc` contains the replacements of the couplings defined in `UFOdecl.inc`.
Typically, the user may want to use this file only towards the end of the calculation to automatically substitute the physical parameters into the result.


## Usage
The typical use case of the `UFOReader` differs a bit from the rest of `tapir`.
The `UFOReader` is used prior to diagram generation to generate the files `.lag` file that is eventually used by `qgraf`.
At the same time, the `.vrtx` and `.prop` files are generated as well as some auxiliary files (see above).

To do so, the user has to provide `tapir` with a `.conf` file where the config entries `tapir.ufo_indir` and `tapir.ufo_outdir` are specified.
The entry `tapir.ufo_indir` incidates the location of the UFO files that are used as input, the entry `tapir.ufo_outdir` contains the path to the directory where `tapir` will write the output files.
**Note:** `tapir` is written in `python3` and as such accepts only `python3` modules, but UFO modules are exported in `python2` (as of end of 2020). To use the `UFOReader`, the UFO files have to be converted to `python3`. In the past, this was however, very easy and could e.g. be achieved using such programs as `2to3`. In the following we will assume that the UFO files are already converted to `python3` style.

The `UFOReader` is then called using
```
./tapir ufo -c UFOCONFFILE.conf
```
or ./tapir ufo -ui UFOINPUTDIR -uo UFOOUTDIR
from the main directory of `tapir`.
For more details, we refer the reader to the example [Feynman rules](../../example/UFO/SM/README.md), where we perform this explicitly for the Standard Model UFO files.


### Helper functions
The parser functions of the `UFOReader` introduce a number of new auxiliary functions. Here we list them, together with either their definition or their corresponding expression in [UFO](https://arxiv.org/pdf/1108.2040.pdf).

#### Propagators
- `auxSlash(-<momentum>,<spinor_index_vertex_2>,<spinor_index_vertex_1>)` : $`\frac{\mathrm{i} \left(-k_\mu \gamma^\mu + m\right)_{i_2, i_1}}{k^2 - m^2}`$ (we are going opposite to the fermion line, therefore the minus sign)
- `Dghost(<momentum>,xiX)` : $`\frac{\mathrm{i}}{k^2 - \xi_X M^2}`$
- `Dgoldst(<momentum>,xiX)` : $`\frac{\mathrm{i}}{k^2 - \xi_X M^2}`$
- `Dph(<lorentz_index_vertex_2>,<lorentz_index_vertex_1>,<momentum>,xiX)` : $`\frac{\mathrm{i} \left(-g_{\mu_2, \mu_1} + (1 - \xi_X) \frac{k^{\mu_2} k^{\mu_1}}{k^2} \right)}{k^2}`$
- `Dgl(<lorentz_index_vertex_2>,<lorentz_index_vertex_1>,<momentum>,xiX)` : $`\frac{\mathrm{i} \left(-g_{\mu_2, \mu_1} + (1 - \xi_X) \frac{k^{\mu_2} k^{\mu_1}}{k^2} \right)}{k^2}`$
- `prop(<colour_index_vertex_2>,<colour_index_vertex_1>)` : $`\delta_{a_2 a_1}`$ (colour space)

#### Lorentz part
For the definitions of these objects, we kindly refer the reader to [Tab. 6 of the UFO paper](https://arxiv.org/pdf/1108.2040.pdf#table.6):
- `d_(<spinor_index_particle_i>,<spinor_index_particle_j>)` : `Identity(i,j)`
- `d_(<lorentz_index_particle_i>,<lorentz_index_particle_j>)` : `Metric(i,j)`
- `auxGamma(<lorentz_index_particle_mu>,<spinor_index_particle_i>,<spinor_index_particle_j>)` : `Gamma(mu,i,j)`
- `auxGamma5(<spinor_index_particle_i>,<spinor_index_particle_j>)` : `Gamma5(i,j)`
- `auxEps(<lorentz_index_particle_mu>,<lorentz_index_particle_nu>,<lorentz_index_particle_rho>,<lorentz_index_particle_sigma>)` : `Epsilon(mu,nu,rho,sigma)`
- `Mom(<lorentz_index_particle_j>,<outgoing_momentum_N>)` : `P(j,N)`
- `auxPR(<spinor_index_particle_i>,<spinor_index_particle_j>)` : `ProjP(i,j)`
- `auxPL(<spinor_index_particle_i>,<spinor_index_particle_j>)` : `ProjM(i,j)`
- `auxSigma(<lorentz_index_particle_mu>,<lorentz_index_particle_nu>,<spinor_index_particle_i>,<spinor_index_particle_j>)` : `Sigma(mu,nu,i,j)`
- `auxCC(<spinor_index_particle_i>,<spinor_index_particle_j>)` : `C(i,j)`

#### Colour part
For the definitions of these objects, we kindly refer the reader to [Tab. 5 of the UFO paper](https://arxiv.org/pdf/1108.2040.pdf#table.5):
- `d_(<spinor_index_particle_i>,<spinor_index_particle_j>)` : `Identity(i,j)`
- `GM_(<colour_index_particle_i>,<spinor_index_particle_k>,<spinor_index_particle_j>)` : `T(i,j,k)`
- `ufocomplex(0,1) * V3g(<colour_index_particle_i>,<colour_index_particle_j>,<colour_index_particle_k>)` : `f(i,j,k)`
- `Vsymmg(<colour_index_particle_i>,<colour_index_particle_j>,<colour_index_particle_k>)` : `d(i,j,k)`
- `ColEps(<spinor_index_particle_i>,<spinor_index_particle_j>,<spinor_index_particle_k>)` : `Epsilon(i,j,k)`
- `ColEpsBar(<spinor_index_particle_i>,<spinor_index_particle_j>,<spinor_index_particle_k>)` : `EpsilonBar(i,j,k)`
- `Tsix(<colour_index_particle_i>,<colour_index_particle_j>,<colour_index_particle_k>)` : `T6(i,j,k)`
- `Ksix(<colour_index_particle_i>,<colour_index_particle_j>,<colour_index_particle_k>)` : `K6(i,j,k)`
- `KsixBar(<colour_index_particle_i>,<spinor_index_particle_j>,<spinor_index_particle_k>)` : `K6Bar(i,j,k)`
