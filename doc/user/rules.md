# Feynman rules

To apply Feynman rules in the "dia" files, the vertex and propagator rules must be specified in two files.
Entries of these "vrtx" and "prop" files (as defined in the [config file](config.md)) have the common form:

```
{particle_1,particle_2,...,particleN:Lorentz|QCD|QED|EW}
```

The entries Lorentz, QCD, QED and EW should contain FORM code for the individual parts of each Feynman rule.
The rules must be defined in such a way that Lorentz, QCD, QED and EW parts factorize (e.g. by splitting the 4-gluon vertex using an ancillary sigma particle). Thus, the different parts can be completely separated and computed independently. If this is not wished, one could write the complete rule in the Lorentz-entry and leave the remainder empty.
The entries may consist of several lines or be completely empty.

## Template parameters
In the Feynman rules files one can make use of several template parameters which are replaced by the code generator.
They are commonly specified within `<...>` brackets. Some template names take one of two values `1/2` or one of several: `N` is a positive integer, `X` can be any `[a-zA-Z0-9]+` string.


### Shared template parameters
Common to both vrtx and prop files is:
- `<spin_line>` : Number of the fermion line/loop. Note that this template can only be used for either the CLI argument `-q2e` or the option `tapir.contract_fermion_lines`. Beware that this option can only be used for Feynman rules with distinct spinor lines. I.e. effective vertices with more than two interacting fermions are not allowed. 
- `<gauge_parameter_X>`: Gauge parameter corresponding to the gauge/Goldstone boson or ghost. Used with [UFO](UFOReader.md) to keep track of different gauge bosons/parameters.

### Propagators template parameters
Only for prop files:
- `<momentum>`                 : Momentum flowing through the line
- `<flavour_vertex_1/2>`       : Flavour tag of the line, supports mixing propagators. Default: 1.
- `<lorentz_index_vertex_1/2>` : Lorentz indices of starting vertex and end vertex: `nuXX`
- `<colour_index_vertex_1/2>`  : Colour (adjoint representation) indices of starting vertex and end vertex: `bXX`
- `<spinor_index_vertex_1/2>`  : Spinor indices of starting vertex and end vertex: `jXX`
- `<add_index_1_vertex_1/2>`   : Additional index type 1, used e.g. for sigma particles: `alXX`
- `<add_index_2_vertex_1/2>`   : Additional index type 2, used e.g. for sigma particles: `beXX`

`vertex_1` and `vertex_2` represent the vertices connected to `particle_1` and `particle_2`, respectively. 

#### New in tapir
Additional options that are not present in the legacy `q2e` code but compatible (even without `-q2e` flag) are
- `<mass>` : Mass of the propagating particle as specified by `tapir.mass`. If a massive particle shares a propagator with another particle, the latter is assumed to be the antiparticle of the former.
- `<lorentz_index_i_vertex_1/2>` : Additional Lorentz indices of starting vertex and end vertex: `nuXXni`, `i` is a positive integer.
- `<fundamental_index_vertex_1/2>`  : Colour (fundamental representation) indices of starting vertex and end vertex: `fiXX`.
- `<fundamental_index_i_vertex_1/2>`  : Colour (fundamental representation) indices of starting vertex and end vertex: `fiXXni`, `i` is a positive integer.
- `<local_index_X>` : This is an additional index which is used to contract different FORM parts within the same propagator definition. `X` can be any `[a-zA-Z0-9]+` string. In the inserted rule, the `X` is the index name with an additional counter (set to the propagator number) attached. Make sure to define different `X` for different propagators (and also vertices) to ensure correct behavior. Also be aware of conflicts with other indices.
- `<add_index_i_vertex_1/2>`   : Additional index type `i`, used e.g. for sigma particles: `alXXni`, `i` is a positive integer.
- `<physical_mass_X>`: Physical mass assigned to massive gauge bosons `X`. The physical mass is always the mass of the [transversal](UFOReader.md#extracting-propagators) component of the gauge propagator. Cf. [longitudinal Z propagator](../../example/UFO/SM/tapir_SM_UFO/UFO.prop). Used with [UFO](UFOReader.md) to connect the transversal and longitudinal components of the massive gauge bosons.

### Vertex template parameters
Only for vrtx files:
- `<outgoing_momentum_N>`      : Outgoing momentum of particle `N`
- `<flavour_particle_N>`       : Flavour tag of particle `N`. Default: 1.
- `<lorentz_index_particle_N>` : Lorentz index of particle `N`. Internal: `nuXX`; External:`muXX`
- `<colour_index_particle_N>`  : Colour indices of particle `N`. Internal: `bXX`; External:`aXX`
- `<spinor_index_particle_N>`  : Spinor indices of particle `N`. Internal: `jXX`; External:`iXX`
- `<add_index_1_particle_N>`   : Additional index type 1 of particle `N`, used e.g. for sigma particles. Only internal: `alXX`
- `<add_index_2_particle_N>`   : Additional index type 2 of particle `N`, used e.g. for sigma particles. Only internal: `beXX`

#### New in tapir
Additional options that are not present in the legacy `q2e` code but compatible (even without `-q2e` flag) are
- `<mass_particle_N>` : Mass of the particle number `N` of the current Feynman rule, with the mass specified by `tapir.mass`.
- `<lorentz_index_i_particle_N>` : i'th additional Lorentz index of particle `N`. internal: `nuXXni`; external: `muXXni`. `i` is a positive integer.
- `<fundamental_index_particle_N>`  : fundamental representation index of particle `N`. internal: `fiXX`; external: `feXX` `i`.
- `<fundamental_index_i_particle_N>`  : i'th fundamental representation index of particle `N`. internal: `fiXXni`; external: `feXXni` `i` is a positive integer.
- `<local_index_X>` : This is an additional index which is used to contract different FORM parts within the same vertex definition. `X` can be any `[a-zA-Z0-9]+` string. In the inserted rule, the `X` is the index name with an additional counter (set to the vertex number) attached. Make sure to define different `X` for different vertices (and also propagators) to ensure correct behavior. Also be aware of conflicts with other indices.
- `<add_index_i_particle_N>`   : Additional index type `i` for particle `N`, used e.g. for sigma particles: `alXXni`, `i` is a positive integer.

### Remark on legacy spinor line handling
With the option `-q2e` or `tapir.contract_fermion_lines` set, the Lorentz-entries are understood as non-commutable objects (and must be specified as such in `FORM`!). This means that the propagators and vertices of fermions are correctly ordered due to connected Fermion lines.
Hence, there is no need to define a `spinor_index` in the Lorentz-entry. This was the reason for the legacy `q2e` to use it as fundamental index in the QCD-fold. 
In our default notation `spinor_index` is intended to be used in the Lorentz-entries. For the fundamental indices of the QCD-entry, we implemented `fundamental_index` to replace the confusing legacy notation of `spinor_index`.

Note that with the options `-q2e` or `tapir.contract_fermion_lines` set fermions are recognized as such with a leading "f" in the particle name.

### Multiple feynman rules
If multiple rules are allowed for the same particle content (e.g. different 4-fermion operators), the diagram is copied and all allowed rules are inserted.
This simplifies the diagram generation with `qgraf`, since the same diagrams are re-used.

## Examples
As an example, we define the gluon and quark propagators as well as the 3-gluon and quark-gluon vertices:

### prop example
```
{g,g:*Dg(<lorentz_index_vertex_1>,<lorentz_index_vertex_2>,<momentum>)|
*prop(<colour_index_vertex_1>,<colour_index_vertex_2>)||
}

{fq,fQ:*FT<spin_line>(<momentum>)|
 *d_(<spinor_index_vertex_1>,<spinor_index_vertex_2>)||
}
```

The latter rule only works properly with the option `contract_fermion_lines` set.
For this to work `FT1`,`FT2`,... must be defined as non-commuting tensors or functions in `FORM`.

### vrtx example
```
{g,g,g:
 *V3g(<lorentz_index_particle_1>,<outgoing_momentum_1>,<lorentz_index_particle_2>,<outgoing_momentum_2>,<lorentz_index_particle_3>,<outgoing_momentum_3>)|
 *V3g(<colour_index_particle_1>,<colour_index_particle_2>,<colour_index_particle_3>)||
}

{fQ,fq,g:*FT<spin_line>(<lorentz_index_particle_3>)|
 *GM(<colour_index_particle_3>,<fundamental_index_particle_1>,<fundamental_index_particle_2>)||
} 
```

As above, the latter rule only works properly with the option `contract_fermion_lines` set with `FT1`,`FT2`,... being non-commuting.
