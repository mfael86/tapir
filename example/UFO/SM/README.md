# Feynman rules with the `UFOReader`

In this example, we will see how the `UFOReader` produces Feynman rules.
We will use the Standard Model [UFO](https://arxiv.org/abs/1108.2040) files in the directory [Standard_Model_UFO](Standard_Model_UFO) that are already contained in this repository.
They have also been converted to `python3` format using `2to3`.

To use these files, all we have to do is specify the input (`tapir.ufo_indir`) and output (`tapir.ufo_outdir`) directories.
This is done in the file `ufo.conf`:
```
* tapir.ufo_indir example/UFO/SM/Standard_Model_UFO/
* tapir.ufo_outdir example/UFO/SM/tapir_SM_UFO/
```

The paths are relative to the main directory of `tapir`.
To produce the Feynman rules in `tapir` format, we simply execute
```
./tapir ufo -c example/UFO/SM/ufo.conf
```
within the main directory of `tapir`.

Now we have produced a set of files in the directory [tapir_SM_UFO](tapir_SM_UFO):
- a `UFO.lag` file containing declarations of all possible [propagators and vertices](../../../doc/user/rules.md)
- a `UFO.prop` file containing the definitions of all propagators
- a `UFO.vrtx` file containing the definitions of all vertices
- a `UFOdecl.inc` file containing the declarations of various symbols and functions that arise in the UFO files and that need to be defined when using a FORM-based setup
- a `UFOrepl.inc` file containing the definitions of the symbols specified in `UFOdecl.inc`, for easier substituting into final results

Let us have a look into these files now.

## The `UFO.lag` file
This file determines which propagators and vertices are allowed in the theory. It can be used as an input file for `qgraf` in order to generate Feynman diagrams.
The `UFOReader` part of `tapir` is therefore usually called before the actual generation of Feynman diagrams if UFO files are used.

The first few lines of the "Propagators" section read
```
[a, a, +]
[ZL, ZL, +]
[ZT, ZT, +]
[WLp, WLm, +]
[WTp, WTm, +]
[g, g, +]
```
As described in the [documentation](../../../doc/user/UFOReader.md), massive vector bosons are split into two propagators, as can be seen from the $`W`$ and $`Z`$ boson propagators, respectively, while the massless photon and gluon propagators are not split.


## The `UFO.prop` and `UFO.vrtx` files
These two files are used to process the `qgraf` output into analytic expressions.

The propagator file contains the definitions of the propagators.
As expected, we find two different propagators for the massive vector bosons:
```
{a,a:*Dph(<lorentz_index_vertex_2>,<lorentz_index_vertex_1>,<momentum>,<gauge_parameter_xia>)|
        ||}

{ZL,ZL:*Dlong(<lorentz_index_vertex_1>,<lorentz_index_vertex_2>,<momentum>,<gauge_parameter_xiZ>,<physical_mass_ZT>)|
        ||}

{ZT,ZT:*Dtran(<lorentz_index_vertex_1>,<lorentz_index_vertex_2>,<momentum>,<gauge_parameter_xiZ>,<physical_mass_ZT>)|
        ||}

{WLp,WLm:*Dlong(<lorentz_index_vertex_1>,<lorentz_index_vertex_2>,<momentum>,<gauge_parameter_xiW>,<physical_mass_WTp>)|
        ||}

{WTp,WTm:*Dtran(<lorentz_index_vertex_1>,<lorentz_index_vertex_2>,<momentum>,<gauge_parameter_xiW>,<physical_mass_WTp>)|
        ||}

{g,g:*Dgl(<lorentz_index_vertex_2>,<lorentz_index_vertex_1>,<momentum>,<gauge_parameter_xig>)|
        *prop(<colour_index_vertex_2>,<colour_index_vertex_1>)||}
```
Note that there is a variable corresponding to the respective gauge parameters of the particles, and that both the "transversal" and "longitudinal" components of the gauge boson propagators carry the same `<physical_mass_ZT>`.
Also note the use of spinor, colour and Lorentz indices in this file.

The vertex file contains the definitions of the vertices.
There are a few noteworthy things:
- It contains a lot of `ufoGC...` variables. These are declared in the `UFOdecl.inc` file and defined in the `UFOrepl.inc` file.
- The 4-gluon vertex contains the `nonfactag` function which allows to tag the occurence of 4-gluon vertices in Feynman diagrams as well as the multiplication of the Lorentz parts with their respective QCD counterparts.
- A few vertices depend on the gauge, e.g. the vertex
```
{cghZ,CghWp,Gp:*<gauge_parameter_xiZ> *( ufoGC95 * ( 1 ))|
        *(1)||}
```
- For each combination of "longitudinal" and "transversal" vector bosons there is an individual vertex (duplicates removed). All of these vertices have the same Feynman rule.

As an example to illustrate the last point, have a look at the two vertices
```
{WLm,Gp,H:*( ufoGC56 * ( Mom(<lorentz_index_particle_1>,<outgoing_momentum_2>) - Mom(<lorentz_index_particle_1>,<outgoing_momentum_3>) ))|
        *(1)||}

{WTm,Gp,H:*( ufoGC56 * ( Mom(<lorentz_index_particle_1>,<outgoing_momentum_2>) - Mom(<lorentz_index_particle_1>,<outgoing_momentum_3>) ))|
```
which are identical except for their particle content.


## The `UFOdecl.inc` and `UFOrepl.inc` files
The file `UFOdecl.inc` looks rather unspectacular.
```
CFunction nonfactag;
CFunction ufocomplex;
CFunction ufosqrt;
CFunction ufocomplexconjugate;


Symbol ufoGC1;
Symbol ufoGC2;
Symbol ufoGC3;
Symbol ufoGC4;
...
```
It contains the declaration of the vertex couplings and some FORM `CFunctions` for the use in a FORM-based setup to do the actual calculations.

The file `UFOrepl.inc` is a bit more interesting.
It contains some more declarations and a lot of replacement rules for the coupling constants in terms of parameters of the theory.
The function `ufocomplex(0,1)` is simply $`0 + 1 \cdot \mathrm{i}`$.
Note the appearance of symbols called `ufoINaxy`, e.g. `ufoI2a23`.
These consist of combinations of CKM parameters and Yukawa couplings and can unfortunately not be automatically replaced due to the structure of the [parameters.py](Standard_Model_UFO/parameters.py) UFO file.
They can, however, simply be looked up in that file (as well as a few other parameters that at the moment need to be replaced by hand).
