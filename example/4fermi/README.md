# Rare B_s decay
This example shows the workflow with `tapir` in an EFT application.
To be more precise, we'll have a look at the process $`\bar{b} s \rightarrow \bar{l} l`$ with NNLO QCD corrections.
Here, we do not repeat the basics from the [3-loop gluon propagator example](../gg/README.md).

## 1. Checking the diagrams
The diagrams for our problem can be generated using [qgraf](http://cfif.ist.utl.pt/~paulo/qgraf.html) with `[qgraf.dat](#qgraf.dat).
The resulting `qlist.2` file is already provided for convenience.

First things first, let's have a look at the generated diagrams:

```bash
$ ../../tapir --conf 4fermi.conf --qlist qlist.2 --diagramart diagram-drawings.tex && lualatex diagram-drawings.tex
```

The file `diagram-drawings.pdf` shows the 14 diagrams with propagator marking, describing which particle they describes.
Note: The arrows on the external lines point in the direction in which the external momentum flows, not the correct fermion flow!

## 2. Feynman rules insertion
The [feynman rules files](../../doc/rules.md) `4fermi.vrtx` and `4fermi.prop` show that we actually have two 4-fermion vertices:

```math
C_1 (\bar{b} \gamma_{\mu} s) (\bar{l} \gamma^{\mu} l) 
```
and
```math
C_2 (\bar{b} s) (\bar{l} l),
```
but qgraf cannot differentiate them...

Let's generate the [dia](../../doc/dia.md) and [edia](../../doc/diagramAndTopologyFiles.md) files with

```bash
$ ../../tapir --conf 4fermi.conf --qlist qlist.2 --diaout 4fermi.dia --ediaout 4fermi.edia
```

`tapir` prints a warning that all combinations of dia and edia where generated that are allowed by the given Feynman rules.
Looking at `4fermi.edia`, we see the amount of diagrams has indeed doubled!
The so extended diagrams get an additional comment in the Lorentz fold of the dia file, describing from which `qlist` entry it was generated and which feynman rules where inserted (e.g. `{1:1,2:2,3:1}` for vertices says that for vertex number 1 and 3, we used the second (counting begins with 0) Feynman rule in the vrtx file, and for vertex number 2 the third).

Comparing the Lorentz-folds of the first two diagrams in `4fermi.dia` (`d2l1` and `d2l2`), we see that they indeed differ by the fifth inserted vertex.

## 3. Graph topologies and analytic topologies

Let us assume we decide to give our gluon a mass via infrared rearrangement. Then the propagator includes terms proportional to 

```math
\frac{1}{(k^2-m_g^2)k^2} .
```

In that case the analytic topology files does not fully match the whole diagram representation. For this case, one can use the option `tapir.extend_analytical_topos` in the config file. 
What this option does is to add to every massive propagator a massless pendent with the same momentum squared.
This definitely leads to linear dependent denominators and we thus have to perform partial fraction decomposition. 

We also want to replace irreducible numerator momentum products by real denominators. For this case we use the option `tapir.topo_complete_momentum_products`.
This means we replace a product $`p_1 q_2`$ by $`(p_1+q_2)^2/2 - p_1^2 - q_2^2`$ where the first term on the right-hand side is used as a new ancillary propagator. 
Enabling this option simplifies further analytic reductions in `FIRE`, for example.

As in the [previous example](../gg/README.md), let us minimize the topologies, write a minimal [topsel](../../doc/diagramAndTopologyFiles.md) file, draw the minimal topologies and generate the [topology files](../../doc/topofiles.md). In addition, we now perform partial fraction decomposition:

```bash
$ ../../tapir -k -c 4fermi.conf -q qlist.2 -m -t 4fermi.topsel -ta topology-drawings.tex -f topologies -pf
```