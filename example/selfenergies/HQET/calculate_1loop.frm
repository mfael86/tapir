#-
#include UFOdecl.inc
#define DIAFILE "1lQSE.dia"

Symbol d;
Dimension d;
Off statistics;

Autodeclare Symbol ufoGC;
CFunctions Dgl, auxGamma, fpropnum, fvertV, propden, Vec, fermionchain, fermiontrace, auxSlashv, ifunc, fvertS;
CFunction auxVecv, invSP;
Symbol xiG;
Autodeclare Indices nu, i, j;
Vectors p, p1,...,p20, q1, v;
Symbol vectorpart, scalarpart;
Symbols n0, ..., n5;
CFunction Gamma, Log, d1l1;
Symbols pi, ep, eulergamma, orderep2, x;
Symbols gs, mutilde, Eq1;


.global
.sort

* Load diagrams (Lorentz fold)
Global d1l1amp = 
        #include `DIAFILE' #d1l1
.sort

* Project out the component proportional to (1 + vslash)/2
multiply 1/4 * (fvertS(i2,i1)*scalarpart + auxGamma(-v,i2,i1)*vectorpart);
.sort

* Rewrite helper functions
** Gluon propagator
id Dgl(i1?,i2?,?x,xiG?) = -i_ * (d_(i1,i2) - (1 - xiG) * Vec(i1,?x) * Vec(i2,?x) * propden(?x) ) * propden(?x);
* Here we also need a minus sign because we run opposite to the fermion line
id auxSlashv(?x,j1?,j2?) = auxGamma(-v,j1,j2);

id auxGamma(?x,j1?,j2?) = fvertV(?x,j1,j2);
id propden(p?) = p.p^-1;
** Heavy quark propagator denominator
id invSP(v?,p?) = p.v^-1;
id auxVecv(nu?) = v(nu);
.sort

* Take the trace
** First, "chain" all objects affected by taking Dirac traces
id fvertV(?a,j1?,j2?) = fermionchain(fvertV(?a), j1, j2);
id fvertS(?a,j1?,j2?) = fermionchain(fvertS(?a), j1, j2);
repeat;
    id fermionchain(?a,j1?,j2?) * fermionchain(?b,j2?,j3?) = fermionchain(?a, ?b, j1, j3);
    id fermionchain(?a,j1?,j1?) = fermiontrace(?a);
endrepeat;

** Secondly, rewrite them using ordinary FORM gamma matrices
repeat;
    id fermiontrace(fvertV(nu?),?a) = g_(1,nu) * fermiontrace(?a);
    id fermiontrace(fvertS(),?a) = gi_(1) * fermiontrace(?a);
endrepeat;
id fermiontrace = 1;
.sort

id Vec(nu?,p?) = p(nu);
.sort
Tracen 1;
id v.v = 1;
.sort

#include 1ltopologies/d1l1
.sort


* Integrate the one-loop function
id d1l1(2,0,-2,1) = Eq1^2 * d1l1(2,0,0,1);
id d1l1(n1?,0,0,n2?) = i_ * (4*pi)^(-d/2) * (-2*Eq1)^(d-2*n1) * Eq1^(-n2) * (-1)^n2 * Gamma(-d + n2 + 2*n1) * Gamma(d/2 - n1) / Gamma(n2) / Gamma(n1);
.sort
id d = 4 - 2*ep;
argument;
    id d = 4 - 2*ep;
endargument;
.sort


* Expand in ep
id pi^(-2+ep) = pi^-2 * (1 + ep*Log(pi) + ep^2 * orderep2);
id 1/Gamma(1) = 1;
id 1/Gamma(2) = 1;
id Gamma(1 - ep) = 1 + ep*eulergamma + ep^2*orderep2;
id Gamma(-1 + 2*ep) = -1/2/ep - 1 + eulergamma + ep*(-2 +2*eulergamma - eulergamma^2 - pi^2/6) + ep^2*orderep2;
id Gamma(-ep) = -1/ep - eulergamma + 1/12*ep*(-6*eulergamma^2 - pi^2) + ep^2*orderep2;
id Gamma(1 + 2*ep) = 1 - 2*ep*eulergamma + ep^2*orderep2;
id (-2*Eq1)^(2-2*ep) = 4*Eq1^2 + ep*(-8*Eq1^2*Log(-2*Eq1)) + ep^2*orderep2;
id ep^n0?{>0} = 0;
.sort


* Simplify result a bit
id v.v^n0? = 1;
id vectorpart = 1;
id scalarpart = 1;
id ufoGC11 = i_ * gs;
.sort

Print +s;
.store

* Definitions
* PATHTOCOLORH needs to point to color.h
#define UFOCOLOUR "0"
#define PATHTOCOLORH "color.h"
#define RANK "14"
#define NUMINDEX "40"

#include `PATHTOCOLORH' #Declarations
Dimension NR;
autodeclare Index funi, funj;
Dimension NA;
autodeclare Index adja, adjb;


#include `PATHTOCOLORH' #color
#include `PATHTOCOLORH' #SORT
#include `PATHTOCOLORH' #adjoint

cf a,b,GM,V3g,prop,ufocomplex;

* Load diagrams (QCD fold)
Global fqcd1l1amp = 
         #include `DIAFILE' #fqcd1l1
.sort
* Apply projector
multiply d_(i1,i2)/NR;
.sort

* Rename indices
#if ( `UFOCOLOUR' == 1)
  repeat id ufocomplex(0,1)^2 = -1;
#endif
#do N = 1, `NUMINDEX'
  argument;
    id a(`N') = adja`N';
    id b(`N') = adjb`N';
    id i`N' = funi`N';
    id j`N' = funj`N';
    #if ( `UFOCOLOUR' == 1)
      id aufo(`N') = adja{`N'+`NUMINDEX'+1};
      id bufo(`N') = adjb{`N'+`NUMINDEX'+1};
    #endif
  endargument;
#enddo
	
* Replace functions
id prop(?x) = d_(?x);
id GM(i1?,i2?,i3?) = T(i2,i3,i1);
id V3g(?x) = (-i_)*f(?x);
.sort

* Call color
#call color
#call SORT(tloop-1)

* Re-express color factor
id NA/NR*I2R = cR;
Print +s;
.store

* Final result
g res1l = fqcd1l1amp*d1l1amp;
b cR,pi,xiG,gs,i_;
print +s;
.end
