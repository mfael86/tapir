#-
#include UFOdecl.inc
#define NUMDIAS "9"
#define NUMLOOPS "2"
#define DIAFILE "2lQSE.dia"

Symbol d;
Dimension d;
Off statistics;

Autodeclare Symbol FLAG;
Autodeclare Symbol ufoGC;
CFunctions Dgl, auxGamma, fpropnum, fvertV, propden, Vec, fermionchain, fermiontrace, auxSlashv, ifunc, fvertS, Mom, Dghost, auxSlash, fprop;
CFunction auxVecv, invSP;
Autodeclare CFunction d`NUMLOOPS'l;
Symbol xiG, gs;
Autodeclare Indices nu, i, j;
Vectors p, p1,...,p20, q1, v;
Symbol vectorpart, scalarpart;
Symbols n0, ..., n9;
Symbols D0,...,D9;
Symbol x;
CFunction diagramlabel;

.global
.sort


* Define the procedure to handle individual diagrams
#procedure dia2l(i)
* Load the i-th diagram
    Global d`NUMLOOPS'l`i'amp = 
        #include `DIAFILE' #d`NUMLOOPS'l`i'
    .sort

* Projector
    multiply 1/4 * (fvertS(i2,i1)*scalarpart + auxGamma(-v,i2,i1)*vectorpart);
    .sort

    id Mom(?x) = Vec(?x);
    id Dghost(?x,xiG?) = i_ * propden(?x);
    id Dgl(i1?,i2?,?x,xiG?) = -i_ * (d_(i1,i2) - (1 - xiG) * Vec(i1,?x) * Vec(i2,?x) * propden(?x) ) * propden(?x);
    id auxSlashv(?x,j1?,j2?) = auxGamma(-v,j1,j2);
    id auxSlash(?x,j1?,j2?) = i_ * fprop(?x,j1,j2);
    id auxGamma(?x,j1?,j2?) = fvertV(?x,j1,j2);
    id fprop(p?, j1?, j2?) = propden(p) * fpropnum(p, j1, j2);
    id fprop(-p?, j1?, j2?) = propden(p) * fpropnum(-p, j1, j2);
    id propden(p?) = p.p^-1;
    id invSP(v?,p?) = p.v^-1;
    id auxVecv(nu?) = v(nu);
    .sort

* Take the trace
    id fpropnum(?a,j1?,j2?) = fermionchain(fpropnum(?a), j1, j2);
    id fvertV(?a,j1?,j2?) = fermionchain(fvertV(?a), j1, j2);
    id fvertS(?a,j1?,j2?) = fermionchain(fvertS(?a), j1, j2);
    repeat;
        id fermionchain(?a,j1?,j2?) * fermionchain(?b,j2?,j3?) = fermionchain(?a, ?b, j1, j3);
        id fermionchain(?a,j1?,j1?) = fermiontrace(?a);
    endrepeat;

    repeat;
        id fermiontrace(fpropnum(p?),?a) = g_(1,p) * fermiontrace(?a);
        id fermiontrace(fvertV(nu?),?a) = g_(1,nu) * fermiontrace(?a);
        id fermiontrace(fvertS(),?a) = gi_(1) * fermiontrace(?a);
    endrepeat;
    .sort
    id fermiontrace = 1;

    id Vec(nu?,p?) = p(nu);
    Tracen 1;
    id v.v = 1;
    .sort


    #include mapping2l.inc #d`NUMLOOPS'l`i'
    `MOMREPLACEMENT'
    #include `NUMLOOPS'ltopologies/`INT1'
    .sort
    .store

    save 2lresults/d`NUMLOOPS'l`i'amp.res d`NUMLOOPS'l`i'amp;
    .sort

* Calculate the colour factor here...
#endprocedure




* Calculate each diagram separately using the procedure defined above
#do i=1, `NUMDIAS'
    #call dia2l(`i')
#enddo
.store

* Definitions
* PATHTOCOLORH needs to point to color.h
#define UFOCOLOUR "0"
#define PATHTOCOLORH "color.h"
#define RANK "14"
#define NUMINDEX "40"

#include `PATHTOCOLORH' #Declarations
Dimension NR;
autodeclare Index funi, funj;
Dimension NA;
autodeclare Index adja, adjb;


#include `PATHTOCOLORH' #color
#include `PATHTOCOLORH' #SORT
#include `PATHTOCOLORH' #adjoint

cf a,b,GM,V3g,prop,ufocomplex;

* Load diagrams (QCD fold)
#do i = 1, `NUMDIAS'
    Global fqcd`NUMLOOPS'l`i'amp = 
        #include `DIAFILE' #fqcd`NUMLOOPS'l`i'
#enddo
.sort
* Apply projector
multiply d_(i1,i2)/NR;
.sort

* Rename indices
#if ( `UFOCOLOUR' == 1)
  repeat id ufocomplex(0,1)^2 = -1;
#endif
#do N = 1, `NUMINDEX'
  argument;
    id a(`N') = adja`N';
    id b(`N') = adjb`N';
    id i`N' = funi`N';
    id j`N' = funj`N';
    #if ( `UFOCOLOUR' == 1)
      id aufo(`N') = adja{`N'+`NUMINDEX'+1};
      id bufo(`N') = adjb{`N'+`NUMINDEX'+1};
    #endif
  endargument;
#enddo
	
* Replace functions
id prop(?x) = d_(?x);
id GM(i1?,i2?,i3?) = T(i2,i3,i1);
id V3g(?x) = (-i_)*f(?x);
.sort

* Call color
#call color
#call SORT(tloop-1)

* Re-express color factor
id NA/NR*I2R = cR;
Print +s;
.store


* Load all results and add them up
Global d`NUMLOOPS'all = 
    #do i = 1, `NUMDIAS'
        + fqcd`NUMLOOPS'l`i'amp*d`NUMLOOPS'l`i'amp * diagramlabel(`i')
    #enddo
    ;
.sort

* Some final simplifications
id diagramlabel(x?) = 1;
id vectorpart = 1;
id scalarpart = 1;
id ufoGC10 = -gs;
id ufoGC11 = i_ * gs;
id ufocomplex(0,1) = i_;
.sort

b gs,I2R,cA,cR;
Print +s;
.end
