#-
#include UFOdecl.inc
#define DIAFILE "1lQSE.dia"

Symbol d;
Dimension d;
Off statistics;

CFunctions Dgl, auxSlash, auxGamma, fprop, fpropnum, fvertV, propden, Vec, fermionchain, fermiontrace;
Symbol xiG;
Autodeclare Indices nu, i, j;
Vectors p, p1,...,p20, q1;
Symbol vectorpart;
Symbols n0, n1;
CFunction Gamma, Log, d1l1;
Symbols pi, ep, eulergamma, orderep2, x;
Symbols gs, mutilde;
.global
.sort

* Load diagrams (Lorentz fold)
Global d1l1amp = 
        #include `DIAFILE' #d1l1
.sort

* Project out the q-slash component
multiply, 1/4 * vectorpart * auxGamma(q1,i2,i1) * q1.q1^-1;
.sort

* Rewrite helper functions
** Gluon propagator
id Dgl(i1?,i2?,?x,xiG?) = -i_ * (d_(i1,i2) - (1 - xiG) * Vec(i1,?x) * Vec(i2,?x) * propden(?x) ) * propden(?x);
** Fermion propagator
id auxSlash(?x,j1?,j2?) = i_ * fprop(?x,j1,j2);
** gamma^mu with open spinor indices
id auxGamma(?x,j1?,j2?) = fvertV(?x,j1,j2);

* Spinor indices are contracted opposite to the fermion direction, hence the numerator minus sign
id fprop(p?, j1?, j2?) = propden(p) * fpropnum(-p, j1, j2);
id fprop(-p?, j1?, j2?) = propden(p) * fpropnum(p, j1, j2);
id propden(p?) = p.p^-1;
id Vec(nu?,p?) = p(nu);
.sort

* Take the trace
** First, "chain" all objects affected by taking Dirac traces
id fpropnum(?a,j1?,j2?) = fermionchain(fpropnum(?a), j1, j2);
id fvertV(?a,j1?,j2?) = fermionchain(fvertV(?a), j1, j2);
repeat;
    id fermionchain(?a,j1?,j2?) * fermionchain(?b,j2?,j3?) = fermionchain(?a, ?b, j1, j3);
    id fermionchain(?a,j1?,j1?) = fermiontrace(?a);
endrepeat;

** Secondly, rewrite them using ordinary FORM gamma matrices
repeat;
    id fermiontrace(fpropnum(p?),?a) = g_(1,p) * fermiontrace(?a);
    id fermiontrace(fvertV(nu?),?a) = g_(1,nu) * fermiontrace(?a);
endrepeat;
id fermiontrace = 1;
.sort

Tracen 1;
.sort

#include 1ltopologies/d1l1
.sort


* Integrate the one-loop function
.sort
id d1l1(n0?{<=0}, n1?) = 0;
id d1l1(n0?, n1?{<=0}) = 0;
.sort
id d1l1(n0?, n1?) = 1/((2*pi)^d) * i_ * pi^(d/2) * (-q1.q1)^(d/2-n0-n1) * Gamma(-d/2 + n0 + n1) * Gamma(d/2 - n0) * Gamma(d/2 - n1) / Gamma(n0) / Gamma(n1) / Gamma(d - n0 - n1);
id d = 4 - 2*ep;
argument;
    id d = 4 - 2*ep;
endargument;
id 1/Gamma(-3 + d) = 1/Gamma(1 - 2*ep);
id 1/Gamma(-2 + d) = 1/Gamma(2 - 2*ep);
.sort


* Expand in ep
id Gamma(2-2*ep) = (1-2*ep) * Gamma(1-2*ep);
id Gamma(1-ep) = -ep * Gamma(-ep);
id Gamma(1+ep) = ep * Gamma(ep);
id 1/Gamma(1-2*ep) = 1 - 2*ep*eulergamma + ep^2*orderep2;
id 1/Gamma(2-2*ep) = 1 + ep*(2 - 2*eulergamma) + ep^2*orderep2;
id Gamma(ep) = 1/ep - eulergamma + 1/12 * ep * (pi^2 + 6 * eulergamma) + ep^2*orderep2;
id Gamma(-ep) = -1/ep - eulergamma - 1/12 * ep * (pi^2 + 6 * eulergamma) + ep^2*orderep2;
id 1/Gamma(1) = 1;
id 1/Gamma(2) = 1;
id (-q1.q1)^(-ep) = 1 - ep*Log(-q1.q1);
id (-q1.q1)^(-1 - ep) = -q1.q1^-1 * (1 - ep * Log(-q1.q1) + ep^2*orderep2);
id 1/((2*pi)^d) = 1/16/(pi^4) * (1 + ep*Log(4*pi^2) + ep^2*orderep2);
id pi^(2-ep) = pi^2 * (1 - ep*Log(pi) + ep^2*orderep2);
.sort

* Drop terms of O(ep)
id ep^n0?{>0} = 0;
.sort


* Simplify result a bit
id vectorpart = 1;
id ufoGC11 = i_ * gs;
id Log(4*pi^2) = Log(4*pi) + Log(pi);
id eulergamma = -Log(mutilde^2) + Log(4*pi);
id Log(mutilde^2) = Log(mutilde^2 / -q1.q1) + Log(-q1.q1);
.sort

Print +s;
.store

* Definitions
* PATHTOCOLORH needs to point to color.h
#define UFOCOLOUR "0"
#define PATHTOCOLORH "color.h"
#define RANK "14"
#define NUMINDEX "40"

#include `PATHTOCOLORH' #Declarations
Dimension NR;
autodeclare Index funi, funj;
Dimension NA;
autodeclare Index adja, adjb;


#include `PATHTOCOLORH' #color
#include `PATHTOCOLORH' #SORT
#include `PATHTOCOLORH' #adjoint

cf a,b,GM,V3g,prop,ufocomplex;

* Load diagrams (QCD fold)
Global fqcd1l1amp = 
         #include `DIAFILE' #fqcd1l1
.sort
* Apply projector
multiply d_(i1,i2)/NR;
.sort

* Rename indices
#if ( `UFOCOLOUR' == 1)
  repeat id ufocomplex(0,1)^2 = -1;
#endif
#do N = 1, `NUMINDEX'
  argument;
    id a(`N') = adja`N';
    id b(`N') = adjb`N';
    id i`N' = funi`N';
    id j`N' = funj`N';
    #if ( `UFOCOLOUR' == 1)
      id aufo(`N') = adja{`N'+`NUMINDEX'+1};
      id bufo(`N') = adjb{`N'+`NUMINDEX'+1};
    #endif
  endargument;
#enddo
	
* Replace functions
id prop(?x) = d_(?x);
id GM(i1?,i2?,i3?) = T(i2,i3,i1);
id V3g(?x) = (-i_)*f(?x);
.sort

* Call color
#call color
#call SORT(tloop-1)

* Re-express color factor
id NA/NR*I2R = cR;
Print +s;
.store

* Final result
g res1l = fqcd1l1amp*d1l1amp;
b cR,pi,xiG,gs,i_;
print +s;
.end
