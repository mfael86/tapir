# Example: QCD self-energy at one- and two-loop order

This example serves three purposes: first, the one-loop calculation will actually be carried out until the very end, including all the steps needed to obtain the final result. Secondly, it will be a preparation for the example about heavy quark effective theory (HQET). Finally, it will also serve as a demonstration of the use of YAML files for the configuration and diagram input.


## 1-loop
### Diagrams
First, we need all diagrams. In the one-loop case, there is only one diagram, namely the gluonic correction to the propagator of a massless quark (in our case the up quark `fuq`). Using the `qgraf-yaml.sty` file (and the Lagrangian file `qcd_ufo.lag`), the `qgraf` output looks like in `qlistyaml_1lQSE.`:
```yaml
diagrams:
  - diagramNumber: 1
    loops: 1
    preFactor: (+1)*1
    externalLines:
      - {incomingMomentum: q1, vertex: 1, incomingParticle: fuq}
      - {incomingMomentum: q2, vertex: 2, incomingParticle: fUq}
    internalLines:
      - {momentum: p1, vertices: [2, 1], incomingParticle: g, outgoingParticle: g}
      - {momentum: p2, vertices: [1, 2], incomingParticle: fUq, outgoingParticle: fuq}
```

### tapir
Next comes `tapir`. In order to read the `qgraf` `YAML` output rather than the usual format produced by `qgraf-tapir.sty`, we have to specify the option `diagram_yaml_in` rather than `qlist`. We need to provide the definitions of propagators and vertices in the `qcd_ufo.prop` and `qcd_ufo.vrtx` files. These contain the QCD subset of the interactions contained in the full [UFO/SM](../../UFO/SM) example.

We further specify the usual `.dia` and `.edia` output files, although we won't need the `.edia` file here. Much more relevant for us is here the directory `1ltopologies` that will be used to generate the topology output. Finally, we specify that the gluon gauge parameter should be called `xiG`, and a couple of drawing options.

The full `YAML`-style configuration file `masslessQCD_SE1l.yaml` then takes the following form:
```yaml
---
config:
  propagator_file: "qcd_ufo.prop"
  vertex_file: "qcd_ufo.vrtx"
  diagram_yaml_in: example/selfenergies/masslessQCD/qlistyaml_1lQSE.
  diaout: example/selfenergies/masslessQCD/1lQSE.dia
  ediaout: example/selfenergies/masslessQCD/1lQSE.edia
  diagramart: example/selfenergies/masslessQCD/1lQSE.tex
  topology_yaml_out: example/selfenergies/masslessQCD/1ltopos.yaml
  topologyfolder: example/selfenergies/masslessQCD/1ltopologies
  gaugeparam:
    - g: xiG
  draw_particle:
    - {fuq: [fermion, '$u$'], fdq: [fermion, '$d$'], fsq: [fermion, '$s$'], fcq: [fermion, '$c$'], fbq: [fermion, '$b$'], ftq: [fermion, '$t$']}
    - {g: [gluon], cghG: [ghost]}
```

Now, we are ready to call `tapir` (from its main directory):
```
./tapir -c example/selfenergies/masslessQCD/masslessQCD_SE1l.yaml
```

### Inspecting with the output
Let us first take a look at the output `1lQSE.dia`:
```
*--#[ d1l1 :

	(+1)*1
	*Dgl(nu6,nu5,p1,xiG)
	*auxSlash(-p2,j7,j8)
	*(ufoGC11*(auxGamma(nu6,i1,j7)))
	*(ufoGC11*(auxGamma(nu5,j8,i2)))
	;

	#define TOPOLOGY "arb"
	#define INT1 "arb"

*--#] d1l1 :

*--#[ fqcd1l1 :

	1
	*prop(b(6),b(5))
	*d_(j7,j8)
	*(GM(b(6),j7,i1))
	*(GM(b(5),i2,j8))
	;

*--#] fqcd1l1 :

*--#[ fqed1l1 :
	1
*--#] fqed1l1 :

*--#[ few1l1 :
	1
*--#] few1l1 :
```

As always, the Lorentz part and colour part are separated and can be dealt with at different stages of the calculation.
The `fqed` and `few` folds are not needed and are thus empty. Another file, `1ltopologies/d1l1` contains information concerning the momenta present in the diagram `d1l1` (the only one we have at one loop), and can be used to rewrite a scalar integral into the function `d1l1(n0,n1)` with two propagator indices.


### Proceeding with the output (Lorentz fold)
Now that we have the symbolic representation of the diagram, we can use `FORM` to continue. **NOTE: You will need FORM installed to continue and the file color.h from Jos Vermaserens Color package in this folder.** (https://www.nikhef.nl/~form/maindir/packages/color/color.html)
In this `README.md` document, we will not list all the declarations of symbols and functions that are needed to make the code run. They are all listed in the file `calculate_1loop.frm` whose contents we list here in abbreviated form. The full `FORM` script can be run using 
```
form calculate_1loop.frm
```


First, we should load the diagram
```
* The UFOdecl.inc file contains declarations of all custom parameters 
* produced during the conversion of the UFO model, e.g. it declares 
* the symbol `ufoGC11`, which will eventually correspond to `i_ * gs` 
* (the strong coupling constant)
#include UFOdecl.inc
#define DIAFILE "1lQSE.dia"

Global d1l1amp = 
        #include `DIAFILE` #d1l1
.sort
```

We know that the self-energy diagram is proportional to $`q_{\mu} \gamma^{\mu}`$ (the momentum of the quark), so let us project onto this component right away (rather than performing a tensor reduction):
```
* Project out the q-slash component
multiply, 1/4 * vectorpart * auxGamma(q1,i2,i1) * q1.q1^-1;
```
The projection will only be completed once we take the fermion trace, but this will happen a few steps later.

Next, we have to rewrite the a priori arbitrary helper functions and relate them to the actual objects
```
* Rewrite helper functions
** Gluon propagator
id Dgl(i1?,i2?,?x,xiG?) = -i_ * (d_(i1,i2) - (1 - xiG) * Vec(i1,?x) * Vec(i2,?x) * propden(?x) ) * propden(?x);
** Fermion propagator
id auxSlash(?x,j1?,j2?) = i_ * fprop(?x,j1,j2);
** gamma^mu with open spinor indices
id auxGamma(?x,j1?,j2?) = fvertV(?x,j1,j2);

* Spinor indices are contracted opposite to the fermion direction, hence the numerator minus sign
id fprop(p?, j1?, j2?) = propden(p) * fpropnum(-p, j1, j2);
id fprop(-p?, j1?, j2?) = propden(p) * fpropnum(p, j1, j2);
id propden(p?) = p.p^-1;
id Vec(nu?,p?) = p(nu);
.sort
```

Now we are ready to take the fermion trace. We only need to do a bit of implementation to correctly concatenate the commuting obects `fpropnum` and `fvert` into strings of gamma matrices in the correct order. This works by concatenating the spinor indices and absorbing everything into a helper function `fermionchain` or `fermiontrace`:
```
* Take the trace
** First, "chain" all objects affected by taking Dirac traces
id fpropnum(?a,j1?,j2?) = fermionchain(fpropnum(?a), j1, j2);
id fvertV(?a,j1?,j2?) = fermionchain(fvertV(?a), j1, j2);
repeat;
    id fermionchain(?a,j1?,j2?) * fermionchain(?b,j2?,j3?) = fermionchain(?a, ?b, j1, j3);
    id fermionchain(?a,j1?,j1?) = fermiontrace(?a);
endrepeat;
```

Now that everything is in the correct order inside the argument of these two helper functions, we can pull the objects out and rewrite them as ordinary gamma matrices:
```
** Secondly, rewrite them using ordinary FORM gamma matrices
repeat;
    id fermiontrace(fpropnum(p?),?a) = g_(1,p) * fermiontrace(?a);
    id fermiontrace(fvertV(nu?),?a) = g_(1,nu) * fermiontrace(?a);
endrepeat;
id fermiontrace = 1;
.sort

Tracen 1;
.sort
```

Now that we have properly projected onto the $`q_{\mu} \gamma^{\mu}`$ component, all that remains to do is the one-loop integral. For this, we can conveniently use the file `1ltopologies/d1l1`:
```
#include 1ltopologies/d1l1
```
This file takes care of rewriting momenta, and combines the two independent propagators into a scalar function
```math
d1l1(n_0, n_1) = \left(-1\right)^{n_0 + n_1} \int \frac{\mathrm{d}^d k}{\left(2 \pi \right)^d} \frac{1}{\left(p_1 \cdot p_1 \right)^{n_0} \left(p_2 \cdot p_2 \right)^{n_1} }
```
which we can immediately integrate:
```
* Integrate the one-loop function
id d1l1(n0?{<=0}, n1?) = 0;
id d1l1(n0?, n1?{<=0}) = 0;
id d1l1(n0?, n1?) = 1/((2*pi)^d) * i_ * pi^(d/2) * (-q1.q1)^(d/2-n0-n1) * Gamma(-d/2 + n0 + n1) * Gamma(d/2 - n0) * Gamma(d/2 - n1) / Gamma(n0) / Gamma(n1) / Gamma(d - n0 - n1);
.sort
```
This result can then be expanded in `ep = (4-d)/2` in order to obtain the final result for the Lorentz part of the diagram.


### Proceeding with the output (colour part)
First we need to set up all variables for Color to work properly. To this end, we set the path to `color.h` (change accordingly if not in this folder) and load all necessary procedures.
Furthermore, we define indices in the fundamental and adjoint representations.
```
#define UFOCOLOUR "0"
#define PATHTOCOLORH "color.h"
#define RANK "14"
#define NUMINDEX "40"

#include `PATHTOCOLORH' #Declarations
Dimension NR;
autodeclare Index funi, funj;
Dimension NA;
autodeclare Index adja, adjb;

#include `PATHTOCOLORH' #color
#include `PATHTOCOLORH' #SORT
#include `PATHTOCOLORH' #adjoint
```

In the next step, we load the QCD fold of the diagram and apply the projector in color space:
```
Global fqcd1l1amp = 
         #include `DIAFILE' #fqcd1l1
.sort
* Apply projector
multiply d_(i1,i2)/NR;
.sort
```

We then replace the indices and functions used by Tapir by indices compatible with Color:
```
* Rename indices
#if ( `UFOCOLOUR' == 1)
  repeat id ufocomplex(0,1)^2 = -1;
#endif
#do N = 1, `NUMINDEX'
  argument;
    id a(`N') = adja`N';
    id b(`N') = adjb`N';
    id i`N' = funi`N';
    id j`N' = funj`N';
    #if ( `UFOCOLOUR' == 1)
      id aufo(`N') = adja{`N'+`NUMINDEX'+1};
      id bufo(`N') = adjb{`N'+`NUMINDEX'+1};
    #endif
  endargument;
#enddo
	
* Replace functions
id prop(?x) = d_(?x);
id GM(i1?,i2?,i3?) = T(i2,i3,i1);
id V3g(?x) = (-i_)*f(?x);
.sort
```

Invoking Color in this one-loop example is rather simple:
```
* Call color
#call color
#call SORT(tloop-1)
```

We obtain `NA*I2R/NR` which can be reexpressed:
```
id NA/NR*I2R = cR;
```


## 2-loop
Now let us turn to second order in the strong coupling. At this order there are closed quark loops. We will consider all quark flavours but the top quark, and assume them to be massless (see the `qgraf` output files `qlist_2lQSE.` or the equivalent `qlistyaml_2lQSE.`). In total, there are 10 diagrams.

### tapir
The configuration file for `tapir` (`masslessQCD_SE2l.yaml`) looks very similar to the one-loop case, with one exception: we use the options `partfrac` and `minimize` to apply partial fraction identities where possible, and minimize the number of topologies. If we do not use it, we have one topology for each diagram, but actually many diagrams have the same topology. Indeed, using the `minimize` step, we are left with only two two-loop topologies. We also specify the name of the mapping file that contains the topology-mapping information using `mapping_file: FILENAME`, otherwise it would simply be called `mapping.inc`.

```
./tapir -c example/selfenergies/masslessQCD/masslessQCD_SE2l.yaml
```
takes care of the rest.

### Proceeding with the output
The next steps take again place in `calculate_2loop.frm`. Since we are dealing with multiple diagrams this time, it is helpful to define a `FORM procedure dia2l(i)` that takes care of loading and processing the `i`-th diagram.
As usual, we apply a projector to obtain the $`q_{\mu} \gamma^{\mu}`$ part, and rewrite the helper functions. Next, we take the trace to complete the projecting out of the relevant component.

Before we proceed, let us have a look into the file `mapping2l.inc`.
It contains several "folds", one for each diagram:
```
* This file contains the mapping of input diagrams on other diagram topologies


*--#[ d2l1 :

   #define INT1 "d2l1"
   #define MOMREPLACEMENT "multiply replace_(p1,p1, p2,p2, p3,p3, p4,p4, p5,p5);"

*--#] d2l1 :


*--#[ d2l2 :

   #define INT1 "d2l1"
   #define MOMREPLACEMENT "multiply replace_(p1,p1, p2,p2, p3,p3, p4,p4, p5,p5);"

*--#] d2l2 :


*--#[ d2l3 :

   #define INT1 "d2l3"
   #define MOMREPLACEMENT "multiply replace_(p1,p1, p2,p2, p3,p3, p4,p4, p5,p5);"

*--#] d2l3 :
...
```
We see that the first diagram is mapped onto the topology with the name `d2l1`; the first diagram obviously is mapped onto a new topology. The second diagram, however, has the same topology structure, so it is mapped in the first one. The third diagram has a different topology, which is then called `d2l3`. All the diagrams 4-10 are actually of the same structure again, and are mapped on `d2l3`.

For each distinct topology found listed in this mapping file, there will be a file in the directory `2ltopologies` (or whatever you choose as `topologyfolder`). These files then take care of rewriting momenta, applying the partial fraction identities, and rewriting the final momentum products in terms of scalar functions again.

Now, let us turn back to the actual `FORM` calculation. We can include only the "fold" that applies to each diagram separately, using
```
#include mapping2l.inc #d`NUMLOOPS'l`i'
```
This will define the variable `INT1` (and some momentum replacements `MOMREPLACEMENT`) for each diagram, according to the content of the mapping file.
We can then perform the momentum replacements and include the appropriate topology file (for each diagram separately) and save the result.
```

#define NUMDIAS "10"
#define NUMLOOPS "2"
#define DIAFILE "2lQSE.dia"

#include mapping2l.inc #d`NUMLOOPS'l`i'
`MOMREPLACEMENT'
#include `NUMLOOPS'ltopologies/`INT1'
.sort
.store

save 2lresults/d`NUMLOOPS'l`i'amp.res d`NUMLOOPS'l`i'amp;
.sort
```

Now we simply have to call this procedure for each diagram separately:
```
* Calculate each diagram separately using the procedure defined above
#do i=1, `NUMDIAS'
    #call dia2l(`i')
#enddo
```
Note that at two loops we did not integrate the scalar functions that result from the use of the topology files anymore. These functions should typically be reduced using integration-by-parts techniques or the like in order to obtain the full result. Instead, we stop the calculation of the non-colour part at this step.

The colour part of this calculation proceeds in the same way as in the one-loop case. We load all routines related to COLOR, load all diagrams and then invoke COLOR.

You can run the script using (you may have to `mkdir -p 2lresults` first)
```
form calculate_2loop.frm
```
